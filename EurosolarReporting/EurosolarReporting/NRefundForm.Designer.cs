using System;
namespace EurosolarReporting
{
    partial class NRefundForm
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NRefundForm));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.table1 = new Telerik.Reporting.Table();
            this.txtProjectNumber = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.txtSTATUS = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.txtDEPOSITDATE = new Telerik.Reporting.TextBox();
            this.txtENTRYDATE = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.txtNOOFPANELS = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.txtProjectNumber1 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.txtBANKNAME = new Telerik.Reporting.TextBox();
            this.txtACCOUNTNAME = new Telerik.Reporting.TextBox();
            this.txtBSBNO = new Telerik.Reporting.TextBox();
            this.txtACCNO = new Telerik.Reporting.TextBox();
            this.txtAMOUNT = new Telerik.Reporting.TextBox();
            this.txtSIGNATURE = new Telerik.Reporting.TextBox();
            this.txtREFUNDDATE = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.table1,
            this.table2,
            this.table4});
            this.detail.Name = "detail";
            this.detail.Style.Font.Name = "Calibri";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(16.305D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.788D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.841D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.868D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.735D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.894D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.788D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.868D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.788D)));
            this.table1.Body.SetCellContent(0, 0, this.txtProjectNumber);
            this.table1.Body.SetCellContent(1, 0, this.textBox5);
            this.table1.Body.SetCellContent(2, 0, this.txtSTATUS);
            this.table1.Body.SetCellContent(3, 0, this.textBox12);
            this.table1.Body.SetCellContent(4, 0, this.txtDEPOSITDATE);
            this.table1.Body.SetCellContent(5, 0, this.txtENTRYDATE);
            this.table1.Body.SetCellContent(6, 0, this.textBox21);
            this.table1.Body.SetCellContent(7, 0, this.txtNOOFPANELS);
            tableGroup1.Name = "tableGroup2";
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtProjectNumber,
            this.textBox5,
            this.txtSTATUS,
            this.textBox12,
            this.txtDEPOSITDATE,
            this.txtENTRYDATE,
            this.textBox21,
            this.txtNOOFPANELS});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.572D), Telerik.Reporting.Drawing.Unit.Cm(3.048D));
            this.table1.Name = "table1";
            tableGroup3.Name = "group";
            tableGroup4.Name = "group1";
            tableGroup5.Name = "group2";
            tableGroup6.Name = "group3";
            tableGroup7.Name = "group4";
            tableGroup8.Name = "group5";
            tableGroup9.Name = "group6";
            tableGroup10.Name = "group7";
            tableGroup2.ChildGroups.Add(tableGroup3);
            tableGroup2.ChildGroups.Add(tableGroup4);
            tableGroup2.ChildGroups.Add(tableGroup5);
            tableGroup2.ChildGroups.Add(tableGroup6);
            tableGroup2.ChildGroups.Add(tableGroup7);
            tableGroup2.ChildGroups.Add(tableGroup8);
            tableGroup2.ChildGroups.Add(tableGroup9);
            tableGroup2.ChildGroups.Add(tableGroup10);
            tableGroup2.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup2.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup2);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.305D), Telerik.Reporting.Drawing.Unit.Cm(6.607D));
            this.table1.Style.Color = System.Drawing.Color.Black;
            this.table1.Style.Font.Name = "Arial";
            this.table1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.table1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.table1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            // 
            // txtProjectNumber
            // 
            this.txtProjectNumber.Name = "txtProjectNumber";
            this.txtProjectNumber.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.305D), Telerik.Reporting.Drawing.Unit.Cm(0.788D));
            this.txtProjectNumber.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtProjectNumber.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtProjectNumber.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtProjectNumber.Value = "";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.305D), Telerik.Reporting.Drawing.Unit.Cm(0.841D));
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox5.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.StyleName = "";
            this.textBox5.TextWrap = false;
            this.textBox5.Value = "=Fields.ContFirst +\" \" + Fields.ContLast";
            // 
            // txtSTATUS
            // 
            this.txtSTATUS.Name = "txtSTATUS";
            this.txtSTATUS.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.305D), Telerik.Reporting.Drawing.Unit.Cm(0.868D));
            this.txtSTATUS.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtSTATUS.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtSTATUS.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSTATUS.StyleName = "";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.305D), Telerik.Reporting.Drawing.Unit.Cm(0.735D));
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox12.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.StyleName = "";
            this.textBox12.Value = "=Fields.SalesRepName";
            // 
            // txtDEPOSITDATE
            // 
            this.txtDEPOSITDATE.Name = "txtDEPOSITDATE";
            this.txtDEPOSITDATE.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.305D), Telerik.Reporting.Drawing.Unit.Cm(0.894D));
            this.txtDEPOSITDATE.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtDEPOSITDATE.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtDEPOSITDATE.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtDEPOSITDATE.StyleName = "";
            // 
            // txtENTRYDATE
            // 
            this.txtENTRYDATE.Name = "txtENTRYDATE";
            this.txtENTRYDATE.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.305D), Telerik.Reporting.Drawing.Unit.Cm(0.788D));
            this.txtENTRYDATE.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtENTRYDATE.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtENTRYDATE.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtENTRYDATE.StyleName = "";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.305D), Telerik.Reporting.Drawing.Unit.Cm(0.868D));
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox21.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.StyleName = "";
            // 
            // txtNOOFPANELS
            // 
            this.txtNOOFPANELS.Name = "txtNOOFPANELS";
            this.txtNOOFPANELS.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.305D), Telerik.Reporting.Drawing.Unit.Cm(0.788D));
            this.txtNOOFPANELS.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtNOOFPANELS.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtNOOFPANELS.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtNOOFPANELS.StyleName = "";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(20.96D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(3.397D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox25);
            tableGroup11.Name = "tableGroup3";
            this.table2.ColumnGroups.Add(tableGroup11);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox25});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(12.192D));
            this.table2.Name = "table2";
            tableGroup13.Name = "group9";
            tableGroup12.ChildGroups.Add(tableGroup13);
            tableGroup12.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup12.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup12);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.96D), Telerik.Reporting.Drawing.Unit.Cm(3.397D));
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.96D), Telerik.Reporting.Drawing.Unit.Cm(3.397D));
            this.textBox25.Style.Color = System.Drawing.Color.White;
            this.textBox25.Style.Font.Name = "Arial";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox25.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox25.StyleName = "";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(15.692D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.914D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.729D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.861D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.809D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.967D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.729D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.887D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.783D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.862D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.807D)));
            this.table4.Body.SetCellContent(0, 0, this.txtProjectNumber1);
            this.table4.Body.SetCellContent(1, 0, this.textBox37);
            this.table4.Body.SetCellContent(2, 0, this.txtBANKNAME);
            this.table4.Body.SetCellContent(3, 0, this.txtACCOUNTNAME);
            this.table4.Body.SetCellContent(4, 0, this.txtBSBNO);
            this.table4.Body.SetCellContent(5, 0, this.txtACCNO);
            this.table4.Body.SetCellContent(6, 0, this.txtAMOUNT);
            this.table4.Body.SetCellContent(7, 0, this.txtSIGNATURE);
            this.table4.Body.SetCellContent(8, 0, this.txtREFUNDDATE);
            this.table4.Body.SetCellContent(9, 0, this.textBox38);
            tableGroup14.Name = "tableGroup8";
            this.table4.ColumnGroups.Add(tableGroup14);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtProjectNumber1,
            this.textBox37,
            this.txtBANKNAME,
            this.txtACCOUNTNAME,
            this.txtBSBNO,
            this.txtACCNO,
            this.txtAMOUNT,
            this.txtSIGNATURE,
            this.txtREFUNDDATE,
            this.textBox38});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.334D), Telerik.Reporting.Drawing.Unit.Cm(18.034D));
            this.table4.Name = "table4";
            tableGroup16.Name = "group13";
            tableGroup17.Name = "group14";
            tableGroup18.Name = "group15";
            tableGroup19.Name = "group16";
            tableGroup20.Name = "group17";
            tableGroup21.Name = "group18";
            tableGroup22.Name = "group19";
            tableGroup23.Name = "group20";
            tableGroup24.Name = "group21";
            tableGroup25.Name = "group12";
            tableGroup15.ChildGroups.Add(tableGroup16);
            tableGroup15.ChildGroups.Add(tableGroup17);
            tableGroup15.ChildGroups.Add(tableGroup18);
            tableGroup15.ChildGroups.Add(tableGroup19);
            tableGroup15.ChildGroups.Add(tableGroup20);
            tableGroup15.ChildGroups.Add(tableGroup21);
            tableGroup15.ChildGroups.Add(tableGroup22);
            tableGroup15.ChildGroups.Add(tableGroup23);
            tableGroup15.ChildGroups.Add(tableGroup24);
            tableGroup15.ChildGroups.Add(tableGroup25);
            tableGroup15.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup15.Name = "detailTableGroup3";
            this.table4.RowGroups.Add(tableGroup15);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.692D), Telerik.Reporting.Drawing.Unit.Cm(8.383D));
            this.table4.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.table4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // txtProjectNumber1
            // 
            this.txtProjectNumber1.Name = "txtProjectNumber1";
            this.txtProjectNumber1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.692D), Telerik.Reporting.Drawing.Unit.Cm(0.914D));
            this.txtProjectNumber1.Style.Color = System.Drawing.Color.Black;
            this.txtProjectNumber1.Style.Font.Name = "Arial";
            this.txtProjectNumber1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtProjectNumber1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtProjectNumber1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtProjectNumber1.Value = "";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.692D), Telerik.Reporting.Drawing.Unit.Cm(0.729D));
            this.textBox37.Style.Color = System.Drawing.Color.Black;
            this.textBox37.Style.Font.Name = "Arial";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox37.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.StyleName = "";
            this.textBox37.Value = "=Fields.ContFirst +\" \" + Fields.ContLast";
            // 
            // txtBANKNAME
            // 
            this.txtBANKNAME.Name = "txtBANKNAME";
            this.txtBANKNAME.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.692D), Telerik.Reporting.Drawing.Unit.Cm(0.861D));
            this.txtBANKNAME.Style.Color = System.Drawing.Color.Black;
            this.txtBANKNAME.Style.Font.Name = "Arial";
            this.txtBANKNAME.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtBANKNAME.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtBANKNAME.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtBANKNAME.StyleName = "";
            this.txtBANKNAME.Value = "";
            // 
            // txtACCOUNTNAME
            // 
            this.txtACCOUNTNAME.Name = "txtACCOUNTNAME";
            this.txtACCOUNTNAME.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.692D), Telerik.Reporting.Drawing.Unit.Cm(0.809D));
            this.txtACCOUNTNAME.Style.Color = System.Drawing.Color.Black;
            this.txtACCOUNTNAME.Style.Font.Name = "Arial";
            this.txtACCOUNTNAME.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtACCOUNTNAME.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtACCOUNTNAME.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtACCOUNTNAME.StyleName = "";
            // 
            // txtBSBNO
            // 
            this.txtBSBNO.Name = "txtBSBNO";
            this.txtBSBNO.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.692D), Telerik.Reporting.Drawing.Unit.Cm(0.967D));
            this.txtBSBNO.Style.Color = System.Drawing.Color.Black;
            this.txtBSBNO.Style.Font.Name = "Arial";
            this.txtBSBNO.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtBSBNO.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtBSBNO.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtBSBNO.StyleName = "";
            // 
            // txtACCNO
            // 
            this.txtACCNO.Name = "txtACCNO";
            this.txtACCNO.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.692D), Telerik.Reporting.Drawing.Unit.Cm(0.729D));
            this.txtACCNO.Style.Color = System.Drawing.Color.Black;
            this.txtACCNO.Style.Font.Name = "Arial";
            this.txtACCNO.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtACCNO.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtACCNO.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtACCNO.StyleName = "";
            // 
            // txtAMOUNT
            // 
            this.txtAMOUNT.Name = "txtAMOUNT";
            this.txtAMOUNT.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.692D), Telerik.Reporting.Drawing.Unit.Cm(0.887D));
            this.txtAMOUNT.Style.Color = System.Drawing.Color.Black;
            this.txtAMOUNT.Style.Font.Name = "Arial";
            this.txtAMOUNT.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.txtAMOUNT.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtAMOUNT.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAMOUNT.StyleName = "";
            // 
            // txtSIGNATURE
            // 
            this.txtSIGNATURE.Name = "txtSIGNATURE";
            this.txtSIGNATURE.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.692D), Telerik.Reporting.Drawing.Unit.Cm(0.783D));
            this.txtSIGNATURE.Style.Color = System.Drawing.Color.Black;
            this.txtSIGNATURE.Style.Font.Name = "Arial";
            this.txtSIGNATURE.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtSIGNATURE.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtSIGNATURE.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtSIGNATURE.StyleName = "";
            // 
            // txtREFUNDDATE
            // 
            this.txtREFUNDDATE.Name = "txtREFUNDDATE";
            this.txtREFUNDDATE.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.692D), Telerik.Reporting.Drawing.Unit.Cm(0.862D));
            this.txtREFUNDDATE.Style.Color = System.Drawing.Color.Black;
            this.txtREFUNDDATE.Style.Font.Name = "Arial";
            this.txtREFUNDDATE.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.txtREFUNDDATE.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txtREFUNDDATE.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtREFUNDDATE.StyleName = "";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.692D), Telerik.Reporting.Drawing.Unit.Cm(0.807D));
            this.textBox38.Style.Font.Name = "Arial";
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox38.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            // 
            // NRefundForm
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "NRefundForm";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox txtProjectNumber;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox txtSTATUS;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox txtDEPOSITDATE;
        private Telerik.Reporting.TextBox txtENTRYDATE;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox txtNOOFPANELS;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox txtProjectNumber1;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox txtBANKNAME;
        private Telerik.Reporting.TextBox txtACCOUNTNAME;
        private Telerik.Reporting.TextBox txtBSBNO;
        private Telerik.Reporting.TextBox txtACCNO;
        private Telerik.Reporting.TextBox txtAMOUNT;
        private Telerik.Reporting.TextBox txtSIGNATURE;
        private Telerik.Reporting.TextBox txtREFUNDDATE;
        private Telerik.Reporting.TextBox textBox38;
    }
}