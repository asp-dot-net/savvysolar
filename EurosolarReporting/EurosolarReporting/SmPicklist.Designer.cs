namespace EurosolarReporting
{
    partial class SmPicklist
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SmPicklist));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.txtinstdate = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.txtaddress = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4677166938781738D), Telerik.Reporting.Drawing.Unit.Cm(0.63500070571899414D));
            this.textBox2.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox2.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "Customer Name";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5033221244812012D), Telerik.Reporting.Drawing.Unit.Cm(0.82640373706817627D));
            this.textBox9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox9.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "=Fields.Customer";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9327592849731445D), Telerik.Reporting.Drawing.Unit.Cm(0.827579915523529D));
            this.textBox25.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox25.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox25.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox25.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.Value = "Phone";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9672415256500244D), Telerik.Reporting.Drawing.Unit.Cm(0.827579915523529D));
            this.textBox26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox26.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.Value = "=Fields.CustPhone";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3167574405670166D), Telerik.Reporting.Drawing.Unit.Cm(0.78358936309814453D));
            this.textBox35.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox35.Style.Font.Bold = true;
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox35.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox35.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox35.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.Value = "Mobile";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7744934558868408D), Telerik.Reporting.Drawing.Unit.Cm(0.78358936309814453D));
            this.textBox36.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox36.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.Value = "=Fields.ContMobile";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7312526702880859D), Telerik.Reporting.Drawing.Unit.Cm(0.92613649368286133D));
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox50.Value = "INSTALLER NOTE  :";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.textBox64,
            this.textBox1,
            this.table1,
            this.table2,
            this.table3,
            this.table4,
            this.textBox14,
            this.txtaddress,
            this.textBox38,
            this.textBox40,
            this.textBox41,
            this.textBox42,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.table5,
            this.table6,
            this.textBox63,
            this.textBox39,
            this.table8,
            this.table7});
            this.detail.Name = "detail";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.15874999761581421D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(296D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // textBox64
            // 
            this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(5.0465263967680585E-08D));
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.299999237060547D), Telerik.Reporting.Drawing.Unit.Cm(1.1999000310897827D));
            this.textBox64.Style.Color = System.Drawing.Color.White;
            this.textBox64.Style.Font.Bold = true;
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(28D);
            this.textBox64.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.Value = "STOCK ALLOCATION";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.9000002145767212D), Telerik.Reporting.Drawing.Unit.Cm(1.2000999450683594D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1000008583068848D), Telerik.Reporting.Drawing.Unit.Cm(1.0999001264572144D));
            this.textBox1.Style.Color = System.Drawing.Color.White;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(38D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "PICK-LIST";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.8731222152709961D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.8466634750366211D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.1220839023590088D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.3656229972839355D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.2435414791107178D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9104137420654297D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D)));
            this.table1.Body.SetCellContent(0, 2, this.textBox3);
            this.table1.Body.SetCellContent(0, 3, this.textBox5);
            this.table1.Body.SetCellContent(0, 5, this.textBox7);
            this.table1.Body.SetCellContent(0, 1, this.txtinstdate);
            this.table1.Body.SetCellContent(0, 0, this.textBox4);
            this.table1.Body.SetCellContent(0, 4, this.textBox6);
            tableGroup1.Name = "group1";
            tableGroup2.Name = "group";
            tableGroup3.Name = "tableGroup";
            tableGroup4.Name = "tableGroup1";
            tableGroup5.Name = "group2";
            tableGroup6.Name = "tableGroup2";
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup5);
            this.table1.ColumnGroups.Add(tableGroup6);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.txtinstdate,
            this.textBox3,
            this.textBox5,
            this.textBox6,
            this.textBox7});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D), Telerik.Reporting.Drawing.Unit.Cm(3.3999998569488525D));
            this.table1.Name = "table1";
            tableGroup7.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup7.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup7);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.361448287963867D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1220831871032715D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.textBox3.Style.Color = System.Drawing.Color.White;
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "Installer Name :";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3656234741210938D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.textBox5.Style.Color = System.Drawing.Color.White;
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "=Fields.InstallerName";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9104137420654297D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.textBox7.Style.Color = System.Drawing.Color.White;
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "=Fields.InstallerMobile";
            // 
            // txtinstdate
            // 
            this.txtinstdate.Name = "txtinstdate";
            this.txtinstdate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8466637134552D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.txtinstdate.Style.Color = System.Drawing.Color.White;
            this.txtinstdate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtinstdate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txtinstdate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtinstdate.StyleName = "";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8731222152709961D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.textBox4.Style.Color = System.Drawing.Color.White;
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox4.Style.Font.Strikeout = false;
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.StyleName = "";
            this.textBox4.Value = "Installer Date :";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2435412406921387D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.textBox6.Style.Color = System.Drawing.Color.White;
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.StyleName = "";
            this.textBox6.Value = "Installer Mobile :";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.4677166938781738D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.5033221244812012D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.40569442510604858D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.92333382368087769D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.27340283989906311D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.870415985584259D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox8);
            this.table2.Body.SetCellContent(0, 1, this.textBox10);
            this.table2.Body.SetCellContent(3, 0, this.textBox13);
            this.table2.Body.SetCellContent(2, 0, this.textBox16);
            this.table2.Body.SetCellContent(2, 1, this.textBox17);
            this.table2.Body.SetCellContent(1, 0, this.textBox19);
            this.table2.Body.SetCellContent(1, 1, this.textBox20);
            this.table2.Body.SetCellContent(3, 1, this.textBox37);
            tableGroup8.Name = "tableGroup3";
            tableGroup8.ReportItem = this.textBox2;
            tableGroup9.Name = "tableGroup4";
            tableGroup9.ReportItem = this.textBox9;
            this.table2.ColumnGroups.Add(tableGroup8);
            this.table2.ColumnGroups.Add(tableGroup9);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox8,
            this.textBox10,
            this.textBox19,
            this.textBox20,
            this.textBox16,
            this.textBox17,
            this.textBox13,
            this.textBox37,
            this.textBox2,
            this.textBox9});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045D), Telerik.Reporting.Drawing.Unit.Cm(4.8000001907348633D));
            this.table2.Name = "table2";
            tableGroup10.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup10.Name = "detailTableGroup1";
            tableGroup11.Name = "group5";
            tableGroup12.Name = "group4";
            tableGroup13.Name = "group3";
            this.table2.RowGroups.Add(tableGroup10);
            this.table2.RowGroups.Add(tableGroup11);
            this.table2.RowGroups.Add(tableGroup12);
            this.table2.RowGroups.Add(tableGroup13);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.971038818359375D), Telerik.Reporting.Drawing.Unit.Cm(3.2992508411407471D));
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4677166938781738D), Telerik.Reporting.Drawing.Unit.Cm(0.40569442510604858D));
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5033221244812012D), Telerik.Reporting.Drawing.Unit.Cm(0.40569442510604858D));
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4677166938781738D), Telerik.Reporting.Drawing.Unit.Cm(0.87041622400283813D));
            this.textBox13.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox13.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox13.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.StyleName = "";
            this.textBox13.Value = "Project No.";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4677166938781738D), Telerik.Reporting.Drawing.Unit.Cm(0.27340283989906311D));
            this.textBox16.StyleName = "";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5033221244812012D), Telerik.Reporting.Drawing.Unit.Cm(0.27340283989906311D));
            this.textBox17.StyleName = "";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4677166938781738D), Telerik.Reporting.Drawing.Unit.Cm(0.92333376407623291D));
            this.textBox19.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox19.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox19.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.StyleName = "";
            this.textBox19.Value = "House Type";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5033221244812012D), Telerik.Reporting.Drawing.Unit.Cm(0.92333376407623291D));
            this.textBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox20.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "=Fields.HouseType";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5033221244812012D), Telerik.Reporting.Drawing.Unit.Cm(0.87041622400283813D));
            this.textBox37.Style.Font.Name = "Calibri";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox37.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox37.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.StyleName = "";
            this.textBox37.Value = "=Fields.ProjectNumber";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9327595233917236D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.9672415256500244D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.40569433569908142D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.92333412170410156D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.85277765989303589D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox11);
            this.table3.Body.SetCellContent(0, 1, this.textBox12);
            this.table3.Body.SetCellContent(3, 0, this.textBox15);
            this.table3.Body.SetCellContent(3, 1, this.textBox18);
            this.table3.Body.SetCellContent(2, 0, this.textBox21);
            this.table3.Body.SetCellContent(2, 1, this.textBox22);
            this.table3.Body.SetCellContent(1, 0, this.textBox23);
            this.table3.Body.SetCellContent(1, 1, this.textBox24);
            tableGroup14.Name = "tableGroup3";
            tableGroup14.ReportItem = this.textBox25;
            tableGroup15.Name = "tableGroup4";
            tableGroup15.ReportItem = this.textBox26;
            this.table3.ColumnGroups.Add(tableGroup14);
            this.table3.ColumnGroups.Add(tableGroup15);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox11,
            this.textBox12,
            this.textBox23,
            this.textBox24,
            this.textBox21,
            this.textBox22,
            this.textBox15,
            this.textBox18,
            this.textBox25,
            this.textBox26});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(4.798823356628418D));
            this.table3.Name = "table3";
            tableGroup16.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup16.Name = "detailTableGroup1";
            tableGroup17.Name = "group5";
            tableGroup18.Name = "group4";
            tableGroup19.Name = "group3";
            this.table3.RowGroups.Add(tableGroup16);
            this.table3.RowGroups.Add(tableGroup17);
            this.table3.RowGroups.Add(tableGroup18);
            this.table3.RowGroups.Add(tableGroup19);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.900001049041748D), Telerik.Reporting.Drawing.Unit.Cm(3.2739694118499756D));
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9327592849731445D), Telerik.Reporting.Drawing.Unit.Cm(0.40569433569908142D));
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9672415256500244D), Telerik.Reporting.Drawing.Unit.Cm(0.40569433569908142D));
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9327592849731445D), Telerik.Reporting.Drawing.Unit.Cm(0.85277765989303589D));
            this.textBox15.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox15.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox15.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.StyleName = "";
            this.textBox15.Value = "Angle";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9672415256500244D), Telerik.Reporting.Drawing.Unit.Cm(0.85277765989303589D));
            this.textBox18.Style.Font.Name = "Calibri";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox18.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox18.StyleName = "";
            this.textBox18.Value = "=Fields.RoofAngle";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9327592849731445D), Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D));
            this.textBox21.StyleName = "";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9672415256500244D), Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D));
            this.textBox22.StyleName = "";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9327592849731445D), Telerik.Reporting.Drawing.Unit.Cm(0.92333412170410156D));
            this.textBox23.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox23.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox23.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.StyleName = "";
            this.textBox23.Value = "Roof Type";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9672415256500244D), Telerik.Reporting.Drawing.Unit.Cm(0.92333412170410156D));
            this.textBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox24.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.StyleName = "";
            this.textBox24.Value = "=Fields.RoofType";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.3167572021484375D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.7744934558868408D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.38805565237998962D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.90569454431533813D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458346843719482D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.88805520534515381D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox27);
            this.table4.Body.SetCellContent(0, 1, this.textBox28);
            this.table4.Body.SetCellContent(3, 0, this.textBox29);
            this.table4.Body.SetCellContent(3, 1, this.textBox30);
            this.table4.Body.SetCellContent(2, 0, this.textBox31);
            this.table4.Body.SetCellContent(2, 1, this.textBox32);
            this.table4.Body.SetCellContent(1, 0, this.textBox33);
            this.table4.Body.SetCellContent(1, 1, this.textBox34);
            tableGroup20.Name = "tableGroup3";
            tableGroup20.ReportItem = this.textBox35;
            tableGroup21.Name = "tableGroup4";
            tableGroup21.ReportItem = this.textBox36;
            this.table4.ColumnGroups.Add(tableGroup20);
            this.table4.ColumnGroups.Add(tableGroup21);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox27,
            this.textBox28,
            this.textBox33,
            this.textBox34,
            this.textBox31,
            this.textBox32,
            this.textBox29,
            this.textBox30,
            this.textBox35,
            this.textBox36});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.699999809265137D), Telerik.Reporting.Drawing.Unit.Cm(4.8516335487365723D));
            this.table4.Name = "table4";
            tableGroup22.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup22.Name = "detailTableGroup1";
            tableGroup23.Name = "group5";
            tableGroup24.Name = "group4";
            tableGroup25.Name = "group3";
            this.table4.RowGroups.Add(tableGroup22);
            this.table4.RowGroups.Add(tableGroup23);
            this.table4.RowGroups.Add(tableGroup24);
            this.table4.RowGroups.Add(tableGroup25);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.0912508964538574D), Telerik.Reporting.Drawing.Unit.Cm(3.2299783229827881D));
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3167574405670166D), Telerik.Reporting.Drawing.Unit.Cm(0.3880557119846344D));
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7744934558868408D), Telerik.Reporting.Drawing.Unit.Cm(0.3880557119846344D));
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3167574405670166D), Telerik.Reporting.Drawing.Unit.Cm(0.88805532455444336D));
            this.textBox29.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox29.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox29.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.StyleName = "";
            this.textBox29.Value = "Store";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7744934558868408D), Telerik.Reporting.Drawing.Unit.Cm(0.88805532455444336D));
            this.textBox30.Style.Font.Name = "Calibri";
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox30.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox30.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox30.StyleName = "";
            this.textBox30.Value = "=Fields.StoreName";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3167574405670166D), Telerik.Reporting.Drawing.Unit.Cm(0.26458346843719482D));
            this.textBox31.StyleName = "";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7744934558868408D), Telerik.Reporting.Drawing.Unit.Cm(0.26458346843719482D));
            this.textBox32.StyleName = "";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3167574405670166D), Telerik.Reporting.Drawing.Unit.Cm(0.90569454431533813D));
            this.textBox33.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox33.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox33.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox33.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.StyleName = "";
            this.textBox33.Value = "Manual Quote";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7744934558868408D), Telerik.Reporting.Drawing.Unit.Cm(0.90569454431533813D));
            this.textBox34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox34.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.StyleName = "";
            this.textBox34.Value = "=Fields.ManualQuoteNumber";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Cm(9.1999998092651367D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox14.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox14.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox14.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.Value = "SITE ADDRESS";
            // 
            // txtaddress
            // 
            this.txtaddress.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D), Telerik.Reporting.Drawing.Unit.Cm(10.199999809265137D));
            this.txtaddress.Name = "txtaddress";
            this.txtaddress.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.414361953735352D), Telerik.Reporting.Drawing.Unit.Cm(1.7000004053115845D));
            this.txtaddress.Value = "";
            // 
            // textBox38
            // 
            this.textBox38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D), Telerik.Reporting.Drawing.Unit.Cm(12.70000171661377D));
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5922868251800537D), Telerik.Reporting.Drawing.Unit.Cm(0.5999983549118042D));
            this.textBox38.Style.Color = System.Drawing.Color.White;
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox38.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox38.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.Value = "Qty";
            // 
            // textBox40
            // 
            this.textBox40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(12.699999809265137D));
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3479218482971191D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox40.Style.Color = System.Drawing.Color.White;
            this.textBox40.Style.Font.Bold = true;
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox40.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox40.Value = "Model";
            // 
            // textBox41
            // 
            this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.700000762939453D), Telerik.Reporting.Drawing.Unit.Cm(12.699999809265137D));
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0999984741210938D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox41.Style.Color = System.Drawing.Color.White;
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox41.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.Value = "Description";
            // 
            // textBox42
            // 
            this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17D), Telerik.Reporting.Drawing.Unit.Cm(12.70000171661377D));
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0614502429962158D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox42.Style.Color = System.Drawing.Color.White;
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox42.Value = "Done";
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.699999809265137D), Telerik.Reporting.Drawing.Unit.Cm(25.867710113525391D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.254791259765625D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox47.Style.Font.Bold = true;
            this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox47.Value = "Installer Notes:";
            // 
            // textBox48
            // 
            this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.57312268018722534D), Telerik.Reporting.Drawing.Unit.Cm(27.5D));
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.326879501342773D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox48.Style.Font.Bold = false;
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox48.Value = "ABN 93 603 941 475   | Phone : 1300 285 885";
            // 
            // textBox49
            // 
            this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.52934104204177856D), Telerik.Reporting.Drawing.Unit.Cm(28.299999237060547D));
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.370660781860352D), Telerik.Reporting.Drawing.Unit.Cm(0.79999959468841553D));
            this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox49.Value = "Address : Level 1, 530 Little Collins Street,Melbourne VIC 3000 | www.solarminer." +
    "com.au | info@solarminer.com.au";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.768298864364624D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.5684728622436523D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.5111489295959473D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.3910770416259766D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.4470837116241455D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox51);
            this.table5.Body.SetCellContent(0, 1, this.textBox53);
            this.table5.Body.SetCellContent(0, 2, this.textBox55);
            this.table5.Body.SetCellContent(0, 3, this.textBox57);
            tableGroup26.Name = "tableGroup5";
            tableGroup27.Name = "tableGroup6";
            tableGroup28.Name = "tableGroup7";
            tableGroup29.Name = "group6";
            this.table5.ColumnGroups.Add(tableGroup26);
            this.table5.ColumnGroups.Add(tableGroup27);
            this.table5.ColumnGroups.Add(tableGroup28);
            this.table5.ColumnGroups.Add(tableGroup29);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox51,
            this.textBox53,
            this.textBox55,
            this.textBox57});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.60000014305114746D), Telerik.Reporting.Drawing.Unit.Cm(13.899998664855957D));
            this.table5.Name = "table5";
            tableGroup30.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup30.Name = "detailTableGroup2";
            this.table5.RowGroups.Add(tableGroup30);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.238996505737305D), Telerik.Reporting.Drawing.Unit.Cm(1.4470837116241455D));
            this.table5.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(10D);
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7682987451553345D), Telerik.Reporting.Drawing.Unit.Cm(1.5000003576278687D));
            this.textBox51.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.textBox51.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox51.Value = "=Fields.NumberPanels";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5684728622436523D), Telerik.Reporting.Drawing.Unit.Cm(1.5000003576278687D));
            this.textBox53.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.textBox53.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox53.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox53.Value = "=Fields.PanelBrandName";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5111484527587891D), Telerik.Reporting.Drawing.Unit.Cm(1.5000003576278687D));
            this.textBox55.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.textBox55.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox55.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox55.Value = "=Fields.PanelModel";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3910770416259766D), Telerik.Reporting.Drawing.Unit.Cm(1.5000003576278687D));
            this.textBox57.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.textBox57.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox57.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox57.StyleName = "";
            this.textBox57.Value = "=Fields.Paneldesc";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.7462499141693115D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.6037507057189941D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.5243778228759766D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.4924983978271484D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.005416750907898D)));
            this.table6.Body.SetCellContent(0, 0, this.textBox58);
            this.table6.Body.SetCellContent(0, 1, this.textBox60);
            this.table6.Body.SetCellContent(0, 2, this.textBox62);
            this.table6.Body.SetCellContent(0, 3, this.textBox52);
            tableGroup31.Name = "tableGroup8";
            tableGroup32.Name = "tableGroup9";
            tableGroup33.Name = "tableGroup10";
            tableGroup34.Name = "group7";
            this.table6.ColumnGroups.Add(tableGroup31);
            this.table6.ColumnGroups.Add(tableGroup32);
            this.table6.ColumnGroups.Add(tableGroup33);
            this.table6.ColumnGroups.Add(tableGroup34);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox58,
            this.textBox60,
            this.textBox62,
            this.textBox52});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.58333081007003784D), Telerik.Reporting.Drawing.Unit.Cm(15.200000762939453D));
            this.table6.Name = "table6";
            tableGroup35.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup35.Name = "detailTableGroup3";
            this.table6.RowGroups.Add(tableGroup35);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.366876602172852D), Telerik.Reporting.Drawing.Unit.Cm(1.005416750907898D));
            this.table6.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(10D);
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7462500333786011D), Telerik.Reporting.Drawing.Unit.Cm(1.005416750907898D));
            this.textBox58.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.textBox58.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox58.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox58.Value = "=Fields.inverterqty";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6037507057189941D), Telerik.Reporting.Drawing.Unit.Cm(1.005416750907898D));
            this.textBox60.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.textBox60.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox60.Value = "=Fields.InverterDetailsName";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5243778228759766D), Telerik.Reporting.Drawing.Unit.Cm(1.005416750907898D));
            this.textBox62.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.textBox62.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox62.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.Value = "=Fields.Invertermodel";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.4924983978271484D), Telerik.Reporting.Drawing.Unit.Cm(1.005416750907898D));
            this.textBox52.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.textBox52.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox52.StyleName = "";
            this.textBox52.Value = "=Fields.Inverterdesc";
            // 
            // textBox63
            // 
            this.textBox63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2125983238220215D), Telerik.Reporting.Drawing.Unit.Inch(7.99212646484375D));
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8410310745239258D), Telerik.Reporting.Drawing.Unit.Inch(2.149442195892334D));
            this.textBox63.Style.Font.Bold = false;
            this.textBox63.Style.Font.Name = "Calibri";
            this.textBox63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox63.Value = "=Fields.InstallerNotes";
            // 
            // textBox39
            // 
            this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4000003337860107D), Telerik.Reporting.Drawing.Unit.Cm(12.699999809265137D));
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.496037483215332D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox39.Style.Color = System.Drawing.Color.White;
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.Value = "Item";
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.3602097034454346D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.3710427284240723D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.2789803743362427D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.93500840663909912D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.4739928245544434D)));
            this.table8.Body.SetCellContent(1, 0, this.textBox70, 1, 2);
            this.table8.Body.SetCellContent(2, 0, this.textBox73, 1, 2);
            this.table8.Body.SetCellContent(0, 0, this.textBox54, 1, 2);
            tableGroup37.Name = "group11";
            tableGroup38.Name = "group12";
            tableGroup36.ChildGroups.Add(tableGroup37);
            tableGroup36.ChildGroups.Add(tableGroup38);
            tableGroup36.Name = "group16";
            tableGroup36.ReportItem = this.textBox50;
            this.table8.ColumnGroups.Add(tableGroup36);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox54,
            this.textBox70,
            this.textBox73,
            this.textBox50});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D), Telerik.Reporting.Drawing.Unit.Cm(20.600000381469727D));
            this.table8.Name = "table8";
            tableGroup40.Name = "group14";
            tableGroup39.ChildGroups.Add(tableGroup40);
            tableGroup39.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup39.Name = "detailTableGroup5";
            tableGroup41.Name = "group9";
            tableGroup42.Name = "group10";
            this.table8.RowGroups.Add(tableGroup39);
            this.table8.RowGroups.Add(tableGroup41);
            this.table8.RowGroups.Add(tableGroup42);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7312526702880859D), Telerik.Reporting.Drawing.Unit.Cm(4.6141180992126465D));
            this.table8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.table8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7312526702880859D), Telerik.Reporting.Drawing.Unit.Cm(0.93500840663909912D));
            this.textBox70.Style.Font.Bold = true;
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox70.StyleName = "";
            this.textBox70.Value = "DATE      :";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7312526702880859D), Telerik.Reporting.Drawing.Unit.Cm(1.4739927053451538D));
            this.textBox73.Style.Font.Bold = true;
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox73.StyleName = "";
            this.textBox73.Value = "SIGN        :";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7312526702880859D), Telerik.Reporting.Drawing.Unit.Cm(1.2789803743362427D));
            this.textBox54.Style.Font.Bold = true;
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox54.Value = "PICKED BY     :";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.719792366027832D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.6037507057189941D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.6037502288818359D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.4395837783813477D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.1906249523162842D)));
            this.table7.Body.SetCellContent(0, 0, this.textBox61);
            this.table7.Body.SetCellContent(0, 1, this.textBox65);
            this.table7.Body.SetCellContent(0, 2, this.textBox67);
            this.table7.Body.SetCellContent(0, 3, this.textBox59);
            tableGroup43.Name = "tableGroup11";
            tableGroup44.Name = "tableGroup12";
            tableGroup45.Name = "tableGroup13";
            tableGroup46.Name = "group8";
            this.table7.ColumnGroups.Add(tableGroup43);
            this.table7.ColumnGroups.Add(tableGroup44);
            this.table7.ColumnGroups.Add(tableGroup45);
            this.table7.ColumnGroups.Add(tableGroup46);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox61,
            this.textBox65,
            this.textBox67,
            this.textBox59});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5724945068359375D), Telerik.Reporting.Drawing.Unit.Cm(16.299999237060547D));
            this.table7.Name = "table7";
            tableGroup47.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup47.Name = "detailTableGroup4";
            this.table7.RowGroups.Add(tableGroup47);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.366876602172852D), Telerik.Reporting.Drawing.Unit.Cm(1.1906249523162842D));
            this.table7.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(10D);
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.719792366027832D), Telerik.Reporting.Drawing.Unit.Cm(1.1906249523162842D));
            this.textBox61.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.textBox61.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox61.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox61.Value = "=Fields.inverterqty2";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6037507057189941D), Telerik.Reporting.Drawing.Unit.Cm(1.1906249523162842D));
            this.textBox65.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.textBox65.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.Value = "=Fields.SecondInverterDetails";
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.6037502288818359D), Telerik.Reporting.Drawing.Unit.Cm(1.1906249523162842D));
            this.textBox67.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.textBox67.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox67.Value = "=Fields.SecondInvertermodel";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.4395837783813477D), Telerik.Reporting.Drawing.Unit.Cm(1.1906249523162842D));
            this.textBox59.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.textBox59.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox59.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox59.StyleName = "";
            this.textBox59.Value = "=Fields.SecondInverterDetailsdesc";
            // 
            // SmPicklist
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SmPicklist";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(211.58750915527344D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox txtinstdate;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox txtaddress;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox58;
    }
}