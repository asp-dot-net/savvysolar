namespace EurosolarReporting
{
    partial class SavvySolarQuote
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SavvySolarQuote));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.pictureBox3 = new Telerik.Reporting.PictureBox();
            this.pictureBox4 = new Telerik.Reporting.PictureBox();
            this.pictureBox5 = new Telerik.Reporting.PictureBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.txtname = new Telerik.Reporting.TextBox();
            this.txtphone = new Telerik.Reporting.TextBox();
            this.txtemail = new Telerik.Reporting.TextBox();
            this.txtsitedetails = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.txtrooftype = new Telerik.Reporting.TextBox();
            this.txtstory = new Telerik.Reporting.TextBox();
            this.txtroofpitch = new Telerik.Reporting.TextBox();
            this.txtenerdist = new Telerik.Reporting.TextBox();
            this.txtenerret = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.pictureBox6 = new Telerik.Reporting.PictureBox();
            this.pictureBox7 = new Telerik.Reporting.PictureBox();
            this.pictureBox8 = new Telerik.Reporting.PictureBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.pictureBox9 = new Telerik.Reporting.PictureBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.txttotalCost = new Telerik.Reporting.TextBox();
            this.txtsubtotal = new Telerik.Reporting.TextBox();
            this.txtlessinc = new Telerik.Reporting.TextBox();
            this.txtgrandtotal = new Telerik.Reporting.TextBox();
            this.txtquotprice = new Telerik.Reporting.TextBox();
            this.txtvicrebate = new Telerik.Reporting.TextBox();
            this.txtvicloan = new Telerik.Reporting.TextBox();
            this.txtfinalprice = new Telerik.Reporting.TextBox();
            this.txtdeposit = new Telerik.Reporting.TextBox();
            this.txtbalance = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.txtstc = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(1485D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.pictureBox2,
            this.pictureBox3,
            this.pictureBox4,
            this.pictureBox5,
            this.table1,
            this.table2,
            this.textBox8,
            this.textBox9,
            this.pictureBox6,
            this.pictureBox7,
            this.pictureBox8,
            this.textBox1,
            this.textBox5,
            this.pictureBox9,
            this.textBox7,
            this.table3,
            this.table4,
            this.txtstc,
            this.textBox13,
            this.textBox14});
            this.detail.Name = "detail";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.pictureBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(11.693D));
            this.pictureBox2.MimeType = "image/jpeg";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(23.355D));
            this.pictureBox3.MimeType = "image/jpeg";
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(290D));
            this.pictureBox3.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox3.Value = ((object)(resources.GetObject("pictureBox3.Value")));
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(35D));
            this.pictureBox4.MimeType = "image/jpeg";
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(290D));
            this.pictureBox4.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox4.Value = ((object)(resources.GetObject("pictureBox4.Value")));
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(46.5D));
            this.pictureBox5.MimeType = "image/jpeg";
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(295D));
            this.pictureBox5.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox5.Value = ((object)(resources.GetObject("pictureBox5.Value")));
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.281D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.063D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.144D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.802D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.21D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.21D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.231D)));
            this.table1.Body.SetCellContent(4, 2, this.textBox4);
            this.table1.Body.SetCellContent(4, 1, this.textBox10);
            this.table1.Body.SetCellContent(4, 0, this.textBox15);
            this.table1.Body.SetCellContent(4, 3, this.textBox6);
            this.table1.Body.SetCellContent(0, 0, this.txtname, 1, 4);
            this.table1.Body.SetCellContent(1, 0, this.txtphone, 1, 4);
            this.table1.Body.SetCellContent(2, 0, this.txtemail, 1, 4);
            this.table1.Body.SetCellContent(3, 0, this.txtsitedetails, 1, 4);
            tableGroup1.Name = "group6";
            tableGroup2.Name = "group5";
            tableGroup3.Name = "tableGroup2";
            tableGroup4.Name = "group7";
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtname,
            this.txtphone,
            this.txtemail,
            this.txtsitedetails,
            this.textBox15,
            this.textBox10,
            this.textBox4,
            this.textBox6});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1D), Telerik.Reporting.Drawing.Unit.Inch(1.9D));
            this.table1.Name = "table1";
            tableGroup6.Name = "group1";
            tableGroup7.Name = "group";
            tableGroup8.Name = "group2";
            tableGroup9.Name = "group3";
            tableGroup10.Name = "group4";
            tableGroup5.ChildGroups.Add(tableGroup6);
            tableGroup5.ChildGroups.Add(tableGroup7);
            tableGroup5.ChildGroups.Add(tableGroup8);
            tableGroup5.ChildGroups.Add(tableGroup9);
            tableGroup5.ChildGroups.Add(tableGroup10);
            tableGroup5.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup5.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup5);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.29D), Telerik.Reporting.Drawing.Unit.Inch(1.051D));
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.144D), Telerik.Reporting.Drawing.Unit.Inch(0.231D));
            this.textBox4.StyleName = "";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.063D), Telerik.Reporting.Drawing.Unit.Inch(0.231D));
            this.textBox10.StyleName = "";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.281D), Telerik.Reporting.Drawing.Unit.Inch(0.231D));
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.StyleName = "";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.802D), Telerik.Reporting.Drawing.Unit.Inch(0.231D));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.StyleName = "";
            // 
            // txtname
            // 
            this.txtname.Name = "txtname";
            this.txtname.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.29D), Telerik.Reporting.Drawing.Unit.Inch(0.21D));
            this.txtname.Style.Font.Bold = true;
            this.txtname.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtname.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtname.StyleName = "";
            this.txtname.Value = "";
            // 
            // txtphone
            // 
            this.txtphone.Name = "txtphone";
            this.txtphone.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.29D), Telerik.Reporting.Drawing.Unit.Inch(0.21D));
            this.txtphone.Style.Font.Bold = true;
            this.txtphone.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtphone.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtphone.StyleName = "";
            this.txtphone.Value = "";
            // 
            // txtemail
            // 
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.29D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.txtemail.Style.Font.Bold = true;
            this.txtemail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtemail.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtemail.StyleName = "";
            this.txtemail.Value = "";
            // 
            // txtsitedetails
            // 
            this.txtsitedetails.Name = "txtsitedetails";
            this.txtsitedetails.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.29D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.txtsitedetails.Style.Font.Bold = true;
            this.txtsitedetails.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtsitedetails.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtsitedetails.StyleName = "";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.1D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.221D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table2.Body.SetCellContent(0, 0, this.txtrooftype);
            this.table2.Body.SetCellContent(1, 0, this.txtstory);
            this.table2.Body.SetCellContent(2, 0, this.txtroofpitch);
            this.table2.Body.SetCellContent(3, 0, this.txtenerdist);
            this.table2.Body.SetCellContent(4, 0, this.txtenerret);
            tableGroup11.Name = "tableGroup1";
            this.table2.ColumnGroups.Add(tableGroup11);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtrooftype,
            this.txtstory,
            this.txtroofpitch,
            this.txtenerdist,
            this.txtenerret});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.8D), Telerik.Reporting.Drawing.Unit.Inch(1.9D));
            this.table2.Name = "table2";
            tableGroup13.Name = "group9";
            tableGroup14.Name = "group8";
            tableGroup15.Name = "group10";
            tableGroup16.Name = "group11";
            tableGroup17.Name = "group12";
            tableGroup12.ChildGroups.Add(tableGroup13);
            tableGroup12.ChildGroups.Add(tableGroup14);
            tableGroup12.ChildGroups.Add(tableGroup15);
            tableGroup12.ChildGroups.Add(tableGroup16);
            tableGroup12.ChildGroups.Add(tableGroup17);
            tableGroup12.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup12.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup12);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1D), Telerik.Reporting.Drawing.Unit.Inch(1.021D));
            // 
            // txtrooftype
            // 
            this.txtrooftype.Name = "txtrooftype";
            this.txtrooftype.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.txtrooftype.Style.Font.Bold = true;
            this.txtrooftype.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtrooftype.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtrooftype.StyleName = "";
            // 
            // txtstory
            // 
            this.txtstory.Name = "txtstory";
            this.txtstory.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.txtstory.Style.Font.Bold = true;
            this.txtstory.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtstory.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtstory.StyleName = "";
            // 
            // txtroofpitch
            // 
            this.txtroofpitch.Name = "txtroofpitch";
            this.txtroofpitch.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1D), Telerik.Reporting.Drawing.Unit.Inch(0.221D));
            this.txtroofpitch.Style.Font.Bold = true;
            this.txtroofpitch.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtroofpitch.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtroofpitch.StyleName = "";
            // 
            // txtenerdist
            // 
            this.txtenerdist.Name = "txtenerdist";
            this.txtenerdist.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.txtenerdist.Style.Font.Bold = true;
            this.txtenerdist.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtenerdist.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtenerdist.StyleName = "";
            // 
            // txtenerret
            // 
            this.txtenerret.Name = "txtenerret";
            this.txtenerret.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.1D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.txtenerret.Style.Font.Bold = true;
            this.txtenerret.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtenerret.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtenerret.StyleName = "";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.7D), Telerik.Reporting.Drawing.Unit.Inch(0.6D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Value = "";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(7.4D), Telerik.Reporting.Drawing.Unit.Inch(1.2D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.642D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox9.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "";
            // 
            // pictureBox6
            // 
            this.pictureBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.1D), Telerik.Reporting.Drawing.Unit.Inch(12.3D));
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.4D), Telerik.Reporting.Drawing.Unit.Inch(5.5D));
            this.pictureBox6.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.1D), Telerik.Reporting.Drawing.Unit.Inch(17.8D));
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.1D), Telerik.Reporting.Drawing.Unit.Inch(4.3D));
            this.pictureBox7.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.5D), Telerik.Reporting.Drawing.Unit.Inch(12.3D));
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.6D), Telerik.Reporting.Drawing.Unit.Inch(5.5D));
            this.pictureBox8.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6D), Telerik.Reporting.Drawing.Unit.Inch(31.5D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6D), Telerik.Reporting.Drawing.Unit.Inch(0.3D));
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "";
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6D), Telerik.Reporting.Drawing.Unit.Inch(32D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6D), Telerik.Reporting.Drawing.Unit.Inch(0.21D));
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.6D), Telerik.Reporting.Drawing.Unit.Inch(32.5D));
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.656D), Telerik.Reporting.Drawing.Unit.Inch(0.9D));
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.5D), Telerik.Reporting.Drawing.Unit.Inch(3.7D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.833D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.342D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.721D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.533D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.512D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.94D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.273D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.325D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.252D)));
            this.table3.Body.SetCellContent(0, 0, this.txttotalCost);
            this.table3.Body.SetCellContent(4, 0, this.txtsubtotal);
            this.table3.Body.SetCellContent(5, 0, this.txtlessinc);
            this.table3.Body.SetCellContent(6, 0, this.txtgrandtotal);
            this.table3.Body.SetCellContent(7, 0, this.txtquotprice);
            this.table3.Body.SetCellContent(8, 0, this.txtvicrebate);
            this.table3.Body.SetCellContent(9, 0, this.txtvicloan);
            this.table3.Body.SetCellContent(10, 0, this.txtfinalprice);
            this.table3.Body.SetCellContent(11, 0, this.txtdeposit);
            this.table3.Body.SetCellContent(12, 0, this.txtbalance);
            this.table3.Body.SetCellContent(1, 0, this.textBox18, 3, 1);
            tableGroup18.Name = "tableGroup3";
            this.table3.ColumnGroups.Add(tableGroup18);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txttotalCost,
            this.textBox18,
            this.txtsubtotal,
            this.txtlessinc,
            this.txtgrandtotal,
            this.txtquotprice,
            this.txtvicrebate,
            this.txtvicloan,
            this.txtfinalprice,
            this.txtdeposit,
            this.txtbalance});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.7D), Telerik.Reporting.Drawing.Unit.Inch(3.7D));
            this.table3.Name = "table3";
            tableGroup19.Name = "group13";
            tableGroup20.Name = "group15";
            tableGroup21.Name = "group14";
            tableGroup22.Name = "group16";
            tableGroup23.Name = "group17";
            tableGroup24.Name = "group18";
            tableGroup25.Name = "group19";
            tableGroup26.Name = "group20";
            tableGroup27.Name = "group21";
            tableGroup28.Name = "group22";
            tableGroup29.Name = "group23";
            tableGroup30.Name = "group24";
            tableGroup31.Name = "group25";
            this.table3.RowGroups.Add(tableGroup19);
            this.table3.RowGroups.Add(tableGroup20);
            this.table3.RowGroups.Add(tableGroup21);
            this.table3.RowGroups.Add(tableGroup22);
            this.table3.RowGroups.Add(tableGroup23);
            this.table3.RowGroups.Add(tableGroup24);
            this.table3.RowGroups.Add(tableGroup25);
            this.table3.RowGroups.Add(tableGroup26);
            this.table3.RowGroups.Add(tableGroup27);
            this.table3.RowGroups.Add(tableGroup28);
            this.table3.RowGroups.Add(tableGroup29);
            this.table3.RowGroups.Add(tableGroup30);
            this.table3.RowGroups.Add(tableGroup31);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.342D), Telerik.Reporting.Drawing.Unit.Inch(4.756D));
            // 
            // txttotalCost
            // 
            this.txttotalCost.Name = "txttotalCost";
            this.txttotalCost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.342D), Telerik.Reporting.Drawing.Unit.Inch(0.721D));
            this.txttotalCost.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txttotalCost.StyleName = "";
            // 
            // txtsubtotal
            // 
            this.txtsubtotal.Name = "txtsubtotal";
            this.txtsubtotal.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.342D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.txtsubtotal.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtsubtotal.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtsubtotal.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtsubtotal.StyleName = "";
            this.txtsubtotal.Value = "";
            // 
            // txtlessinc
            // 
            this.txtlessinc.Name = "txtlessinc";
            this.txtlessinc.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.342D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.txtlessinc.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtlessinc.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtlessinc.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtlessinc.StyleName = "";
            // 
            // txtgrandtotal
            // 
            this.txtgrandtotal.Name = "txtgrandtotal";
            this.txtgrandtotal.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.342D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.txtgrandtotal.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtgrandtotal.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtgrandtotal.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtgrandtotal.StyleName = "";
            // 
            // txtquotprice
            // 
            this.txtquotprice.Name = "txtquotprice";
            this.txtquotprice.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.342D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.txtquotprice.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtquotprice.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtquotprice.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtquotprice.StyleName = "";
            // 
            // txtvicrebate
            // 
            this.txtvicrebate.Name = "txtvicrebate";
            this.txtvicrebate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.342D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.txtvicrebate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtvicrebate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtvicrebate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtvicrebate.StyleName = "";
            // 
            // txtvicloan
            // 
            this.txtvicloan.Name = "txtvicloan";
            this.txtvicloan.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.342D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.txtvicloan.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtvicloan.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtvicloan.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtvicloan.StyleName = "";
            // 
            // txtfinalprice
            // 
            this.txtfinalprice.Name = "txtfinalprice";
            this.txtfinalprice.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.342D), Telerik.Reporting.Drawing.Unit.Inch(0.273D));
            this.txtfinalprice.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtfinalprice.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtfinalprice.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtfinalprice.StyleName = "";
            this.txtfinalprice.Value = "";
            // 
            // txtdeposit
            // 
            this.txtdeposit.Name = "txtdeposit";
            this.txtdeposit.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.342D), Telerik.Reporting.Drawing.Unit.Inch(0.325D));
            this.txtdeposit.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtdeposit.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtdeposit.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtdeposit.StyleName = "";
            this.txtdeposit.Value = "";
            // 
            // txtbalance
            // 
            this.txtbalance.Name = "txtbalance";
            this.txtbalance.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.342D), Telerik.Reporting.Drawing.Unit.Inch(0.252D));
            this.txtbalance.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtbalance.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtbalance.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtbalance.StyleName = "";
            this.txtbalance.Value = "";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.342D), Telerik.Reporting.Drawing.Unit.Inch(1.985D));
            this.textBox18.StyleName = "";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(4.833D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.242D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table4.Body.SetCellContent(0, 1, this.textBox2);
            this.table4.Body.SetCellContent(1, 1, this.textBox3);
            this.table4.Body.SetCellContent(0, 0, this.textBox11);
            this.table4.Body.SetCellContent(1, 0, this.textBox12);
            tableGroup32.Name = "group28";
            tableGroup33.Name = "tableGroup";
            this.table4.ColumnGroups.Add(tableGroup32);
            this.table4.ColumnGroups.Add(tableGroup33);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox11,
            this.textBox2,
            this.textBox12,
            this.textBox3});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.5D), Telerik.Reporting.Drawing.Unit.Inch(3.9D));
            this.table4.Name = "table4";
            tableGroup34.Name = "group26";
            tableGroup35.Name = "group27";
            this.table4.RowGroups.Add(tableGroup34);
            this.table4.RowGroups.Add(tableGroup35);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.833D), Telerik.Reporting.Drawing.Unit.Inch(0.442D));
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.833D), Telerik.Reporting.Drawing.Unit.Inch(0.242D));
            this.textBox2.Style.Font.Bold = false;
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox2.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox2.StyleName = "";
            this.textBox2.Value = "";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.833D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox3.Style.Font.Bold = false;
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.StyleName = "";
            this.textBox3.Value = "";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.242D));
            this.textBox11.StyleName = "";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox12.StyleName = "";
            // 
            // txtstc
            // 
            this.txtstc.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.9D), Telerik.Reporting.Drawing.Unit.Inch(6.606D));
            this.txtstc.Name = "txtstc";
            this.txtstc.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.7D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.txtstc.Style.Font.Bold = true;
            this.txtstc.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtstc.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtstc.Value = "";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.4D), Telerik.Reporting.Drawing.Unit.Inch(6.6D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7D), Telerik.Reporting.Drawing.Unit.Inch(0.987D));
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox13.Value = "";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2D), Telerik.Reporting.Drawing.Unit.Inch(22.5D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.042D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.textBox14.Style.Color = System.Drawing.Color.White;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox14.Value = "";
            // 
            // SavvySolarQuote
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SavvySolarQuote";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.PictureBox pictureBox3;
        private Telerik.Reporting.PictureBox pictureBox4;
        private Telerik.Reporting.PictureBox pictureBox5;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox txtname;
        private Telerik.Reporting.TextBox txtphone;
        private Telerik.Reporting.TextBox txtemail;
        private Telerik.Reporting.TextBox txtsitedetails;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox txtrooftype;
        private Telerik.Reporting.TextBox txtstory;
        private Telerik.Reporting.TextBox txtroofpitch;
        private Telerik.Reporting.TextBox txtenerdist;
        private Telerik.Reporting.TextBox txtenerret;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.PictureBox pictureBox6;
        private Telerik.Reporting.PictureBox pictureBox7;
        private Telerik.Reporting.PictureBox pictureBox8;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.PictureBox pictureBox9;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox txttotalCost;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox txtsubtotal;
        private Telerik.Reporting.TextBox txtlessinc;
        private Telerik.Reporting.TextBox txtgrandtotal;
        private Telerik.Reporting.TextBox txtquotprice;
        private Telerik.Reporting.TextBox txtvicrebate;
        private Telerik.Reporting.TextBox txtvicloan;
        private Telerik.Reporting.TextBox txtfinalprice;
        private Telerik.Reporting.TextBox txtdeposit;
        private Telerik.Reporting.TextBox txtbalance;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox txtstc;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
    }
}