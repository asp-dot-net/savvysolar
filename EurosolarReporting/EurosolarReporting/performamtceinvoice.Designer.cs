namespace EurosolarReporting
{
    partial class performamtceinvoice
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(performamtceinvoice));
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.txtdate = new Telerik.Reporting.DetailSection();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox127 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.lblInverterDetails = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.lblTotalQuotePrice = new Telerik.Reporting.TextBox();
            this.lblNetCost = new Telerik.Reporting.TextBox();
            this.lblDepositPaid = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.lblBalanceDue = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.lblCommissionGST = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.lblinvoiceno = new Telerik.Reporting.TextBox();
            this.lblSalesInvDate = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.panel5 = new Telerik.Reporting.Panel();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.htmlTextBox2 = new Telerik.Reporting.HtmlTextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.054170608520508D), Telerik.Reporting.Drawing.Unit.Cm(0.60854154825210571D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Arial";
            this.textBox2.Value = "Customer Details";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8954133987426758D), Telerik.Reporting.Drawing.Unit.Cm(0.60854154825210571D));
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Arial";
            this.textBox16.Value = "Installation Detail";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.94354248046875D), Telerik.Reporting.Drawing.Unit.Cm(0.60854184627532959D));
            this.textBox8.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Arial";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Value = "Payment Option :";
            // 
            // txtdate
            // 
            this.txtdate.Height = Telerik.Reporting.Drawing.Unit.Inch(10.09999942779541D);
            this.txtdate.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table8,
            this.lblinvoiceno,
            this.lblSalesInvDate,
            this.textBox41,
            this.textBox4,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.panel5,
            this.table3,
            this.htmlTextBox2,
            this.table2,
            this.textBox6,
            this.pictureBox2});
            this.txtdate.Name = "txtdate";
            this.txtdate.Style.Font.Name = "Arial";
            this.txtdate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.041130542755127D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(11.313304901123047D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0028202533721924D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.6062889099121094D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.84666651487350464D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.73812484741210938D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table8.Body.SetCellContent(1, 0, this.textBox127);
            this.table8.Body.SetCellContent(2, 0, this.textBox126);
            this.table8.Body.SetCellContent(1, 1, this.textBox78);
            this.table8.Body.SetCellContent(2, 1, this.lblInverterDetails);
            this.table8.Body.SetCellContent(0, 0, this.textBox1, 1, 2);
            this.table8.Body.SetCellContent(0, 3, this.lblTotalQuotePrice);
            this.table8.Body.SetCellContent(1, 3, this.lblNetCost);
            this.table8.Body.SetCellContent(2, 3, this.lblDepositPaid);
            this.table8.Body.SetCellContent(0, 2, this.textBox31);
            this.table8.Body.SetCellContent(1, 2, this.textBox3);
            this.table8.Body.SetCellContent(2, 2, this.textBox25);
            this.table8.Body.SetCellContent(4, 3, this.textBox26);
            this.table8.Body.SetCellContent(3, 3, this.lblBalanceDue);
            this.table8.Body.SetCellContent(3, 2, this.textBox33);
            this.table8.Body.SetCellContent(4, 0, this.lblCommissionGST, 1, 3);
            this.table8.Body.SetCellContent(3, 0, this.textBox27, 1, 2);
            tableGroup1.Name = "tableGroup13";
            tableGroup2.Name = "tableGroup14";
            tableGroup3.Name = "group11";
            tableGroup4.Name = "group12";
            this.table8.ColumnGroups.Add(tableGroup1);
            this.table8.ColumnGroups.Add(tableGroup2);
            this.table8.ColumnGroups.Add(tableGroup3);
            this.table8.ColumnGroups.Add(tableGroup4);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBox31,
            this.lblTotalQuotePrice,
            this.textBox127,
            this.textBox78,
            this.textBox3,
            this.lblNetCost,
            this.textBox126,
            this.lblInverterDetails,
            this.textBox25,
            this.lblDepositPaid,
            this.textBox27,
            this.textBox33,
            this.lblBalanceDue,
            this.lblCommissionGST,
            this.textBox26});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(6.5000004768371582D));
            this.table8.Name = "table8";
            tableGroup6.Name = "group10";
            tableGroup7.Name = "group28";
            tableGroup8.Name = "group29";
            tableGroup9.Name = "group5";
            tableGroup10.Name = "group4";
            tableGroup5.ChildGroups.Add(tableGroup6);
            tableGroup5.ChildGroups.Add(tableGroup7);
            tableGroup5.ChildGroups.Add(tableGroup8);
            tableGroup5.ChildGroups.Add(tableGroup9);
            tableGroup5.ChildGroups.Add(tableGroup10);
            tableGroup5.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup5.Name = "detailTableGroup7";
            this.table8.RowGroups.Add(tableGroup5);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.963544845581055D), Telerik.Reporting.Drawing.Unit.Cm(3.0847914218902588D));
            this.table8.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.table8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table8.Style.Font.Name = "Arial";
            this.table8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // textBox127
            // 
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0411303043365479D), Telerik.Reporting.Drawing.Unit.Cm(0.84666651487350464D));
            this.textBox127.Style.Font.Bold = true;
            this.textBox127.Style.Font.Name = "Arial";
            this.textBox127.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox127.Value = "Panels Details :";
            // 
            // textBox126
            // 
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0411303043365479D), Telerik.Reporting.Drawing.Unit.Cm(0.73812484741210938D));
            this.textBox126.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox126.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox126.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox126.Style.Font.Bold = true;
            this.textBox126.Style.Font.Name = "Arial";
            this.textBox126.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox126.StyleName = "";
            this.textBox126.Value = "Inverter Details :";
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.313303947448731D), Telerik.Reporting.Drawing.Unit.Cm(0.84666651487350464D));
            this.textBox78.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox78.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox78.Style.Font.Name = "Arial";
            this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox78.Value = "=Fields.PanelDetails";
            // 
            // lblInverterDetails
            // 
            this.lblInverterDetails.Name = "lblInverterDetails";
            this.lblInverterDetails.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.313303947448731D), Telerik.Reporting.Drawing.Unit.Cm(0.73812484741210938D));
            this.lblInverterDetails.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.lblInverterDetails.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.lblInverterDetails.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.lblInverterDetails.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.lblInverterDetails.Style.Font.Name = "Arial";
            this.lblInverterDetails.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.354434967041016D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox1.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Arial";
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.StyleName = "";
            this.textBox1.Value = "Maintenance";
            // 
            // lblTotalQuotePrice
            // 
            this.lblTotalQuotePrice.Name = "lblTotalQuotePrice";
            this.lblTotalQuotePrice.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6062893867492676D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.lblTotalQuotePrice.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.lblTotalQuotePrice.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.lblTotalQuotePrice.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.lblTotalQuotePrice.Style.Font.Bold = true;
            this.lblTotalQuotePrice.Style.Font.Name = "Arial";
            this.lblTotalQuotePrice.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.lblTotalQuotePrice.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblTotalQuotePrice.StyleName = "";
            this.lblTotalQuotePrice.Value = "";
            // 
            // lblNetCost
            // 
            this.lblNetCost.Name = "lblNetCost";
            this.lblNetCost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6062893867492676D), Telerik.Reporting.Drawing.Unit.Cm(0.84666651487350464D));
            this.lblNetCost.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.lblNetCost.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.lblNetCost.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.lblNetCost.Style.Font.Name = "Microsoft Sans Serif";
            this.lblNetCost.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.lblNetCost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblNetCost.StyleName = "";
            this.lblNetCost.Value = "";
            // 
            // lblDepositPaid
            // 
            this.lblDepositPaid.Name = "lblDepositPaid";
            this.lblDepositPaid.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6062893867492676D), Telerik.Reporting.Drawing.Unit.Cm(0.73812484741210938D));
            this.lblDepositPaid.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.lblDepositPaid.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.lblDepositPaid.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.lblDepositPaid.Style.Font.Name = "Microsoft Sans Serif";
            this.lblDepositPaid.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.lblDepositPaid.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblDepositPaid.StyleName = "";
            this.lblDepositPaid.Value = "";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.002819299697876D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox31.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox31.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox31.Style.Font.Bold = true;
            this.textBox31.Style.Font.Name = "Arial";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox31.Value = "Mtce Cost:";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.002819299697876D), Telerik.Reporting.Drawing.Unit.Cm(0.84666651487350464D));
            this.textBox3.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox3.Style.Font.Name = "Arial";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox3.StyleName = "";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.002819299697876D), Telerik.Reporting.Drawing.Unit.Cm(0.73812484741210938D));
            this.textBox25.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox25.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox25.Style.Font.Name = "Arial";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.StyleName = "";
            this.textBox25.Value = "Deposit Paid:";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6062893867492676D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox26.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.StyleName = "";
            // 
            // lblBalanceDue
            // 
            this.lblBalanceDue.Name = "lblBalanceDue";
            this.lblBalanceDue.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6062893867492676D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.lblBalanceDue.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.lblBalanceDue.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.lblBalanceDue.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.lblBalanceDue.Style.Font.Name = "Microsoft Sans Serif";
            this.lblBalanceDue.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.lblBalanceDue.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblBalanceDue.StyleName = "";
            this.lblBalanceDue.Value = "=Fields.lblBalanceToPay";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.002819299697876D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox33.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox33.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox33.Style.Color = System.Drawing.Color.Black;
            this.textBox33.Style.Font.Bold = false;
            this.textBox33.Style.Font.Name = "Arial";
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.Value = "Balance Due:";
            // 
            // lblCommissionGST
            // 
            this.lblCommissionGST.Name = "lblCommissionGST";
            this.lblCommissionGST.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.357255935668945D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.lblCommissionGST.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.lblCommissionGST.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.lblCommissionGST.Style.Font.Name = "Arial";
            this.lblCommissionGST.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.lblCommissionGST.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.354434967041016D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox27.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox27.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox27.Style.Font.Name = "Microsoft Sans Serif";
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.StyleName = "";
            // 
            // lblinvoiceno
            // 
            this.lblinvoiceno.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.94488191604614258D));
            this.lblinvoiceno.Name = "lblinvoiceno";
            this.lblinvoiceno.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8541674613952637D), Telerik.Reporting.Drawing.Unit.Inch(0.29999995231628418D));
            this.lblinvoiceno.Style.Color = System.Drawing.Color.Black;
            this.lblinvoiceno.Style.Font.Bold = true;
            this.lblinvoiceno.Style.Font.Name = "Arial";
            this.lblinvoiceno.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.lblinvoiceno.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.lblinvoiceno.Value = "";
            // 
            // lblSalesInvDate
            // 
            this.lblSalesInvDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.2204724550247192D));
            this.lblSalesInvDate.Name = "lblSalesInvDate";
            this.lblSalesInvDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.8541669845581055D), Telerik.Reporting.Drawing.Unit.Inch(0.21102355420589447D));
            this.lblSalesInvDate.Style.Color = System.Drawing.Color.Black;
            this.lblSalesInvDate.Style.Font.Bold = false;
            this.lblSalesInvDate.Style.Font.Name = "Arial";
            this.lblSalesInvDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.lblSalesInvDate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.lblSalesInvDate.Value = "";
            // 
            // textBox41
            // 
            this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.8267722129821777D), Telerik.Reporting.Drawing.Unit.Inch(-4.9670538793122887E-09D));
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.0376434326171875D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.Style.Font.Name = "Arial";
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox41.Value = "ARISE SOLAR";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.8691399097442627D), Telerik.Reporting.Drawing.Unit.Inch(0.23622052371501923D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.0023212432861328D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox4.Style.Font.Name = "Arial";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox4.Value = "ABN 95 130 845 199 | Phone : 1300 38 76 76";
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.34645676612854D), Telerik.Reporting.Drawing.Unit.Inch(0.43629947304725647D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.3000006675720215D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox43.Style.Font.Name = "Arial";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox43.Value = "Visit us on : www.eurosolar.com.au,";
            // 
            // textBox44
            // 
            this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.6715011596679688D), Telerik.Reporting.Drawing.Unit.Inch(0.43629947304725647D));
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.2083799839019775D), Telerik.Reporting.Drawing.Unit.Inch(0.20000004768371582D));
            this.textBox44.Style.Font.Name = "Arial";
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox44.Value = "Email : contact@eurosolar.com.au";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3714613914489746D), Telerik.Reporting.Drawing.Unit.Inch(0.63637810945510864D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.5D), Telerik.Reporting.Drawing.Unit.Inch(0.14135169982910156D));
            this.textBox45.Style.Font.Name = "Arial";
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox45.Value = "Fax No: 07 3319 6197";
            // 
            // panel5
            // 
            this.panel5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.83610266447067261D));
            this.panel5.Name = "panel5";
            this.panel5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.864415168762207D), Telerik.Reporting.Drawing.Unit.Inch(0.0833333358168602D));
            this.panel5.Style.BorderColor.Default = System.Drawing.Color.DarkGray;
            this.panel5.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.panel5.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.panel5.Style.Font.Name = "Arial";
            this.panel5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(10.054170608520508D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(9.89541244506836D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60854178667068481D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table3.Body.SetCellContent(0, 1, this.textBox17);
            this.table3.Body.SetCellContent(2, 0, this.textBox19);
            this.table3.Body.SetCellContent(2, 1, this.textBox21);
            this.table3.Body.SetCellContent(1, 0, this.textBox22);
            this.table3.Body.SetCellContent(1, 1, this.textBox23);
            this.table3.Body.SetCellContent(0, 0, this.textBox24);
            tableGroup11.Name = "tableGroup4";
            tableGroup11.ReportItem = this.textBox2;
            tableGroup12.Name = "tableGroup5";
            tableGroup12.ReportItem = this.textBox16;
            this.table3.ColumnGroups.Add(tableGroup11);
            this.table3.ColumnGroups.Add(tableGroup12);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox24,
            this.textBox17,
            this.textBox22,
            this.textBox23,
            this.textBox19,
            this.textBox21,
            this.textBox2,
            this.textBox16});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(3.7000000476837158D));
            this.table3.Name = "table3";
            tableGroup14.Name = "group7";
            tableGroup15.Name = "group9";
            tableGroup16.Name = "group8";
            tableGroup13.ChildGroups.Add(tableGroup14);
            tableGroup13.ChildGroups.Add(tableGroup15);
            tableGroup13.ChildGroups.Add(tableGroup16);
            tableGroup13.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup13.Name = "detailTableGroup2";
            this.table3.RowGroups.Add(tableGroup13);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.949583053588867D), Telerik.Reporting.Drawing.Unit.Cm(2.21708345413208D));
            this.table3.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.table3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table3.Style.Font.Name = "Arial";
            this.table3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8954133987426758D), Telerik.Reporting.Drawing.Unit.Cm(0.60854178667068481D));
            this.textBox17.Value = "=Fields.InstallAddress";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.054170608520508D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox19.StyleName = "";
            this.textBox19.Value = "=Fields.PostalCity + \", \" + Fields.PostalState + \", \" + Fields.PostalPostCode";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8954133987426758D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox21.StyleName = "";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.054170608520508D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox22.StyleName = "";
            this.textBox22.Value = "=Fields.PostalAddress";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.8954133987426758D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox23.StyleName = "";
            this.textBox23.Value = "=Fields.InstallCity + \", \" + Fields.InstallState + \", \" + Fields.InstallPostCode";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.054170608520508D), Telerik.Reporting.Drawing.Unit.Cm(0.60854178667068481D));
            this.textBox24.Value = "=Fields.Customer";
            // 
            // htmlTextBox2
            // 
            this.htmlTextBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Inch(4.2125983238220215D));
            this.htmlTextBox2.Name = "htmlTextBox2";
            this.htmlTextBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Inch(0.19999980926513672D));
            this.htmlTextBox2.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.htmlTextBox2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox2.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox2.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.htmlTextBox2.Style.Font.Name = "Arial";
            this.htmlTextBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.htmlTextBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox2.Value = resources.GetString("htmlTextBox2.Value");
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9897918701171875D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.8100006580352783D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.3704156875610352D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0318727493286133D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.7672920227050781D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.9741697311401367D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0027085542678833D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60854178667068481D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table2.Body.SetCellContent(0, 1, this.textBox46);
            this.table2.Body.SetCellContent(0, 3, this.textBox48);
            this.table2.Body.SetCellContent(0, 5, this.textBox50);
            this.table2.Body.SetCellContent(0, 0, this.textBox18);
            this.table2.Body.SetCellContent(0, 2, this.textBox30);
            this.table2.Body.SetCellContent(0, 4, this.textBox35);
            this.table2.Body.SetCellContent(1, 0, this.textBox15, 1, 2);
            this.table2.Body.SetCellContent(2, 0, this.textBox57, 1, 2);
            this.table2.Body.SetCellContent(1, 2, this.textBox40, 1, 2);
            this.table2.Body.SetCellContent(2, 2, this.textBox59, 1, 2);
            this.table2.Body.SetCellContent(1, 4, this.textBox36, 1, 2);
            this.table2.Body.SetCellContent(2, 4, this.textBox61, 1, 2);
            tableGroup18.Name = "group19";
            tableGroup19.Name = "tableGroup6";
            tableGroup20.Name = "group14";
            tableGroup21.Name = "group13";
            tableGroup22.Name = "group6";
            tableGroup23.Name = "tableGroup7";
            tableGroup17.ChildGroups.Add(tableGroup18);
            tableGroup17.ChildGroups.Add(tableGroup19);
            tableGroup17.ChildGroups.Add(tableGroup20);
            tableGroup17.ChildGroups.Add(tableGroup21);
            tableGroup17.ChildGroups.Add(tableGroup22);
            tableGroup17.ChildGroups.Add(tableGroup23);
            tableGroup17.Name = "tableGroup3";
            tableGroup17.ReportItem = this.textBox8;
            this.table2.ColumnGroups.Add(tableGroup17);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox18,
            this.textBox46,
            this.textBox30,
            this.textBox48,
            this.textBox35,
            this.textBox50,
            this.textBox15,
            this.textBox40,
            this.textBox36,
            this.textBox57,
            this.textBox59,
            this.textBox61,
            this.textBox8});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(11.799999237060547D));
            this.table2.Name = "table2";
            tableGroup25.Name = "group16";
            tableGroup26.Name = "group15";
            tableGroup27.Name = "group18";
            tableGroup24.ChildGroups.Add(tableGroup25);
            tableGroup24.ChildGroups.Add(tableGroup26);
            tableGroup24.ChildGroups.Add(tableGroup27);
            tableGroup24.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup24.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup24);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.94354248046875D), Telerik.Reporting.Drawing.Unit.Cm(2.7197921276092529D));
            this.table2.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.table2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table2.Style.Font.Name = "Arial";
            this.table2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8100018501281738D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox46.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox46.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox46.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox46.StyleName = "";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0318737030029297D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox48.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox48.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox48.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox48.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox48.StyleName = "";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9741692543029785D), Telerik.Reporting.Drawing.Unit.Cm(1.0027085542678833D));
            this.textBox50.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox50.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox50.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox50.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox50.StyleName = "";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9897937774658203D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox18.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox18.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox18.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox18.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox18.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox18.Style.Color = System.Drawing.Color.Black;
            this.textBox18.Style.Font.Name = "Arial";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox18.Value = "EFT / Bank\r\nTransfer";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3704161643981934D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox30.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox30.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox30.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox30.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox30.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox30.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox30.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox30.Style.Color = System.Drawing.Color.Black;
            this.textBox30.Style.Font.Name = "Arial";
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox30.StyleName = "";
            this.textBox30.Value = "Credit Card";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7672913074493408D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox35.Style.BackgroundColor = System.Drawing.Color.LightGray;
            this.textBox35.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox35.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox35.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox35.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox35.Style.Color = System.Drawing.Color.Black;
            this.textBox35.Style.Font.Name = "Arial";
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox35.Value = "Cheque / Money Order";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.7997961044311523D), Telerik.Reporting.Drawing.Unit.Cm(0.60854166746139526D));
            this.textBox15.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox15.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox15.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox15.Value = "A/c Name: Arise Solar";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.7997961044311523D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox57.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox57.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox57.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox57.StyleName = "";
            this.textBox57.Value = "BSB: 034037 A/c No. 250324";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.402289867401123D), Telerik.Reporting.Drawing.Unit.Cm(0.60854184627532959D));
            this.textBox40.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox40.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox40.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox40.StyleName = "";
            this.textBox40.Value = "(Master / Visa cards)";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.402289867401123D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox59.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox59.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox59.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox59.StyleName = "";
            this.textBox59.Value = "with 1.5% surcharge";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.7414612770080566D), Telerik.Reporting.Drawing.Unit.Cm(0.60854178667068481D));
            this.textBox36.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox36.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox36.StyleName = "";
            this.textBox36.Value = "Cheque payable to \"Arise Solar\" and mail it to";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.7414612770080566D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox61.StyleName = "";
            this.textBox61.Value = "PO BOX 570, Archerfield QLD 4108.";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(15.19999885559082D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.989999771118164D), Telerik.Reporting.Drawing.Unit.Cm(1.0999990701675415D));
            this.textBox6.Style.BorderColor.Default = System.Drawing.Color.LightGray;
            this.textBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Arial";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "Customer must pay the Balance Due amount on the day of the installation. A $20 la" +
    "te payment each day/collection fee, plus interest, will be applicable if custome" +
    "r fails to pay the Balance Due on time.";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.50799989700317383D), Telerik.Reporting.Drawing.Unit.Cm(0.25399994850158691D));
            this.pictureBox2.MimeType = "image/png";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(42.596942901611328D), Telerik.Reporting.Drawing.Unit.Mm(18.837335586547852D));
            this.pictureBox2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox2.Style.Font.Name = "Open Sans";
            this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
            // 
            // performamtceinvoice
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtdate});
            this.Name = "performamtceinvoice";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(20.0148983001709D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection txtdate;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox textBox127;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox lblinvoiceno;
        private Telerik.Reporting.TextBox lblSalesInvDate;
        private Telerik.Reporting.TextBox lblInverterDetails;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.Panel panel5;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox lblTotalQuotePrice;
        private Telerik.Reporting.TextBox lblNetCost;
        private Telerik.Reporting.TextBox lblDepositPaid;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox lblBalanceDue;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox lblCommissionGST;
        private Telerik.Reporting.HtmlTextBox htmlTextBox2;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.PictureBox pictureBox2;
    }
}