using System;

namespace EurosolarReporting
{
    partial class Npicklist
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Npicklist));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.lblPanDesc = new Telerik.Reporting.TextBox();
            this.pictureBox3 = new Telerik.Reporting.PictureBox();
            this.table4 = new Telerik.Reporting.Table();
            this.lblqty1 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.pictureBox4 = new Telerik.Reporting.PictureBox();
            this.table5 = new Telerik.Reporting.Table();
            this.lblqty2 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.152617335319519D), Telerik.Reporting.Drawing.Unit.Inch(0.34470838308334351D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Calibri";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox1.Value = "Installer Date";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458379626274109D), Telerik.Reporting.Drawing.Unit.Inch(0.34470838308334351D));
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.Font.Name = "Calibri";
            this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox50.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox50.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox50.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox50.StyleName = "";
            this.textBox50.Value = ":";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4962735176086426D), Telerik.Reporting.Drawing.Unit.Inch(0.34470838308334351D));
            this.textBox3.Style.Font.Bold = false;
            this.textBox3.Style.Font.Name = "Calibri";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox3.Value = "";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0128620862960815D), Telerik.Reporting.Drawing.Unit.Inch(0.34470838308334351D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.Font.Name = "Calibri";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox21.StyleName = "";
            this.textBox21.Value = "Installer Name";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458373665809631D), Telerik.Reporting.Drawing.Unit.Inch(0.34470838308334351D));
            this.textBox55.Style.Font.Bold = true;
            this.textBox55.Style.Font.Name = "Calibri";
            this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox55.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox55.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox55.StyleName = "";
            this.textBox55.Value = ":";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4242726564407349D), Telerik.Reporting.Drawing.Unit.Inch(0.34470838308334351D));
            this.textBox31.Style.Font.Bold = false;
            this.textBox31.Style.Font.Name = "Calibri";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox31.StyleName = "";
            this.textBox31.Value = "=Fields.InstallerName";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3632138967514038D), Telerik.Reporting.Drawing.Unit.Inch(0.34470838308334351D));
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Calibri";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox16.StyleName = "";
            this.textBox16.Value = "Installer Mobile:";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4217616319656372D), Telerik.Reporting.Drawing.Unit.Inch(0.34470838308334351D));
            this.textBox5.Style.Font.Bold = false;
            this.textBox5.Style.Font.Name = "Calibri";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox5.StyleName = "";
            this.textBox5.Value = "=Fields.InstallerMobile";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.73785006999969482D), Telerik.Reporting.Drawing.Unit.Inch(0.43185949325561523D));
            this.textBox26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox26.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox26.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox26.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox26.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Name = "Calibri";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.Value = "Qty";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8258787393569946D), Telerik.Reporting.Drawing.Unit.Inch(0.43185949325561523D));
            this.textBox32.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox32.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Font.Name = "Calibri";
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox32.Value = "Item";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6236222982406616D), Telerik.Reporting.Drawing.Unit.Inch(0.43185952305793762D));
            this.textBox38.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox38.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox38.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox38.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox38.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Font.Name = "Calibri";
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            this.textBox38.Value = "Model";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0100903511047363D), Telerik.Reporting.Drawing.Unit.Inch(0.43185949325561523D));
            this.textBox36.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox36.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.Font.Name = "Calibri";
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.StyleName = "";
            this.textBox36.Value = "Description";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0331634283065796D), Telerik.Reporting.Drawing.Unit.Inch(0.43185952305793762D));
            this.textBox34.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox34.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.Font.Name = "Calibri";
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.Value = "Done";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.table1,
            this.textBox12,
            this.table2,
            this.table4,
            this.table5,
            this.textBox60,
            this.textBox33,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox47,
            this.textBox45,
            this.textBox46,
            this.textBox48,
            this.textBox61,
            this.textBox62,
            this.textBox63});
            this.detail.Name = "detail";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.pictureBox1.Style.Visible = true;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.152617335319519D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.26458379626274109D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4962735176086426D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0128620862960815D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.26458373665809631D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4242726564407349D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.3632138967514038D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.4217616319656372D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.37595838308334351D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.38159725069999695D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.39201390743255615D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.37118056416511536D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox2);
            this.table1.Body.SetCellContent(0, 2, this.textBox4);
            this.table1.Body.SetCellContent(3, 0, this.textBox7);
            this.table1.Body.SetCellContent(3, 2, this.textBox8);
            this.table1.Body.SetCellContent(2, 0, this.textBox10);
            this.table1.Body.SetCellContent(2, 2, this.textBox11);
            this.table1.Body.SetCellContent(1, 0, this.textBox13);
            this.table1.Body.SetCellContent(1, 2, this.textBox14);
            this.table1.Body.SetCellContent(0, 6, this.textBox17);
            this.table1.Body.SetCellContent(1, 6, this.textBox18);
            this.table1.Body.SetCellContent(3, 6, this.textBox20);
            this.table1.Body.SetCellContent(0, 3, this.textBox22);
            this.table1.Body.SetCellContent(1, 3, this.textBox23);
            this.table1.Body.SetCellContent(2, 3, this.textBox24);
            this.table1.Body.SetCellContent(3, 3, this.textBox25);
            this.table1.Body.SetCellContent(0, 5, this.textBox27);
            this.table1.Body.SetCellContent(1, 5, this.textBox28);
            this.table1.Body.SetCellContent(2, 5, this.textBox29);
            this.table1.Body.SetCellContent(3, 5, this.textBox30);
            this.table1.Body.SetCellContent(0, 7, this.textBox6);
            this.table1.Body.SetCellContent(1, 7, this.textBox9);
            this.table1.Body.SetCellContent(3, 7, this.textBox15);
            this.table1.Body.SetCellContent(2, 6, this.textBox19);
            this.table1.Body.SetCellContent(2, 7, this.textBox70);
            this.table1.Body.SetCellContent(0, 1, this.textBox51);
            this.table1.Body.SetCellContent(1, 1, this.textBox52);
            this.table1.Body.SetCellContent(2, 1, this.textBox53);
            this.table1.Body.SetCellContent(3, 1, this.textBox54);
            this.table1.Body.SetCellContent(0, 4, this.textBox56);
            this.table1.Body.SetCellContent(1, 4, this.textBox57);
            this.table1.Body.SetCellContent(2, 4, this.textBox58);
            this.table1.Body.SetCellContent(3, 4, this.textBox59);
            tableGroup1.Name = "tableGroup";
            tableGroup1.ReportItem = this.textBox1;
            tableGroup2.Name = "group10";
            tableGroup2.ReportItem = this.textBox50;
            tableGroup3.Name = "tableGroup1";
            tableGroup3.ReportItem = this.textBox3;
            tableGroup4.Name = "group4";
            tableGroup4.ReportItem = this.textBox21;
            tableGroup5.Name = "group11";
            tableGroup5.ReportItem = this.textBox55;
            tableGroup6.Name = "group5";
            tableGroup6.ReportItem = this.textBox31;
            tableGroup7.Name = "group3";
            tableGroup7.ReportItem = this.textBox16;
            tableGroup8.Name = "group6";
            tableGroup8.ReportItem = this.textBox5;
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup5);
            this.table1.ColumnGroups.Add(tableGroup6);
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2,
            this.textBox51,
            this.textBox4,
            this.textBox22,
            this.textBox56,
            this.textBox27,
            this.textBox17,
            this.textBox6,
            this.textBox13,
            this.textBox52,
            this.textBox14,
            this.textBox23,
            this.textBox57,
            this.textBox28,
            this.textBox18,
            this.textBox9,
            this.textBox10,
            this.textBox53,
            this.textBox11,
            this.textBox24,
            this.textBox58,
            this.textBox29,
            this.textBox19,
            this.textBox70,
            this.textBox7,
            this.textBox54,
            this.textBox8,
            this.textBox25,
            this.textBox59,
            this.textBox30,
            this.textBox20,
            this.textBox15,
            this.textBox1,
            this.textBox50,
            this.textBox3,
            this.textBox21,
            this.textBox55,
            this.textBox31,
            this.textBox16,
            this.textBox5});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.11811026185750961D), Telerik.Reporting.Drawing.Unit.Inch(2.5984251499176025D));
            this.table1.Name = "table1";
            tableGroup9.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup9.Name = "detailTableGroup";
            tableGroup10.Name = "group2";
            tableGroup11.Name = "group1";
            tableGroup12.Name = "group";
            this.table1.RowGroups.Add(tableGroup9);
            this.table1.RowGroups.Add(tableGroup10);
            this.table1.RowGroups.Add(tableGroup11);
            this.table1.RowGroups.Add(tableGroup12);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.1348896026611328D), Telerik.Reporting.Drawing.Unit.Inch(1.9210139513015747D));
            this.table1.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.table1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table1.Style.Color = System.Drawing.Color.White;
            this.table1.Style.Font.Bold = false;
            this.table1.Style.Font.Name = "Calibri";
            this.table1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.table1.Style.LineColor = System.Drawing.Color.Transparent;
            this.table1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.table1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.table1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.table1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.152617335319519D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Calibri";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox2.Value = "Customer Name\t";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4962732791900635D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox4.Style.Font.Name = "Calibri";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox4.Value = "=Fields.Customer";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1526172161102295D), Telerik.Reporting.Drawing.Unit.Inch(0.37118053436279297D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Calibri";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox7.StyleName = "";
            this.textBox7.Value = "Store";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.496273398399353D), Telerik.Reporting.Drawing.Unit.Inch(0.37118053436279297D));
            this.textBox8.Style.Font.Name = "Calibri";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox8.StyleName = "";
            this.textBox8.Value = "=Fields.StoreName";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.1526172161102295D), Telerik.Reporting.Drawing.Unit.Inch(0.39201387763023376D));
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Calibri";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox10.StyleName = "";
            this.textBox10.Value = "Manual Quote";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.496273398399353D), Telerik.Reporting.Drawing.Unit.Inch(0.39201387763023376D));
            this.textBox11.Style.Font.Name = "Calibri";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox11.StyleName = "";
            this.textBox11.Value = "=Fields.ManualQuoteNumber";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.152617335319519D), Telerik.Reporting.Drawing.Unit.Inch(0.38159719109535217D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Calibri";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox13.StyleName = "";
            this.textBox13.Value = "House Type";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.496273398399353D), Telerik.Reporting.Drawing.Unit.Inch(0.38159719109535217D));
            this.textBox14.Style.Font.Name = "Calibri";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox14.StyleName = "";
            this.textBox14.Value = "=Fields.HouseType";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3632137775421143D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Calibri";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox17.StyleName = "";
            this.textBox17.Value = "Mob:";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3632138967514038D), Telerik.Reporting.Drawing.Unit.Inch(0.38159719109535217D));
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Name = "Calibri";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox18.StyleName = "";
            this.textBox18.Value = "Angle:";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3632138967514038D), Telerik.Reporting.Drawing.Unit.Inch(0.37118053436279297D));
            this.textBox20.Style.Font.Name = "Calibri";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox20.StyleName = "";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.012861967086792D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.Font.Name = "Calibri";
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox22.StyleName = "";
            this.textBox22.Value = "Phone";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.012861967086792D), Telerik.Reporting.Drawing.Unit.Inch(0.38159719109535217D));
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Name = "Calibri";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox23.StyleName = "";
            this.textBox23.Value = "Roof Type";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.012861967086792D), Telerik.Reporting.Drawing.Unit.Inch(0.39201387763023376D));
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Font.Name = "Calibri";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox24.StyleName = "";
            this.textBox24.Value = "Project No.";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.012861967086792D), Telerik.Reporting.Drawing.Unit.Inch(0.37118053436279297D));
            this.textBox25.Style.Font.Name = "Calibri";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox25.StyleName = "";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4242727756500244D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox27.Style.Font.Name = "Calibri";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox27.StyleName = "";
            this.textBox27.Value = "=Fields.CustPhone";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.424272894859314D), Telerik.Reporting.Drawing.Unit.Inch(0.38159719109535217D));
            this.textBox28.Style.Font.Name = "Calibri";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox28.StyleName = "";
            this.textBox28.Value = "=Fields.RoofType";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.424272894859314D), Telerik.Reporting.Drawing.Unit.Inch(0.39201387763023376D));
            this.textBox29.Style.Font.Name = "Calibri";
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox29.StyleName = "";
            this.textBox29.Value = "=Fields.ProjectNumber";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.424272894859314D), Telerik.Reporting.Drawing.Unit.Inch(0.37118053436279297D));
            this.textBox30.Style.Font.Name = "Calibri";
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox30.StyleName = "";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4217615127563477D), Telerik.Reporting.Drawing.Unit.Inch(0.37595838308334351D));
            this.textBox6.Style.Font.Name = "Calibri";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox6.StyleName = "";
            this.textBox6.Value = "=Fields.ContMobile";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4217615127563477D), Telerik.Reporting.Drawing.Unit.Inch(0.38159725069999695D));
            this.textBox9.Style.Font.Name = "Calibri";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox9.StyleName = "";
            this.textBox9.Value = "=Fields.RoofAngle";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4217615127563477D), Telerik.Reporting.Drawing.Unit.Inch(0.37118056416511536D));
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox15.StyleName = "";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3632138967514038D), Telerik.Reporting.Drawing.Unit.Inch(0.39201387763023376D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Calibri";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox19.StyleName = "";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.4217615127563477D), Telerik.Reporting.Drawing.Unit.Inch(0.39201390743255615D));
            this.textBox70.Style.Font.Bold = true;
            this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox70.StyleName = "";
            this.textBox70.Value = "";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458373665809631D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.Style.Font.Name = "Calibri";
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox51.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox51.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox51.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox51.StyleName = "";
            this.textBox51.Value = ":";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458367705345154D), Telerik.Reporting.Drawing.Unit.Inch(0.38159719109535217D));
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.Font.Name = "Calibri";
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox52.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox52.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox52.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox52.StyleName = "";
            this.textBox52.Value = ":";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458373665809631D), Telerik.Reporting.Drawing.Unit.Inch(0.39201387763023376D));
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.Font.Name = "Calibri";
            this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox53.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox53.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox53.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox53.StyleName = "";
            this.textBox53.Value = ":";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458373665809631D), Telerik.Reporting.Drawing.Unit.Inch(0.37118053436279297D));
            this.textBox54.Style.Font.Bold = true;
            this.textBox54.Style.Font.Name = "Calibri";
            this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox54.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0.10000000149011612D);
            this.textBox54.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox54.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox54.StyleName = "";
            this.textBox54.Value = ":";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458364725112915D), Telerik.Reporting.Drawing.Unit.Inch(0.37595835328102112D));
            this.textBox56.Style.Font.Bold = true;
            this.textBox56.Style.Font.Name = "Calibri";
            this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox56.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox56.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox56.StyleName = "";
            this.textBox56.Value = ":";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458361744880676D), Telerik.Reporting.Drawing.Unit.Inch(0.38159719109535217D));
            this.textBox57.Style.Font.Bold = true;
            this.textBox57.Style.Font.Name = "Calibri";
            this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox57.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox57.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox57.StyleName = "";
            this.textBox57.Value = ":";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458364725112915D), Telerik.Reporting.Drawing.Unit.Inch(0.39201387763023376D));
            this.textBox58.Style.Font.Bold = true;
            this.textBox58.Style.Font.Name = "Calibri";
            this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox58.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox58.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox58.StyleName = "";
            this.textBox58.Value = ":";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458367705345154D), Telerik.Reporting.Drawing.Unit.Inch(0.37118053436279297D));
            this.textBox59.Style.Font.Bold = true;
            this.textBox59.Style.Font.Name = "Calibri";
            this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox59.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox59.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox59.StyleName = "";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.30000004172325134D), Telerik.Reporting.Drawing.Unit.Cm(11.926457405090332D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox12.Style.Color = System.Drawing.Color.Black;
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Calibri";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox12.Value = "SITE ADDRESS :";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.7378503680229187D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8258785009384155D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.6236222982406616D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.0100905895233154D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0331634283065796D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox35);
            this.table2.Body.SetCellContent(0, 1, this.textBox40);
            this.table2.Body.SetCellContent(0, 2, this.textBox49);
            this.table2.Body.SetCellContent(0, 3, this.lblPanDesc);
            this.table2.Body.SetCellContent(0, 4, this.pictureBox3);
            tableGroup13.Name = "tableGroup2";
            tableGroup13.ReportItem = this.textBox26;
            tableGroup14.Name = "tableGroup3";
            tableGroup14.ReportItem = this.textBox32;
            tableGroup15.Name = "group8";
            tableGroup15.ReportItem = this.textBox38;
            tableGroup16.Name = "group7";
            tableGroup16.ReportItem = this.textBox36;
            tableGroup17.Name = "tableGroup4";
            tableGroup17.ReportItem = this.textBox34;
            this.table2.ColumnGroups.Add(tableGroup13);
            this.table2.ColumnGroups.Add(tableGroup14);
            this.table2.ColumnGroups.Add(tableGroup15);
            this.table2.ColumnGroups.Add(tableGroup16);
            this.table2.ColumnGroups.Add(tableGroup17);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox35,
            this.textBox40,
            this.textBox49,
            this.lblPanDesc,
            this.pictureBox3,
            this.textBox26,
            this.textBox32,
            this.textBox38,
            this.textBox36,
            this.textBox34});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.029325485229492188D), Telerik.Reporting.Drawing.Unit.Inch(5.7086615562438965D));
            this.table2.Name = "table2";
            tableGroup19.Name = "group9";
            tableGroup18.ChildGroups.Add(tableGroup19);
            tableGroup18.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup18.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup18);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.2306051254272461D), Telerik.Reporting.Drawing.Unit.Inch(0.90551191568374634D));
            this.table2.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.table2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.table2.Style.Color = System.Drawing.Color.White;
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.73785006999969482D), Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D));
            this.textBox35.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox35.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox35.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox35.Style.Font.Name = "Calibri";
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox35.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.Value = "=Fields.NumberPanels";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8258787393569946D), Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D));
            this.textBox40.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox40.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox40.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox40.Style.Font.Name = "Calibri";
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox40.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox40.Value = "=Fields.PanelBrandName";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6236222982406616D), Telerik.Reporting.Drawing.Unit.Inch(0.47365239262580872D));
            this.textBox49.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox49.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox49.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox49.Style.Font.Name = "Calibri";
            this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox49.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox49.Value = "=Fields.PanelModel";
            // 
            // lblPanDesc
            // 
            this.lblPanDesc.Name = "lblPanDesc";
            this.lblPanDesc.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.0100903511047363D), Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D));
            this.lblPanDesc.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.lblPanDesc.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.lblPanDesc.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.lblPanDesc.Style.Font.Name = "Calibri";
            this.lblPanDesc.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.lblPanDesc.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.lblPanDesc.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.lblPanDesc.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblPanDesc.Value = "=Fields.Paneldesc";
            // 
            // pictureBox3
            // 
            this.pictureBox3.MimeType = "image/png";
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0331634283065796D), Telerik.Reporting.Drawing.Unit.Inch(0.4736524224281311D));
            this.pictureBox3.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.pictureBox3.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.pictureBox3.Style.Font.Name = "Calibri";
            this.pictureBox3.Style.LineColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.pictureBox3.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D);
            this.pictureBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox3.StyleName = "";
            this.pictureBox3.Value = ((object)(resources.GetObject("pictureBox3.Value")));
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.71738898754119873D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8437255620956421D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.6268075704574585D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.9986953735351562D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0231516361236572D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D)));
            this.table4.Body.SetCellContent(0, 0, this.lblqty1);
            this.table4.Body.SetCellContent(0, 1, this.textBox39);
            this.table4.Body.SetCellContent(0, 2, this.textBox71);
            this.table4.Body.SetCellContent(0, 3, this.textBox75);
            this.table4.Body.SetCellContent(0, 4, this.pictureBox4);
            tableGroup20.Name = "tableGroup8";
            tableGroup21.Name = "tableGroup9";
            tableGroup22.Name = "group15";
            tableGroup23.Name = "group16";
            tableGroup24.Name = "tableGroup10";
            this.table4.ColumnGroups.Add(tableGroup20);
            this.table4.ColumnGroups.Add(tableGroup21);
            this.table4.ColumnGroups.Add(tableGroup22);
            this.table4.ColumnGroups.Add(tableGroup23);
            this.table4.ColumnGroups.Add(tableGroup24);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lblqty1,
            this.textBox39,
            this.textBox71,
            this.textBox75,
            this.pictureBox4});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.039370059967041016D), Telerik.Reporting.Drawing.Unit.Inch(6.6142520904541016D));
            this.table4.Name = "table4";
            tableGroup25.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup25.Name = "detailTableGroup3";
            this.table4.RowGroups.Add(tableGroup25);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.2097692489624023D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.table4.Style.Color = System.Drawing.Color.White;
            // 
            // lblqty1
            // 
            this.lblqty1.Name = "lblqty1";
            this.lblqty1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.71738874912261963D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.lblqty1.Style.Font.Name = "Calibri";
            this.lblqty1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.lblqty1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.lblqty1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblqty1.Value = "=Fields.inverterqty";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8437256813049316D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.textBox39.Style.Font.Name = "Calibri";
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.Value = "=Fields.InverterDetailsName";
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6268069744110107D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.textBox71.Style.Font.Name = "Calibri";
            this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox71.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox71.Value = "=Fields.Invertermodel";
            // 
            // textBox75
            // 
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.9986958503723145D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.textBox75.Style.Font.Name = "Calibri";
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox75.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox75.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox75.Value = "=Fields.Inverterdesc";
            // 
            // pictureBox4
            // 
            this.pictureBox4.MimeType = "image/png";
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0231518745422363D), Telerik.Reporting.Drawing.Unit.Inch(0.39362207055091858D));
            this.pictureBox4.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.pictureBox4.Style.Font.Name = "Calibri";
            this.pictureBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.pictureBox4.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D);
            this.pictureBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox4.StyleName = "";
            this.pictureBox4.Value = ((object)(resources.GetObject("pictureBox4.Value")));
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.72739428281784058D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.8235548734664917D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.6248607635498047D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.0102112293243408D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.0137065649032593D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D)));
            this.table5.Body.SetCellContent(0, 0, this.lblqty2);
            this.table5.Body.SetCellContent(0, 1, this.textBox73);
            this.table5.Body.SetCellContent(0, 2, this.textBox37);
            this.table5.Body.SetCellContent(0, 3, this.textBox69);
            this.table5.Body.SetCellContent(0, 4, this.pictureBox2);
            this.table5.Body.SetCellContent(1, 0, this.textBox64);
            this.table5.Body.SetCellContent(1, 1, this.textBox65);
            this.table5.Body.SetCellContent(1, 2, this.textBox66);
            this.table5.Body.SetCellContent(1, 3, this.textBox67);
            this.table5.Body.SetCellContent(1, 4, this.textBox68);
            this.table5.Body.SetCellContent(2, 0, this.textBox72);
            this.table5.Body.SetCellContent(2, 1, this.textBox74);
            this.table5.Body.SetCellContent(2, 2, this.textBox76);
            this.table5.Body.SetCellContent(2, 3, this.textBox77);
            this.table5.Body.SetCellContent(2, 4, this.textBox78);
            tableGroup26.Name = "group18";
            tableGroup27.Name = "group17";
            tableGroup28.Name = "tableGroup11";
            tableGroup29.Name = "tableGroup12";
            tableGroup30.Name = "tableGroup13";
            this.table5.ColumnGroups.Add(tableGroup26);
            this.table5.ColumnGroups.Add(tableGroup27);
            this.table5.ColumnGroups.Add(tableGroup28);
            this.table5.ColumnGroups.Add(tableGroup29);
            this.table5.ColumnGroups.Add(tableGroup30);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.lblqty2,
            this.textBox73,
            this.textBox37,
            this.textBox69,
            this.pictureBox2,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox68,
            this.textBox72,
            this.textBox74,
            this.textBox76,
            this.textBox77,
            this.textBox78});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.039370059967041016D), Telerik.Reporting.Drawing.Unit.Inch(7.0078740119934082D));
            this.table5.Name = "table5";
            tableGroup31.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup31.Name = "detailTableGroup4";
            tableGroup32.Name = "group12";
            tableGroup33.Name = "group13";
            this.table5.RowGroups.Add(tableGroup31);
            this.table5.RowGroups.Add(tableGroup32);
            this.table5.RowGroups.Add(tableGroup33);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.1997280120849609D), Telerik.Reporting.Drawing.Unit.Inch(1.1763451099395752D));
            this.table5.Style.Color = System.Drawing.Color.White;
            // 
            // lblqty2
            // 
            this.lblqty2.Name = "lblqty2";
            this.lblqty2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72739404439926147D), Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D));
            this.lblqty2.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.lblqty2.Style.Font.Name = "Calibri";
            this.lblqty2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.lblqty2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.lblqty2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblqty2.Value = "=Fields.inverterqty2";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.823554277420044D), Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D));
            this.textBox73.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox73.Style.Font.Name = "Calibri";
            this.textBox73.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox73.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox73.Value = "=Fields.SecondInverterDetails";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6248600482940674D), Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D));
            this.textBox37.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox37.Style.Font.Name = "Calibri";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.Value = "=Fields.SecondInvertermodel";
            // 
            // textBox69
            // 
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.01021146774292D), Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D));
            this.textBox69.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox69.Style.Font.Name = "Calibri";
            this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox69.Value = "=Fields.SecondInverterDetailsdesc";
            // 
            // pictureBox2
            // 
            this.pictureBox2.MimeType = "image/png";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0137063264846802D), Telerik.Reporting.Drawing.Unit.Inch(0.51181095838546753D));
            this.pictureBox2.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Style.Font.Name = "Calibri";
            this.pictureBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.pictureBox2.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D);
            this.pictureBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox2.StyleName = "";
            this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
            // 
            // textBox64
            // 
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.727394163608551D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox64.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox64.Style.Font.Name = "Calibri";
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox64.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.StyleName = "";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8235546350479126D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox65.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox65.Style.Font.Name = "Calibri";
            this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.StyleName = "";
            this.textBox65.Value = "Rail";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6248606443405151D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox66.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox66.Style.Font.Name = "Calibri";
            this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox66.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox66.StyleName = "";
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.01021146774292D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox67.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox67.Style.Font.Name = "Calibri";
            this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox67.StyleName = "";
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0137064456939697D), Telerik.Reporting.Drawing.Unit.Cm(1.0027084350585938D));
            this.textBox68.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox68.Style.Font.Name = "Calibri";
            this.textBox68.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.textBox68.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D);
            this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox68.StyleName = "";
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.72739428281784058D), Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D));
            this.textBox72.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox72.Style.Font.Name = "Calibri";
            this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox72.StyleName = "";
            // 
            // textBox74
            // 
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8235548734664917D), Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D));
            this.textBox74.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox74.Style.Font.Name = "Calibri";
            this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox74.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox74.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox74.StyleName = "";
            this.textBox74.Value = "Mounting";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.6248607635498047D), Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D));
            this.textBox76.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox76.Style.Font.Name = "Calibri";
            this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox76.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox76.StyleName = "";
            // 
            // textBox77
            // 
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.01021146774292D), Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D));
            this.textBox77.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox77.Style.Font.Name = "Calibri";
            this.textBox77.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox77.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox77.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox77.StyleName = "";
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0137065649032593D), Telerik.Reporting.Drawing.Unit.Cm(0.68520838022232056D));
            this.textBox78.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox78.Style.Font.Name = "Calibri";
            this.textBox78.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(1D);
            this.textBox78.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.40000000596046448D);
            this.textBox78.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox78.StyleName = "";
            // 
            // textBox60
            // 
            this.textBox60.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2125983238220215D), Telerik.Reporting.Drawing.Unit.Inch(10.603428840637207D));
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2653465270996094D), Telerik.Reporting.Drawing.Unit.Inch(0.20000012218952179D));
            this.textBox60.Style.Font.Bold = true;
            this.textBox60.Style.Font.Name = "Calibri";
            this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox60.Value = "Installer Notes:";
            // 
            // textBox33
            // 
            this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.35433068871498108D), Telerik.Reporting.Drawing.Unit.Inch(8.4645671844482422D));
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.3385038375854492D), Telerik.Reporting.Drawing.Unit.Inch(0.19684982299804688D));
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Name = "Calibri";
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox33.Value = "Installer Notes:";
            // 
            // textBox41
            // 
            this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(22.69999885559082D));
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.0576328039169312D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.Style.Font.Name = "Calibri";
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox41.Value = "PICKED BY:";
            // 
            // textBox42
            // 
            this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(23.899999618530273D));
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.59055131673812866D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.Font.Name = "Calibri";
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox42.Value = "DATE:";
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(25.200000762939453D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.550000011920929D), Telerik.Reporting.Drawing.Unit.Inch(0.20000000298023224D));
            this.textBox43.Style.Font.Bold = true;
            this.textBox43.Style.Font.Name = "Calibri";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox43.Value = "SIGN:";
            // 
            // textBox44
            // 
            this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(21.5D));
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox44.Style.Font.Name = "Calibri";
            this.textBox44.Value = "_________________________";
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5D), Telerik.Reporting.Drawing.Unit.Cm(25.200000762939453D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox47.Style.Font.Name = "Calibri";
            this.textBox47.Value = "_________________________";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.8000001907348633D), Telerik.Reporting.Drawing.Unit.Cm(22.674304962158203D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox45.Style.Font.Name = "Calibri";
            this.textBox45.Value = "_________________________";
            // 
            // textBox46
            // 
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.5999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(23.899999618530273D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox46.Style.Font.Name = "Calibri";
            this.textBox46.Value = "_________________________";
            // 
            // textBox48
            // 
            this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.72645837068557739D), Telerik.Reporting.Drawing.Unit.Cm(27.8856258392334D));
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.413614273071289D), Telerik.Reporting.Drawing.Unit.Cm(1.1000006198883057D));
            this.textBox48.Style.Font.Name = "Calibri";
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox48.Value = "ABN 32168697907 | Phone : 1300 274 737\r\nAddress : Level 6/140 Creek Street, Birsb" +
    "ane QLD 4019 | www.arisesolar.com.au | info@arisesolar.com.au";
            // 
            // textBox61
            // 
            this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.2125983238220215D), Telerik.Reporting.Drawing.Unit.Inch(8.3622865676879883D));
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8410310745239258D), Telerik.Reporting.Drawing.Unit.Inch(2.149442195892334D));
            this.textBox61.Style.Font.Bold = false;
            this.textBox61.Style.Font.Name = "Calibri";
            this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox61.Value = "=Fields.InstallerNotes";
            // 
            // textBox62
            // 
            this.textBox62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.30000025033950806D), Telerik.Reporting.Drawing.Unit.Cm(12.773124694824219D));
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.75D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.Style.Font.Name = "Calibri";
            this.textBox62.Value = "";
            // 
            // textBox63
            // 
            this.textBox63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.3179166316986084D), Telerik.Reporting.Drawing.Unit.Cm(13.467290878295898D));
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.352917671203613D), Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D));
            this.textBox63.Style.Font.Bold = true;
            this.textBox63.Style.Font.Name = "Calibri";
            this.textBox63.Value = "";
            // 
            // Npicklist
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "picklist";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox lblPanDesc;
        private Telerik.Reporting.PictureBox pictureBox3;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox lblqty1;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.PictureBox pictureBox4;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox lblqty2;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox78;
    }
}