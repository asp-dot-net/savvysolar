namespace EurosolarReporting
{
    partial class Smpmt
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Smpmt));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.txtinvoiceno = new Telerik.Reporting.TextBox();
            this.txtinvoicedate = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.txtInvoTotal = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.txttotalcost = new Telerik.Reporting.TextBox();
            this.txtnetcost = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.txtlessdisc = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.txtstcrebate = new Telerik.Reporting.TextBox();
            this.txtAdd = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.panel1 = new Telerik.Reporting.Panel();
            this.txttotal = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.874D), Telerik.Reporting.Drawing.Unit.Cm(0.915D));
            this.textBox11.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.Value = "=Fields.Contact";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.554D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox10.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.Value = "=Fields.CustPhone";
            // 
            // txtinvoiceno
            // 
            this.txtinvoiceno.Name = "txtinvoiceno";
            this.txtinvoiceno.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.207D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.txtinvoiceno.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtinvoiceno.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // txtinvoicedate
            // 
            this.txtinvoicedate.Name = "txtinvoicedate";
            this.txtinvoicedate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.339D), Telerik.Reporting.Drawing.Unit.Cm(0.952D));
            this.txtinvoicedate.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtinvoicedate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.318D), Telerik.Reporting.Drawing.Unit.Cm(0.394D));
            this.textBox36.StyleName = "";
            // 
            // txtInvoTotal
            // 
            this.txtInvoTotal.Name = "txtInvoTotal";
            this.txtInvoTotal.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.318D), Telerik.Reporting.Drawing.Unit.Cm(0.873D));
            this.txtInvoTotal.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtInvoTotal.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.table4,
            this.table5,
            this.table1,
            this.table2,
            this.table6,
            this.txtAdd,
            this.table7,
            this.panel1});
            this.detail.Name = "detail";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0.079D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(296D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7.874D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.383D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.968D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox18);
            this.table4.Body.SetCellContent(1, 0, this.textBox43);
            tableGroup1.Name = "tableGroup4";
            tableGroup1.ReportItem = this.textBox11;
            this.table4.ColumnGroups.Add(tableGroup1);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox18,
            this.textBox43,
            this.textBox11});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.286D), Telerik.Reporting.Drawing.Unit.Cm(5.334D));
            this.table4.Name = "table4";
            tableGroup3.Name = "group12";
            tableGroup4.Name = "group6";
            tableGroup2.ChildGroups.Add(tableGroup3);
            tableGroup2.ChildGroups.Add(tableGroup4);
            tableGroup2.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup2.Name = "detailTableGroup1";
            this.table4.RowGroups.Add(tableGroup2);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.874D), Telerik.Reporting.Drawing.Unit.Cm(2.266D));
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.874D), Telerik.Reporting.Drawing.Unit.Cm(0.383D));
            this.textBox18.StyleName = "";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.874D), Telerik.Reporting.Drawing.Unit.Cm(0.968D));
            this.textBox43.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox43.Style.Font.Bold = false;
            this.textBox43.Style.Font.Name = "Calibri";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox43.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.StyleName = "";
            this.textBox43.Value = "=Fields.ContMobile";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(8.554D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.423D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.95D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox12);
            this.table5.Body.SetCellContent(1, 0, this.textBox20);
            tableGroup5.Name = "tableGroup7";
            tableGroup5.ReportItem = this.textBox10;
            this.table5.ColumnGroups.Add(tableGroup5);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox12,
            this.textBox20,
            this.textBox10});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.446D), Telerik.Reporting.Drawing.Unit.Cm(5.334D));
            this.table5.Name = "table5";
            tableGroup6.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup6.Name = "detailTableGroup3";
            tableGroup7.Name = "group2";
            this.table5.RowGroups.Add(tableGroup6);
            this.table5.RowGroups.Add(tableGroup7);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.554D), Telerik.Reporting.Drawing.Unit.Cm(2.273D));
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.554D), Telerik.Reporting.Drawing.Unit.Cm(0.423D));
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.554D), Telerik.Reporting.Drawing.Unit.Cm(0.95D));
            this.textBox20.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox20.Style.Font.Bold = false;
            this.textBox20.Style.Font.Name = "Calibri";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "=Fields.ContEmail";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.207D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.688D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.632D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.659D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.897D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox17);
            this.table1.Body.SetCellContent(2, 0, this.textBox24);
            this.table1.Body.SetCellContent(1, 0, this.txttotalcost);
            this.table1.Body.SetCellContent(3, 0, this.txtnetcost);
            tableGroup8.Name = "tableGroup1";
            tableGroup8.ReportItem = this.txtinvoiceno;
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox17,
            this.txttotalcost,
            this.textBox24,
            this.txtnetcost,
            this.txtinvoiceno});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.286D), Telerik.Reporting.Drawing.Unit.Cm(12.192D));
            this.table1.Name = "table1";
            tableGroup9.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup9.Name = "detailTableGroup";
            tableGroup10.Name = "group1";
            tableGroup11.Name = "group";
            tableGroup12.Name = "group3";
            this.table1.RowGroups.Add(tableGroup9);
            this.table1.RowGroups.Add(tableGroup10);
            this.table1.RowGroups.Add(tableGroup11);
            this.table1.RowGroups.Add(tableGroup12);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.207D), Telerik.Reporting.Drawing.Unit.Cm(3.511D));
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.207D), Telerik.Reporting.Drawing.Unit.Cm(0.688D));
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.207D), Telerik.Reporting.Drawing.Unit.Cm(0.659D));
            this.textBox24.StyleName = "";
            // 
            // txttotalcost
            // 
            this.txttotalcost.Name = "txttotalcost";
            this.txttotalcost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.207D), Telerik.Reporting.Drawing.Unit.Cm(0.632D));
            this.txttotalcost.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txttotalcost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txttotalcost.StyleName = "";
            // 
            // txtnetcost
            // 
            this.txtnetcost.Name = "txtnetcost";
            this.txtnetcost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.207D), Telerik.Reporting.Drawing.Unit.Cm(0.897D));
            this.txtnetcost.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtnetcost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtnetcost.StyleName = "";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.339D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.265D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.056D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox27);
            this.table2.Body.SetCellContent(1, 0, this.txtlessdisc);
            tableGroup13.Name = "tableGroup9";
            tableGroup13.ReportItem = this.txtinvoicedate;
            this.table2.ColumnGroups.Add(tableGroup13);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox27,
            this.txtlessdisc,
            this.txtinvoicedate});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.144D), Telerik.Reporting.Drawing.Unit.Cm(12.07D));
            this.table2.Name = "table2";
            tableGroup15.Name = "group4";
            tableGroup16.Name = "group5";
            tableGroup14.ChildGroups.Add(tableGroup15);
            tableGroup14.ChildGroups.Add(tableGroup16);
            tableGroup14.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup14.Name = "detailTableGroup5";
            this.table2.RowGroups.Add(tableGroup14);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.339D), Telerik.Reporting.Drawing.Unit.Cm(2.273D));
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.339D), Telerik.Reporting.Drawing.Unit.Cm(0.265D));
            // 
            // txtlessdisc
            // 
            this.txtlessdisc.Name = "txtlessdisc";
            this.txtlessdisc.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.339D), Telerik.Reporting.Drawing.Unit.Cm(1.056D));
            this.txtlessdisc.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtlessdisc.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtlessdisc.StyleName = "";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.318D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.953D)));
            this.table6.Body.SetCellContent(0, 0, this.txtstcrebate);
            tableGroup18.Name = "group8";
            tableGroup18.ReportItem = this.textBox36;
            tableGroup17.ChildGroups.Add(tableGroup18);
            tableGroup17.Name = "tableGroup11";
            tableGroup17.ReportItem = this.txtInvoTotal;
            this.table6.ColumnGroups.Add(tableGroup17);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtstcrebate,
            this.txtInvoTotal,
            this.textBox36});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.256D), Telerik.Reporting.Drawing.Unit.Cm(12.192D));
            this.table6.Name = "table6";
            tableGroup19.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup19.Name = "detailTableGroup6";
            this.table6.RowGroups.Add(tableGroup19);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.318D), Telerik.Reporting.Drawing.Unit.Cm(2.22D));
            // 
            // txtstcrebate
            // 
            this.txtstcrebate.Name = "txtstcrebate";
            this.txtstcrebate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.318D), Telerik.Reporting.Drawing.Unit.Cm(0.953D));
            this.txtstcrebate.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtstcrebate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // txtAdd
            // 
            this.txtAdd.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.81D), Telerik.Reporting.Drawing.Unit.Cm(8.89D));
            this.txtAdd.Name = "txtAdd";
            this.txtAdd.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.731D), Telerik.Reporting.Drawing.Unit.Cm(1.016D));
            this.txtAdd.Style.Color = System.Drawing.Color.White;
            this.txtAdd.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtAdd.Value = "textBox72";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.683D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.636D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.106D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.334D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.866D)));
            this.table7.Body.SetCellContent(0, 0, this.textBox42);
            this.table7.Body.SetCellContent(0, 1, this.textBox45);
            this.table7.Body.SetCellContent(0, 2, this.textBox48);
            this.table7.Body.SetCellContent(0, 3, this.textBox50);
            tableGroup20.Name = "tableGroup12";
            tableGroup21.Name = "tableGroup13";
            tableGroup22.Name = "tableGroup14";
            tableGroup23.Name = "group10";
            this.table7.ColumnGroups.Add(tableGroup20);
            this.table7.ColumnGroups.Add(tableGroup21);
            this.table7.ColumnGroups.Add(tableGroup22);
            this.table7.ColumnGroups.Add(tableGroup23);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox42,
            this.textBox45,
            this.textBox48,
            this.textBox50});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.254D), Telerik.Reporting.Drawing.Unit.Cm(18.542D));
            this.table7.Name = "table7";
            tableGroup24.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup24.Name = "detailTableGroup7";
            this.table7.RowGroups.Add(tableGroup24);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.759D), Telerik.Reporting.Drawing.Unit.Cm(0.866D));
            this.table7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.683D), Telerik.Reporting.Drawing.Unit.Cm(0.866D));
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox42.Value = "=Fields.DtPayDate";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.636D), Telerik.Reporting.Drawing.Unit.Cm(0.866D));
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox45.Value = "=Fields.InvoicePayTotal1";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.106D), Telerik.Reporting.Drawing.Unit.Cm(0.866D));
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox48.Value = "=Fields.CCSurcharge1";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.334D), Telerik.Reporting.Drawing.Unit.Cm(0.866D));
            this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.StyleName = "";
            this.textBox50.Value = "=Fields.InvoicePayMethod";
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txttotal});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(17.526D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(5.118D));
            // 
            // txttotal
            // 
            this.txttotal.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.398D), Telerik.Reporting.Drawing.Unit.Cm(4.318D));
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.556D), Telerik.Reporting.Drawing.Unit.Cm(0.508D));
            this.txttotal.Style.Color = System.Drawing.Color.White;
            this.txttotal.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.txttotal.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.txttotal.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txttotal.Value = "";
            // 
            // Smpmt
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SmTaxInvoice";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox txttotal;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox txttotalcost;
        private Telerik.Reporting.TextBox txtnetcost;
        private Telerik.Reporting.TextBox txtinvoiceno;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox txtlessdisc;
        private Telerik.Reporting.TextBox txtinvoicedate;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox txtstcrebate;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox txtInvoTotal;
        private Telerik.Reporting.TextBox txtAdd;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.Panel panel1;
    }
}