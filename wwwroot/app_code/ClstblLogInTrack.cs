using System;
using System.Data;
using System.Data.Common;

public struct SttblLogInTrack
{
    public string UserId;
    public string LogInTime;
    public string IPAddress;
    public string LogOutTime;
    public string IsSession;
}

public class ClstblLogInTrack
{
    public static SttblLogInTrack tblLogInTrack_SelectByID(String ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLogInTrack_SelectByID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblLogInTrack details = new SttblLogInTrack();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.UserId = dr["UserId"].ToString();
            details.LogInTime = dr["LogInTime"].ToString();
            details.IPAddress = dr["IPAddress"].ToString();
            details.LogOutTime = dr["LogOutTime"].ToString();
            details.IsSession = dr["IsSession"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblLogInTrack_Select(string UserId, string IPAddress)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLogInTrack_Select";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        param.Value = UserId;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IPAddress";
        param.Value = IPAddress;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblLogInTrack_Insert(String UserId, String LogInTime, String IPAddress, String LogOutTime, String IsSession)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLogInTrack_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        param.Value = UserId;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LogInTime";
        param.Value = LogInTime;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IPAddress";
        param.Value = IPAddress;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LogOutTime";
        if (LogOutTime != string.Empty)
            param.Value = LogOutTime;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsSession";
        param.Value = IsSession;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblLogInTrack_Update(String ID, String LogOutTime, String IsSession)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLogInTrack_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LogOutTime";
        param.Value = LogOutTime;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsSession";
        param.Value = IsSession;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblLogInTrack_InsertUpdate(Int32 ID, String UserId, String LogInTime, String IPAddress, String LogOutTime, String IsSession)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLogInTrack_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        param.Value = UserId;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LogInTime";
        param.Value = LogInTime;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IPAddress";
        param.Value = IPAddress;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LogOutTime";
        param.Value = LogOutTime;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsSession";
        param.Value = IsSession;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblLogInTrack_Delete(string ID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLogInTrack_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = ID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblLogInTrack_Search(string UserId, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblLogInTrack_Search";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        if (UserId != string.Empty)
            param.Value = UserId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}