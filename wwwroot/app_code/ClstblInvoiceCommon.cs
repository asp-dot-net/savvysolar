using System;
using System.Data;
using System.Data.Common;
using System.Web;

public struct SttblInvoiceCommon
{
    public string InvoiceID;
    public string InvType;
    public string ProjectID;
    public string InvNo;
    public string InvAmnt;
    public string InvDate;
    public string PayDate;
    public string Notes;
    public string InvDoc;
    public string InvEntered;
    public string InvEnteredBy;
    public string UpdatedBy;
    public string ExtraWork;
    public string ExtraAmount;
    public string ApprovedBy;
    public string Installer;
    public string MarketPrice;
    public string LandingPrice;
    public string AdvanceAmount;
    public string AdvanceDate;
    public string AdvanceLessAmount;
    public string AdvanceTotalAmount;
    public string AdvanceType;
}

public struct Sttbl_projectamount
{
    public string id;
    public string projectno;
    public string projectid;
    public string inv_type;
    public string invoiceid;
    public string amount;
   
}

public class ClstblInvoiceCommon
{
    public static SttblInvoiceCommon tblInvoiceCommon_SelectByInvoiceID(String InvoiceID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_SelectByInvoiceID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceID";
        param.Value = InvoiceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblInvoiceCommon details = new SttblInvoiceCommon();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.InvType = dr["InvType"].ToString();
            details.ProjectID = dr["ProjectID"].ToString();
            details.InvNo = dr["InvNo"].ToString();
            details.InvAmnt = dr["InvAmnt"].ToString();
            details.InvDate = dr["InvDate"].ToString();
            details.PayDate = dr["PayDate"].ToString();
            details.Notes = dr["Notes"].ToString();
            details.InvDoc = dr["InvDoc"].ToString();
            details.InvEntered = dr["InvEntered"].ToString();
            details.InvEnteredBy = dr["InvEnteredBy"].ToString();
            details.UpdatedBy = dr["UpdatedBy"].ToString();
            details.ExtraWork = dr["ExtraWork"].ToString();
            details.ExtraAmount = dr["ExtraAmount"].ToString();
            details.ApprovedBy = dr["ApprovedBy"].ToString();
            details.Installer = dr["Installer"].ToString();
            details.MarketPrice = dr["MarketPrice"].ToString();
            details.LandingPrice = dr["LandingPrice"].ToString();
            details.AdvanceAmount = dr["AdvanceAmount"].ToString();
            details.AdvanceDate = dr["AdvanceDate"].ToString();
            details.AdvanceLessAmount = dr["AdvanceLessAmount"].ToString();
            details.AdvanceTotalAmount = dr["AdvanceTotalAmount"].ToString();
            details.AdvanceType = dr["AdvanceType"].ToString();
        }
        // return structure details
        return details;
    }
    public static Sttbl_projectamount tbl_projectamount_SelectByInvoiceID(String id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_projectamount_SelectByInvoiceID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_projectamount details = new Sttbl_projectamount();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.id = dr["id"].ToString();
            details.projectno = dr["projectno"].ToString();
            details.projectid = dr["projectid"].ToString();
            details.inv_type = dr["inv_type"].ToString();
            details.invoiceid = dr["invoiceid"].ToString();
            details.amount = dr["amount"].ToString();
           
        }
        // return structure details
        return details;
    }


    public static SttblInvoiceCommon tblInvoiceCommon_SelectByInvType(String InvType, string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_SelectByInvType";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvType";
        param.Value = InvType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        SttblInvoiceCommon details = new SttblInvoiceCommon();
        if (table.Rows.Count > 0)
        {
            DataRow dr = table.Rows[0];
            details.InvoiceID = dr["InvoiceID"].ToString();
            details.InvType = dr["InvType"].ToString();
            details.ProjectID = dr["ProjectID"].ToString();
            details.InvNo = dr["InvNo"].ToString();
            details.InvAmnt = dr["InvAmnt"].ToString();
            details.InvDate = dr["InvDate"].ToString();
            details.PayDate = dr["PayDate"].ToString();
            details.Notes = dr["Notes"].ToString();
            details.InvDoc = dr["InvDoc"].ToString();
            details.InvEntered = dr["InvEntered"].ToString();
            details.InvEnteredBy = dr["InvEnteredBy"].ToString();
            details.UpdatedBy = dr["UpdatedBy"].ToString();
            details.ExtraWork = dr["ExtraWork"].ToString();
            details.ExtraAmount = dr["ExtraAmount"].ToString();
            details.ApprovedBy = dr["ApprovedBy"].ToString();
            details.Installer = dr["Installer"].ToString();
            details.MarketPrice = dr["MarketPrice"].ToString();
            details.LandingPrice = dr["LandingPrice"].ToString();
        }
        return details;
    }
    public static DataTable tblInvoiceCommon_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblInvoiceCommon_SelectByProjectID(String InvType, string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvType";
        param.Value = InvType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblInvoiceCommon_Insert(String InvType, String ProjectID, String InvNo, String InvAmnt, String InvDate, String PayDate, String Notes, String InvEnteredBy, String UpdatedBy, string ExtraWork, string ExtraAmount, string ApprovedBy, string Installer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvType";
        if (InvType != string.Empty)
            param.Value = InvType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvNo";
        if (InvNo != string.Empty)
            param.Value = InvNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvAmnt";
        if (InvAmnt != string.Empty)
            param.Value = InvAmnt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvDate";
        if (InvDate != string.Empty)
            param.Value = InvDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PayDate";
        if (PayDate != string.Empty)
            param.Value = PayDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        param.Value = Notes;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvEnteredBy";
        if (InvEnteredBy != string.Empty)
            param.Value = InvEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraWork";
        if (ExtraWork != string.Empty)
            param.Value = ExtraWork;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1000;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraAmount";
        if (ExtraAmount != string.Empty)
            param.Value = ExtraAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ApprovedBy";
        if (ApprovedBy != string.Empty)
            param.Value = ApprovedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblInvoiceCommon_InsertD2D(String InvType, String ProjectID, String InvNo, String InvAmnt, String InvDate, String PayDate, String Notes, String InvEnteredBy, String UpdatedBy, string ExtraWork, string ExtraAmount, string ApprovedBy, string Installer, string MarketPrice, string LandingPrice, string AdvanceAmount, string AdvanceDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_InsertD2D";

       // HttpContext.Current.Response.Write(ProjectID + "insert" + InvType + InvNo);
       //HttpContext.Current.Response.End();

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvType";
        if (InvType != string.Empty)
            param.Value = InvType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvNo";
        if (InvNo != string.Empty)
            param.Value = InvNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvAmnt";
        if (InvAmnt != string.Empty)
            param.Value = InvAmnt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvDate";
        if (InvDate != string.Empty)
            param.Value = InvDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PayDate";
        if (PayDate != string.Empty)
            param.Value = PayDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        param.Value = Notes;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvEnteredBy";
        if (InvEnteredBy != string.Empty)
            param.Value = InvEnteredBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraWork";
        if (ExtraWork != string.Empty)
            param.Value = ExtraWork;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1000;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraAmount";
        if (ExtraAmount != string.Empty)
            param.Value = ExtraAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ApprovedBy";
        if (ApprovedBy != string.Empty)
            param.Value = ApprovedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MarketPrice";
        if (MarketPrice != string.Empty)
            param.Value = MarketPrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LandingPrice";
        if (LandingPrice != string.Empty)
            param.Value = LandingPrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdvanceAmount";
        if (AdvanceAmount != string.Empty)
            param.Value = AdvanceAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdvanceDate";
        if (AdvanceDate != string.Empty)
            param.Value = AdvanceDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblInvoiceCommon_Update(string InvoiceID, String InvType, String ProjectID, String InvNo, String InvAmnt, String InvDate, String PayDate, String Notes, String UpdatedBy, string ExtraWork, string ExtraAmount, string ApprovedBy, string Installer)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceID";
        param.Value = InvoiceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvType";
        param.Value = InvType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvNo";
        if (InvNo != string.Empty)
            param.Value = InvNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvAmnt";
        if (InvAmnt != string.Empty)
            param.Value = InvAmnt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvDate";
        if (InvDate != string.Empty)
            param.Value = InvDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PayDate";
        if (PayDate != string.Empty)
            param.Value = PayDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        param.Value = Notes;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraWork";
        if (ExtraWork != string.Empty)
            param.Value = ExtraWork;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1000;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraAmount";
        if (ExtraAmount != string.Empty)
            param.Value = ExtraAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ApprovedBy";
        if (ApprovedBy != string.Empty)
            param.Value = ApprovedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
      
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblInvoiceCommon_UpdateD2D(string InvoiceID, String InvType, String ProjectID, String InvNo, String InvAmnt, String InvDate, String PayDate, String Notes, String UpdatedBy, string ExtraWork, string ExtraAmount, string ApprovedBy, string Installer, string MarketPrice, string LandingPrice, string AdvanceAmount, string AdvanceDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_UpdateD2D";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceID";
        param.Value = InvoiceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvType";
        param.Value = InvType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvNo";
        if (InvNo != string.Empty)
            param.Value = InvNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvAmnt";
        if (InvAmnt != string.Empty)
            param.Value = InvAmnt;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvDate";
        if (InvDate != string.Empty)
            param.Value = InvDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PayDate";
        if (PayDate != string.Empty)
            param.Value = PayDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        param.Value = Notes;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        if (UpdatedBy != string.Empty)
            param.Value = UpdatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraWork";
        if (ExtraWork != string.Empty)
            param.Value = ExtraWork;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1000;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraAmount";
        if (ExtraAmount != string.Empty)
            param.Value = ExtraAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ApprovedBy";
        if (ApprovedBy != string.Empty)
            param.Value = ApprovedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MarketPrice";
        if (MarketPrice != string.Empty)
            param.Value = MarketPrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@LandingPrice";
        if (LandingPrice != string.Empty)
            param.Value = LandingPrice;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdvanceAmount";
        if (AdvanceAmount != string.Empty)
            param.Value = AdvanceAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdvanceDate";
        if (AdvanceDate != string.Empty)
            param.Value = AdvanceDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblInvoiceCommon_InsertUpdate(Int32 InvoiceID, String InvType, String ProjectID, String InvNo, String InvAmnt, String InvDate, String PayDate, String Notes, String InvEntered, String InvEnteredBy, String UpdatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceID";
        param.Value = InvoiceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvType";
        param.Value = InvType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvNo";
        param.Value = InvNo;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvAmnt";
        param.Value = InvAmnt;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvDate";
        param.Value = InvDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PayDate";
        param.Value = PayDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        param.Value = Notes;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvEntered";
        param.Value = InvEntered;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvEnteredBy";
        param.Value = InvEnteredBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UpdatedBy";
        param.Value = UpdatedBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblInvoiceCommon_Delete(string InvoiceID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceID";
        param.Value = InvoiceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static int tblInvoiceCommon_ExistsByInvType(string InvType, string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_ExistsByInvType";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvType";
        param.Value = InvType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }
    public static bool tblInvoiceCommon_UpdateInvDoc(string InvoiceID, String InvDoc)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_UpdateInvDoc";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceID";
        param.Value = InvoiceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvDoc";
        if (InvDoc != string.Empty)
            param.Value = InvDoc;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static DataTable tblInvoiceCommon_SelectByProject(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_SelectByProject";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblInvoiceCommon_SelectByCustomerID(string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_SelectByCustomerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblInvCommonType_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvCommonType_SelectASC";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblInvCommonType_SelectAll()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvCommonType_SelectAll";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblInvCommonType_SelectTeamC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvCommonType_SelectTeamC";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblInvCommonType_SelectInst()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvCommonType_SelectInst";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblInvoiceCommon_SelectAdvance(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_SelectAdvance";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblInvoiceCommon_UpdateAdvance(string InvoiceID, string AdvanceAmount, string AdvanceDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_UpdateAdvance";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceID";
        param.Value = InvoiceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdvanceAmount";
        if (AdvanceAmount != string.Empty)
            param.Value = AdvanceAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdvanceDate";
        if (AdvanceDate != string.Empty)
            param.Value = AdvanceDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblInvoiceCommon_UpdateAdvanceTotal(string InvoiceID, string AdvanceLessAmount, string AdvanceTotalAmount, String AdvanceType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_UpdateAdvanceTotal";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceID";
        param.Value = InvoiceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdvanceLessAmount";
        if (AdvanceLessAmount != string.Empty)
            param.Value = AdvanceLessAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdvanceTotalAmount";
        if (AdvanceTotalAmount != string.Empty)
            param.Value = AdvanceTotalAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@AdvanceType";
        if (AdvanceType != string.Empty)
            param.Value = AdvanceType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static int tblInvoiceCommon_ExistsByInvoiceID(string InvoiceID, string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_ExistsByInvoiceID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceID";
        param.Value = InvoiceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tbl_projectamount_Insert(String projectno, String ProjectID, String inv_type, String invoiceid, String amount)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_projectamount_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectno";
        if (projectno != string.Empty)
            param.Value = projectno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@invtype";
        if (inv_type != string.Empty)
            param.Value = inv_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Invid";
        if (invoiceid != string.Empty)
            param.Value = invoiceid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@amount";
        if (amount != string.Empty)
            param.Value = amount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

       

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static DataTable tbl_projectamount_Select(string projectid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_projectamount_Select";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@projectid";
        if (projectid != string.Empty)
            param.Value = projectid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tbl_projectamount_ExistsById(string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_projectamount_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

       

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }


    public static bool tbl_projectamount_Update(string id,string projectno, String ProjectID, String inv_type, String invoiceid, String amount)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_projectamount_Update";

       DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
            
            
            param = comm.CreateParameter();
        param.ParameterName = "@projectno";
        if (projectno != string.Empty)
            param.Value = projectno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@invtype";
        if (inv_type != string.Empty)
            param.Value = inv_type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Invid";
        if (invoiceid != string.Empty)
            param.Value = invoiceid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@amount";
        if (amount != string.Empty)
            param.Value = amount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);



        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblInvoiceCommon_Updatemanagecomm(string InvoiceID, string panelprcost, string flatjob, String panelno)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblInvoiceCommon_Updatemanagecomm";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceID";
        param.Value = InvoiceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@panelprcost";
        if (panelprcost != string.Empty)
            param.Value = panelprcost;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@flatjob";
        if (flatjob != string.Empty)
            param.Value = flatjob;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@panelno";
        if (panelno != string.Empty)
            param.Value = panelno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tbl_projectamount_Delete(string InvoiceID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_projectamount_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceID";
        param.Value = InvoiceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
       
        catch
        {
        }
        return (result != -1);
    }
    public static bool tbl_projectamount_Delete_row(string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_projectamount_Delete_row";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ID";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
}