using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct SttblTrackPanelDetails
	{
		public string ProjectID;
		public string create_date;
		public string userid;
		public string PanelBrandID;
		public string PanelBrand;
		public string RECRebate;
		public string NumberPanels;
		public string PanelOutput;
		public string STCMultiplier;
		public string SystemCapKW;
		public string InverterDetailsID;
		public string InverterBrand;
		public string InverterSeries;
		public string SecondInverterDetailsID;
		public string STCNumber;
		public string InverterModel;
		public string InverterOutput;

	}


public class ClstblTrackPanelDetails
{
	public static SttblTrackPanelDetails tblTrackPanelDetails_SelectByTrackID (String TrackID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblTrackPanelDetails_SelectByTrackID";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@TrackID";
		param.Value = TrackID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 SttblTrackPanelDetails details = new SttblTrackPanelDetails();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.ProjectID = dr["ProjectID"].ToString();
			details.create_date = dr["create_date"].ToString();
			details.userid = dr["userid"].ToString();
			details.PanelBrandID = dr["PanelBrandID"].ToString();
			details.PanelBrand = dr["PanelBrand"].ToString();
			details.RECRebate = dr["RECRebate"].ToString();
			details.NumberPanels = dr["NumberPanels"].ToString();
			details.PanelOutput = dr["PanelOutput"].ToString();
			details.STCMultiplier = dr["STCMultiplier"].ToString();
			details.SystemCapKW = dr["SystemCapKW"].ToString();
			details.InverterDetailsID = dr["InverterDetailsID"].ToString();
			details.InverterBrand = dr["InverterBrand"].ToString();
			details.InverterSeries = dr["InverterSeries"].ToString();
			details.SecondInverterDetailsID = dr["SecondInverterDetailsID"].ToString();
			details.STCNumber = dr["STCNumber"].ToString();
			details.InverterModel = dr["InverterModel"].ToString();
			details.InverterOutput = dr["InverterOutput"].ToString();

		}
		// return structure details
		return details;
	}
	public static DataTable tblTrackPanelDetails_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblTrackPanelDetails_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
	public static int tblTrackPanelDetails_Insert ( String ProjectID, String create_date, String userid, String PanelBrandID, String PanelBrand, String RECRebate, String NumberPanels, String PanelOutput, String STCMultiplier, String SystemCapKW, String InverterDetailsID, String InverterBrand, String InverterSeries, String SecondInverterDetailsID, String STCNumber, String InverterModel, String InverterOutput)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblTrackPanelDetails_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectID";
		param.Value = ProjectID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@create_date";
        if (create_date != string.Empty)
            param.Value = create_date;
        else
            param.Value = DBNull.Value;
		param.DbType = DbType.DateTime;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@userid";
		param.Value = userid;
		param.DbType = DbType.String;
		param.Size = 200;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@PanelBrandID";
        if (PanelBrandID != string.Empty)
            param.Value = PanelBrandID;
        else
            param.Value = DBNull.Value;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@PanelBrand";
		param.Value = PanelBrand;
		param.DbType = DbType.String;
		param.Size = 100;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@RECRebate";
        if (RECRebate != string.Empty)
            param.Value = RECRebate;
        else
            param.Value = DBNull.Value;
		param.DbType = DbType.Decimal;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@NumberPanels";
        if(NumberPanels !=string.Empty)
		param.Value = NumberPanels;
        else
            param.Value = DBNull.Value;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@PanelOutput";
		param.Value = PanelOutput;
		param.DbType = DbType.String;
		param.Size = 10;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@STCMultiplier";
        if(STCMultiplier !=string.Empty)
		param.Value = STCMultiplier;
        else
            param.Value = DBNull.Value;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@SystemCapKW";
		param.Value = SystemCapKW;
		param.DbType = DbType.String;
		
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterDetailsID";
        if(InverterDetailsID!=string.Empty)
		param.Value = InverterDetailsID;
        else
            param.Value = DBNull.Value;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterBrand";
		param.Value = InverterBrand;
		param.DbType = DbType.String;
		param.Size = 100;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterSeries";
		param.Value = InverterSeries;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@SecondInverterDetailsID";
        if(SecondInverterDetailsID !=string.Empty)
		param.Value = SecondInverterDetailsID;
        else
            param.Value = DBNull.Value;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@STCNumber";
        if(STCNumber !=string.Empty)
		param.Value = STCNumber;
        else
            param.Value = DBNull.Value;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterModel";
		param.Value = InverterModel;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterOutput";
		param.Value = InverterOutput;
		param.DbType = DbType.String;
		
		comm.Parameters.Add(param);


		int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	}
	public static bool tblTrackPanelDetails_Update (string TrackID, String ProjectID, String create_date, String userid, String PanelBrandID, String PanelBrand, String RECRebate, String NumberPanels, String PanelOutput, String STCMultiplier, String SystemCapKW, String InverterDetailsID, String InverterBrand, String InverterSeries, String SecondInverterDetailsID, String STCNumber, String InverterModel, String InverterOutput)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblTrackPanelDetails_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@TrackID";
		param.Value = TrackID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ProjectID";
		param.Value = ProjectID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@create_date";
		param.Value = create_date;
		param.DbType = DbType.DateTime;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@userid";
		param.Value = userid;
		param.DbType = DbType.String;
		param.Size = 200;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@PanelBrandID";
		param.Value = PanelBrandID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@PanelBrand";
		param.Value = PanelBrand;
		param.DbType = DbType.String;
		param.Size = 100;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@RECRebate";
		param.Value = RECRebate;
		param.DbType = DbType.Decimal;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@NumberPanels";
		param.Value = NumberPanels;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@PanelOutput";
		param.Value = PanelOutput;
		param.DbType = DbType.String;
		param.Size = 10;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@STCMultiplier";
		param.Value = STCMultiplier;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@SystemCapKW";
		param.Value = SystemCapKW;
		param.DbType = DbType.String;
		
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterDetailsID";
		param.Value = InverterDetailsID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterBrand";
		param.Value = InverterBrand;
		param.DbType = DbType.String;
		param.Size = 100;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterSeries";
		param.Value = InverterSeries;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@SecondInverterDetailsID";
		param.Value = SecondInverterDetailsID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@STCNumber";
		param.Value = STCNumber;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterModel";
		param.Value = InverterModel;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterOutput";
		param.Value = InverterOutput;
		param.DbType = DbType.String;
		
		comm.Parameters.Add(param);


		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static string tblTrackPanelDetails_InsertUpdate (Int32 TrackID, String ProjectID, String create_date, String userid, String PanelBrandID, String PanelBrand, String RECRebate, String NumberPanels, String PanelOutput, String STCMultiplier, String SystemCapKW, String InverterDetailsID, String InverterBrand, String InverterSeries, String SecondInverterDetailsID, String STCNumber, String InverterModel, String InverterOutput)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblTrackPanelDetails_InsertUpdate";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@TrackID";
		param.Value = TrackID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ProjectID";
		param.Value = ProjectID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@create_date";
		param.Value = create_date;
		param.DbType = DbType.DateTime;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@userid";
		param.Value = userid;
		param.DbType = DbType.String;
		param.Size = 200;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@PanelBrandID";
		param.Value = PanelBrandID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@PanelBrand";
		param.Value = PanelBrand;
		param.DbType = DbType.String;
		param.Size = 100;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@RECRebate";
		param.Value = RECRebate;
		param.DbType = DbType.Decimal;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@NumberPanels";
		param.Value = NumberPanels;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@PanelOutput";
		param.Value = PanelOutput;
		param.DbType = DbType.String;
		param.Size = 10;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@STCMultiplier";
		param.Value = STCMultiplier;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@SystemCapKW";
		param.Value = SystemCapKW;
		param.DbType = DbType.String;
		
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterDetailsID";
		param.Value = InverterDetailsID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterBrand";
		param.Value = InverterBrand;
		param.DbType = DbType.String;
		param.Size = 100;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterSeries";
		param.Value = InverterSeries;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@SecondInverterDetailsID";
		param.Value = SecondInverterDetailsID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@STCNumber";
		param.Value = STCNumber;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterModel";
		param.Value = InverterModel;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@InverterOutput";
		param.Value = InverterOutput;
		param.DbType = DbType.String;
		
		comm.Parameters.Add(param);


		string result = "";
		try
		{
			result = DataAccess.ExecuteScalar(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
	public static bool tblTrackPanelDetails_Delete (string TrackID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblTrackPanelDetails_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@TrackID";
		param.Value = TrackID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}
}