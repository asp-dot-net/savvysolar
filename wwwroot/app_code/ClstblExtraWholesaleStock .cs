using System;
using System.Data;
using System.Data.Common;

public struct SttblWholesaleExtraStock
{
    public string ExtraStockID;
    public string WholesaleOrderItemID;
    public string WholesaleOrderID;
    public string InvoiceNo;
    public string StockItemID;
    public string ExtraStock;
    public string StockLocation;
}


public class ClstblExtraWholesaleStock
{
    public static SttblWholesaleExtraStock tblExtraStock_SelectByExtraStockID(String ExtraStockID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraStock_SelectByExtraStockID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ExtraStockID";
        param.Value = ExtraStockID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblWholesaleExtraStock details = new SttblWholesaleExtraStock();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.WholesaleOrderItemID = dr["WholesaleOrderItemID"].ToString();
            details.WholesaleOrderItemID = dr["WholesaleOrderItemID"].ToString();
            details.InvoiceNo = dr["InvoiceNo"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.ExtraStock = dr["ExtraStock"].ToString();
            details.StockLocation = dr["StockLocation"].ToString();

        }
        // return structure details
        return details;
    }

    public static int tblExtraStock_Insert(String ProjectID, String InstallerID, String StockItemID, String ExtraStock, string StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraStock_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        param.Value = InstallerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraStock";
        param.Value = ExtraStock;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        param.Value = StockLocation;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblExtraWholesaleStock_Insert(String WholesaleOrderItemID,String WholesaleOrderID,String InvoiceNo, String StockItemID, String ExtraStock, string StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraWholesaleStock_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderItemID";
        param.Value = WholesaleOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraStock";
        param.Value = ExtraStock;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        param.Value = StockLocation;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tblExtraWholesaleStock_SelectByInstallerID(string InvoiceNo, string StockItemID, string StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraWholesaleStock_SelectByInstallerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        param.Value = StockLocation;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblExtraWholesaleStock_Update(String WholesaleOrderItemID,String InvoiceNo, String StockItemID, String ExtraStock, string StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraWholesaleStock_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderItemID";
        param.Value = WholesaleOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraStock";
        param.Value = ExtraStock;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        param.Value = StockLocation;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblExtraWholesaleStock_DeleteByWholeOrderID(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraWholesaleStock_DeleteByWholeOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
}