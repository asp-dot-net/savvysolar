using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct SttblProjectType
	{
		public string ProjectType;
		public string Active;
		public string Seq;

	}


public class ClstblProjectType
{
	public static SttblProjectType tblProjectType_SelectByProjectTypeID (String ProjectTypeID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectType_SelectByProjectTypeID";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectTypeID";
		param.Value = ProjectTypeID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 SttblProjectType details = new SttblProjectType();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.ProjectType = dr["ProjectType"].ToString();
			details.Active = dr["Active"].ToString();
			details.Seq = dr["Seq"].ToString();

		}
		// return structure details
		return details;
	}
	public static DataTable tblProjectType_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectType_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
    public static DataTable tblProjectType_SelectActive()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectType_SelectActive";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblProjectType_SelectType()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectType_SelectType";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

	public static int tblProjectType_Insert ( String ProjectType, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectType_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectType";
		param.Value = ProjectType;
		param.DbType = DbType.String;
		param.Size = 30;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


		int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	}
	public static bool tblProjectType_Update (string ProjectTypeID, String ProjectType, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectType_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectTypeID";
		param.Value = ProjectTypeID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ProjectType";
		param.Value = ProjectType;
		param.DbType = DbType.String;
		param.Size = 30;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static string tblProjectType_InsertUpdate (Int32 ProjectTypeID, String ProjectType, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectType_InsertUpdate";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectTypeID";
		param.Value = ProjectTypeID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ProjectType";
		param.Value = ProjectType;
		param.DbType = DbType.String;
		param.Size = 30;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


		string result = "";
		try
		{
			result = DataAccess.ExecuteScalar(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
	public static bool tblProjectType_Delete (string ProjectTypeID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectType_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectTypeID";
		param.Value = ProjectTypeID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}
    public static int ProjectTypeNameExists(string title)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "ProjectTypeNameExists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int ProjectTypeNameExistsWithID(string title, string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "ProjectTypeNameExistsWithID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static DataTable tblProjectTypeGetDataByAlpha(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectTypeGetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblProjectTypeGetDataBySearch(string alpha, string Active)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblProjectTypeGetDataBySearch";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
}