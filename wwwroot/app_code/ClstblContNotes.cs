using System;
using System.Data;
using System.Data.Common;

public struct SttblContNotes
{
    public string ContactID;
    public string EmployeeID;
    public string NoteSetBy;
    public string ContNoteDate;
    public string ContNoteTypeID;
    public string ContNote;
    public string ContNoteDone;
    public string ContNoteSubject;
    public string ContWordDoc;
    public string ContNoteTag;
    public string upsize_ts;
}

public class ClstblContNotes
{
    public static SttblContNotes tblContNotes_SelectByContNoteID(String ContNoteID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContNotes_SelectByContNoteID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContNoteID";
        param.Value = ContNoteID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblContNotes details = new SttblContNotes();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.ContactID = dr["ContactID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.NoteSetBy = dr["NoteSetBy"].ToString();
            details.ContNoteDate = dr["ContNoteDate"].ToString();
            details.ContNoteTypeID = dr["ContNoteTypeID"].ToString();
            details.ContNote = dr["ContNote"].ToString();
            details.ContNoteDone = dr["ContNoteDone"].ToString();
            details.ContNoteSubject = dr["ContNoteSubject"].ToString();
            details.ContWordDoc = dr["ContWordDoc"].ToString();
            details.ContNoteTag = dr["ContNoteTag"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblContNotes_SelectByContactID(string ContactID, string NoteSetBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContNotes_SelectByContactID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NoteSetBy";
        param.Value = NoteSetBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmployees_getcustcount(string SalesTeamID, string EmpType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_getcustcount";


        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmpType";
        param.Value = EmpType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        param.Value = SalesTeamID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblEmployees_getprojectcount(string SalesTeamID, string EmpType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_getprojectcount";


        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmpType";
        param.Value = EmpType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        param.Value = SalesTeamID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmployees_getdeposite(string SalesTeamID, string EmpType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmployees_getdeposite";


        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmpType";
        param.Value = EmpType;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        param.Value = SalesTeamID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblContNotes_SelectByContNoteDate(string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContNotes_SelectByContNoteDate";


        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblContNotes_Insert(String ContactID, String EmployeeID, String NoteSetBy, String ContNote)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContNotes_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NoteSetBy";
        param.Value = NoteSetBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNote";
        param.Value = ContNote;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblContNotes_Update(string ContNoteID, String ContactID, String EmployeeID, String NoteSetBy, String ContNoteTypeID, String ContNote, String ContNoteDone, String ContNoteSubject, String ContWordDoc, String ContNoteTag, String upsize_ts)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContNotes_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContNoteID";
        param.Value = ContNoteID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NoteSetBy";
        param.Value = NoteSetBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNoteTypeID";
        param.Value = ContNoteTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNote";
        param.Value = ContNote;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNoteDone";
        param.Value = ContNoteDone;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNoteSubject";
        param.Value = ContNoteSubject;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContWordDoc";
        param.Value = ContWordDoc;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNoteTag";
        param.Value = ContNoteTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        param.Value = upsize_ts;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }



    public static bool tblContNotes_Update_ContNoteDone(string ContNoteID, String ContNoteDone)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContNotes_Update_ContNoteDone";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContNoteID";
        param.Value = ContNoteID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNoteDone";
        param.Value = ContNoteDone;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblContNotes_InsertUpdate(Int32 ContNoteID, String ContactID, String EmployeeID, String NoteSetBy, String ContNoteDate, String ContNoteTypeID, String ContNote, String ContNoteDone, String ContNoteSubject, String ContWordDoc, String ContNoteTag, String upsize_ts)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContNotes_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContNoteID";
        param.Value = ContNoteID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NoteSetBy";
        param.Value = NoteSetBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNoteDate";
        param.Value = ContNoteDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNoteTypeID";
        param.Value = ContNoteTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNote";
        param.Value = ContNote;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNoteDone";
        param.Value = ContNoteDone;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNoteSubject";
        param.Value = ContNoteSubject;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContWordDoc";
        param.Value = ContWordDoc;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContNoteTag";
        param.Value = ContNoteTag;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@upsize_ts";
        param.Value = upsize_ts;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblContNotes_Delete(string ContNoteID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblContNotes_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContNoteID";
        param.Value = ContNoteID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
}