﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClsClientuser
/// </summary>
/// 

public struct stuser
{
    public string UserId;
    public string username;
    public string firstname;
    public string lastname;
    public string phoneno1;
    public string email;
    public string reg_date;
    public string address;
    public string city;
    public string state;
    public string country;

    public string phoneno2;
    public string address1;
    public string address2;
    public string moblieno;
    public string zipcode;
    public string sfirstname;
    public string slastname;
    public string sphoneno1;
    public string saddress1;
    public string saddress2;
    public string sstate;
    public string szipcode;
    public string scity;
    public string bfirstname;
    public string blastname;
    public string bphoneno1;
    public string baddress1;
    public string baddress2;
    public string bstate;
    public string bzipcode;
    public string bcity;
    public string scountry;
    public string bcountry;
    public string password;
}



public class ClsClientuser
{
	public ClsClientuser()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    public static stuser ClientUserAspnetUserGetDataStructByGUID(string GUID)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "ClientUserAspnetUserGetDataStructByGUID";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@GUID";
        param.Value = GUID;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        stuser details = new stuser();

        if (table.Rows.Count > 0)
        {
            DataRow dr = table.Rows[0];
            details.UserId = dr["UserId"].ToString();
            details.username = dr["UserName"].ToString();
           
        }
        // return product details
        return details;
    }

    public static stuser ClientUserAspnetUserGetDataStructByUserId(string UserID)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "ClientUserAspnetUserGetDataStructByUserId";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@UserID";
        param.Value = UserID;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        stuser details = new stuser();

        if (table.Rows.Count > 0)
        {
            DataRow dr = table.Rows[0];
            details.UserId = dr["UserId"].ToString();
            details.username = dr["UserName"].ToString();
            details.email = dr["Email"].ToString();

        }
        // return product details
        return details;
    }

    public static stuser ClientUserAspnetUserGetDataStructByUserName(string UserName)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "ClientUserAspnetUserGetDataStructByUserName";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@UserName";
        param.Value = UserName;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        stuser details = new stuser();

        if (table.Rows.Count > 0)
        {
            DataRow dr = table.Rows[0];
            details.UserId = dr["UserId"].ToString();
            details.username = dr["UserName"].ToString();
            details.password = dr["password"].ToString();
            details.email = dr["email"].ToString();


        }
        // return product details
        return details;
    }

    public static bool ClientUserUpdateUserPassword(string userid, string password)
    {
        //get DbCommand object
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "ClientUserUpdateUserPassword";

        //create a new parameter

        DbParameter para = comm.CreateParameter();
        para = comm.CreateParameter();
        para.ParameterName = "@userid";
        para.Value = userid;
        para.DbType = DbType.String;
        para.Size = 200;
        comm.Parameters.Add(para);

        para = comm.CreateParameter();
        para.ParameterName = "@password";
        para.Value = password;
        para.DbType = DbType.String;
        para.Size = 128;
        comm.Parameters.Add(para);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);


        return (result != -1);
    }

    public static int forgorpassword_Username_Exists(string username)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "forgorpassword_Username_Exists";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@username";
        param.Value = username;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);
        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int forgorpassword_UserId_Exists(string UserId)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "forgorpassword_UserId_Exists";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        param.Value = UserId;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);
        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int changepassword_Password_Exists(string UserId, string password)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "changepassword_Password_Exists";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        param.Value = UserId;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@password";
        param.Value = password;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool aspnet_Membership_UpdatePassword(string UserId, string Password)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "aspnet_Membership_UpdatePassword";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        param.Value = UserId;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Password";
        param.Value = Password;
        param.DbType = DbType.String;
        param.Size = 128;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }
}