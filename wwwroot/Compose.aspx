﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="Compose.aspx.cs" Inherits="Compose" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link data-require="fontawesome@*" data-semver="4.3.0" rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />

    <!--<script data-require="jquery@2.1.3" data-semver="2.1.3" src="http://code.jquery.com/jquery-2.1.3.min.js"></script>-->

    <!--<script data-require="bootstrap@*" data-semver="3.3.2" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>-->
    <link rel="stylesheet" href="style.css" />
    <link href="admin/assets/js/editors/summernote/summernote.css" rel="stylesheet" />




    <%--  <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>--%>


    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Compose Message</h5>
        <div id="tdExport" class="pull-right" runat="server">
            <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">
                <%--<asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                    CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>--%>
            </ol>
        </div>
    </div>




    <div class="page-body padtopzero">
        <div class="page-body no-padding">
            <div class="mail-container">
                <div class="mail-header">
                    <ul class="header-buttons">
                        <li>
                            <asp:LinkButton ID="btnsend" runat="server"><i class="fa fa-external-link"></i></asp:LinkButton>
                            <a class="tooltip-primary" runat="server" data-toggle="tooltip" onclick="SendEmail" data-original-title="Send"></a>
                        </li>
                        <li>
                            <a class="tooltip-primary" data-toggle="tooltip" data-original-title="Attach"><i class="glyphicon glyphicon-paperclip"></i></a>
                        </li>
                        <li>
                            <a class="tooltip-primary" data-toggle="tooltip" data-original-title="Cancel"><i class="glyphicon glyphicon-remove"></i></a>
                        </li>
                        <li>
                            <a class="tooltip-primary" data-toggle="tooltip" data-original-title="Draft"><i class="glyphicon glyphicon-save"></i></a>
                        </li>
                    </ul>

                    <div class="draft">
                        Last Draft Saved 1 minutes ago
                    </div>
                </div>
                <div class="mail-body">
                    <div class="mail-compose">

                        <div class="form-group bordered-left-4 bordered-themeprimary">
                            <label for="to">To:</label>
                            <input type="text" runat="server" class="form-control" id="txtto" tabindex="1" />
                            <div class="field-options">
                                <a href="javascript:;" onclick="$(this).hide(); $('#cc').parent().removeClass('hidden'); $('#cc').focus();">CC</a>
                                <a href="javascript:;" onclick="$(this).hide(); $('#bcc').parent().removeClass('hidden'); $('#bcc').focus();">BCC</a>
                            </div>
                        </div>
                        <div class="form-group hidden bordered-left-4 bordered-themethirdcolor">
                            <label for="cc">CC:</label>
                            <input type="text" class="form-control" id="cc" tabindex="2" />
                        </div>
                        <div class="form-group hidden bordered-left-4 bordered-themefourthcolor">
                            <label for="bcc">BCC:</label>
                            <input type="text" class="form-control" id="bcc" tabindex="2" />
                        </div>
                        <div class="form-group bordered-left-4 bordered-themesecondary">
                            <label for="subject">Subject:</label>
                            <input type="text" class="form-control" runat="server" id="txtSubject" tabindex="1" />
                        </div>

                        <%-- <div class="compose-message-editor">--%>

                        <%--           <asp:TextBox runat="server" class="summernote"  id="TextBox1">
                            
                        </asp:TextBox>--%>
                        <div id="txtTest" runat="server"></div>



                        <%--  </div>--%>
                    </div>
                </div>
                <div class="mail-sidebar">
                    <ul class="mail-menu">
                        <li>
                            <a href="inbox.html">
                                <i class="fa fa-inbox"></i>
                                <span class="badge badge-default badge-square pull-right">6</span>
                                Inbox
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="fa fa-star"></i>
                                <span class="badge badge-default badge-square pull-right">1</span>
                                Important
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="glyphicon glyphicon-share"></i>
                                <span class="badge badge-default badge-square pull-right">1</span>
                                Sent
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-envelope"></i>
                                Drafts
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-ban"></i>
                                <span class="badge badge-default badge-square pull-right">1</span>
                                Spam
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <i class="fa fa-trash-o"></i>
                                Trash
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">+ Add Folder
                            </a>
                        </li>
                    </ul>
                    <ul class="mail-menu">
                        <li class="menu-title">
                            <h6>Tags</h6>
                        </li>
                        <li>
                            <a href="#">
                                <span class="badge badge-palegreen badge-tag badge-square"></span>
                                Business
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <span class="badge badge-darkorange badge-tag badge-square"></span>
                                Sports
                            </a>
                        </li>

                        <li>
                            <a href="#">
                                <span class="badge badge-yellow badge-tag badge-square"></span>
                                Friends
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">+ Add Tag
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>


    </div>
    <div id="result">
    </div>


    <div class="loaderPopUP">
        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedpro);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);

            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                $('.loading-container').css('display', 'block');

            }
            function endrequesthandler(sender, args) {
                //hide the modal popup - the update progress
            }

            function pageLoadedpro() {

                $('.loading-container').css('display', 'none');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $("[data-toggle=tooltip]").tooltip();


            }
        </script>
    </div>
    <script src="admin/assets/js/editors/summernote/summernote.js"></script>
    <script src="admin/vendor/jquery/dist/jquery.min.js"></script>
    <script src="admin/assets/js/bootstrap.min.js"></script>
    <script>


            $(function () {

                $('#<%=btnsend.ClientID %>').click(function () {
                    var data = {};
                    data.body = $('#divEmail').text();
                    data.to = $('#<%=txtto.ClientID %>').val();
                    data.subject = $('#<%=txtSubject.ClientID %>').val();


                    $.ajax({
                        type: "POST",
                        url: "Compose.aspx/ExecuteSql",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify(data)
                    }).done(function (result) {
                        console.log(result.d);
                        window.location = "inbox.aspx";
                        $('#result').text(result.d);
                    });

                });

            });

            $(document).ready(function () {

                $(function () {
                    // Set up your summernote instance
                    $('#<%=txtTest.ClientID %>').summernote();
                   <%-- alert();
                    // When the summernote instance loses focus, update the content of your <textarea>
                    $('#<%=txtTest.ClientID %>').on('summernote.blur', function () {

                        $('#<%=txtTest.ClientID %>').html($('#<%=txtTest.ClientID %>').summernote('code'));
                    });--%>
                });
            });
    </script>
    <%-- </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>

