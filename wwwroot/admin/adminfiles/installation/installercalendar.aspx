<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="installercalendar.aspx.cs" Inherits="admin_adminfiles_installation_installercalendar" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .select2-drop.select2-drop-above.select2-drop-active {
            border-top: 1px solid #ff0000;
            z-index: 999999 !important;
        }
    </style>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Installer Calendar</h5>
    </div>

    <section>
        <div class="panel-body animate-panel padbtmzero">
            <%--  <div>
                <div >--%>
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel marbtmzero">
                        <%-- <div class="panel-heading">
                                   
                                    <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                   Installer Calendar
                                </div>--%>

                        <script>
                            var prm = Sys.WebForms.PageRequestManager.getInstance();
                            prm.add_pageLoaded(pageLoadedpro);
                            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                            prm.add_beginRequest(beginrequesthandler);
                            // raised after an asynchronous postback is finished and control has been returned to the browser.
                            prm.add_endRequest(endrequesthandler);

                            function beginrequesthandler(sender, args) {
                                //shows the modal popup - the update progress
                                $('.loading-container').css('display', 'block');

                            }
                            function endrequesthandler(sender, args) {
                                //hide the modal popup - the update progress
                            }

                            function pageLoadedpro() {

                                $('.loading-container').css('display', 'none');
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $("[data-toggle=tooltip]").tooltip();

                            }

                        </script>
                        <div class="panel-body">
                            <div class="form-horizontal">

                                <%-- <div class="contactsarea">
                                    <div class="contacttoparea ">
                                        <div class="form-inline">
                                            <div class="contactbrmbtm padd10all">--%>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="btnmarzero">
                                            <asp:Button ID="btntoday" CssClass="btn btn-dark" runat="server" Text="Today" OnClick="btntoday_Click" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div style="text-align: center;">
                                            <div class="form-group">
                                                <h5>
                                                    <asp:Literal ID="ltdate" runat="server"></asp:Literal></h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div style="text-align: right;">
                                            <div>
                                                <div class="week myweek">
                                                    <ul>
                                                        <li>
                                                            <asp:LinkButton ID="btnpreviousweek" runat="server" Text="Previous Week" OnClick="btnpreviousweek_Click"><i class="fa fa-angle-left"></i></asp:LinkButton></li>
                                                        <li>Week</li>
                                                        <li>
                                                            <asp:LinkButton ID="btnnextweek" runat="server" Text="Next Week" OnClick="btnnextweek_Click"><i class="fa fa-angle-right"></i></asp:LinkButton></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>


                        </div>

                    </div>
                </div>
            </div>
            <%-- </div>
            </div>--%>
        </div>
    </section>

    <div class="panel-body">
        <div class="panel panel-default teamcalendar">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#BDC3C8" class="table">
                <tbody>
                    <tr>
                        <asp:Repeater ID="RptDays" runat="server" OnItemDataBound="RptDays_ItemDataBound">
                            <ItemTemplate>
                                <td valign="top">
                                    <table width="100%" cellpadding="0" cellspacing="0" class="table table-striped table-bordered teamcaltable">
                                        <thead>
                                            <tr>
                                                <th colspan="2">
                                                    <asp:HiddenField ID="hdnDate" runat="server" Value='<%# Eval("CDate")%>' />
                                                    <%# Eval("CDate")%>
                                                    <%# Eval("CDay") %>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="title">
                                                <td>Installer
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <asp:Repeater ID="rptinstaller1" runat="server" OnItemCommand="rptinstaller1_ItemCommand">
                                                <ItemTemplate>
                                                    <tr class="brdergray">
                                                        <td align="left">
                                                            <%# Eval("InstallerName")%>
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="gvbtnDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Assign"
                                                                CommandArgument='<%# Eval("Installer")%>' CommandName="Assign">
                                                                       <i class="fa fa-tasks"></i></asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" width="100%" class="nopad">
                                                            <div class="brdbtm">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <cc1:ModalPopupExtender ID="ModalPopupProject" runat="server" BackgroundCssClass="modalbackground"
        CancelControlID="btnCancel" DropShadow="false" PopupControlID="divDetail" TargetControlID="btnNULL">
    </cc1:ModalPopupExtender>
    <div id="divDetail" runat="server" style="display: none;" class="modal_popup" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">
                     <div style="float: right">
                    <asp:LinkButton ID="btnCancel" CausesValidation="false" runat="server" CssClass="btn btn-danger btncancelicon" data-dismiss="modal">
                        Close
                    </asp:LinkButton>
                         </div>
                    <h4 class="modal-title" id="H2">
                        <asp:Label ID="Label14" runat="server" Text=""></asp:Label>
                        Projects Detail</h4>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <div class="formainline">
                            <label class="col-sm-3"></label>
                            <div class="col-sm-8">
                                <div class="form-group spical">
                                    <asp:DropDownList ID="ddlSearchState" runat="server" Width="200px" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                        <asp:ListItem Value="">State</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="formainline">
                            <label class="col-sm-3 control-label">Project</label>
                            <div class="col-sm-8">
                                <div class="form-group spical">
                                    <asp:DropDownList ID="ddlProject" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                        <asp:ListItem Value="">Select</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                        ControlToValidate="ddlProject" Display="Dynamic" ValidationGroup="instcal"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div class="formainline">
                            <label class="col-sm-3 control-label">Install Date</label>
                            <div class="col-sm-8">
                                <div class="input-group date datetimepicker1">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                    <asp:TextBox ID="txtInstallBookingDate" runat="server" class="form-control" placeholder="Start Date">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                        ControlToValidate="txtInstallBookingDate" Display="Dynamic" ValidationGroup="instcal"></asp:RequiredFieldValidator>

                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div class="formainline">

                            <%--   <label class="col-sm-3">&nbsp;</label>--%>
                            <div class="col-sm-8">
                                <span style="float: right; padding-top: 10px">
                                    <asp:Button ID="ibtnAssign" runat="server" Text="Add" OnClick="ibtnAssign_Click"
                                        ValidationGroup="instcal" CssClass="btn btn-primary addwhiteicon" />
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnNULL" Style="display: none;" runat="server" />

</asp:Content>

