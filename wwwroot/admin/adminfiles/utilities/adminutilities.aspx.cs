using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

public partial class admin_adminfiles_utilities_adminutilities : System.Web.UI.Page
{
    string foldername = "utilities";
    string sitelogofoldername = "sitelogo";
    string emailtopfoldername = "emailtop";
    string emailbottomfoldername = "emailbottom";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            InitUpdate();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            tbsitename.Text = st.sitename;
            tbsiteurl.Text = st.siteurl;
            txtMailServer.Text = st.MailServer;
            txtfrom.Text = st.from;
            txtcc.Text = st.cc;
            txtusername.Text = st.username;
            txtpassword.Text = st.Password;
            txtorderthanksmessage.Text = st.OrderThanksMessage;
            txtcontactusthenksmsg.Text = st.ContactUsThanksmessage;
            txtordersubject.Text = st.OrderSubject;
            txtcontactussubject.Text = st.Contactussubject;
            txtregardsname.Text = st.Regards_Name;
            txtemailbodycolor.Text = st.emailbodybordercolor;
            txtadminfooter.Text = st.adminfooter;
            txtpaypalaccountname.Text = st.PaypalAccountName;
            txtpaypalmode.Text = st.PaypalMode;
            txtcurrencysymbol.Text = st.PaypalCurrencySymbol;
            imageemailtop.ImageUrl = "~/userfiles/emailtop/" + st.emailtop;
            imageemailbottom.ImageUrl = "~/userfiles/emailbottom/" + st.emailbottom;

            imagesitelogo.ImageUrl = "~/userfiles/sitelogo/" + st.sitelogoupload;

            txtsslportno.Text = st.SSLPortno;
            txtsslvalue.Text = st.SSLValue;
            ddlauthenticate.SelectedValue = st.Authenticate;
            txtusername.Text = st.username;
            txtpassword.Text = st.Password;
            hideAuthenticate();
        }
    }
    public static bool ThumbnailCallBack()
    {
        return false;
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string sitename = tbsitename.Text;
        string siteurl = tbsiteurl.Text;
        String mailserver = txtMailServer.Text;
        string from = txtfrom.Text;
        string cc = txtcc.Text;
        string username = txtusername.Text;
        string password = txtpassword.Text;
        string ordermsg = txtorderthanksmessage.Text;
        String contactusmsg = txtcontactusthenksmsg.Text;
        string ordersub = txtordersubject.Text;
        string contactussub = txtcontactussubject.Text;
        String regardsname = txtregardsname.Text;
        string emailbodycolor = txtemailbodycolor.Text;
        String adminfooter = txtadminfooter.Text;
        String paypalaccountname = txtpaypalaccountname.Text;
        String paypalmode = txtpaypalmode.Text;
        string currencysymbol = txtcurrencysymbol.Text;
        string sslportno = txtsslportno.Text;
        string sslportvalue = txtsslvalue.Text;

        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        String emailtop = st.emailtop;
        String emailbottom = st.emailbottom;
        String filelogo = st.sitelogoupload;
        //String authenticate = chkauthenticate.Checked.ToString();
        String authenticate = ddlauthenticate.SelectedValue.ToString();


        if (fulemailtop.HasFile)
        {
            SiteConfiguration.deleteimage(st.emailtop, emailtopfoldername);
            emailtop = fulemailtop.FileName;
            fulemailtop.SaveAs(Request.PhysicalApplicationPath + "\\userfiles\\" + emailtopfoldername + "\\" + emailtop);
        }
        if (fulemailbottom.HasFile)
        {
            SiteConfiguration.deleteimage(st.emailbottom, emailbottomfoldername);
            emailbottom = fulemailbottom.FileName;
            fulemailbottom.SaveAs(Request.PhysicalApplicationPath + "\\userfiles\\" + emailbottomfoldername + "\\" + emailbottom);
        }
        if (fulsitelogo.HasFile)
        {
            SiteConfiguration.deleteimage(st.sitelogoupload, sitelogofoldername);
            filelogo = fulsitelogo.FileName;
            fulsitelogo.SaveAs(Request.PhysicalApplicationPath + "\\userfiles\\" + sitelogofoldername + "\\" + filelogo);
        }
        bool success1 = ClsAdminUtilities.SpUtilitiesUpdateData("1", sitename, "", siteurl, mailserver, from, cc, username, password, ordermsg, contactusmsg, ordersub, contactussub, regardsname, emailbodycolor, adminfooter, paypalaccountname, paypalmode, currencysymbol, emailtop, emailbottom, filelogo, authenticate, sslportvalue, sslportno);
        if (success1)
        {
            SetUpdate();
        }
        else
        {
            SetError();
        }
        Reset();
    }


    //================================== Start Common Code ==================================


    private string ConvertSortDirectionToSql(SortDirection sortDireciton)
    {
        string m_SortDirection = String.Empty;

        switch (sortDireciton)
        {
            case SortDirection.Ascending:
                m_SortDirection = "ASC";
                break;

            case SortDirection.Descending:
                m_SortDirection = "DESC";
                break;
        }

        return m_SortDirection;
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    public void SetUpdate()
    {
        //Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;

    }
    public void SetDelete()
    {
        //Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }

    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }

    public void InitUpdate()
    {
        HidePanels();
        PanAddUpdate.Visible = true;
        btnUpdate.Visible = true;
        //   lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
        PanAddUpdate.Visible = false;
    }
    public void Reset()
    {
        PanAddUpdate.Visible = true;
        tbsitename.Text = string.Empty;
        tbsiteurl.Text = string.Empty;
        //tblogo.Text = string.Empty;
        txtMailServer.Text = string.Empty;
        txtfrom.Text = string.Empty;
        txtcc.Text = string.Empty;
        txtusername.Text = string.Empty;
        txtpassword.Text = string.Empty;
        txtorderthanksmessage.Text = string.Empty;
        txtcontactusthenksmsg.Text = string.Empty;
        txtordersubject.Text = string.Empty;
        txtcontactussubject.Text = string.Empty;
        txtregardsname.Text = string.Empty;
        txtemailbodycolor.Text = string.Empty;
        txtadminfooter.Text = string.Empty;
        txtpaypalaccountname.Text = string.Empty;
        txtpaypalmode.Text = string.Empty;
        txtcurrencysymbol.Text = string.Empty;
        txtsslportno.Text = string.Empty;
        txtsslvalue.Text = string.Empty;
        ddlauthenticate.SelectedValue = "0";
    }
    //================================== End Common Code =====================================

    protected void ddlauthenticate_SelectedIndexChanged(object sender, EventArgs e)
    {
        hideAuthenticate();
        txtusername.Text = string.Empty;
        txtpassword.Text = string.Empty;
        txtsslportno.Text = string.Empty;
        txtsslvalue.Text = string.Empty;
        // InitUpdate();
    }
    public void hideAuthenticate()
    {
        if (ddlauthenticate.SelectedValue == "1")
        {
            trusername.Visible = true;
            trpassword.Visible = true;
        }
        else if (ddlauthenticate.SelectedValue == "2")
        {
            trusername.Visible = true;
            trpassword.Visible = true;
            trsslportno.Visible = true;
            trsslportvalue.Visible = true;
        }
        else
        {
            trusername.Visible = false;
            trpassword.Visible = false;
            trsslportno.Visible = false;
            trsslportvalue.Visible = false;
        }
    }
}
