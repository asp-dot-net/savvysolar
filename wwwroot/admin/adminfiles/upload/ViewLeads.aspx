﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="ViewLeads.aspx.cs" Inherits="admin_adminfiles_upload_ViewLeads" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            HighlightControlToValidate();
            ;
        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>

    <script>
        function ComfirmDelete(event, ctl) {
            event.preventDefault();
            var defaultAction = $(ctl).prop("href");

            swal({
                title: "Are you sure you want to delete this Record?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: true
            },

                function (isConfirm) {
                    if (isConfirm) {
                        // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        eval(defaultAction);

                        //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        return true;
                    } else {
                        // swal("Cancelled", "Your imaginary file is safe :)", "error");
                        return false;
                    }
                });
        }
    </script>

    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Lead</h5>
        <div id="tdExport" class="pull-right" runat="server">
        </div>
    </div>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }
    </script>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');

        }
        function endrequesthandler(sender, args) {

        }

        function pageLoadedpro() {

            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });
            $(".myval1").select2({
                minimumResultsForSearch: -1
            });


            $("[data-toggle=tooltip]").tooltip();
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
        </ContentTemplate>

    </asp:UpdatePanel>

    <div class="page-body padtopzero">
        <%--<div class="topfileuploadbox marbtm15">
            <div class="widget-body shadownone brdrgray">
               
            </div>
        </div>--%>



        <asp:Panel runat="server" ID="PanGridSearch">

            <div class="animate-panelmessesgarea padbtmzero">
                <div class="messesgarea">

                    <div class="alert alert-danger" id="PanEmpty" runat="server" visible="false">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Please Enter Lead Source. </strong>
                    </div>

                </div>
            </div>

            <div class="searchfinal">
                <div class="widget-body shadownone brdrgray">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div class="dataTables_filter">
                            <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                    <tr>
                                        <td>
                                            <div class="inlineblock martop5">
                                                <div>
                                                    <div class="input-group col-sm-2" style="width: 150px;">
                                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                                                            WatermarkText="Company Name" />
                                                    </div>
                                                    <div class="input-group col-sm-1" id="divCustomer" runat="server">
                                                        <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                            <asp:ListItem Value="">State</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1" id="div1" runat="server" style="display: none">
                                                        <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Team</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="input-group col-sm-1 martop5" style="display: none">
                                                        <asp:DropDownList ID="ddlEmpCategory" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalemployee">
                                                            <asp:ListItem Value="" Text="Select Emp Category"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="input-group col-sm-2" id="div2" runat="server" style="width: 150px;">
                                                        <asp:DropDownList ID="ddlSource" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Source</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="input-group col-sm-2" id="div6" runat="server" style="width: 150px;">
                                                        <asp:DropDownList ID="ddlsubsource" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Sub Source</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="input-group col-sm-1" id="div3" runat="server">
                                                        <asp:DropDownList ID="ddlDup" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="0">Select DB Dup</asp:ListItem>
                                                            <asp:ListItem Value="1">Dup</asp:ListItem>
                                                            <asp:ListItem Value="2">Not Dup</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1" id="div7" runat="server">
                                                        <asp:DropDownList ID="ddlDelete" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="0">Select WebDup</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="2">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group col-sm-1" id="div8" runat="server" style="display: none">
                                                        <asp:DropDownList ID="ddlUpload" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Select Upload</asp:ListItem>
                                                            <asp:ListItem Value="1">Yes</asp:ListItem>
                                                            <asp:ListItem Value="0">No</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group date datetimepicker1 col-sm-1">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                    <div class="input-group date datetimepicker1 col-sm-1">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                        <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                    </div>
                                                    <div class="input-group">
                                                        <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-toggle="tooltip" data-placement="left"
                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:LinkButton ID="lbtnExport" CssClass="btn btn-success btn-xs Excel" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                                            CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel</asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="datashowbox">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="dataTables_length showdata ">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myval">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                <asp:Button ID="btnNULLData1" Style="display: none;" Text="test" runat="server" />
                <cc1:ModalPopupExtender ID="MPECustomerName" runat="server" BackgroundCssClass="modalbackground"
                    DropShadow="false" PopupControlID="divCustomerName" TargetControlID="btnNULLData1"
                    CancelControlID="btncanelCustomer">
                </cc1:ModalPopupExtender>

                <div id="divCustomerName" runat="server" style="display: none; width: 1000px;" class="modal_popup">
                    <div>
                        <div class="modal-content">
                            <div class="color-line"></div>
                            <div class="modal-header text-center">
                                <%--<asp:LinkButton ID="btncanelCustomer" CssClass="close" runat="server"><span aria-hidden="true">
                        &times;</span><span class="sr-only">Close</span></asp:LinkButton>--%>
                                <button id="btncanelCustomer" runat="server" type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;
                                    </span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Duplicate Record</h4>
                            </div>
                            <asp:HiddenField ID="hdnWebDownloadID" runat="server" Value='<%# Eval("WebDownloadID")%>' />
                            <div class="panel-body">
                                <asp:GridView ID="DataList1" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="DataList1_PageIndexChanging" AutoGenerateColumns="false"
                                    CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-CssClass="gridpagination" AllowSorting="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Customer" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("Customer")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Phone" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("CustPhone")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("ContMobile")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="120px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <%# Eval("ContEmail")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Employee Name" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("EmployeeName")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Source" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("sourcename")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("projectstatus")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="150px" HorizontalAlign="Left" />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Address" HeaderStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%# Eval("Address")%>
                                            </ItemTemplate>
                                            <ItemStyle Width="100px" HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>


                <asp:Button ID="btnNULLData2" Style="display: none;" Text="test" runat="server" />
                <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                    DropShadow="false" PopupControlID="divwebdup" TargetControlID="btnNULLData2"
                    CancelControlID="btncanelCustomer1">
                </cc1:ModalPopupExtender>
                <div id="divwebdup" runat="server" style="display: none; width: 1200px;" class="modal_popup">
                    <div>
                        <div class="modal-content">
                            <div class="color-line"></div>
                            <div class="modal-header">
                                <div style="float: right">
                                    <button id="btncanelCustomer1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">
                                        Close</button>
                                </div>
                                <h4 class="modal-title" id="H1">Duplicate Record</h4>
                            </div>
                            <asp:HiddenField ID="hdnwebdup" runat="server" />
                            <div class="panel-body">
                                <asp:Panel runat="server" ID="PanLead">
                                    <div class="row finalgrid">
                                        <div class="col-md-12" runat="server" id="divpl" style="width: 100%">
                                            <table cellpadding="0" id="tblPL" cellspacing="0" border="0" class="table table-striped table-bordered quotetable table_WrapContent"
                                                style="overflow-x: scroll">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center; width:130px;">Customer
                                                        </th>
                                                        <th style="text-align: center; width:100px;">Phone
                                                        </th>
                                                        <th style="text-align: center; width:100px;">Mobile
                                                        </th>
                                                        <th style="text-align: center; width:200px;">Address
                                                        </th>
                                                        <th style="text-align: center; width:70px;">City  
                                                        </th>
                                                        <th style="text-align: center; width:50px;">State  
                                                        </th>
                                                        <th style="text-align: center; width:50px;">PostCode
                                                        </th>
                                                        <th style="text-align: center; width:100px;">Source
                                                        </th>
                                                        <th style="text-align: center; width:100px;">Sub Source
                                                        </th>
                                                        <th style="text-align: center; width:100px;">Lead Date
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <asp:Repeater ID="rptLead" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="text-align: center; width:130px;">
                                                                <%#Eval("Customer")%>
                                                            </td>

                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("CustPhone")%>
                                                            </td>

                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("ContMobile")%>
                                                            </td>

                                                            <td style="text-align: center; width:200px;">
                                                                <%#Eval("Address")%>
                                                            </td>

                                                            <td style="text-align: center; width:80px;">
                                                                <%#Eval("City")%>
                                                            </td>

                                                            <td style="text-align: center; width:50px;">
                                                                <%#Eval("State")%>
                                                            </td>

                                                            <td style="text-align: center; width:50px;">
                                                                <%#Eval("PostCode")%>
                                                            </td>

                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("Source")%>
                                                            </td>
                                                            
                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("SubSource")%>
                                                            </td>

                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("EntryDate", "{0:dd/MM/yyyy}")%>
                                                            </td>

                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
            
                <asp:Button ID="Button1" Style="display: none;" Text="test" runat="server" />
                <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                    DropShadow="false" PopupControlID="div4" TargetControlID="Button1"
                    CancelControlID="Button2">
                </cc1:ModalPopupExtender>
                <div id="div4" runat="server" style="display: none; width: 1200px;" class="modal_popup">
                    <div>
                        <div class="modal-content">
                            <div class="color-line"></div>
                            <div class="modal-header">
                                <div style="float: right">
                                    <button id="Button2" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">
                                        Close</button>
                                </div>
                                <h4 class="modal-title" id="H1">Updated Record</h4>
                            </div>
                            <div class="panel-body">
                                <asp:Panel runat="server" ID="Panel2">
                                    <div class="row finalgrid">
                                        <div class="col-md-12" runat="server" id="DivUploadedLead" style="width: 100%">
                                            <table cellpadding="0" id="tblUploadedLead" cellspacing="0" border="0" class="table table-striped table-bordered quotetable table_WrapContent"
                                                style="overflow-x: scroll">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center; width:130px;">Customer
                                                        </th>
                                                        <th style="text-align: center; width:100px;">Phone
                                                        </th>
                                                        <th style="text-align: center; width:100px;">Mobile
                                                        </th>
                                                        <th style="text-align: center; width:200px;">Email
                                                        </th>
                                                        <th style="text-align: center; width:100px;">Emp Name  
                                                        </th>
                                                        <th style="text-align: center; width:100px;">Source  
                                                        </th>
                                                        <th style="text-align: center; width:100px;">SubSource
                                                        </th>
                                                        <th style="text-align: center; width:100px;">Lead Status
                                                        </th>
                                                        <th style="text-align: center; width:100px;">Project Num
                                                        </th>
                                                        <th style="text-align: center; width:100px;">Project Status
                                                        </th>
                                                        <th style="text-align: center; width:100px;">FollupDate
                                                        </th>
                                                        <th style="text-align: center; width:100px;">Description
                                                        </th>
                                                        <th style="text-align: center; width:100px;">Lead Date
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <asp:Repeater ID="RptUploadedLead" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="text-align: center; width:130px;">
                                                                <%#Eval("Customer")%>
                                                            </td>

                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("ContPhone")%>
                                                            </td>

                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("ContMobile")%>
                                                            </td>

                                                            <td style="text-align: center; width:200px;">
                                                                <%#Eval("ContEmail")%>
                                                            </td>

                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("EmpName")%>
                                                            </td>

                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("CustSource")%>
                                                            </td>

                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("CustSourceSub")%>
                                                            </td>

                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("CustLeadStatus")%>
                                                            </td>

                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("ProjectNumber")%>
                                                            </td>
                                                            
                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("ProjectStatus")%>
                                                            </td>
                                                            
                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("NextFollowupDate", "{0:dd/MM/yyyy}")%>
                                                            </td>
                                                            
                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("Description")%>
                                                            </td>
                                                            
                                                            <td style="text-align: center; width:100px;">
                                                                <%#Eval("LeadDate", "{0:dd/MM/yyyy}")%>
                                                            </td>

                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </asp:Panel>
        <asp:Panel ID="panel" runat="server">
            <div class="searchfinal">
                <div class="widget-body shadownone brdrgray">
                    <div class="printorder" style="font-size: medium">
                        <b>Total:&nbsp;</b><asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>&nbsp;&nbsp;
                                <b>Facebook:&nbsp;</b><asp:Literal ID="lblfb" runat="server"></asp:Literal>&nbsp;&nbsp;
                                 <b>Online:&nbsp;</b><asp:Literal ID="Literal1" runat="server"></asp:Literal>&nbsp;&nbsp;
                                <b>Others:&nbsp;</b><asp:Literal ID="Literal2" runat="server"></asp:Literal>

                    </div>
                </div>
            </div>

        </asp:Panel>

        <div class="finalgrid">
            <div class="padtopzero padrightzero">
                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                    <div id="PanGrid" runat="server">
                        <div class="table-responsive xscroll leadtablebox">
                            <asp:GridView ID="GridView1" DataKeyNames="WebDownloadID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnSelectedIndexChanging="GridView1_SelectedIndexChanging" OnRowCommand="GridView1_RowCommand"
                                OnDataBound="GridView1_DataBound" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25" OnRowCreated="GridView1_RowCreated">
                                <Columns>

                                    <asp:TemplateField HeaderText="Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="Customer">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Width="170px">
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("WebDownloadID")%>' />
                                                <asp:HiddenField ID="hdnSource" runat="server" Value='<%# Eval("Source")%>' />
                                                <asp:HiddenField ID="hdnSourceSub" runat="server" Value='<%# Eval("SubSource")%>' />
                                                <%#Eval("Customer")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="ContEmail">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnEmail" runat="server" Value='<%# Eval("ContEmail")%>' />
                                            <asp:Label ID="Label21898" runat="server" Width="200px">
                                                <%#Eval("ContEmail")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Phone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="CustPhone">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Width="100px">
                                                <%#Eval("CustPhone")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" SortExpression="ContMobile">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Width="100px">
                                                <asp:HiddenField ID="hdnMobile" runat="server" Value='<%# Eval("ContMobile")%>' />
                                                <%#Eval("ContMobile")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Address"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Width="150px" data-placement="top" data-original-title='<%#Eval("Address")%>' data-toggle="tooltip">
                                                <%#Eval("Address")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="City" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="City"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Width="100px">
                                                <%#Eval("City")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" SortExpression="State">
                                        <ItemTemplate>
                                            <asp:Label ID="Label7" runat="server" Width="50px">
                                                <%#Eval("State")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PostCode" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" SortExpression="PostCode">
                                        <ItemTemplate>
                                            <asp:Label ID="Label99" runat="server" Width="60px">
                                                <%#Eval("PostCode")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SubSource" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" SortExpression="SubSource">
                                        <ItemTemplate>
                                            <asp:Label ID="Label199" runat="server" Width="100px">
                                                <%#Eval("SubSource")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lead Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="EntryDate">
                                        <ItemTemplate>
                                            <asp:Label ID="Label21" runat="server" Width="200px">
                                                <%#Eval("EntryDate","{0:dd MMM yyyy}")%></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dup. Count" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" SortExpression="cnt">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCount" Enabled='<%# Eval("dbCount").ToString() != "0" ? true : false %>' CommandName="ViewDup"
                                                Text='<%# Bind("dbCount") %>' runat="server" CommandArgument='<%#Eval("WebDownloadID")%>'></asp:LinkButton>
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:LinkButton ID="lnkUploadCount" CommandName="UploadedDup"
                                                runat="server" CommandArgument='<%#Eval("WebDownloadID")%>'><i class="fa fa-info" aria-hidden="true"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle />
                                <PagerTemplate>
                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                    <div class="pagination">
                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                    </div>
                                </PagerTemplate>
                                <PagerStyle CssClass="paginationGrid" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                            </asp:GridView>
                        </div>
                        <div class="paginationnew1" runat="server" id="divnopage">
                            <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                <tr>
                                    <td>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>

                                        <div class="pagination paginationGrid">
                                            <asp:LinkButton ID="lnkfirst" runat="server" CausesValidation="false" CssClass="Linkbutton firstpage nextbtn" OnClick="lnkfirst_Click">First</asp:LinkButton>
                                            <asp:LinkButton ID="lnkprevious" runat="server" CausesValidation="false" CssClass="Linkbutton prebtn" OnClick="lnkprevious_Click">Previous</asp:LinkButton>
                                            <asp:Repeater runat="server" ID="rptpage" OnItemCommand="rptpage_ItemCommand">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="psotId" runat="server" Value='<%#Eval("ID") %>' />
                                                    <asp:LinkButton runat="server" ID="lnkpagebtn" CssClass="Linkbutton" CausesValidation="false" CommandArgument='<%#Eval("ID") %>' CommandName="Pagebtn"><%#Eval("ID") %></asp:LinkButton>

                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <asp:LinkButton ID="lnknext" runat="server" CausesValidation="false" CssClass="Linkbutton nextbtn" OnClick="lnknext_Click">Next</asp:LinkButton>
                                            <asp:LinkButton ID="lnklast" runat="server" CausesValidation="false" CssClass="Linkbutton lastpage nextbtn" OnClick="lnklast_Click">Last</asp:LinkButton>
                                        </div>
                                    </td>

                                </tr>
                            </table>
                        </div>
                        <%--<div style="float: right;" runat="server" visible="false">
                            <asp:LinkButton ID="lnkdelete" runat="server" CausesValidation="false" OnClick="lnkdelete_Click" CssClass="btn btn-info margin10">Delete</asp:LinkButton>
                        </div>--%>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <asp:HiddenField runat="server" ID="hdncountdata" />

    <script type="text/javascript">
        function pageLoad(sender, args) { if (!args.get_isPartialLoad()) { $addHandler(document, "keydown", onKeyDown); } }
        function onKeyDown(e) {
            if (e && e.keyCode == Sys.UI.Key.esc) {
                $find("<%=MPECustomerName.ClientID%>").hide();
                $find("<%=ModalPopupExtender1.ClientID%>").hide();
            }
        }
    </script>
    <%--   <script type="text/javascript">
        $(document).ready(function () {
            gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            $('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });
        }
    </script>--%>
    <style type="text/css">
        .selected_row {
            background-color: #A1DCF2 !important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=.] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView2] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=DataList1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=DataList1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

</asp:Content>

