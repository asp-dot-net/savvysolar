<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="custinstusers.aspx.cs" Inherits="admin_adminfiles_master_custinstusers" %>

<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                 function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                     $('.loading-container').removeClass('loading-inactive');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    $('.loading-container').addClass('loading-inactive');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();
                }
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $(".myvalcust").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                    
                     $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    //$('.i-checks').iCheck({
                    //    checkboxClass: 'icheckbox_square-green',
                    //    radioClass: 'iradio_square-green'
                    //});

                    $('.redreq').click(function () {
                        formValidate();
                    });
                }
            </script>
            
            <div class="page-body headertopbox">
              <h5 class="row-title"><i class="typcn typcn-th-small"></i>Cust/Inst Users</h5>
              <div id="hbreadcrumb" class="pull-right">                                
                                 <ol class="hbreadcrumb breadcrumb fontsize16" id="ol" runat="server" visible="false">
                                    <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" CssClass="btn btn-default purple"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBack" runat="server" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                                </ol>
                            </div>
               </div>

            <div class="finaladdupdate">
             <div id="PanAddUpdate" runat="server" visible="false">
                <div class="panel-body animate-panel padtopzero"> 
                <div class="well with-header with-footer addform">
                  	 <div class="header bordered-blue">                         
                          <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                                Cust/Inst Users
                      </div>
                      <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <asp:Label ID="Label1" runat="server" class="col-sm-2 control-label">
                                                <strong>Post Code</strong></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtPostCode" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                            <br />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required." ControlToValidate="txtPostCode"
                                                                Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" CssClass="reqerror" Display="Dynamic" ValidationExpression="^[0-9]*$"
                                                                ErrorMessage="Enter valid digit" ControlToValidate="txtPostCode"></asp:RegularExpressionValidator>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label3" runat="server" class="col-sm-2 control-label">
                                                <strong>Suburb</strong></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtSuburb" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                            <br />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="This value is required." ControlToValidate="txtSuburb"
                                                                Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>

                                                    <div class="form-group ">
                                                        <asp:Label ID="Label6" runat="server" class="col-sm-2 control-label">
                                                <strong>State</strong></asp:Label>
                                                        <asp:DropDownList ID="ddlState" runat="server" AppendDataBoundItems="true" Width="200px"
                                                            aria-controls="DataTables_Table_0" class="myvalcust">
                                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                            ControlToValidate="ddlState" Display="Dynamic"></asp:RequiredFieldValidator>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label4" runat="server" class="col-sm-2 control-label">
                                                <strong>PO Boxes</strong></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtPOBoxes" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                            <br />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label5" runat="server" class="col-sm-2 control-label">
                                                <strong>Area</strong></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtArea" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." ControlToValidate="txtArea"
                                                                Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>
                                                            <br />
                                                        </div>
                                                    </div>



                                                    <div class="form-group checkareanew">
                                                        <div class="col-sm-2 rightalign">
                                                            <asp:Label ID="Label2" runat="server" class="control-label">
                                                <strong>Is Active?</strong></asp:Label>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <asp:CheckBox ID="chkActive" runat="server" />
                                                            <label for="<%=chkActive.ClientID %>">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                        <div class="clear"></div>
                                                    </div>
                                                    <div class="hr-line-dashed"></div>
                                                    <div class="form-group">
                                                        <div class="col-sm-8 col-sm-offset-2">
                                                            <asp:Button CssClass="btn btn-primary" ID="btnAdd" runat="server"
                                                                Text="Add" />
                                                            <asp:Button CssClass="btn btn-primary savewhiteicon" ID="btnUpdate" runat="server"
                                                                Text="Save" Visible="false" />
                                                            <asp:Button CssClass="btn btn-default" ID="btnReset" runat="server"
                                                                CausesValidation="false" Text="Reset" />
                                                            <asp:Button CssClass="btn btn-default" ID="btnCancel" runat="server"
                                                                CausesValidation="false" Text="Cancel" />
                                                        </div>
                                                    </div>
                                                </div>
                    </div>
                 </div>
                 </div>
                 </div>

            <div class="page-body padtopzero"> 
            <asp:Panel runat="server" ID="PanGridSearch">             
                    <div class="animate-panelmessesgarea padbtmzero">
                        <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                        </div>
                    </div>

						<div class="searchfinal">
                         <div class="widget-body shadownone brdrgray"> 
                         <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                            <div class="dataTables_filter">
                            <table border="0" cellspacing="0" width="100%" style="text-align: right;" cellpadding="0">
                                                            <tr>
                                                                <td class="left-text dataTables_length showdata padtopzero"><asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="myval">
                                           <asp:ListItem Value="25">Show entries</asp:ListItem>
                                  </asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                    <div class="input-group">
                                                                        <asp:TextBox ID="txtSearch" runat="server" placeholder="UserName" CssClass="form-control m-b"></asp:TextBox>

                                                                        <%-- <span class="input-group-btn">
                                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />
                                                                </span>--%>
                                                                    </div>

                                                                    <div class="input-group col-sm-2">
                                                                        <asp:DropDownList ID="ddlsearchrole" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcust">
                                                                            <asp:ListItem Value="">Role</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group">
                                                                        <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-primary btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                            
                            </div>
                          </div>
                          </div>
                          </div>
                            
                            
                    <div class="finalgrid">
                                <div class="table-responsive printArea"> 
                                    <div id="PanGrid" runat="server">
                                                    <div class="table-responsive">
                                                        <asp:GridView ID="GridView1" DataKeyNames="UserID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand"
                                                            OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Center"
                                                                    ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                        <%#Eval("LastActivityDate","{0:dd MMM yyyy}")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="UserName" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                    ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <%#Eval("UserName")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Password" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                        <%#Eval("Password")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="RoleName" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                        <%#Eval("RoleName")%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <AlternatingRowStyle />

                                                            <PagerTemplate>
                                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                <div class="pagination">
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                                </div>
                                                            </PagerTemplate>
                                                            <PagerStyle CssClass="paginationGrid" />
                                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                        </asp:GridView>
                                                    </div>
                                                    <div class="paginationnew1" runat="server" id="divnopage">
                                                        <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                </div>
                            </div>
            </asp:Panel>
            </div>
            
            
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
