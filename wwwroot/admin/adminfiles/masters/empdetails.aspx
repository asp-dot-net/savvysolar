<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    Theme="admin" CodeFile="empdetails.aspx.cs" Inherits="admin_adminfiles_master_empdetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<div class="page-body headertopbox">
              <h5 class="row-title"><i class="typcn typcn-th-small"></i>Manage Employee</h5>
              <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb fontsize16">
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                    </ol>
                    </div>
               </div>  
               
                <div class="finaladdupdate"> 
                 <div id="PanAddUpdate" runat="server">
    <div class="panel-body animate-panel padtopzero"> 
             	   <div class="well with-header with-footer addform">
                  	 <div class="header bordered-blue"> 
                       Employee Detail</div>
                       
                       <div class="form-horizontal">
                                        <div class="form-group">
                                            <asp:Label ID="lblFirstName" runat="server" class="col-sm-2 control-label">
                                                <strong> First</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblfirst" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lblLastName" runat="server" class="col-sm-2 control-label">
                                                <strong>Last</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lbllast" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lbltitlename" runat="server" class="col-sm-2 control-label">
                                                <strong>Title</strong></asp:Label>
                                            <div class="col-md-3">
                                                <asp:Label ID="lbltitile" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lblinitialsname" runat="server" class="col-sm-2 control-label">
                                                <strong>Initials</strong></asp:Label>
                                            <div class="col-md-3">
                                                <asp:Label ID="lblinitials" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lblEmail" runat="server" class="col-sm-2 control-label">
                                                <strong>Email</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblEmpEmail" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lblmobile" runat="server" class="col-sm-2 control-label">
                                                <strong>Mobile</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblmobilenum" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lblphnno" runat="server" class="col-sm-2 control-label">
                                                <strong>Phone</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblphonenum" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="lblphnex" runat="server" class="col-sm-2 control-label">
                                                <strong>PhoneExtNo</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblphoneextno" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lblteam" runat="server" class="col-sm-2 control-label">
                                                <strong>Team OutDoor</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblteamoutdoor" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lblclose" runat="server" class="col-sm-2 control-label">
                                                <strong>Team Closer</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblclosers" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lblnic" runat="server" class="col-sm-2 control-label">
                                                <strong>Nic Name </strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblnicname" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="lblrolename" runat="server" class="col-sm-2 control-label">
                                                <strong>Role</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblrole" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label2" runat="server" class="col-sm-2 control-label">
                                                <strong>SalesTeam</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <span class="col-sm-6 control-label" Style="text-align: left;">
                                                    <asp:Repeater ID="rptTeam" runat="server">
                                                        <ItemTemplate><%# Eval("SalesTeam")%> </ItemTemplate>
                                                        <SeparatorTemplate>, </SeparatorTemplate>
                                                    </asp:Repeater>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lblemp" runat="server" class="col-sm-2 control-label">
                                                <strong>Emp Type </strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblemptype" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lblstatname" runat="server" class="col-sm-2 control-label">
                                                <strong>State</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblstate" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label11" runat="server" class="col-sm-2 control-label">
                                                <strong>UserName</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblusername" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label13" runat="server" class="col-sm-2 control-label">
                                                <strong>Location</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lbllocation" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label4" runat="server" class="col-sm-2 control-label">
                                                <strong>Start Time</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblstarttime" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label10" runat="server" class="col-sm-2 control-label">
                                                <strong>End Time</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblendtime" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label14" runat="server" class="col-sm-2 control-label">
                                                <strong>Break Time</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblbreaktime" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label16" runat="server" class="col-sm-2 control-label">
                                                <strong>Date Hired</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblhireddate" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lblinclude" runat="server" class="col-sm-2 control-label">
                                                <strong>Include in Lists</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblincludeinlist" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lblactive" runat="server" class="col-sm-2 control-label">
                                                <strong>Active Employee</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblactiveemployee" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lblshow" runat="server" class="col-sm-2 control-label">
                                                <strong>Show Excel</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblshowexcel" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label8" runat="server" class="col-sm-2 control-label">
                                                <strong>Info</strong></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:Label ID="lblinfo" Style="text-align: left;" runat="server" class="col-sm-2 control-label"></asp:Label>
                                            </div>
                                        </div>

                                    </div>
                      </div>
                   </div>
                   </div>
                   </div>
                          
                         
   
        
  
</asp:Content>

