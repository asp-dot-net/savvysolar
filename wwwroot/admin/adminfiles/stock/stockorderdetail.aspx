﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
     CodeFile="stockorderdetail.aspx.cs" Inherits="admin_adminfiles_stock_stockorderdetail" %>

<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="row m-b-md">
        <div class="col-sm-12 minhegihtarea">
            <h3 class="m-b-xs text-black">Stock Order Detail</h3>
            <div class="contactsarea">
                <div class="addcontent">
                    <asp:LinkButton ID="lbtnBack" runat="server" CausesValidation="false" OnClick="lbtnBack_Click"><img src="../../../images/btn_back.png" /></asp:LinkButton>
                </div>
                <div id="Div1" class="row" runat="server">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="5" class="tblDisplay widthPc tablebrd marginbtmsp20">
                                            <tr>
                                                <td width="200px" valign="top">Stock Location
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblStockLocation" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">Vendor
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblVendor" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">BOL Received
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblBOLReceived" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">Manual Order No
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblManualOrderNo" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">Expected Delevery
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblExpectedDelevery" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="200px" valign="top">Notes
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblNotes" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr id="trOrderItem" runat="server">
                                                <td colspan="2">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="5" class="tblDisplay widthPc tablebrd marginbtmsp20">
                                                        <tr class="graybgarea">
                                                            <td colspan="3">
                                                                <h4>Stock Order Item Detail</h4>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Stock Category</b></td>
                                                            <td><b>Quantity</b></td>
                                                            <td><b>Stock Item</b></td>
                                                        </tr>
                                                        <asp:Repeater ID="rptOrder" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%#Eval("StockOrderItem") %></td>
                                                                    <td><%#Eval("OrderQuantity") %></td>
                                                                    <td><%#Eval("StockItem") %></td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

