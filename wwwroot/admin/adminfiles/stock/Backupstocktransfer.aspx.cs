using System;
using System.Data;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_stocktransfer : System.Web.UI.Page
{
    protected string mode = "";
    protected string SiteURL;
    static DataView dv;
    static int MaxAttribute = 1;
    protected DataTable rptproduct;
    //protected string tqty;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        HidePanels();
        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindGrid(0);
            BindDropDown();
            MaxAttribute = 1;
            bindrepeaterBox();
            BindAddedAttributeBox();

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st1 = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        //    lblname.Text = st1.EmpFirst + " " + st1.EmpLast;
           // lblname.Text = "";

            ltTransfered.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToShortDateString()));

            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("WarehouseManager")))
            {
                lnkAdd.Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
            }
            else
            {
                lnkAdd.Visible = false;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
            }
        }
        //ScriptManager.RegisterStartupScript(UpdatePanel1, this.GetType(), "MyAction", "doMyAction();", true);
        BindScript();
    }
    //public void BindDropDown()
    //{
    //    DataTable dt = ClstblEmployees.tblEmployees_SelectAll();
    //    ddlemployee.DataSource = dt;
    //    ddlemployee.DataTextField = "fullname";
    //    ddlemployee.DataValueField = "EmployeeID";
    //    ddlemployee.DataBind();

    //    string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
    //    SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid2);

    //    ddlemployee.SelectedValue = st.EmployeeID;
    //    DataTable dt2 = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
    //    ddltransferfrom.DataSource = dt2;
    //    ddltransferfrom.DataTextField = "location";
    //    ddltransferfrom.DataValueField = "CompanyLocationID";
    //    ddltransferfrom.DataBind();

    //    ddltransferto.DataSource = dt2;
    //    ddltransferto.DataTextField = "location";
    //    ddltransferto.DataValueField = "CompanyLocationID";
    //    ddltransferto.DataBind();

    //    ddlsearchtransferfrom.DataSource = dt2;
    //    ddlsearchtransferfrom.DataTextField = "location";
    //    ddlsearchtransferfrom.DataValueField = "CompanyLocationID";
    //    ddlsearchtransferfrom.DataBind();

    //    ddlsearchtransferto.DataSource = dt2;
    //    ddlsearchtransferto.DataTextField = "location";
    //    ddlsearchtransferto.DataValueField = "CompanyLocationID";
    //    ddlsearchtransferto.DataBind();

    //    ddlby.DataSource = dt;
    //    ddlby.DataTextField = "fullname";
    //    ddlby.DataValueField = "EmployeeID";
    //    ddlby.DataBind();
    //}

    public void BindDropDown()
    {
        DataTable dt = ClstblEmployees.tblEmployees_SelectAll();
        ddlemployee.DataSource = dt;
        ddlemployee.DataTextField = "fullname";
        ddlemployee.DataValueField = "EmployeeID";
        ddlemployee.DataBind();
        if ((Roles.IsUserInRole("Administrator")))
        {
            ddlemployee.Enabled = true;
           
            string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
          
            DataTable dt_user = ClstblEmployees.tblEmployees_SelectAll_userselect(userid2);
          //  Response.Write(dt_user.Rows.Count);
            if (dt_user.Rows.Count > 0)
            {
               // Response.Write(dt_user.Rows[0]["EmployeeID"].ToString());
                ddlemployee.SelectedValue = dt_user.Rows[0]["EmployeeID"].ToString();

            }
        }
        else
        {
            ddlemployee.Enabled = false;
            string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            // Response.Write(userid2);
            DataTable dt_user = ClstblEmployees.tblEmployees_SelectAll_userselect(userid2);
            if (dt_user.Rows.Count > 0)
            {

                ddlemployee.SelectedValue = dt_user.Rows[0]["EmployeeID"].ToString();

            }
            //SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid2);
          //  ddlemployee.SelectedValue = st.EmployeeID;
        }
        DataTable dt2 = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddltransferfrom.DataSource = dt2;
        ddltransferfrom.DataTextField = "location";
        ddltransferfrom.DataValueField = "CompanyLocationID";
        ddltransferfrom.DataBind();

        ddltransferto.DataSource = dt2;
        ddltransferto.DataTextField = "location";
        ddltransferto.DataValueField = "CompanyLocationID";
        ddltransferto.DataBind();

        ddlsearchtransferfrom.DataSource = dt2;
        ddlsearchtransferfrom.DataTextField = "location";
        ddlsearchtransferfrom.DataValueField = "CompanyLocationID";
        ddlsearchtransferfrom.DataBind();

        ddlsearchtransferto.DataSource = dt2;
        ddlsearchtransferto.DataTextField = "location";
        ddlsearchtransferto.DataValueField = "CompanyLocationID";
        ddlsearchtransferto.DataBind();

        ddlby.DataSource = dt;
        ddlby.DataTextField = "fullname";
        ddlby.DataValueField = "EmployeeID";
        ddlby.DataBind();
    }
    protected DataTable GetGridData()
    {
        DataTable dt = new DataTable();

        dt = ClstblStockTransfers.tblStockTransfersGetDataBySearch(ddlby.SelectedValue.ToString(), ddlsearchtransferfrom.SelectedValue.ToString(), ddlsearchtransferto.SelectedValue.ToString(), ddlReceived.SelectedValue.ToString(), txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue);

        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue !="All")
            {
            if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
            {
                //========label Hide
                divnopage.Visible = false;
            }
            else
            {
                divnopage.Visible = true;
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
        }
		else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string Transfered = ltTransfered.Text;
        string employee = ddlemployee.SelectedValue.ToString();
        string transferfrom = ddltransferfrom.SelectedValue.ToString();
        string transferto = ddltransferto.SelectedValue.ToString();
        string tracking = txttracking.Text;
        string datereceived = txtdatereceived.Text;
        string note = txtnote.Text;
        string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid2);
        int success = 0;
        int existaddress = ClstblStockTransfers.tblStockTransfers_Exits_TrackingNumber(tracking);
        if (existaddress != 1)
        {
            success = ClstblStockTransfers.tblStockTransfers_Insert(transferfrom, transferto, Transfered, employee, datereceived,"", note, tracking);
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            foreach (RepeaterItem item in rptstock.Items)
            {
                TextBox txtqty = (TextBox)item.FindControl("txtqty");
                DropDownList ddlitem = (DropDownList)item.FindControl("ddlitem");
                DropDownList ddlcategory = (DropDownList)item.FindControl("ddlcategory");
                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
                string item1 = ddlitem.SelectedValue.ToString();
                string qty = txtqty.Text.Trim();

                //if (hdntype.Value == string.Empty)
                {
                    SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(success.ToString());
                    SttblStockItemsLocation stLocationFrom = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item1, st.LocationFrom);
                    SttblStockItemsLocation stLocationTo = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item1, st.LocationTo);

                    string qty1 = (Convert.ToInt32(stLocationFrom.StockQuantity) - (Convert.ToInt32(qty))).ToString();
                    //string qty2 = (Convert.ToInt32(stLocationTo.StockQuantity) + (Convert.ToInt32(qty))).ToString();

                    ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item1, st.LocationFrom, qty1);
                    //ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item1, st.LocationTo, qty2);

                    ClstblStockItems.tblStockItemInventoryHistory_Insert("3", item1, st.LocationFrom, stLocationFrom.StockQuantity, (0 - Convert.ToInt32(qty)).ToString(), userid, "0");
                    //ClstblStockItems.tblStockItemInventoryHistory_Insert("3", item1, st.LocationTo, stLocationTo.StockQuantity, qty, userid, "0");

                    int success1 = ClstblStockTransfers.tblStockTransferItems_Insert(success.ToString(), item1, qty);
                    //Response.Write(success1);
                }
            }
            //Response.End();
            if (success > 0)
            {
                ClstblStockTransfers.tblStockTransfers_UpdateTransferNumber(Convert.ToString(success));
                SetAdd();
            }
            else
            {
                SetError();
            }
            Reset();
        }
        else
        {
            PAnAddress.Visible = true;
            BindGrid(0);
            //lblexistame.Visible = true;
            //lblexistame.Text = "Record with this Address already exists ";
        }
        BindGrid(0);
        BindScript();
        ////--- do not chage this code end------
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        string Transfered = ltTransfered.Text;
        string employee = ddlemployee.SelectedValue.ToString();
        string transferfrom = ddltransferfrom.SelectedValue.ToString();
        string transferto = ddltransferto.SelectedValue.ToString();
        string tracking = txttracking.Text;
        string datereceived = txtdatereceived.Text;
        string note = txtnote.Text;
        string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid2);
        bool success = false;
        SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(id1);
        //int existaddress = ClstblStockTransfers.tblStockTransfers_Exits_TrackingNumber(tracking);
        //if (existaddress != 1)
        {
            success = ClstblStockTransfers.tblStockTransfers_Update(id1, transferfrom, transferto, Transfered, employee, txtdatereceived.Text, stemp.EmployeeID, note, tracking);
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            ClstblStockTransfers.tblStockTransferItems_DeletebyStockTransferID(id1);
            foreach (RepeaterItem item in rptstock.Items)
            {
                TextBox txtqty = (TextBox)item.FindControl("txtqty");
                DropDownList ddlitem = (DropDownList)item.FindControl("ddlitem");
                DropDownList ddlcategory = (DropDownList)item.FindControl("ddlcategory");
                HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
                string item1 = ddlitem.SelectedValue.ToString();
                string qty = txtqty.Text.Trim();

                if (hdntype.Value == string.Empty)
                {
                    SttblStockItemsLocation stLocationFrom = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item1, st.LocationFrom);
                    //SttblStockItemsLocation stLocationFrom = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item1, st.LocationFrom);
                    //SttblStockItemsLocation stLocationTo = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item1, st.LocationTo);

                    //string qty1 = (Convert.ToInt32(stLocationFrom.StockQuantity) - (Convert.ToInt32(qty))).ToString();
                    //string qty2 = (Convert.ToInt32(stLocationTo.StockQuantity) + (Convert.ToInt32(qty))).ToString();

                    //ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item1, st.LocationFrom, qty1);
                    //ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item1, st.LocationTo, qty2);
                    string qty1 = (Convert.ToInt32(stLocationFrom.StockQuantity) - (Convert.ToInt32(qty))).ToString();
                    ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item1, st.LocationFrom, qty1);
                    //ClstblStockItems.tblStockItemInventoryHistory_Insert("3", item1, st.LocationFrom, stLocationFrom.StockQuantity, (0 - Convert.ToInt32(qty)).ToString(), userid, "0");
                    //ClstblStockItems.tblStockItemInventoryHistory_Insert("3", item1, st.LocationTo, stLocationTo.StockQuantity, qty, userid, "0");

                    int success1 = ClstblStockTransfers.tblStockTransferItems_Insert(id1, item1, qty);
                    //Response.Write(success1);
                }
            }
            //Response.End();
            if (success)
            {
                ClstblStockTransfers.tblStockTransfers_UpdateTransferNumber(id1);
                SetUpdate();
            }
            else
            {
                SetError();
            }
            Reset();
            BindGrid(0);
        }
        //else
        //{
        //    PAnAddress.Visible = true;
        //    BindGrid(0);
        //    //lblexistame.Visible = true;
        //    //lblexistame.Text = "Record with this Address already exists ";
        //}
        BindScript();
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);
   
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        mode = "Edit";
        PanGrid.Visible = false;
            
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(id);
        //Response.Write(st.TransferBy);
        //Response.End();



        ltTransfered.Text = SiteConfiguration.ConvertToDateView(st.TransferDate);//st.TransferDate;
        try
        {

            ddlemployee.SelectedValue = st.TransferBy;
        }
        catch
        {
        }
        ddltransferfrom.SelectedValue = st.LocationFrom;
        try
        {
            ddltransferto.SelectedValue = st.LocationTo;
        }
        catch
        {
        }
        txttracking.Text = st.TrackingNumber;
        txtdatereceived.Text = st.ReceivedDate;
        txtnote.Text = st.TransferNotes;


        DataTable dt = ClstblStockTransfers.tblStockTransferItems_SelectByStockTransferID(id);
        //Response.Write(id);
        //Response.End();
        if (dt.Rows.Count > 0)
        {
            MaxAttribute = dt.Rows.Count;
            rptstock.DataSource = dt;
            rptstock.DataBind();
        }
        else
        {
            MaxAttribute = 1;
            bindrepeaterBox();
            BindAddedAttributeBox();
        }
        ////--- do not chage this code start------
        InitUpdate();
        //--- do not chage this code end------
        BindScript();
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
        BindScript();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
        BindScript();
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        Reset();
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        PanGrid.Visible = false;
        Reset();
        PanAddUpdate.Visible = true;
        string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        DataTable dt_user = ClstblEmployees.tblEmployees_SelectAll_userselect(userid2);
        //  Response.Write(dt_user.Rows.Count);
        if (dt_user.Rows.Count > 0)
        {
            // Response.Write(dt_user.Rows[0]["EmployeeID"].ToString());
            ddlemployee.SelectedValue = dt_user.Rows[0]["EmployeeID"].ToString();

        }
        //ModalPopupExtender2.Show();
        InitAdd();
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        PanAddUpdate.Visible = false;
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        HidePanels();
        BindGrid(0);
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;

        //lnkBack.Visible = false;
        //lnkAdd.Visible = true;
        ////ModalPopupExtender2.Hide();
        //PanAddUpdate.Visible = false;
        //Reset();
        //btnAdd.Visible = true;
        //btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = false;
        //lblAddUpdate.Visible = false;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;


        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;

        lblAddUpdate.Text = "Add ";
    }
    public void InitUpdate()
    {
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        //  HidePanels();
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;

        lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        PanAlreadExists.Visible = false;
     
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }
    public void Reset()
    {
        PanAddUpdate.Visible = true;
        string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid2);
        ddlemployee.SelectedValue = st.EmployeeID;
        ddltransferfrom.ClearSelection();
        ddlemployee.ClearSelection();
        ddltransferto.ClearSelection();
        txttracking.Text = string.Empty;
        txtdatereceived.Text = string.Empty;
        txtnote.Text = string.Empty;

        MaxAttribute = 1;
        bindrepeaterBox();
        BindAddedAttributeBox();

        BindScript();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindScript();
    }

    protected void ddltransferfrom_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddltransferto.Items.Remove(ddltransferto.Items.FindByValue(ddltransferfrom.SelectedValue.ToString()));
       // ModalPopupExtender2.Show();
        BindScript();
    }
    protected void ddltransferto_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddltransferfrom.Items.Remove(ddltransferfrom.Items.FindByValue(ddltransferto.SelectedValue.ToString()));
       // ModalPopupExtender2.Show();
        BindScript();
    }

    //protected void ddlitem_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (ddlitem.SelectedValue != "")
    //    {
    //        if (ddltransferfrom.SelectedValue != "")
    //        {
    //            SttblStockItemsLocation st = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(ddlitem.SelectedValue, ddltransferfrom.SelectedValue);
    //            txtfromqty.Text = st.StockQuantity;
    //        }
    //        if (ddltransferto.SelectedValue != "")
    //        {
    //            SttblStockItemsLocation st1 = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(ddlitem.SelectedValue, ddltransferto.SelectedValue);
    //            txttoqty.Text = st1.StockQuantity;
    //        }
    //    }
    //}

    //protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    ddlitem.Items.Clear();
    //    ListItem item = new ListItem();
    //    item.Value = "";
    //    item.Text = "Select";
    //    ddlitem.Items.Add(item);
    //    if (ddlcategory.SelectedValue != "")
    //    {
    //        DataTable dt = ClstblStockItems.tblStockItems_SelectbyAsc(ddlcategory.SelectedValue);
    //        ddlitem.DataSource = dt;
    //        ddlitem.DataTextField = "StockItem";
    //        ddlitem.DataValueField = "StockCode";
    //        ddlitem.DataBind();
    //    }
    //}

    //protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    //{
    //    ddlReceived.SelectedValue = "";
    //    ddlby.SelectedValue = "";
    //    ddlsearchtransferfrom.SelectedValue = "";
    //    ddlsearchtransferto.SelectedValue = "";
    //    ddlSearchDate.SelectedValue = "";
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;

    //    BindGrid(0);
    //    BindScript();
    //}
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "received")
        {
            string StockTransferID = e.CommandArgument.ToString();

            DataTable dtQty = ClstblStockTransfers.tblStockTransferItems_SelectQty(StockTransferID);
            string qty = "0";
            string item = "";
            if (dtQty.Rows.Count > 0)
            {
                for (int i = 0; i < dtQty.Rows.Count; i++)
                {
                    qty = dtQty.Rows[i]["TransferQty"].ToString();
                    item = dtQty.Rows[i]["StockCode"].ToString();

                    SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(StockTransferID);
                    SttblStockItemsLocation stLocationTo = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item, st.LocationTo);
                    string qty2 = (Convert.ToInt32(stLocationTo.StockQuantity) + (Convert.ToInt32(qty))).ToString();

                    ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item, st.LocationTo, qty2);
                }
            }
            ClstblStockTransfers.tblStockTransfers_UpdateReceived(StockTransferID);
        }

        if (e.CommandName.ToLower() == "print")
        {
            string StockTransferID = e.CommandArgument.ToString();
            Response.Redirect("~/admin/adminfiles/reports/transfer.aspx?id=" + StockTransferID);
        }
        if (e.CommandName.ToLower() == "PrintPage")
        {
            
        }
        BindGrid(0);
    }

    //protected void GridView1_DataBound(object sender, EventArgs e)
    //{
    //    GridViewRow gvrow = GridView1.BottomPagerRow;
    //    Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
    //    lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
    //    int[] page = new int[7];
    //    page[0] = GridView1.PageIndex - 2;
    //    page[1] = GridView1.PageIndex - 1;
    //    page[2] = GridView1.PageIndex;
    //    page[3] = GridView1.PageIndex + 1;
    //    page[4] = GridView1.PageIndex + 2;
    //    page[5] = GridView1.PageIndex + 3;
    //    page[6] = GridView1.PageIndex + 4;
    //    for (int i = 0; i < 7; i++)
    //    {
    //        if (i != 3)
    //        {
    //            if (page[i] < 1 || page[i] > GridView1.PageCount)
    //            {
    //                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
    //                lnkbtn.Visible = false;
    //            }
    //            else
    //            {
    //                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
    //                lnkbtn.Text = Convert.ToString(page[i]);
    //                lnkbtn.CommandName = "PageNo";
    //                lnkbtn.CommandArgument = lnkbtn.Text;

    //            }
    //        }
    //    }
    //    if (GridView1.PageIndex == 0)
    //    {
    //        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
    //        lnkbtn.Visible = false;
    //        lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
    //        lnkbtn.Visible = false;

    //    }
    //    if (GridView1.PageIndex == GridView1.PageCount - 1)
    //    {
    //        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
    //        lnkbtn.Visible = false;
    //        lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
    //        lnkbtn.Visible = false;

    //    }
    //    Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
    //    if (dv.ToTable().Rows.Count > 0)
    //    {
    //        int iTotalRecords = dv.ToTable().Rows.Count;
    //        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
    //        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
    //        if (iEndRecord > iTotalRecords)
    //        {
    //            iEndRecord = iTotalRecords;
    //        }
    //        if (iStartsRecods == 0)
    //        {
    //            iStartsRecods = 1;
    //        }
    //        if (iEndRecord == 0)
    //        {
    //            iEndRecord = iTotalRecords;
    //        }
    //        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
    //    }
    //    else
    //    {
    //        ltrPage.Text = "";
    //    }
    //}
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }
    //protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.Pager)
    //    {
    //        GridViewRow gvr = e.Row;
    //        LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p1");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p2");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p4");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p5");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //        lb = (LinkButton)gvr.Cells[0].FindControl("p6");
    //        lb.Command += new CommandEventHandler(lb_Command);
    //    }
    //}
    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);
    }
    protected void lnkReceived_Click(object sender, EventArgs e)
    {
        LinkButton lnkReceived = (LinkButton)sender;
        GridViewRow item1 = lnkReceived.NamingContainer as GridViewRow;
        HiddenField hdnstockid = (HiddenField)item1.FindControl("hdnstockid");
        
        DataTable dt = ClstblStockTransfers.tblStockTransferItems_SelectByStockTransferID(hdnstockid.Value);

        if (dt.Rows.Count > 0)
        {            
            hdnstocktransferid.Value = hdnstockid.Value;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
            H1.Visible = true;
            btnYes.Visible = true;
            btnNo.Visible = true;
            divdetail.Visible = true;
            rptstockdetail.DataSource = dt;
            rptstockdetail.DataBind();
            divdetailmsg.Visible = false;
        }
        else
        {
            divdetailmsg.Visible = true;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
        }

    }
    protected void btnYes_Click(object sender, EventArgs e)
    {
      
        if (hdnstocktransferid.Value != string.Empty)
        {
            string StockTransferID = hdnstocktransferid.Value;
         
            DataTable dtQty = ClstblStockTransfers.tblStockTransferItems_SelectQty(StockTransferID);
            string qty = "0";
            string item = "";
          
            if (dtQty.Rows.Count > 0)
            {
              
               
                for (int i = 0; i < dtQty.Rows.Count; i++)
                {
                   
                    qty = dtQty.Rows[i]["TransferQty"].ToString();
                    item = dtQty.Rows[i]["StockCode"].ToString();

                    SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(StockTransferID);
                    SttblStockItemsLocation stLocationTo = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item, st.LocationTo);
                    string qty2 = (Convert.ToInt32(stLocationTo.StockQuantity) + (Convert.ToInt32(qty))).ToString();

                   bool succ= ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item, st.LocationTo, qty2);
                 
                }
               
            }
            ClstblStockTransfers.tblStockTransfers_UpdateReceived(StockTransferID);
            string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid2);
           bool success = ClstblStockTransfers.tblStockTransfers_Update_receive(StockTransferID,SiteConfiguration.Getdate_current(), stemp.EmployeeID);
            
        }
        BindGrid(0);
        BindScript();
    }
    protected void btnNo_Click(object sender, EventArgs e)
    {
        hdnstocktransferid.Value = "";
        ModalPopupExtender1.Hide();
        divstockdetail.Visible = false;
        rptstockdetail.DataSource = null;
        rptstockdetail.DataBind();
        BindGrid(0);
        //BindScript();

        divdetailmsg.Visible = false;
        divdetail.Visible = false;
    }

    //==Repeater
    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*]{2})",
                        RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);

        return Convert.ToInt32(match.Value);
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptstock.Items[index];
        BindItem(item);
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        BindScript();
    }
    public void BindItem(RepeaterItem item)
    {
        DropDownList ddlcategory = (DropDownList)item.FindControl("ddlcategory");
        DropDownList ddlitem = (DropDownList)item.FindControl("ddlitem");

        ddlitem.Items.Clear();
        ListItem item1 = new ListItem();
        item1.Value = "";
        item1.Text = "Select";
        ddlitem.Items.Add(item1);

        //if (!string.IsNullOrEmpty(Request.QueryString["StockTransferID"]))
        {
            //string StockTransferID = Request.QueryString["StockTransferID"];
            //SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(StockTransferID);
            if (ddltransferfrom.SelectedValue != string.Empty && ddltransferto.SelectedValue != string.Empty)
            {
                if (ddlcategory.SelectedValue != "")
                {
                    DataTable dt = ClstblStockItems.tblStockItems_SelectByCategoryID_CompanyLocationID(ddlcategory.SelectedValue, ddltransferfrom.SelectedValue, ddltransferto.SelectedValue);
                    ddlitem.DataSource = dt;
                    ddlitem.DataTextField = "StockItem";
                    ddlitem.DataValueField = "StockCode";
                    ddlitem.DataBind();
                }
            }
        }
    }
    protected void rptstock_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        HiddenField hndcategory = (HiddenField)e.Item.FindControl("hndcategory");
        HiddenField hndid = (HiddenField)e.Item.FindControl("hndid");
        HiddenField hdnstockcode = (HiddenField)e.Item.FindControl("hdnstockcode");
        HiddenField hdnQty = (HiddenField)e.Item.FindControl("hdnQty");
        TextBox txtqty = (TextBox)e.Item.FindControl("txtqty");
        DropDownList ddlitem = (DropDownList)e.Item.FindControl("ddlitem");
        DropDownList ddlcategory = (DropDownList)e.Item.FindControl("ddlcategory");

        ddlcategory.Items.Clear();
        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlcategory.Items.Add(item1);

        DataTable dt = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlcategory.DataSource = dt;
        ddlcategory.DataTextField = "StockCategory";
        ddlcategory.DataValueField = "StockCategoryID";
        ddlcategory.DataBind();

        ddlcategory.SelectedValue = hndcategory.Value;

        BindItem(item);

        txtqty.Text = hdnQty.Value;

        if (mode == "Edit")
        {
            DataTable dt11 = ClstblStockItems.tblStockItems_Select();
            ddlitem.DataSource = dt11;
            ddlitem.DataTextField = "StockItem";
            ddlitem.DataValueField = "StockCode";
            ddlitem.DataBind();
            ddlitem.SelectedValue = hdnstockcode.Value;

            DataTable dtC = ClstblStockItems.tblStockItems_SelectByItemID_CompanyLocationID(ddlitem.SelectedValue, ddltransferfrom.SelectedValue, ddltransferto.SelectedValue);
         //   Response.Write(ddlitem.SelectedValue + "==" + ddltransferfrom.SelectedValue + "==" + ddltransferto.SelectedValue);
            ddlcategory.DataSource = dtC;
            ddlcategory.DataTextField = "StockCategory";
            ddlcategory.DataValueField = "StockCategoryID";
            ddlcategory.DataBind();
            try
            {
                // Response.Write(dtC.Rows[0]["StockCategoryID"].ToString());
                ddlcategory.SelectedValue = dtC.Rows[0]["StockCategoryID"].ToString();
            }
            catch
            {
            }
            BindItem(item);
        }
       // Response.End();
        ddlitem.SelectedValue = hdnstockcode.Value;



        ImageButton lbremove = (ImageButton)e.Item.FindControl("lbremove");
        if (MaxAttribute == 1)
        {
            lbremove.Visible = false;
        }

    }
    protected void BindAddedAttributeBox()
    {
        rptproduct = new DataTable();
        rptproduct.Columns.Add("StockCategoryID", Type.GetType("System.String"));
        rptproduct.Columns.Add("StockCode", Type.GetType("System.String"));
        rptproduct.Columns.Add("TransferQty", Type.GetType("System.String"));
        rptproduct.Columns.Add("StockTransferItemsID", Type.GetType("System.String"));
        rptproduct.Columns.Add("type", Type.GetType("System.String"));

        foreach (RepeaterItem item in rptstock.Items)
        {
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
            HiddenField hndcategory = (HiddenField)item.FindControl("hndcategory");
            HiddenField hndid = (HiddenField)item.FindControl("hndid");
            HiddenField hdnstockcode = (HiddenField)item.FindControl("hdnstockcode");
            HiddenField hdnQty = (HiddenField)item.FindControl("hdnQty");
            TextBox txtqty = (TextBox)item.FindControl("txtqty");
            DropDownList ddlitem = (DropDownList)item.FindControl("ddlitem");
            DropDownList ddlcategory = (DropDownList)item.FindControl("ddlcategory");

            DataRow dr = rptproduct.NewRow();
            dr["StockCategoryID"] = ddlcategory.SelectedValue;
            dr["StockCode"] = ddlitem.SelectedValue;
            dr["TransferQty"] = txtqty.Text;
            dr["type"] = hdntype.Value;
            rptproduct.Rows.Add(dr);
        }
    }
    protected void bindrepeaterBox()
    {
        if (rptproduct == null)
        {
            rptproduct = new DataTable();
            rptproduct.Columns.Add("StockCategoryID", Type.GetType("System.String"));
            rptproduct.Columns.Add("StockCode", Type.GetType("System.String"));
            rptproduct.Columns.Add("TransferQty", Type.GetType("System.String"));
            rptproduct.Columns.Add("StockTransferItemsID", Type.GetType("System.String"));
            rptproduct.Columns.Add("type", Type.GetType("System.String"));
        }

        for (int i = rptproduct.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rptproduct.NewRow();

            dr["StockCategoryID"] = "";
            dr["StockCode"] = "";
            dr["TransferQty"] = "";
            dr["type"] = "";
            rptproduct.Rows.Add(dr);
        }

        rptstock.DataSource = rptproduct;
        rptstock.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in rptstock.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            ImageButton lbremove1 = (ImageButton)item1.FindControl("lbremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptstock.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptstock.Items)
                {
                    ImageButton lbremove = (ImageButton)item2.FindControl("lbremove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }
    protected void btnAddBoxRow_Click(object sender, EventArgs e)
    {
        foreach (RepeaterItem item in rptstock.Items)
        {
            HiddenField hndcategory = (HiddenField)item.FindControl("hndcategory");
            HiddenField hndid = (HiddenField)item.FindControl("hndid");
            HiddenField hdnstockcode = (HiddenField)item.FindControl("hdnstockcode");
            HiddenField hdnQty = (HiddenField)item.FindControl("hdnQty");
            TextBox txtqty = (TextBox)item.FindControl("txtqty");

            DropDownList ddlitem = (DropDownList)item.FindControl("ddlitem");
            DropDownList ddlcategory = (DropDownList)item.FindControl("ddlcategory");
            HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

            hndcategory.Value = ddlcategory.SelectedValue;
            hdnstockcode.Value = ddlitem.SelectedValue;
            hdnQty.Value = txtqty.Text;
        }
        AddmoreAttributeBox();
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
    }
    protected void AddmoreAttributeBox()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttributeBox();
        bindrepeaterBox();
    }
    protected void lbremove_Click(object sender, EventArgs e)
    {

        int index = GetControlIndex(((ImageButton)sender).ClientID);
        RepeaterItem item = rptstock.Items[index];
        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");

        hdntype.Value = "1";
        int y = 0;

        foreach (RepeaterItem item1 in rptstock.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            ImageButton lbremove1 = (ImageButton)item1.FindControl("lbremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptstock.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptstock.Items)
                {
                    ImageButton lbremove = (ImageButton)item2.FindControl("lbremove");
                    lbremove.Visible = false;
                }
            }
        }
        PanAddUpdate.Visible = true;
       // ModalPopupExtender2.Show();
        divstockdetail.Visible = true;
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        hdnstocktransferid.Value = "";
        ModalPopupExtender1.Hide();
        divstockdetail.Visible = false;
        rptstockdetail.DataSource = null;
        rptstockdetail.DataBind();
        BindGrid(0);
        //BindScript();

        divdetailmsg.Visible = false;
        divdetail.Visible = false;
        Response.Redirect(Request.Url.PathAndQuery);
    }
    protected void txttracking_TextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
       // ModalPopupExtender2.Show();
      //  myModal.Visible = true;
        txttracking.Focus();

        if (txttracking.Text.Trim() != string.Empty)
        {
            int existaddress = ClstblStockTransfers.tblStockTransfers_Exits_TrackingNumber(txttracking.Text.Trim());
            if (existaddress == 1)
            {
                ModalPopupExtenderTracker.Show();
                DataTable dt = ClstblStockTransfers.tblStockTransfers_SelectExits_TrackingNumber(txttracking.Text.Trim());
                GridView2.DataSource = dt;
                GridView2.DataBind();
            }
        }

        BindScript();
    }

    protected void gvbtnPrint_Click(object sender, EventArgs e)
    {
       // Response.End();
        LinkButton gvbtnPrint = (LinkButton)sender;
        GridViewRow item1 = gvbtnPrint.NamingContainer as GridViewRow;
        HiddenField hdnstockid = (HiddenField)item1.FindControl("hdnstockid");

        string id = hdnstockid.Value;
        
        SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(id);
        lblFrom.Text = st.FromLocation;
        lblTo.Text = st.ToLocation;
        try
        {
            lblTransferDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.TransferDate));
        }
        catch { }
        lblTransferBy.Text = st.TransferByName;

        lblTrackingNo.Text = st.TrackingNumber;
        try
        {
            lblReceivedDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.ReceivedDate));
        }
        catch { }
        lblReceivedBy.Text = st.ReceivedByName;
        lblNotes.Text = st.TransferNotes;

        DataTable dt = ClstblStockTransfers.tblStockTransferItems_SelectByStockTransferID(id);
        if (dt.Rows.Count > 0)
        {
            rptItems.DataSource = dt;
            rptItems.DataBind();

            int qty = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["TransferQty"].ToString() != string.Empty)
                {
                    qty += Convert.ToInt32(dr["TransferQty"].ToString());
                }
            }
            lbltotalqty.Text = qty.ToString();
        }
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>alert('');</script>");
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>javascript:printContent();</script>");
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
        BindGrid(0);
    }
    //protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    dt = dv.ToTable();

    //    GridViewSortExpression = e.SortExpression;
    //    GridView1.DataSource = SortDataTable(dt, false);
    //    GridView1.DataBind();
    //}
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlReceived.SelectedValue = "";
        ddlby.SelectedValue = "";
        ddlsearchtransferfrom.SelectedValue = "";
        ddlsearchtransferto.SelectedValue = "";
        ddlSearchDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        BindGrid(0);
        BindScript();
    }
    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    //protected void ibtnCancel_Click(object sender, EventArgs e)
    //{
    //    ModalPopupExtenderTracker.Hide();

    //}
    protected void ibtnCancel_Click1(object sender, EventArgs e)
    {
       // Response.End();
      //  ModalPopupExtenderTracker.Hide();
    }
    protected void ibtnCancelActive_Click(object sender, EventArgs e)
    {
         ModalPopupExtenderTracker.Hide();
    
    }
    protected void lnkrevert_Click(object sender, EventArgs e)
    {
        LinkButton lnkrevert = (LinkButton)sender;
        GridViewRow item1 = lnkrevert.NamingContainer as GridViewRow;
        HiddenField hdnstockid = (HiddenField)item1.FindControl("hdnstockid");

        DataTable dt = ClstblStockTransfers.tblStockTransferItems_SelectByStockTransferID(hdnstockid.Value);
        if (dt.Rows.Count > 0)
        {
            hdnstocktransferid.Value = hdnstockid.Value;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
            H3.Visible = true;
            btnReturn.Visible = true;
            btntansite.Visible = true;
            divdetail.Visible = true;
            H1.Visible = false;
            btnYes.Visible = false;
            btnNo.Visible = false;
            rptstockdetail.DataSource = dt;
            rptstockdetail.DataBind();
            divdetailmsg.Visible = false;
        }
        else
        {
            divdetailmsg.Visible = true;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
        }
      
    }
    protected void btn_yes_Click(object sender, EventArgs e)
    {

    }
    protected void btntansite_Click(object sender, EventArgs e)
    {

            if (hdnstocktransferid.Value != string.Empty)
            {
                string StockTransferID = hdnstocktransferid.Value;

                DataTable dtQty = ClstblStockTransfers.tblStockTransferItems_SelectQty(StockTransferID);
                string qty = "0";
                string item = "";
                if (dtQty.Rows.Count > 0)
                {
                    for (int i = 0; i < dtQty.Rows.Count; i++)
                    {
                        qty = dtQty.Rows[i]["TransferQty"].ToString();
                        item = dtQty.Rows[i]["StockCode"].ToString();

                        SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(StockTransferID);
                        SttblStockItemsLocation stLocationTo = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item, st.LocationTo);
                        string qty2 = (Convert.ToInt32(stLocationTo.StockQuantity) - (Convert.ToInt32(qty))).ToString();

                        ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item, st.LocationTo, qty2);
                    }
                }
                ClstblStockTransfers.tblStockTransfers_UpdateReceived_revert(StockTransferID);
            }
      
        BindGrid(0);
        BindScript();
    }
    protected void btnReturn_Click(object sender, EventArgs e)
    {
        if (hdnstocktransferid.Value != string.Empty)
        {
            string StockTransferID = hdnstocktransferid.Value;

            DataTable dtQty = ClstblStockTransfers.tblStockTransferItems_SelectQty(StockTransferID);
            string qty = "0";
            string item = "";
            if (dtQty.Rows.Count > 0)
            {
                for (int i = 0; i < dtQty.Rows.Count; i++)
                {
                    qty = dtQty.Rows[i]["TransferQty"].ToString();
                    item = dtQty.Rows[i]["StockCode"].ToString();

                    SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(StockTransferID);
                    SttblStockItemsLocation stLocationTo = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item, st.LocationTo);
                    string qty2 = (Convert.ToInt32(stLocationTo.StockQuantity) - (Convert.ToInt32(qty))).ToString();

                    ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item, st.LocationTo, qty2);

                    SttblStockItemsLocation stLocationfrom = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(item, st.LocationFrom);
                    string qty3 = (Convert.ToInt32(stLocationfrom.StockQuantity) + (Convert.ToInt32(qty))).ToString();

                    ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(item, st.LocationFrom, qty3);
                }
            }
            ClstblStockTransfers.tblStockTransfers_UpdateReceived_revert(StockTransferID);
        }

        BindGrid(0);
        BindScript();
    }
    protected void btnDupeMobile_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderTracker.Hide();
        HidePanels();
        txttracking.Text = string.Empty;
    }
    protected void btnNotDupeMobile_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderTracker.Hide();
        DupButton();
        txtdatereceived.Focus();
        BindScript();
    }
    public void DupButton()
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //PanSearch.Visible = false;
        PanGrid.Visible = false;
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(id);
        DataTable dt = ClstblStockTransfers.tblStockTransferItems_SelectByStockTransferID(id);
        foreach (DataRow dr in dt.Rows)
        {

            string item1 = dr["StockItem"].ToString();
            string qty = dr["TransferQty"].ToString();

            if (item1 != string.Empty && qty != string.Empty)
            {
                //Response.Write(item1+"=="+ st.LocationFrom);
                SttblStockItemsLocation stLocationFrom = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(dr["StockCode"].ToString(), st.LocationFrom);

                string qty1 = (Convert.ToInt32(stLocationFrom.StockQuantity) + (Convert.ToInt32(qty))).ToString();

                ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(dr["StockCode"].ToString(), st.LocationFrom, qty1);

                //int success1 = ClstblStockTransfers.tblStockTransferItems_Insert(id, dr["StockCode"].ToString(), qty);
            }
        }
       bool success= ClstblStockTransfers.tblStockTransferItems_DeletebyStockTransferID(id);
        bool success1=ClstblStockTransfers.tblStockTransfers_Delete(id);
        if (success && success1)
        {
            SetDelete();
        }
        else
        {
            SetError1();
        }
        GridView1.EditIndex = -1;
        BindGrid(0);

        //ddlhead.Items.Clear();
        //ListItem item = new ListItem();
        //item.Value = "";
        //item.Text = "Select";
        //ddlhead.Items.Add(item);

        //DataTable dt_head = ClstblStockItems.tblStockItemHeadMaster_Select_StockQuantity();
        //ddlhead.DataSource = dt_head;
        //ddlhead.DataTextField = "HeadName";
        //ddlhead.DataValueField = "id";
        //ddlhead.DataBind();

        //MPEUpdateStock.Show();
        //hdnid.Value = id;
        //SttblStockItemsLocation st = ClstblStockItemsLocation.tblStockItemsLocation_SelectByid(id);
        //ltstockquantity.Text = st.StockQuantity;
        //ltlocation.Text = st.CompanyLocation;
        //string StockItemID = st.StockItemID;

        //if (st.CompanyLocationID == "12")
        //{
        //   ddlhead.Items.Remove(ddlhead.Items.FindByValue("2"));
        //}

        //SttblStockItems stStockItem = ClstblStockItems.tblStockItems_SelectByStockItemID(StockItemID);
        //ltcategory.Text = stStockItem.StockCategory;
        //ltstockitem.Text = stStockItem.StockItem;
        //ltbrand.Text = stStockItem.StockManufacturer;
        //ltmodel.Text = stStockItem.StockModel;
        //ltseries.Text = stStockItem.StockSeries;

        //txtquantity.Text = "";
        //ddlhead.SelectedValue = "";
    }
}