using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_stockin : System.Web.UI.Page
{

    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    static DataView dv2;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            ddlSelectRecords2.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords2.DataBind();

            DataTable dt = ClstblContacts.tblCustType_SelectVender();


            BindGrid(0);
            BindGrid2(0);


            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("WarehouseManager")) || (Roles.IsUserInRole("Installation Manager")) || (Roles.IsUserInRole("PreInstaller") || (Roles.IsUserInRole("PostInstaller"))))
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
                GridView2.Columns[GridView2.Columns.Count - 1].Visible = true;
            }
            else
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
                GridView2.Columns[GridView2.Columns.Count - 1].Visible = false;
            }

            BindDropDown();

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                //tdExport.Visible = true;
            }
            else
            {
                // tdExport.Visible = false;
            }
        }
    }

    public void BindDropDown()
    {
        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

        ddlSearchState2.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState2.DataMember = "State";
        ddlSearchState2.DataTextField = "State";
        ddlSearchState2.DataValueField = "State";
        ddlSearchState2.DataBind();

        ddllocationsearch.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch.DataTextField = "location";
        ddllocationsearch.DataValueField = "CompanyLocationID";
        ddllocationsearch.DataMember = "CompanyLocationID";
        ddllocationsearch.DataBind();

        ddllocationsearch2.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch2.DataTextField = "location";
        ddllocationsearch2.DataValueField = "CompanyLocationID";
        ddllocationsearch2.DataMember = "CompanyLocationID";
        ddllocationsearch2.DataBind();

        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        lstSearchStatus.DataBind();
    }
    protected DataTable GetGridData1()
    {
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        DataTable dt1 = ClstblProjects.tblProjects_SelectWarehouseAllocated("1", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddllocationsearch.SelectedValue, selectedItem);
        return dt1;
    }

    protected DataTable GetGridData2()
    {
        DataTable dt4 = new DataTable();
        dt4 = ClstblProjects.tblProjects_StockRevert("1", txtSearch2.Text, txtProjectNumber2.Text, txtSerachCity2.Text, ddlSearchState2.SelectedValue, txtStartDate2.Text, txtEndDate2.Text, ddlSearchDate2.SelectedValue, ddllocationsearch2.SelectedValue, ddlRevert.SelectedValue);
        return dt4;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt1 = new DataTable();
        dt1 = GetGridData1();
        dv = new DataView(dt1);

        if (dt1.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {          
            GridView1.DataSource = dt1;
            GridView1.DataBind();
            PanGrid.Visible = true;
            PanNoRecord.Visible = false;
            divnopage.Visible = true;
            if (dt1.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt1.Rows.Count)
                {
                    //========label Hide
                     divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt1.Rows.Count + " entries";
                }
            }
        }
    }

    public void BindGrid2(int deleteFlag)
    {
        DataTable dt2 = new DataTable();
        dt2 = GetGridData2();
        dv2 = new DataView(dt2);

        if (dt2.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid2.Visible = false;
            divnopage2.Visible = false;
        }
        else
        {
            GridView2.DataSource = dt2;
            GridView2.DataBind();
            PanGrid2.Visible = true;
            PanNoRecord.Visible = false;
            divnopage2.Visible = true;
            if (dt2.Rows.Count > 0 && ddlSelectRecords2.SelectedValue != string.Empty && ddlSelectRecords2.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords2.SelectedValue) < dt2.Rows.Count)
                {
                    //========label Hide
                    divnopage2.Visible = false;
                }
                else
                {
                    divnopage2.Visible = true;
                    int iTotalRecords = dv2.ToTable().Rows.Count;
                    int iEndRecord = GridView2.PageSize * (GridView2.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView2.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage2.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords2.SelectedValue == "All")
                {
                    divnopage2.Visible = true;
                    ltrPage2.Text = "Showing " + dt2.Rows.Count + " entries";
                }
            }
        }
    }


        protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void ddlSelectRecords2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords2.SelectedValue) == "All")
        {
            GridView2.AllowPaging = false;
            BindGrid2(0);
        }
        else if (Convert.ToString(ddlSelectRecords2.SelectedValue) == "")
        {
            GridView2.AllowPaging = false;
        }
        else
        {
            GridView2.AllowPaging = true;
            GridView2.PageSize = Convert.ToInt32(ddlSelectRecords2.SelectedValue);
            BindGrid2(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        BindGrid2(0);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "revert")
        {
            divMatch.Visible = false;
            string ProjectID = e.CommandArgument.ToString();
            //Response.Write(ProjectID);
            //Response.End();
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (st.IsDeduct == "True")
            {
                hndProjectID.Value = e.CommandArgument.ToString();
                ModalPopupExtenderDR.Show();
                rptDeductRev.DataSource = null;
                rptDeductRev.DataBind();
                rptDeductRevSerial.DataSource = null;
                rptDeductRevSerial.DataBind();

                lblCustomer2.Text = st.Customer;
                lblProject2.Text = st.Project;
                lblPanelDetails2.Text = st.PanelDetails;
                lblInverterDetails2.Text = st.InverterDetails;
                lblInstallCity2.Text = st.InstallCity;
                lblInstallState2.Text = st.InstallState;
                //lblRoofType2.Text = st.RoofType;
                lblRoofType2.Text = st.ProjectNumber;

                DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_NewSelectStock(ProjectID);
                if (dt.Rows.Count > 0)
                {
                    rptDeductRev.DataSource = dt;
                    rptDeductRev.DataBind();
                }

                DataTable dt1 = ClstblStockOrders.tblStockDeductSerialNo_SelectBy_ProjectNumber_RFlag(st.ProjectNumber, "0");
                if (dt1.Rows.Count > 0)
                {
                    rptDeductRevSerial.DataSource = dt1;
                    rptDeductRevSerial.DataBind();
                }

                for (int item = 0; item < rptDeductRev.Items.Count; item++)
                {
                    HiddenField hndStockItemID = rptDeductRev.Items[item].FindControl("hndStockItemID") as HiddenField;
                    Label lblStockDeduct = rptDeductRev.Items[item].FindControl("lblStockDeduct") as Label;
                    TextBox txtDeductRevert = rptDeductRev.Items[item].FindControl("txtDeductRevert") as TextBox;

                    if (lblStockDeduct.Text == "0")
                    {
                        txtDeductRevert.ReadOnly = true;
                    }

                    //These if-else is used to check if any stock is reverted for particular projectid
                    if (item == 0)//since panel is only one
                    {
                        if (lblStockDeduct.Text != st.NumberPanels)
                        {
                            btnRevertAll.Disabled = true;
                        }
                    }
                    else if (item == 1)//since no. of inverter can be 1,2,3
                    {
                        if (lblStockDeduct.Text != st.inverterqty)
                        {
                            btnRevertAll.Disabled = true;
                        }
                    }
                    else if (item == 2)//since no. of inverter can be 1,2,3
                    {
                        if (lblStockDeduct.Text != st.inverterqty2)
                        {
                            btnRevertAll.Disabled = true;
                        }
                    }
                    else if (item == 3)//since no. of inverter can be 1,2,3
                    {
                        if (lblStockDeduct.Text != st.inverterqty3)
                        {
                            btnRevertAll.Disabled = true;
                        }
                    }
                }
            }
        }

        BindGrid(0);
    }

    protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        BindGrid2(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }
    protected void btnSearch2_Click(object sender, EventArgs e)
    {
        BindGrid2(0);
    }

    protected void btnRevert_Click(object sender, EventArgs e)
    {
        string ProjectID = hndProjectID.Value;
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        String ProjectNumber = st.ProjectNumber;
        string StockAllocationStore = st.StockAllocationStore;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        String Currentdate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        int suc1 = 0;
        int count = 0;
        divMatch.Visible = false;

        foreach (RepeaterItem item in rptDeductRevSerial.Items)//here we are counting the no of checked row so that we can compare it later.
        {
            CheckBox chkSerialNo = (CheckBox)item.FindControl("chkSerialNo");
            if (chkSerialNo.Checked == true)
            {
                count++;
            }
        }

        //if (count==)

        //foreach (RepeaterItem item in rptDeductRev.Items)
        //{
        //    TextBox txtDeductRevert = (TextBox)item.FindControl("txtDeductRevert");
        //    HiddenField hndStockItemID = (HiddenField)item.FindControl("hndStockItemID");

        //    string Stock = txtDeductRevert.Text.Trim();
        //    string StockItemID = hndStockItemID.Value;

        //    if (txtDeductRevert.Text.Trim() != string.Empty)
        //    {
        //        SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(StockItemID, StockAllocationStore);
        //        suc1 = ClstblStockItems.tblStockItemInventoryHistory_Insert("5", StockItemID, StockAllocationStore, stOldQty.StockQuantity, Stock, userid, ProjectID);
        //        ClstblStockItems.tblStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "1", DateTime.Now.AddHours(14).ToString());
        //        bool suc = ClsProjectSale.tblStockItems_RevertStock(StockItemID, Stock, StockAllocationStore);
        //    }
        //}
        //foreach (RepeaterItem item in rptDeductRevSerial.Items)
        //{
        //    CheckBox chkSerialNo = (CheckBox)item.FindControl("chkSerialNo");
        //    if(chkSerialNo.Checked==true)
        //    {
        //        String Pallet = "";
        //        Label lblPallet = (Label)item.FindControl("lblPallet");
        //        Label lblSerialNo = (Label)item.FindControl("lblSerialNo");
        //        HiddenField hdnInventoryHistId = (HiddenField)item.FindControl("hdnInventoryHistId");
        //        if (!string.IsNullOrEmpty(lblPallet.Text))
        //        {
        //            Pallet = lblPallet.Text;
        //        }               
        //        ClstblStockOrders.tblStockDeductSerialNo_UpdateRevert(lblSerialNo.Text, Pallet, ProjectNumber, Currentdate, stEmp.EmployeeID,suc1.ToString());
        //        //ClstblStockOrders.tblStockRevertSerialNo_Insert
        //    }
        //}


        for (int item = 0; item < rptDeductRev.Items.Count; item++)
        {
            HiddenField hndStockItemID = rptDeductRev.Items[item].FindControl("hndStockItemID") as HiddenField;
            Label lblStockDeduct = rptDeductRev.Items[item].FindControl("lblStockDeduct") as Label;
            TextBox txtDeductRevert = rptDeductRev.Items[item].FindControl("txtDeductRevert") as TextBox;
            SttblStockItems st2 = ClstblStockItems.tblStockItems_SelectByStockItemID(hndStockItemID.Value);
            String ItemType = st2.StockCategory;

            string Stock = txtDeductRevert.Text.Trim();
            string StockItemID = hndStockItemID.Value;

            if (txtDeductRevert.Text.Trim() != string.Empty)
            {
                if (ItemType == "Modules")//since we have only one panel fixed and we need to match repeater 2 with that row only.Row 1 is Panel.
                {
                    if (txtDeductRevert.Text == count.ToString())
                    {
                        //this block is for reverting data to stockitems
                        SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(StockItemID, StockAllocationStore);
                        suc1 = ClstblStockItems.tblStockItemInventoryHistory_Insert("5", StockItemID, StockAllocationStore, stOldQty.StockQuantity, Stock, userid, ProjectID);
                        ClstblStockItems.tblStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "1", DateTime.Now.AddHours(14).ToString());
                        bool suc = ClsProjectSale.tblStockItems_RevertStock(StockItemID, Stock, StockAllocationStore);

                        //this loop is to store selected row for revert in table tblStockDeductSerialNo
                        foreach (RepeaterItem item2 in rptDeductRevSerial.Items)
                        {
                            CheckBox chkSerialNo = (CheckBox)item2.FindControl("chkSerialNo");
                            if (chkSerialNo.Checked == true)
                            {
                                String Pallet = "";
                                Label lblPallet = (Label)item2.FindControl("lblPallet");
                                Label lblSerialNo = (Label)item2.FindControl("lblSerialNo");
                                HiddenField hdnInventoryHistId = (HiddenField)item2.FindControl("hdnInventoryHistId");
                                if (!string.IsNullOrEmpty(lblPallet.Text))
                                {
                                    Pallet = lblPallet.Text;
                                }
                                ClstblStockOrders.tblStockDeductSerialNo_UpdateRevert(lblSerialNo.Text, Pallet, ProjectNumber, Currentdate, stEmp.EmployeeID, hdnInventoryHistId.Value, suc1.ToString());
                            }
                        }

                        ModalPopupExtenderDR.Hide();
                        BindGrid(0);
                    }
                    else
                    {
                        ModalPopupExtenderDR.Show();
                        divMatch.Visible = true; //div mismatch                            
                    }
                }
                else //for inverters they can vary from 1-3 which we do not match with 'serial no repeater'.therefore we have kept original revert block
                {
                    SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(StockItemID, StockAllocationStore);
                    suc1 = ClstblStockItems.tblStockItemInventoryHistory_Insert("5", StockItemID, StockAllocationStore, stOldQty.StockQuantity, Stock, userid, ProjectID);
                    ClstblStockItems.tblStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "1", DateTime.Now.AddHours(14).ToString());
                    bool suc = ClsProjectSale.tblStockItems_RevertStock(StockItemID, Stock, StockAllocationStore);

                    ModalPopupExtenderDR.Hide();
                    BindGrid(0);
                }
            }
        }

        // ClstblProjects.tblProjects_UpdateStockDeductDate(ProjectID,"");
        //  ClstblProjects.tblProjects_UpdateStockDeductBy(ProjectID, "");

    }
    protected void btnRevertAll_Click(object sender, EventArgs e)

    {
        string ProjectID = hndProjectID.Value;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        //DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(ProjectID);
        DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_NewSelectStock(ProjectID);
        int count = 0;

        foreach (RepeaterItem item in rptDeductRevSerial.Items)//here we are counting the no of checked row so that we can compare it later.
        {
            count++;
        }

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string StockItemID = dt.Rows[i]["StockItemID"].ToString();
            string StockSize = dt.Rows[i]["Stock"].ToString();
            string StockLocation = st.StockAllocationStore;

            SttblStockItems st2 = ClstblStockItems.tblStockItems_SelectByStockItemID(StockItemID);
            String ItemType = st2.StockCategory;

            decimal size = Math.Round(Convert.ToDecimal(StockSize));
            int myStock = (Convert.ToInt32(size)) * (-1);



            if (ItemType == "Modules")
            {
                if (count.ToString() == st.NumberPanels)
                {
                    SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(StockItemID, StockLocation);
                    int suc1 = ClstblStockItems.tblStockItemInventoryHistory_Insert("5", StockItemID, StockLocation, stOldQty.StockQuantity, Convert.ToString(myStock), userid, ProjectID);
                    ClstblStockItems.tblStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "2", DateTime.Now.AddHours(14).ToString());
                    bool suc = ClsProjectSale.tblStockItems_RevertStock(StockItemID, Convert.ToString(myStock), StockLocation);

                    String Currentdate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
                    String ProjectNumber = st.ProjectNumber;

                    foreach (RepeaterItem item2 in rptDeductRevSerial.Items)
                    {
                        String Pallet = "";
                        CheckBox chkSerialNo = (CheckBox)item2.FindControl("chkSerialNo");
                        Label lblPallet = (Label)item2.FindControl("lblPallet");
                        Label lblSerialNo = (Label)item2.FindControl("lblSerialNo");
                        HiddenField hdnInventoryHistId = (HiddenField)item2.FindControl("hdnInventoryHistId");
                        if (!string.IsNullOrEmpty(lblPallet.Text))
                        {
                            Pallet = lblPallet.Text;
                        }
                        ClstblStockOrders.tblStockDeductSerialNo_UpdateRevert(lblSerialNo.Text, Pallet, ProjectNumber, Currentdate, stEmp.EmployeeID, hdnInventoryHistId.Value, suc1.ToString());
                    }

                    ClstblProjects.tblProjects_UpdateRevert(ProjectID, stEmp.EmployeeID, "0");
                    ClstblProjects.tblProjects_UpdateStockDeductDate(ProjectID, "");
                    ClstblProjects.tblProjects_UpdateStockDeductBy(ProjectID, "");
                    ClstblExtraStock.tblExtraStock_DeleteByProjectID(ProjectID);

                    /* -------------------- Update Project Status -------------------- */
                    if (st.InstallBookingDate != string.Empty)
                    {
                        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("11", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                        ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "11");
                    }
                    else
                    {
                        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                        ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                    ModalPopupExtenderDR.Hide();
                    /* -------------------------------------------------------------- */
                    BindGrid(0);

                }
                else
                {
                    ModalPopupExtenderDR.Show();
                    divMatch.Visible = true; //div mismatch    
                }
            }
            else
            {
                SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(StockItemID, StockLocation);
                int suc1 = ClstblStockItems.tblStockItemInventoryHistory_Insert("5", StockItemID, StockLocation, stOldQty.StockQuantity, Convert.ToString(myStock), userid, ProjectID);
                ClstblStockItems.tblStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "2", DateTime.Now.AddHours(14).ToString());
                bool suc = ClsProjectSale.tblStockItems_RevertStock(StockItemID, Convert.ToString(myStock), StockLocation);

                ClstblProjects.tblProjects_UpdateRevert(ProjectID, stEmp.EmployeeID, "0");
                ClstblProjects.tblProjects_UpdateStockDeductDate(ProjectID, "");
                ClstblProjects.tblProjects_UpdateStockDeductBy(ProjectID, "");
                ClstblExtraStock.tblExtraStock_DeleteByProjectID(ProjectID);

                /* -------------------- Update Project Status -------------------- */
                if (st.InstallBookingDate != string.Empty)
                {
                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("11", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                    ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "11");
                }
                else
                {
                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                    ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                }
                ModalPopupExtenderDR.Hide();
                /* -------------------------------------------------------------- */
                BindGrid(0);
            }

        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData2();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView2.DataSource = sortedView;
        GridView2.DataBind();
    }


    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }

    protected void GridView2_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView2.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView2.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView2.PageIndex - 2;
        page[1] = GridView2.PageIndex - 1;
        page[2] = GridView2.PageIndex;
        page[3] = GridView2.PageIndex + 1;
        page[4] = GridView2.PageIndex + 2;
        page[5] = GridView2.PageIndex + 3;
        page[6] = GridView2.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView2.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView2.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView2.PageIndex == GridView2.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage2 = (Label)gvrow.Cells[0].FindControl("ltrPage2");
        if (dv2.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv2.ToTable().Rows.Count;
            int iEndRecord = GridView2.PageSize * (GridView2.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView2.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage2.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage2.Text = "";
        }
    }


    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);

    }

    void lb_Command2(object sender, CommandEventArgs e)
    {
        GridView2.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid2(0);

    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView2_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command2);
        }
    }

    protected void lnkReceived_Click(object sender, EventArgs e)
    {
        LinkButton lnkReceived = (LinkButton)sender;
        GridViewRow item1 = lnkReceived.NamingContainer as GridViewRow;
        HiddenField hdnstockid = (HiddenField)item1.FindControl("hdnstockid");

        DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectByProjectId(hdnstockid.Value);
        if (dt.Rows.Count > 0)
        {
            hdnstocktransferid.Value = hdnstockid.Value;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
            divdetail.Visible = true;
            rptstockdetail.DataSource = dt;
            rptstockdetail.DataBind();
            divdetailmsg.Visible = false;
        }
        else
        {
            divdetailmsg.Visible = true;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
        }

    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        hdnstocktransferid.Value = "";
        ModalPopupExtender1.Hide();
        divstockdetail.Visible = false;
        rptstockdetail.DataSource = null;
        rptstockdetail.DataBind();
        BindGrid(0);
        //BindScript();

        divdetailmsg.Visible = false;
        divdetail.Visible = false;

        divright.Visible = true;
    }
    //protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        Label lblAllocatePanels = (Label)e.Row.FindControl("lblAllocatePanels");
    //        HiddenField hdnProjectID = (HiddenField)e.Row.FindControl("hdnProjectID");
    //        SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectID.Value);
    //        if (stpro.StockDeductDate != string.Empty)
    //        {
    //            DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(hdnProjectID.Value);
    //            if (dt.Rows.Count > 0)
    //            {
    //                decimal deduct = 0;
    //                foreach (DataRow dr in dt.Rows)
    //                {
    //                    deduct += Convert.ToDecimal(dr["Deduct"].ToString());
    //                }
    //                lblAllocatePanels.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0}", deduct);
    //            }
    //            //lblAllocatePanels.Text = stpro.NumberPanels;
    //        }

    //    }
    //}
    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }
    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = ClstblProjects.tblProjects_SelectWarehouse("", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddllocationsearch.SelectedValue, chkHistoric.Checked.ToString());
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "StockDeduct" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 11, 14, 301, 299, 52, 300 };
            string[] arrHeader = { "ProjectNumber", "Project", "InstallBookingDate", "Installer", "Details", "Store Name" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void lblexport7_Click(object sender, EventArgs e)
    {

    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtProjectNumber.Text = string.Empty;
        txtSerachCity.Text = string.Empty;
        txtSearch.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddllocationsearch.SelectedValue = "";
        ddlSearchState.SelectedValue = "";
        ddlSearchDate.SelectedValue = "1";

        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        BindGrid(0);
    }


    protected void btnClearAll2_Click1(object sender, EventArgs e)
    {
        txtProjectNumber2.Text = string.Empty;
        txtSerachCity2.Text = string.Empty;
        txtSearch2.Text = string.Empty;
        txtStartDate2.Text = string.Empty;
        txtEndDate2.Text = string.Empty;
        ddllocationsearch2.SelectedValue = "";
        ddlSearchState2.SelectedValue = "";
        ddlSearchDate2.SelectedValue = "1";
        ddlRevert.SelectedValue = "";
        BindGrid2(0);
    }
}