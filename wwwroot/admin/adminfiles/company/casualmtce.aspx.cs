using System;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_casualmtce : System.Web.UI.Page
{
    static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        HidePanels();
        if (!IsPostBack)
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "MyAction", "vali();", true);
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>vali();</script>");
           // PanGrid.Visible = false;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindGrid(0);

            ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataMember = "Contact";
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataBind();

            ddlInstallerSearch.DataSource = ClstblContacts.tblContacts_SelectInverter();
            ddlInstallerSearch.DataValueField = "ContactID";
            ddlInstallerSearch.DataMember = "Contact";
            ddlInstallerSearch.DataTextField = "Contact";
            ddlInstallerSearch.DataBind();
        }
    }
    protected DataTable GetGridData()
    {
        DataTable dt = ClstblProjectMaintenance.tblProjectMaintenance_SelectCasual("1", "", "", "", "", "", "", "","","","","","","","","","","","");
       
        if (dt.Rows.Count == 0)
        {
           
        }
       
        if ((!string.IsNullOrEmpty(txtSearch.Text)) || (!string.IsNullOrEmpty(ddlInstallerSearch.SelectedValue.ToString())) || (!string.IsNullOrEmpty(txtStartDate.Text)) || (!string.IsNullOrEmpty(txtEndDate.Text)))
        {
            dt = ClstblProjectMaintenance.tblProjectMaintenance_Search_Casual(txtSearch.Text, ddlInstaller.SelectedValue, txtStartDate.Text, txtEndDate.Text);
            //Response.Write(dt.Rows.Count);
            //Response.End();
        }
      
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;

        DataTable dt = GetGridData();
        if (dt.Rows.Count == 0)
        {
            HidePanels();

            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;

            }
            PanGrid.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string CasualCustomer = txtCasualContFirst.Text + " " + txtCasualContLast.Text;
        string CasualContFirst = txtCasualContFirst.Text;
        string CasualContLast = txtCasualContLast.Text;
        string CasualCustPhone = txtCasualCustPhone.Text;
        string CasualContMobile = txtCasualContMobile.Text;
        string CasualContEmail = txtCasualContEmail.Text;
        string CasualAddress = txtCasualAddress.Text;
        string CasualCity = txtCasualCity.Text;
        string CasualState = txtCasualState.Text;
        string CasualPostCode = txtCasualPostCode.Text;
        string CasualSystemDetails = txtCasualSystemDetails.Text;
        string CasualRefNumber = txtCasualRefNumber.Text;
        string InstallerID = ddlInstaller.SelectedValue;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stEmp.EmployeeID;

        int success = ClstblProjectMaintenance.tblProjectMaintenance_InsertCasual(CasualCustomer, CasualContFirst, CasualContLast, CasualCustPhone, CasualContMobile, CasualContEmail, CasualAddress, CasualCity, CasualState, CasualPostCode, CasualSystemDetails, CasualRefNumber, EmployeeID, InstallerID);
        //--- do not chage this code start------
        if (Convert.ToString(success) != "")
        {
            SetAdd();
        }
        else
        {
            SetError();
        }
       
        //Reset();
        BindGrid(0);
        //--- do not chage this code end------
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        string CasualCustomer = txtCasualContFirst.Text + " " + txtCasualContLast.Text;
        string CasualContFirst = txtCasualContFirst.Text;
        string CasualContLast = txtCasualContLast.Text;
        string CasualCustPhone = txtCasualCustPhone.Text;
        string CasualContMobile = txtCasualContMobile.Text;
        string CasualContEmail = txtCasualContEmail.Text;
        string CasualAddress = txtCasualAddress.Text;
        string CasualCity = txtCasualCity.Text;
        string CasualState = txtCasualState.Text;
        string CasualPostCode = txtCasualPostCode.Text;
        string CasualSystemDetails = txtCasualSystemDetails.Text;
        string CasualRefNumber = txtCasualRefNumber.Text;
        string InstallerID = ddlInstaller.SelectedValue;

        bool success = ClstblProjectMaintenance.tblProjectMaintenance_UpdateCasual(id1, CasualCustomer, CasualContFirst, CasualContLast, CasualCustPhone, CasualContMobile, CasualContEmail, CasualAddress, CasualCity, CasualState, CasualPostCode, CasualSystemDetails, CasualRefNumber, InstallerID);
        //--- do not chage this code Start------
        if (success)
        {
            SetUpdate();
        }
        else
        {
            SetError();
        }
        Reset();
        BindGrid(0);
        //--- do not chage this code end------
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;

        ModalPopupExtenderDelete.Show();

        //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "modal_danger", "$('#modal_danger').modal('show');", true);
        BindGrid(0);      
  
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        SttblProjectMaintenance st = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectMaintenanceID(id);

        //txtCasualCustomer.Text = st.CasualCustomer;
        txtCasualContFirst.Text = st.CasualContFirst;
        txtCasualContLast.Text = st.CasualContLast;
        txtCasualCustPhone.Text = st.CasualCustPhone;
        txtCasualContMobile.Text = st.CasualContMobile;
        txtCasualContEmail.Text = st.CasualContEmail;
        txtCasualAddress.Text = st.CasualAddress;
        txtCasualCity.Text = st.CasualCity;
        txtCasualState.Text = st.CasualState;
        txtCasualPostCode.Text = st.CasualPostCode;
        txtCasualSystemDetails.Text = st.CasualSystemDetails;
        txtCasualRefNumber.Text = st.CasualRefNumber;
        ddlInstaller.SelectedValue = st.InstallerID;
        //--- do not chage this code start------
        InitUpdate();
        //--- do not chage this code end------
    }

    protected void ddlcategorysearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView1.EditIndex = e.NewEditIndex;
        BindGrid(0);
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if(Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else{
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGrid(0);
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        //Response.Write("ss");
        //Response.End();
        Reset();
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
        BindGrid(0);
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        InitAdd();
    }
    private string ConvertSortDirectionToSql(SortDirection sortDireciton)
    {
        string m_SortDirection = String.Empty;

        switch (sortDireciton)
        {
            case SortDirection.Ascending:
                m_SortDirection = "ASC";
                break;

            case SortDirection.Descending:
                m_SortDirection = "DESC";
                break;
        }
        return m_SortDirection;
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        PanAddUpdate.Visible = false;
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        HidePanels();
        BindGrid(0);
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;

        //lnkBack.Visible = false;
        //lnkAdd.Visible = true;
        ////ModalPopupExtender2.Hide();
        //PanAddUpdate.Visible = false;
        //Reset();
        //btnAdd.Visible = true;
        //btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = false;
        //lblAddUpdate.Visible = false;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;


        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;

        lblAddUpdate.Text = "Add ";
    }
    public void InitUpdate()
    {
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        //  HidePanels();
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;

        lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        PanAlreadExists.Visible = false;
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }
    protected void ddlStreetCity_OnTextChanged(object sender, EventArgs e)
    {
        string[] cityarr = txtCasualCity.Text.Split('|');
        if (cityarr.Length > 1)
        {
            txtCasualCity.Text = cityarr[0].Trim();
            txtCasualState.Text = cityarr[1].Trim();
            txtCasualPostCode.Text = cityarr[2].Trim();
        }
        else
        {
            txtCasualCity.Text = string.Empty;
            txtCasualState.Text = string.Empty;
            txtCasualPostCode.Text = string.Empty;
        }
    }
    public void Reset()
    {
        //HidePanels();
        PanAddUpdate.Visible = true;
        //txtCasualCustomer.Text = string.Empty;
        txtCasualContFirst.Text = string.Empty;
        txtCasualContLast.Text = string.Empty;
        txtCasualCustPhone.Text = string.Empty;
        txtCasualContMobile.Text = string.Empty;
        txtCasualContEmail.Text = string.Empty;
        txtCasualAddress.Text = string.Empty;
        txtCasualCity.Text = string.Empty;
        txtCasualState.Text = string.Empty;
        txtCasualPostCode.Text = string.Empty;
        txtCasualSystemDetails.Text = string.Empty;
        txtCasualRefNumber.Text = string.Empty;
        ddlInstaller.SelectedValue = "";
        lblAddUpdate.Text = "";
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "invoiceadd")
        {
            string ProjectID = e.CommandArgument.ToString();
            Response.Redirect("~/admin/adminfiles/invoice/invcommon.aspx?id=" + ProjectID + "&type=5");
        }
        if (e.CommandName.ToLower() == "invoiceupdate")
        {
            string ProjectID = e.CommandArgument.ToString();
            Response.Redirect("~/admin/adminfiles/invoice/updateinvcomm.aspx?id=" + ProjectID + "&type=5");
        }
    }
    //protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        LinkButton lbtnInvAdd = (LinkButton)e.Row.FindControl("lbtnInvAdd");
    //        LinkButton lbtnInvUpdate = (LinkButton)e.Row.FindControl("lbtnInvUpdate");
    //        HiddenField hndProjectID = (HiddenField)e.Row.FindControl("hndProjectID");

    //        string ProjectID = hndProjectID.Value;
    //        DataTable dt = ClstblInvoiceCommon.tblInvoiceCommon_SelectByProjectID("5", ProjectID);
    //        if (dt.Rows.Count > 0)
    //        {
    //            lbtnInvAdd.Visible = false;
    //            lbtnInvUpdate.Visible = true;
    //        }
    //        else
    //        {
    //            lbtnInvAdd.Visible = true;
    //            lbtnInvUpdate.Visible = false;
    //        }
    //    }
    //}
    //protected void btnClearAll_Click(object sender, System.Web.UI.ImageClickEventArgs e)
    //{
    //    txtSearch.Text = string.Empty;
    //    ddlInstallerSearch.SelectedValue = "";
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;

    //    BindGrid(0);
    //}
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = dv.ToTable();

        GridViewSortExpression = e.SortExpression;
        GridView1.DataSource = SortDataTable(dt, false);
        GridView1.DataBind();
    }
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtSearch.Text = string.Empty;
        ddlInstallerSearch.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        BindGrid(0);
    }
    protected void GridView1_RowDataBound1(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lbtnInvAdd = (LinkButton)e.Row.FindControl("lbtnInvAdd");
            LinkButton lbtnInvUpdate = (LinkButton)e.Row.FindControl("lbtnInvUpdate");
            HiddenField hndProjectID = (HiddenField)e.Row.FindControl("hndProjectID");

            string ProjectID = hndProjectID.Value;
            DataTable dt = ClstblInvoiceCommon.tblInvoiceCommon_SelectByProjectID("5", ProjectID);
            if (dt.Rows.Count > 0)
            {
                lbtnInvAdd.Visible = false;
                lbtnInvUpdate.Visible = true;
            }
            else
            {
                lbtnInvAdd.Visible = true;
                lbtnInvUpdate.Visible = false;
            }
        }
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        bool sucess1 = ClstblProjectMaintenance.tblProjectMaintenance_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            SetDelete();
        }
        else
        {
            SetError();
        }
        //--- do not chage this code end------
        //--- do not chage this code start------
        GridView1.EditIndex = -1;
        BindGrid(0);
        BindGrid(1);
        //--- do not chage this code end------
    }
}