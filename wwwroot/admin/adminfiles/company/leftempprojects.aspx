<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="leftempprojects.aspx.cs" Inherits="admin_adminfiles_company_leftempprojects" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').removeClass('loading-inactive');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    $('.loading-container').addClass('loading-inactive');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                }
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $(".myvalleftemployee").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                    $("[data-toggle=tooltip]").tooltip();
                     $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                     $('.datetimepicker1').datetimepicker({
                         format: 'DD/MM/YYYY'
                     });
                   
                }
            </script>
            
            <div class="page-body headertopbox">
              <h5 class="row-title"><i class="typcn typcn-th-small"></i>Left Employee Projects</h5>
              <div id="hbreadcrumb" class="pull-right">                                
                                <ol class="hbreadcrumb breadcrumb fontsize16" id="ol" runat="server" visible="false">
                                    <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" CssClass="btn btn-info"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBack" runat="server" CausesValidation="false" CssClass="btn btn-info"><i class="fa fa-chevron-left"></i> Back</asp:LinkButton>
                                </ol>
                                
                            </div>
               </div>
			<div class="page-body padtopzero">
            <asp:Panel runat="server" ID="PanGridSearch">                
                    <div class="animate-panelmessesgarea padbtmzero">
                        <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                        </div>
                    </div>                    
                       <div class="searchfinal">
                         <div class="widget-body shadownone brdrgray"> 
                         <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                            <div class="dataTables_filter">
                            <div class="dataTables_length">
                                                <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                    <tr>
                                                        <td style="padding: 0px;">
                                                            <div class="input-group">
                                                                <asp:DropDownList ID="ddlType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalleftemployee">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                    <asp:ListItem Value="1">Project</asp:ListItem>
                                                                    <asp:ListItem Value="2">Lead</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group col-sm-1">
                                                                <asp:DropDownList ID="ddlLeftEmp" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalleftemployee">
                                                                    <asp:ListItem Value="">Employee</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="input-group  col-sm-1">
                                                                <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalleftemployee">
                                                                    <asp:ListItem Value="">Team</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group  col-sm-1">
                                                                <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalleftemployee">
                                                                    <asp:ListItem Value="">State</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="col-sm-2 input-group date datetimepicker1">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtStartDate" runat="server"  class="form-control" placeholder="Start Date">
                                                                </asp:TextBox>
                                                            </div>
                                                            <div class=" col-sm-2 input-group date datetimepicker1">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtEndDate" runat="server"  class="form-control" placeholder="End Date">
                                                                </asp:TextBox>
                                                            </div>

                                                            <div class="input-group " id="salesrap" runat="server" visible="false">
                                                                <asp:DropDownList ID="ddlSalesRep" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalleftemployee">
                                                                    <asp:ListItem Value="">State</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="col-sm-2 input-group date datetimepicker1">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtNextFollowupDate" runat="server"  class="form-control" placeholder="Next Followup Date">
                                                                </asp:TextBox>
                                                            </div>

                                                            <div class="input-group col-sm-1">
                                                                <asp:DropDownList ID="ddlUserSelect" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalleftemployee">
                                                                    <asp:ListItem Value="">Role</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group">
                                                                <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-primary btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>

                                                  <div class="row martop5 printorder">
                                            <div>
                                                <div class="dataTables_length showdata col-sm-3 printorder">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td class="padtopzero"><asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="myval">
                                           <asp:ListItem Value="25">Show entries</asp:ListItem>
                                  </asp:DropDownList>
                                                            </td>

                                                        </tr>
                                                    </table>
                                                </div>

                                            </div>
                                        </div>

                                            </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    
                    <div class="finalgrid">
                       <div class="table-responsive printArea"> 
                            <div id="PanGrid" runat="server">
                                            <div class="table-responsive">
                                                <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand"
                                                    OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="10">
                                                    <Columns>
                                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                                            <HeaderTemplate>
                                                                &nbsp;<asp:CheckBox ID="chkAssignAll" runat="server" OnCheckedChanged="chkAssignAll_CheckedChanged" AutoPostBack="true" />
                                                                <label for='<%# ((GridViewRow)Container).FindControl("chkAssignAll").ClientID %>'>
                                                                    <span></span>
                                                                </label>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkAssign" runat="server" />
                                                                <label for='<%# ((GridViewRow)Container).FindControl("chkAssign").ClientID %>'>
                                                                    <span></span>
                                                                </label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="40px" HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Opened" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="CustEntered" ItemStyle-Width="100px">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hndCustomerID" runat="server" Value='<%# Eval("CustomerID") %>' />
                                                                <%# Eval("CustEntered", "{0:dd MMM yyyy}")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="NextFollowupDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="NextFollowupDate" ItemStyle-Width="100px">
                                                            <ItemTemplate>
                                                                <%# Eval("NextFollowupDate", "{0:dd MMM yyyy}")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" SortExpression="Customer">
                                                            <ItemTemplate>
                                                                <%#Eval("Customer")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Employee" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                                            <ItemTemplate>
                                                                <%#Eval("Employee") %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle />

                                                    <PagerTemplate>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                        <div class="pagination">
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                        </div>
                                                    </PagerTemplate>
                                                    <PagerStyle CssClass="paginationGrid" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                </asp:GridView>
                                            </div>
                                            <div class="paginationnew1" runat="server" id="divnopage">
                                                <table cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                        </div>
                    </div>             
            </asp:Panel>
          </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
