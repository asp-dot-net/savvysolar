﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing.Imaging;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_CustomerNew : System.Web.UI.Page
{
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["m"]) && !string.IsNullOrEmpty(Request.QueryString["compid"]))
            {
                SttblCustomers st_cust = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"].ToString());
                ltcompname.Text = " - " + st_cust.Customer;
            }

            if (Request.QueryString["m"] == "comp")
            {
                divright.Visible = true;
                TabContainer1.ActiveTabIndex = 0;
                //PanGrid.Visible = false;
            }
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        BindScript();
        //PanGrid.Visible = false;

        if (TabContainer1.ActiveTabIndex == 0)
        {
            companysummary1.BindSummary();
        }
        else if (TabContainer1.ActiveTabIndex == 1)
        {
            info1.BindInfo();
        }
        else if (TabContainer1.ActiveTabIndex == 2)
        {
            newProject1.BindProjectNew();
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "dodropdown();", true);
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

}