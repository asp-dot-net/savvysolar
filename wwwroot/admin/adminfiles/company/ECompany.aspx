﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    MaintainScrollPositionOnPostback="true" Culture="en-GB" UICulture="en-GB"
    CodeFile="ECompany.aspx.cs" Inherits="admin_adminfiles_company_ECompany"
    EnableEventValidation="false" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/controls/companysummary.ascx" TagName="companysummary" TagPrefix="uc1" %>
<%@ Register Src="../../../includes/controls/contacts.ascx" TagName="contacts" TagPrefix="uc2" %>
<%@ Register Src="../../../includes/controls/project.ascx" TagName="project" TagPrefix="uc3" %>
<%@ Register Src="../../../includes/controls/info.ascx" TagName="info" TagPrefix="uc4" %>
<%@ Register Src="../../../includes/controls/conversation.ascx" TagName="conversation" TagPrefix="uc5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <input type="hidden" id="chkaddval" value="0" />
    <input type="hidden" id="chkaddval1" value="0" />
    <script src="../../js/jquery.min.js"></script>
    <script>
        function ComfirmDelete(event, ctl) {
            event.preventDefault();
            var defaultAction = $(ctl).prop("href");

            swal({
                title: "Are you sure you want to delete this Record?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: true
            },

              function (isConfirm) {
                  if (isConfirm) {
                      // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                      eval(defaultAction);

                      //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                      return true;
                  } else {
                      // swal("Cancelled", "Your imaginary file is safe :)", "error");
                      return false;
                  }
              });
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
                $("#<%=txtformbaystreetname.ClientID %>").removeClass('greenborder');
                $("#<%=txtPostalformbaystreetname.ClientID %>").removeClass('greenborder');
            });
            $('#<%=btnUpdate.ClientID %>').click(function () {
                formValidate();
            });


            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptName] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptName] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptMobile] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptMobile] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptPhone] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptPhone] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptEmail] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptEmail] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptaddress] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptaddress] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            //gridviewScroll();
        });
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
        function dodropdown() {
            $(".ddlval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".ddlval1").select2({
                minimumResultsForSearch: -1
            });
        }

       

        function doMyAction() {
            //$(".ddlval").select2({
            //    placeholder: "select",
            //    allowclear: true
            //});
            //$(".ddlval1").select2({
            //    minimumResultsForSearch: -1
            //});
            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
               
            });
            $('#<%=btnUpdate.ClientID %>').click(function () {
                formValidate();
              
            });

            $("[data-toggle=tooltip]").tooltip();

            //gridviewScroll();
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptName] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptName] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptMobile] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptMobile] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptPhone] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptPhone] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptEmail] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptEmail] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
            $("[id*=rptaddress] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=rptaddress] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });

            //$(".ddlval").select2({
            //    placeholder: "select",
            //    allowclear: true
            //});
            //$(".ddlval1").select2({
            //    minimumResultsForSearch: -1
            //});
        }
        //function gridviewScroll() {
        //   $('#<%=GridView1.ClientID%>').gridviewScroll({
        //        width: $("#content").width() - 42,
        //         height: 6000,
        //         freezesize: 0
        //     });
        // }
        // $("#nav").on("click", "a", function () {
        //     $('#content').animate({ opacity: 0 }, 500, function () {
        //         gridviewScroll();
        //         $('#content').delay(250).animate({ opacity: 1 }, 500);
        //     });
        // });

    </script>




    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                // prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    $('.loading-container').css('display', 'none');
                }

            </script>

            <script>
                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                function BeginRequestHandler(sender, args) {

                    var fe = document.activeElement;
                    if (fe != null) {
                        focusedElementId = fe.id;
                    } else {
                        focusedElementId = "";
                    }
                }

                prm.add_pageLoaded(pageLoaded);
                prm.add_beginRequest(BeginRequestHandler);
                // prm.add_endRequest(EndRequestHandler);
                function pageLoaded() {
                    $(".ddlval").select2({
                        // placeholder: "select",
                        allowclear: true
                    });
                    $(".ddlval1").select2({
                        minimumResultsForSearch: -1
                    });

                    //gridviewScroll();
                    //$(".ddlval").select2({
                    //    placeholder: "select",
                    //    allowclear: true
                    //});
                    //$(".ddlval1").select2({
                    //    minimumResultsForSearch: -1
                    //});
                    $('#<%=btnAdd.ClientID %>').click(function () {
                        formValidate();
                    });
                    $('#<%=btnUpdate.ClientID %>').click(function () {
                        formValidate();
                    });

                    $("[data-toggle=tooltip]").tooltip();

                    //gridviewScroll();
                    $("[id*=GridView1] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=GridView1] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptName] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptName] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptMobile] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptMobile] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptPhone] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptPhone] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptEmail] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptEmail] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });
                    $("[id*=rptaddress] td").bind("click", function () {
                        var row = $(this).parent();
                        $("[id*=rptaddress] tr").each(function () {
                            if ($(this)[0] != row[0]) {
                                $("td", this).removeClass("selected_row");
                            }
                        });
                        $("td", row).each(function () {
                            if (!$(this).hasClass("selected_row")) {
                                $(this).addClass("selected_row");
                            } else {
                                $(this).removeClass("selected_row");
                            }
                        });
                    });

                    ////alert($(".search-select").attr("class"));
                    //$(".ddlval").select2({
                    //    placeholder: "select",
                    //    allowclear: true
                    //});
                    //$("[data-toggle=tooltip]").tooltip();
                    //$("searchbar").attr("imgbtn");
                    //gridviewScroll();

                    //doMyAction(); 
                    //$(".ddlval").select2({
                    //    placeholder: "select",
                    //    allowclear: true
                    //});
                    //$(".ddlval1").select2({
                    //    minimumResultsForSearch: -1
                    //});
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                }


            </script>

            <script type="text/javascript">
                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.jpg";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.jpg";
                        //  divexpandcollapse("", "");
                    }
                }
            </script>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $(".myvalcomp").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    //$('.i-checks').iCheck({
                    //    checkboxClass: 'icheckbox_square-green',
                    //    radioClass: 'iradio_square-green'
                    //});
                }
            </script>

            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                }
                function endrequesthandler(sender, args) {
                }
                function pageLoaded() {

                }
                function address() {
                    //=============Street 
                    var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();

                    $.ajax({
                        type: "POST",
                        url: "company.aspx/Getstreetname",
                        data: "{'streetname':'" + streetname + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == true) {
                                $("#<%=txtformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                                $("#chkaddval").val("1");
                            }
                            else {
                                $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                                $("#chkaddval").val("0");
                            }
                        }
                    });


                }
                function postaladdress() {
                    //=================Street

                    var streetname = $("#<%=txtPostalformbaystreetname.ClientID %>").val();
            $.ajax({
                type: "POST",
                //action:"continue.aspx",
                url: "company.aspx/Getstreetname",
                data: "{'streetname':'" + streetname + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d == true) {

                        $("#<%=txtPostalformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');

                                $("#chkaddval1").val("1");
                            }
                            else {
                                $("#<%=txtPostalformbaystreetname.ClientID %>").addClass('errormassage');

                                $("#chkaddval1").val("0");
                            }
                        }
                    });
                }
                function validatestreetAddress(source, args) {
                    var streetname = $("#<%=txtformbaystreetname.ClientID %>").val();

                    $.ajax({
                        type: "POST",
                        url: "company.aspx/Getstreetname",
                        data: "{'streetname':'" + streetname + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            //alert(data.d);
                            if (data.d == true) {
                                $("#<%=txtformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                                $("#chkaddval").val("1");
                            }
                            else {
                                $("#<%=txtformbaystreetname.ClientID %>").addClass('errormassage');
                                $("#chkaddval").val("0");
                            }
                        }
                    });

                    var mydataval = $("#chkaddval").val();
                    if (mydataval == "1") {
                        args.IsValid = true;
                    }
                    else {
                        args.IsValid = false;
                    }
                }
                function postalvalidatestreetAddress(source, args) {
                    var streetname = $("#<%=txtPostalformbaystreetname.ClientID %>").val();
                    $.ajax({
                        type: "POST",
                        url: "company.aspx/Getstreetname",
                        data: "{'streetname':'" + streetname + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d == true) {
                                $("#<%=txtPostalformbaystreetname.ClientID %>").removeClass('errormassage').addClass('greenborder');
                                $("#chkaddval1").val("1");
                            }
                            else {
                                $("#<%=txtPostalformbaystreetname.ClientID %>").addClass('errormassage');
                                $("#chkaddval1").val("0");
                            }
                        }
                    });
                    var mydataval1 = $("#chkaddval1").val();
                    if (mydataval1 == "1") {
                        args.IsValid = true;
                    }
                    else {
                        args.IsValid = false;
                    }
                }
            </script>

            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Manage Company
          <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                </h5>
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb fontsize16">
                        <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-default purple"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                        <asp:LinkButton ID="lnkclose" runat="server" CssClass="btn btn-maroon" CausesValidation="false" Visible="false"
                            OnClick="lnkclose_Click"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                    </ol>
                </div>
            </div>

            <div class="page-body padtopzero">
                <div class="finaladdupdate">
                    <div id="PanAddUpdate" runat="server" visible="false">
                        <div class="animate-panel">
                            <div class="form-horizontal row">

                                <%--  <div class="col-md-12">
                                                 
                                                </div>--%>
                                <div class="col-md-6">
                                    <div class="animate-panel padtopzero">
                                        <div class="well with-header  companyddform addform" style="min-height: 826px;">
                                            <div class="header bordered-blue">
                                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                                <h4 id="Haddcomp" runat="server">
                                                    <asp:Label runat="server" ID="lbladdcompany" Text="Add New Company"></asp:Label>
                                                </h4>
                                            </div>

                                            <div class="form-group" id="divD2DEmployee" visible="false" runat="server">
                                                <asp:Label ID="Label33" runat="server" class="col-sm-12">Company</asp:Label>
                                                <div class="col-sm-12">
                                                    <div class="drpValidate">
                                                        <asp:DropDownList ID="ddlD2DEmployee" runat="server" AppendDataBoundItems="true" CssClass="myvalcomp"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlD2DEmployee_SelectedIndexChanged" aria-controls="DataTables_Table_0">
                                                            <asp:ListItem Value="">Employee</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="" CssClass="comperror"
                                                            ControlToValidate="ddlD2DEmployee" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>




                                            <div class="row">
                                                <div class="col-md-6 marginbtm15" visible="false" id="divD2DAppDate" runat="server">
                                                    <asp:Label ID="Label23" runat="server" class="control-label">Appointment Date</asp:Label>
                                                    <div>
                                                        <div class="input-group date datetimepicker1">
                                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                            <asp:TextBox ID="txtD2DAppDate" runat="server" class="form-control" placeholder="Start Date"> </asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="marginbtm15 col-md-6" id="divD2DAppTime" runat="server">
                                                    <asp:Label ID="Label1" runat="server" class="control-label">Appointment Time</asp:Label>
                                                    <div class="drpValidate">
                                                        <asp:DropDownList ID="ddlAppTime" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                            <asp:ListItem Value="">Time</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="* Required" CssClass="" InitialValue="Unit Type"
                                                            ControlToValidate="ddlAppTime" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="form-group spicaldivin" id="divMrMs" runat="server">
                                                <asp:Label ID="Label37" runat="server" class="col-sm-12">Name</asp:Label>
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <span class="mrdiv">
                                                                <asp:TextBox ID="txtContMr" runat="server" MaxLength="10" CssClass="form-control"
                                                                    placeholder="Salutation"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span class="fistname">
                                                                <asp:TextBox ID="txtContFirst" runat="server" MaxLength="30" CssClass="form-control"
                                                                    placeholder="First Name" AutoPostBack="true" OnTextChanged="txtContFirst_TextChanged"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="" CssClass=""
                                                                    ControlToValidate="txtContFirst" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                                <%--   <asp:RequiredFieldValidator ID="reqvalCategoryName" runat="server" ControlToValidate="txtContFirst"
                                                                            ErrorMessage="" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                                <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtContFirst"
                                                                            ErrorMessage="" CssClass="" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                            </span>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <span class="lastname">
                                                                <asp:TextBox ID="txtContLast" runat="server" MaxLength="40" CssClass="form-control"
                                                                    placeholder="Last Name" OnTextChanged="txtContLast_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label22" runat="server" class="col-sm-12">Company</asp:Label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtCompany" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtCompany"
                                                        ErrorMessage="" Display="None" CssClass="errormassage" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ErrorMessage="This value is required." CssClass=""
                                                                                 ControlToValidate="txtCompany" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" class="col-sm-12 ">Mobile</asp:Label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtContMobile" runat="server" MaxLength="10" OnTextChanged="txtContMobile_TextChanged" AutoPostBack="true" class="form-control modaltextbox"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtContMobile" SetFocusOnError="true" Display="Dynamic"
                                                        ValidationGroup="company1" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)" ForeColor="Red"
                                                        ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="form-group" id="divEmail" runat="server">
                                                <asp:Label ID="Label4" runat="server" class="col-sm-12">Email</asp:Label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtContEmail" runat="server" MaxLength="200" OnTextChanged="txtContEmail_TextChanged" AutoPostBack="true" class="form-control modaltextbox"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtContEmail"
                                                        ValidationGroup="company1" Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address" ForeColor="Red"
                                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\+)*\.\w+([-.]\w+)*"> </asp:RegularExpressionValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="" CssClass="" SetFocusOnError="true"
                                                        ControlToValidate="txtContEmail" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <div class="form-group" id="div1" runat="server">
                                                <asp:Label ID="Label5" runat="server" class="col-sm-12">Phone</asp:Label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtCustPhone" runat="server" MaxLength="200" OnTextChanged="txtCustPhone_TextChanged" AutoPostBack="true" class="form-control modaltextbox"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtCustPhone"
                                                        ValidationGroup="company1" Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03/08XXXXXXXX)"
                                                        ValidationExpression="^(07|03|08)[\d]{8}$"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="form-group" id="div2" runat="server">
                                                <asp:Label ID="Label6" runat="server" class="col-sm-12">Alt Phone</asp:Label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtCustAltPhone" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtCustAltPhone"
                                                        ValidationGroup="company1" Display="Dynamic" ErrorMessage="Please enter valid number"
                                                        ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>
                                                </div>
                                            </div>
                                            <div class="form-group" id="divCustType" runat="server">
                                                <asp:Label ID="lblLastName" runat="server" class="col-sm-12">Type</asp:Label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlCustTypeID" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                        <%--    <asp:ListItem Value="" Text="Select"></asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                    <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                        ControlToValidate="ddlCustTypeID" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </div>
                                            <div class="form-group spicaldivin" id="div3" runat="server" style="margin-bottom: 13px!important;">
                                                <label class="col-sm-12">Solar Type </label>

                                                <div class="col-sm-12">
                                                    <div class="radio radio-info radio-inline" style="margin-bottom: 7px; padding-left: 0px;">
                                                        <label for="<%=rblResCom1.ClientID %>">
                                                            <asp:RadioButton runat="server" ID="rblResCom1" GroupName="qq" />
                                                            <span class="text">Res&nbsp;</span>
                                                        </label>
                                                        <label for="<%=rblResCom2.ClientID %>">
                                                            <asp:RadioButton runat="server" ID="rblResCom2" GroupName="qq" />
                                                            <span class="text">Com&nbsp;</span>
                                                        </label>
                                                    </div>
                                                    <%--<div class="radio i-checks">
                                                                                                <asp:RadioButtonList ID="rblResCom" runat="server" AppendDataBoundItems="true" RepeatDirection="Horizontal">
                                                                                                    <asp:ListItem Value="1" Selected="True">Res&nbsp;</asp:ListItem>
                                                                                                    <asp:ListItem Value="2">Com</asp:ListItem>
                                                                                                </asp:RadioButtonList>
                    
                    
                                                                                            </div>--%>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="form-group" id="div4" runat="server">
                                                <asp:Label ID="Label7" runat="server" class="col-sm-12">Source</asp:Label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlCustSourceID" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                        <%--  <asp:ListItem Value="" Text="Select"></asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                    <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                        ControlToValidate="ddlCustSourceID" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>--%>
                                                </div>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlCustSourceSubID" runat="server" AppendDataBoundItems="true"
                                                        aria-controls="DataTables_Table_0" CssClass="myval" Visible="false">
                                                        <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                            </div>
                                            <div class="form-group" id="div5" runat="server">
                                                <asp:Label ID="Label8" runat="server" class="col-sm-12">Notes</asp:Label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtCustNotes" runat="server" Width="" TextMode="MultiLine" Columns="4" Rows="4" class="form-control modaltextbox notestextbox"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="graybgarea col-md-12" runat="server" id="divformbayaddress" visible="false">
                                    <div class="form-group spicaldivin textareabox" style="margin-bottom: 15px;">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="padding-right: 4px;">
                                                    <asp:CheckBox ID="chkformbayid" runat="server" Checked="false" Class="i-checks" OnCheckedChanged="chkformbayid_CheckedChanged"
                                                        AutoPostBack="true" />
                                                    <label for="<%=chkformbayid.ClientID %>"><span></span></label>
                                                </td>
                                                <td>Is Frombay Address </td>
                                            </tr>
                                        </table>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="animate-panel padtopzero">
                                        <div class="well with-header companyddform  addform" style="min-height: 826px;">
                                            <div class="header bordered-blue">
                                                <h4 id="Haddcompadd" runat="server">
                                                    <asp:Label runat="server" ID="lblcompanyaddress" Text="Add Company Address"></asp:Label></h4>
                                            </div>
                                            <div id="Div6" class="form-group spicaldivin streatfield" visible="false" runat="server">
                                                <span class="name">
                                                    <label class="control-label">Street Address Line<span class="symbol required"></span> </label>
                                                </span><span>
                                                    <asp:HiddenField ID="hndaddress" runat="server" />
                                                    <asp:TextBox ID="txtstreetaddressline" runat="server" MaxLength="50" CssClass="form-control" name="address" AutoPostBack="true" OnTextChanged="txtstreetaddressline_TextChanged"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="rfvstreetaddressline" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                        ControlToValidate="txtstreetaddressline" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    <%-- <asp:CustomValidator ID="Customstreetadd" runat="server"
                                                                ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company1"
                                                                ClientValidationFunction="ChkFun"></asp:CustomValidator>--%>
                                                </span>
                                                <asp:Label ID="lblexistame" runat="server" Visible="false"></asp:Label>
                                                <asp:HiddenField ID="hndstreetno" runat="server" />
                                                <asp:HiddenField ID="hndstreetname" runat="server" />
                                                <asp:HiddenField ID="hndstreettype" runat="server" />
                                                <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                                <asp:HiddenField ID="hndunittype" runat="server" />
                                                <asp:HiddenField ID="hndunitno" runat="server" />
                                                <div id="validaddressid" style="display: none">
                                                    <img src="../../../images/check.png" alt="check">Address is valid.
                                                </div>
                                                <div id="invalidaddressid" style="display: none">
                                                    <img src="../../../images/x.png" alt="cross">
                                                    Address is invalid.
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="form-group" id="div7" runat="server">
                                                <asp:Label ID="Label9" runat="server" class="col-md-12">Street Address</asp:Label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtStreetAddress" runat="server" MaxLength="50" Enabled="false" class="form-control modaltextbox"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 marginbtm15" visible="false" id="divUnitno" runat="server">
                                                    <asp:Label ID="Label10" runat="server">Unit No</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtformbayUnitNo" runat="server" name="address" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="txtformbayUnitNo_TextChanged"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="marginbtm15 col-md-6" id="divunittype" runat="server">
                                                    <asp:Label ID="Label11" runat="server">Unit Type</asp:Label>
                                                    <div>
                                                        <asp:DropDownList ID="ddlformbayunittype" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalcomp" AutoPostBack="true" OnSelectedIndexChanged="ddlformbayunittype_SelectedIndexChanged">
                                                            <asp:ListItem Value="">Unit Type</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="* Required" CssClass="" InitialValue="Unit Type"
                                                            ControlToValidate="ddlformbayunittype" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="marginbtm15 col-md-4" visible="false" id="divStreetno" runat="server">
                                                    <asp:Label ID="Label12" runat="server">Street No</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtformbayStreetNo" runat="server" name="address" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtformbayStreetNo" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="marginbtm15 spicaldivin col-md-4 streatfield" visible="false" id="divstname" runat="server">
                                                    <asp:Label ID="Label2" CssClass="marbtmzero" runat="server">Street Name</asp:Label>

                                                    <div class="autocompletedropdown">
                                                        <asp:TextBox ID="txtformbaystreetname" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtformbaystreetname_TextChanged"></asp:TextBox>
                                                        <%--<asp:DropDownList ID="ddlformbaystreetname" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="ddlval">
                                                                                <asp:ListItem Value="">Street Name</asp:ListItem>
                                                                            </asp:DropDownList>--%>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="" CssClass="requiredfield"
                                                            ControlToValidate="txtformbaystreetname" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                            ServiceMethod="GetStreetNameList" CompletionListCssClass="autocompletedrop"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />


                                                        <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                            ErrorMessage="Enter Valid Street" Display="Dynamic" CssClass="requiredfield"
                                                            ClientValidationFunction="validatestreetAddress" ControlToValidate="txtformbaystreetname"></asp:CustomValidator>


                                                        <div id="Divvalidstreetname" style="display: none">
                                                            <%--<img src="../../../images/check.png" alt="check">--%>
                                                            <i class="fa fa-check"></i>Address is valid.
                                                        </div>
                                                        <div id="DivInvalidstreetname" style="display: none">
                                                            <%--<img src="../../../images/x.png" alt="cross">--%>
                                                            <i class="fa fa-close"></i>Address is invalid.
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="marginbtm15 col-md-4" visible="false" id="divsttype" runat="server">
                                                    <asp:Label ID="Label13" runat="server">Street Type</asp:Label>
                                                    <div id="Div8" class="drpValidate" runat="server">
                                                        <asp:DropDownList ID="ddlformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" AutoPostBack="true" CssClass="myvalcomp" OnSelectedIndexChanged="ddlformbaystreettype_SelectedIndexChanged">
                                                            <asp:ListItem Value="">Street Type</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage=""
                                                            ControlToValidate="ddlformbaystreettype" Display="Dynamic" ValidationGroup="company1" InitialValue=""> </asp:RequiredFieldValidator>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="marginbtm15 col-md-4" id="seq" runat="server">
                                                    <asp:Label ID="lblMobile" runat="server">Street City</asp:Label>
                                                    <div class="autocompletedropdown">
                                                        <asp:TextBox ID="ddlStreetCity" runat="server" MaxLength="50" Enabled="false" OnTextChanged="ddlStreetCity_TextChanged" AutoPostBack="true"
                                                            class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlStreetCity" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                        <cc1:AutoCompleteExtender ID="AutoCompletestreet" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="ddlStreetCity" ServicePath="~/Search.asmx"
                                                            ServiceMethod="GetCitiesList"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                    </div>
                                                </div>
                                                <div class="marginbtm15 col-md-4" id="Div9" runat="server">
                                                    <asp:Label ID="Label15" runat="server">Street State</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtStreetState" runat="server" MaxLength="50" Enabled="false" class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtStreetState" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="marginbtm15 col-md-4" id="Div10" runat="server">
                                                    <asp:Label ID="Label16" runat="server">Street Post Code</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtStreetPostCode" runat="server" MaxLength="50" Enabled="false" class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtStreetPostCode" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                        <%-- <asp:RegularExpressionValidator ID="regularexpressionvalidator1" runat="server" ControlToValidate="txtStreetPostCode"
                                                                    ValidationGroup="company1" Display="dynamic" ErrorMessage="please enter a number"
                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>--%>
                                                        <%-- <asp:RequiredFieldValidator ID="requiredfieldvalidator6" runat="server" ErrorMessage="this value is required." CssClass="comperror"
                                                    ValidationGroup="company1" ControlToValidate="txtStreetPostCode" Display="dynamic"></asp:RequiredFieldValidator>--%>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row checkbox-info checkbox-info" style="padding-top: 16px; margin-bottom: 13px;">
                                                <div class="col-md-12 spicaldivin marginbtm17 ">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td style="padding-right: 4px;">

                                                                <label for="<%=chkSameasAbove.ClientID %>">
                                                                    <asp:CheckBox ID="chkSameasAbove" runat="server" OnCheckedChanged="chkSameasAbove_CheckedChanged1" AutoPostBack="true" />
                                                                    <span class="text">&nbsp;</span>
                                                                </label>

                                                            </td>
                                                            <td>Same as street address </td>
                                                            <%--<asp:Button CssClass="btn btn-primary savewhiteicon" ID="btnUpdateAddress" runat="server" OnClick="btnUpdateAddress_Click"
                                                                                    Text="Update Address" />--%>
                                                            <%--<td runat="server" id="tdupdateaddress" visible="false">
                                                                                <asp:ImageButton ID="btnUpdateAddress" runat="server"
                                                                                    OnClick="btnUpdateAddress_Click" ImageUrl="~/images/icon_updateaddress.png" /></td>--%>
                                                        </tr>
                                                    </table>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div id="Div11" class="spicaldivin marginbtm15" runat="server" visible="false">
                                                <span class="name">
                                                    <label class="control-label">Postal Address Line </label>
                                                </span><span>
                                                    <asp:HiddenField ID="hndaddress1" runat="server" />
                                                    <asp:TextBox ID="txtpostaladdressline" runat="server" MaxLength="50" CssClass="form-control" name="address"> </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rfvpostaladdress" runat="server" ErrorMessage="* Required" Enabled="false"
                                                        ControlToValidate="txtpostaladdressline" Display="Dynamic" ValidationGroup="company"></asp:RequiredFieldValidator>
                                                    <%-- <asp:CustomValidator ID="Custompostaladdress" runat="server"
                                                                ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="company1"
                                                                ClientValidationFunction="ChkFun1"></asp:CustomValidator>--%>
                                                </span>
                                                <asp:HiddenField ID="hndstreetno1" runat="server" />
                                                <asp:HiddenField ID="hndstreetname1" runat="server" />
                                                <asp:HiddenField ID="hndstreettype1" runat="server" />
                                                <asp:HiddenField ID="hndstreetsuffix1" runat="server" />
                                                <asp:HiddenField ID="hndunittype1" runat="server" />
                                                <asp:HiddenField ID="hndunitno1" runat="server" />
                                                <div id="validaddressid1" style="display: none">
                                                    <%--<img src="../../../images/check.png" alt="check">--%>
                                                    <i class="fa fa-check"></i>Address is valid.
                                                </div>
                                                <div id="invalidaddressid1" style="display: none">
                                                    <%--<img src="../../../images/x.png" alt="cross">--%>
                                                    <i class="fa fa-close"></i>Address is invalid.
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="row marginbtm15">
                                                <div id="Div12" runat="server">
                                                    <asp:Label ID="Label17" runat="server" class="col-sm-12">Postal Address</asp:Label>
                                                    <div class="col-md-12">
                                                        <asp:TextBox ID="txtPostalAddress" name="address" Enabled="false" MaxLength="50" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row marginbtm15">
                                                <div class="col-md-6" visible="false" id="divPostalformbayUnitNo" runat="server">
                                                    <asp:Label ID="Label18" runat="server">Postal Unit No</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtPostalformbayUnitNo" name="address" runat="server" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="txtPostalformbayUnitNo_TextChanged"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-6" visible="false" id="divPostalformbayunittype" runat="server">
                                                    <asp:Label ID="Label19" runat="server">Postal Unit Type</asp:Label>
                                                    <div>
                                                        <asp:DropDownList ID="ddlPostalformbayunittype" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalcomp" AutoPostBack="true" OnSelectedIndexChanged="ddlPostalformbayunittype_SelectedIndexChanged">
                                                            <asp:ListItem Value="">Unit Type</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row marginbtm15">
                                                <div class="col-md-4" visible="false" id="divPostalformbayStreetNo" runat="server">
                                                    <asp:Label ID="Label20" runat="server">Postal Street No</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtPostalformbayStreetNo" name="address" runat="server" class="form-control modaltextbox" AutoPostBack="true" OnTextChanged="txtPostalformbayStreetNo_TextChanged"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtPostalformbayStreetNo" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 spicaldivin streatfield" visible="false" id="divPostalformbaystreetname" runat="server">
                                                    <asp:Label ID="Label21" runat="server">Postal Street Name</asp:Label>

                                                    <div class="autocompletedropdown">
                                                        <asp:TextBox ID="txtPostalformbaystreetname" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtPostalformbaystreetname_TextChanged1"></asp:TextBox>


                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtPostalformbaystreetname" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender11" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="txtPostalformbaystreetname" ServicePath="~/Search.asmx"
                                                            ServiceMethod="GetStreetNameList"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />


                                                        <asp:CustomValidator ID="CustomValidator2" runat="server"
                                                            ErrorMessage="" Display="Dynamic" CssClass="requiredfield"
                                                            ClientValidationFunction="postalvalidatestreetAddress" ControlToValidate="txtPostalformbaystreetname"></asp:CustomValidator>

                                                        <div id="PostalDivvalidstreetname" style="display: none">




                                                            <img src="../../../images/check.png" alt="check">Address is valid.
                                                        </div>
                                                        <div id="PostalDivInvalidstreetname" style="display: none">
                                                            <img src="../../../images/x.png" alt="cross">
                                                            Address is invalid.
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="col-md-4" visible="false" id="divpostalformbaystreettype" runat="server">
                                                    <asp:Label ID="Label24" runat="server">Postal Street Type</asp:Label>
                                                    <div id="Div13" class="drpValidate" runat="server">
                                                        <asp:DropDownList ID="ddlPostalformbaystreettype" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myvalcomp" AutoPostBack="true" OnSelectedIndexChanged="ddlPostalformbaystreettype_SelectedIndexChanged">
                                                            <asp:ListItem Value="">Street Type</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlPostalformbaystreettype" Display="Dynamic" ValidationGroup="company1" InitialValue=""></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row marginbtm15">
                                                <div class="col-md-4" id="Div14" runat="server">
                                                    <asp:Label ID="Label25" runat="server">Postal City</asp:Label>
                                                    <div class="autocompletedropdown">
                                                        <asp:TextBox ID="ddlPostalCity" AutoPostBack="true"
                                                            OnTextChanged="ddlPostalCity_OnTextChanged" MaxLength="50" Enabled="false" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="ddlPostalCity" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                        <cc1:AutoCompleteExtender ID="AutoCompleteSearch" MinimumPrefixLength="2" runat="server"
                                                            UseContextKey="true" TargetControlID="ddlPostalCity" ServicePath="~/Search.asmx"
                                                            ServiceMethod="GetCitiesList"
                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                    </div>
                                                </div>
                                                <div class="col-md-4" id="Div15" runat="server">
                                                    <asp:Label ID="Label26" runat="server">Postal States</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtPostalState"
                                                            Enabled="false" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtPostalState" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="col-md-4" id="Div16" runat="server">
                                                    <asp:Label ID="Label27" runat="server">Postal Post Code</asp:Label>
                                                    <div>
                                                        <asp:TextBox ID="txtPostalPostCode"
                                                            Enabled="false" runat="server" class="form-control modaltextbox"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="" CssClass=""
                                                            ControlToValidate="txtPostalPostCode" Display="Dynamic" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                        <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                                                    ValidationGroup="company1" ControlToValidate="txtPostalPostCode" Display="Dynamic"
                                                                    ErrorMessage="Please enter a number" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>--%>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group spicaldivin">
                                                <span class="name">
                                                    <label class="col-sm-12">Area<span class="symbol required"></span> </label>
                                                </span>
                                                <div class="col-sm-12">
                                                    <%--<div class="radio margintopnone">
                                                                            <asp:RadioButtonList ID="rblArea" runat="server" AppendDataBoundItems="true" RepeatDirection="Horizontal">
                                                                                <asp:ListItem Value="1">Metro</asp:ListItem>
                                                                                <asp:ListItem Value="2">Regional</asp:ListItem>
                                                                            </asp:RadioButtonList>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                ValidationGroup="company1" ControlToValidate="rblArea" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                        </div>--%>
                                                    <div class="radio radio-info radio-inline" style="padding-left: 0px;">
                                                        <label for="<%=rblArea1.ClientID %>">
                                                            <asp:RadioButton runat="server" ID="rblArea1" GroupName="ar" />
                                                            <span class="text">Metro&nbsp;</span>
                                                        </label>


                                                        <label for="<%=rblArea2.ClientID %>">
                                                            <asp:RadioButton runat="server" ID="rblArea2" GroupName="ar" />
                                                            <span class="text">Regional&nbsp; </span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="form-group" id="Div17" runat="server">
                                                <asp:Label ID="Label28" runat="server" class="col-sm-12">Country</asp:Label>
                                                <div class="col-md-12">
                                                    <asp:TextBox ID="txtCountry"
                                                        Enabled="false" runat="server" MaxLength="50" value="Australia" class="form-control modaltextbox"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group" id="Div18" runat="server">
                                                <asp:Label ID="Label29" runat="server" class="col-sm-12">ABN</asp:Label>
                                                <div class="col-md-12">
                                                    <asp:TextBox ID="txtCustWebSiteLink"
                                                        runat="server" MaxLength="100" Text="Australia" class="form-control modaltextbox"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group" id="Div19" runat="server">
                                                <asp:Label ID="Label30" runat="server" class="col-sm-12">Fax</asp:Label>
                                                <div class="col-md-12">
                                                    <asp:TextBox ID="txtCustFax"
                                                        runat="server" MaxLength="20" class="form-control modaltextbox"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group col-md-12">
                                    <div class="col-sm-8 col-sm-offset-5">
                                        <asp:Button CssClass="btn btn-primary  redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                            Text="Add" ValidationGroup="company1" />
                                        <asp:Button CssClass="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                            Text="Save" Visible="false" />
                                        <asp:Button CssClass="btn btn-default btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                            CausesValidation="false" Text="Reset" />
                                        <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                            CausesValidation="false" Text="Cancel" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%-- <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="updatepanelgrid" DisplayAfter="0">
                <ProgressTemplate>
                    
                </ProgressTemplate>
            </asp:UpdateProgress>
            <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress1"
                PopupControlID="updateprogress1" BackgroundCssClass="modalPopup z_index_loader" />--%>
                <div class="page-body padtopzero">
                    <asp:Panel runat="server" ID="Panel1" Visible="false">
                        <div class="animate-panel">
                            <div id="div20" runat="server">
                                <div class="messesgarea">
                                    <div class="alert alert-success" id="Div21" runat="server"><i class="icon-ok-sign"></i>&nbsp;Transaction Successful! </div>
                                    <div class="alert alert-danger" id="Div22" runat="server">
                                        <i class="icon-remove-sign"></i>&nbsp;
                                        <asp:Label ID="Label31" runat="server" Text="Transaction Failed." Visible="false"></asp:Label>
                                    </div>
                                    <div class="alert alert-danger" id="Div23" runat="server"><i class="icon-remove-sign" visible="false"></i>&nbsp;Record with this name already exists. </div>
                                    <%--  <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view
                            </div>--%>
                                    <div class="alert alert-info" id="Div24" runat="server"><i class="icon-info-sign" visible="false"></i>&nbsp;Record with this Address already exists. </div>
                                    <div class="alert alert-info" id="div25" runat="server"><i class="icon-info-sign" visible="false"></i>&nbsp;Appointment Time over. </div>
                                </div>
                                <div class="page-body padtopzero">
                                    <div class="searchfinal">

                                        <div class="widget-body shadownone brdrgray">
                                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                <div class="dataTables_filter">
                                                    <div class="dataTables_filter Responsive-search row">
                                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                                            <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <div class="inlineblock">
                                                                            <div class="col-md-12">
                                                                                <div class="input-group col-sm-1">
                                                                                    <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                        <asp:ListItem Value="">Type</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <div class="input-group col-sm-1" style="width: 146px;">
                                                                                    <asp:DropDownList ID="DropDownList2" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                        <asp:ListItem Value="">Rec/Com</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Residential</asp:ListItem>
                                                                                        <asp:ListItem Value="2">Commercial</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <div class="input-group col-sm-1">
                                                                                    <asp:DropDownList ID="DropDownList3" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                        <asp:ListItem Value="">Area</asp:ListItem>
                                                                                        <asp:ListItem Value="1">Metro</asp:ListItem>
                                                                                        <asp:ListItem Value="2">Regional</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <div class="input-group col-sm-2">
                                                                                    <asp:DropDownList ID="DropDownList4" runat="server"
                                                                                        AutoPostBack="true" OnSelectedIndexChanged="ddlSearchSource_SelectedIndexChanged1" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                        <asp:ListItem Value="">Source</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <div class="input-group col-sm-2">
                                                                                    <asp:DropDownList ID="DropDownList5" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                        <asp:ListItem Value="">Sub Source</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <div class="input-group col-sm-2">
                                                                                    <asp:TextBox ID="TextBox1" runat="server" placeholder="CompanyNumber" CssClass="form-control m-b"></asp:TextBox>
                                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtSearchCompanyNo"
                                                                                        WatermarkText="CompanyNumber" />
                                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                                                        UseContextKey="true" TargetControlID="txtSearchCompanyNo" ServicePath="~/Search.asmx"
                                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyNumber"
                                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="search"
                                                                                        ControlToValidate="txtSearchCompanyNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                                        ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                                </div>
                                                                                <div class="input-group col-sm-1">
                                                                                    <asp:DropDownList ID="DropDownList6" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                        <asp:ListItem Value="">State</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                                <div class="input-group col-sm-1">
                                                                                    <asp:DropDownList ID="DropDownList7" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                                        <asp:ListItem Value="">Add.Ver</asp:ListItem>
                                                                                        <asp:ListItem Value="0">NO</asp:ListItem>
                                                                                        <asp:ListItem Value="1">YES</asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="inlineblock">
                                                                            <div class="col-md-12">
                                                                                <div class="input-group col-sm-2 autocompletedropdown">
                                                                                    <asp:TextBox ID="TextBox2" runat="server" placeholder="Company Name" CssClass="form-control m-b"></asp:TextBox>
                                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtSearch"
                                                                                        WatermarkText="Company Name" />
                                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender7" MinimumPrefixLength="2" runat="server"
                                                                                        UseContextKey="true" TargetControlID="txtSearch" ServicePath="~/Search.asmx"
                                                                                        ServiceMethod="GetCompanyList"
                                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                                </div>
                                                                                <div class="input-group col-sm-1" style="width: 200px">
                                                                                    <asp:TextBox ID="TextBox3" runat="server" placeholder="Street" CssClass="form-control m-b"></asp:TextBox>
                                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server" TargetControlID="txtSearchStreet"
                                                                                        WatermarkText="Street" />
                                                                                </div>
                                                                                <div class="input-group col-sm-1" style="width: 200px">
                                                                                    <asp:TextBox ID="TextBox4" runat="server" placeholder="City" CssClass="form-control m-b"></asp:TextBox>
                                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender9" runat="server" TargetControlID="txtSerachCity"
                                                                                        WatermarkText="City" />
                                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender8" MinimumPrefixLength="2" runat="server"
                                                                                        UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                                    </cc1:AutoCompleteExtender>
                                                                                </div>
                                                                                <div class="input-group col-sm-1" style="width: 138px">
                                                                                    <asp:TextBox ID="TextBox5" runat="server" placeholder="Post Code" CssClass="form-control m-b"></asp:TextBox>
                                                                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtSearchPostCode"
                                                                                        WatermarkText="Post Code" />
                                                                                    <cc1:AutoCompleteExtender ID="AutoCompleteExtender9" MinimumPrefixLength="2" runat="server"
                                                                                        UseContextKey="true" TargetControlID="txtSearchPostCode" ServicePath="~/Search.asmx"
                                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                                    </cc1:AutoCompleteExtender>
                                                                                </div>
                                                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                                                    <asp:TextBox ID="TextBox6" placeholder="From" runat="server" class="form-control"></asp:TextBox>
                                                                                    <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="dynamic"
                                                                                            ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                                </div>
                                                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                                                    <asp:TextBox ID="TextBox7" placeholder="To" runat="server" class="form-control"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" Display="dynamic"
                                                                                        ControlToValidate="txtEndDate" ErrorMessage="* Required" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                                                    <asp:CompareValidator ID="CompareValidator2" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                        ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                                        Display="Dynamic" ValidationGroup="company1"></asp:CompareValidator>
                                                                                </div>
                                                                                <div class="input-group">
                                                                                    <asp:Button ID="Button3" runat="server" CausesValidation="false" CssClass="btn btn-primary btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                                </div>
                                                                                <div class="form-group">
                                                                                    <asp:LinkButton ID="LinkButton5" runat="server" data-toggle="tooltip" data-placement="left"
                                                                                        CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="datashowbox">
                                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="Div26" runat="server">
                                                    <div class="row ">
                                                        <div class="dataTables_length showdata col-sm-8">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:DropDownList ID="DropDownList8" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                            aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                            <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                    <%-- <td valign="middle" id="tdAll2" class="paddtop3td" runat="server">&nbsp;View All</td>--%>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <%--  <div class="col-sm-6">
                                                        <div class="checkbox checkbox-info">
                                                            <asp:CheckBox ID="chkActive" runat="server" />
                                                            <label for="<%=chkActive.ClientID %>">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>--%>
                                                            <div class="pull-right martop7 marleft8">
                                                                <div class="checkbox-info ">
                                                                    <!-- <a href="#" class="btn btn-darkorange btn-xs Excel"><i class="fa fa-check-square-o"></i> View All</a>-->

                                                                    <span valign="top" id="Span1" class="paddtop3td alignchkbox btnviewallorange" runat="server" align="right">
                                                                        <label for="<%=chkViewAll.ClientID %>" class="btn btn-darkorange btn-xs">
                                                                            <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" OnCheckedChanged="chkViewAll_OnCheckedChanged" />
                                                                            <span class="text">&nbsp;View All</span>
                                                                        </label>
                                                                    </span>

                                                                </div>
                                                            </div>
                                                            <div id="Div27" class="pull-right btnexelicon" runat="server">

                                                                <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                                                <asp:LinkButton ID="LinkButton6" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                                    CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                                <%-- </ol>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>
                    </asp:Panel>

                    <div class="content padtopzero marbtm50 finalgrid" id="div28" runat="server">
                        <div id="Div29" runat="server">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div id="Div30" runat="server" visible="false">
                                    <div class="">
                                        <asp:GridView ID="GridView2" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                            OnRowDataBound="GridView1_RowDataBound" OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_OnRowCommand"
                                            OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                            OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="20px">
                                                    <ItemTemplate>
                                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("CustomerID") %>','tr<%# Eval("CustomerID") %>');">
                                                            <img id='imgdiv<%# Eval("CustomerID") %>' src="../../../images/icon_plus.png" />
                                                            <%--    <i id='imgdiv<%# Eval("CustomerID") %>' class="fa fa-plus"></i>--%>
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hndCustomerID" runat="server" Value='<%#Eval("CustomerID") %>' />
                                                        <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                        <asp:Label ID="Label2" runat="server" Width="150px"><%#Eval("Customer")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label3" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("StreetAddress")%>'
                                                            Width="180px"><%#Eval("StreetAddress")%> </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label4" runat="server" Width="180px"><%#Eval("Location")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Phone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label5" runat="server" Width="100px"><%#Eval("CustPhone")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label6" runat="server" Width="100px"><%#Eval("ContMobile")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" ItemStyle-Width="260px">
                                                    <ItemTemplate>

                                                        <asp:HyperLink ID="gvbtnDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" CssClass="btn btn-primary btn-xs"
                                                            Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/company/ECompany.aspx?m=comp&compid="+Eval("CustomerID") %>'> <i class="fa fa-link"></i> Detail</asp:HyperLink>


                                                        <asp:LinkButton runat="server" ID="lnkphoto" CommandName="lnkphoto" CssClass="btn btn-warning btn-xs" CommandArgument='<%#Eval("CustomerID")%>' CausesValidation="false" data-toggle="tooltip" data-placement="top" title="" data-original-title="Image"> <i class="fa fa-image"></i> Image</asp:LinkButton>
                                                        <%--  <asp:HyperLink ID="gvbtndetails" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail"
                                                        Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/company/customerimage.aspx?id="+Eval("CustomerID") %>'> <i class="fa fa-eye"></i></asp:HyperLink>--%>


                                                        <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"> <i class="fa fa-edit"></i> Edit </asp:LinkButton>

                                                        <!--DELETE Modal Templates-->

                                                        <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-xs" CausesValidation="false"
                                                            CommandName="Delete" CommandArgument='<%#Eval("CustomerID")%>'>
                        <i class="fa fa-trash"></i> Delete
                                                        </asp:LinkButton>

                                                        <!--DELETE Modal Templates-->


                                                        <!--END DELETE Modal Templates-->
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" CssClass="verticaaline" />
                                                </asp:TemplateField>



                                                <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                    <ItemTemplate>
                                                        <tr id='tr<%# Eval("CustomerID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                            <td colspan="98%" class="details">
                                                                <div id='div<%# Eval("CustomerID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                    <table width="100%" class="table table-bordered table-hover subtablecolor">
                                                                        <tr class="GridviewScrollItem">
                                                                            <td width="180px"><b>Company Number</b></td>
                                                                            <td runat="server" id="tdcompnum">
                                                                                <asp:Label ID="Label1" runat="server" Width="130px"> <%#Eval("CompanyNumber")%> </asp:Label></td>
                                                                            <td width="180px" runat="server" id="tdassignto"><b>Assigned To</b></td>
                                                                            <td runat="server" id="tdassignto1">
                                                                                <asp:Label ID="Label7" runat="server" Width="130px"><%#Eval("AssignedTo")%> </asp:Label></td>
                                                                        </tr>
                                                                        <tr runat="server" id="tdtype">
                                                                            <td><b>Type</b></td>
                                                                            <td>
                                                                                <asp:Label ID="Label8" runat="server" Width="100px"><%#Eval("CustType")%> </asp:Label></td>
                                                                            <td><b>Res Com</b></td>
                                                                            <td>
                                                                                <asp:Label ID="lblres" runat="server" Width="100px"></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Email</b></td>
                                                                            <td>
                                                                                <asp:Label ID="lblEmail" runat="server" Width="100px"><%#Eval("ContEmail")%></asp:Label></td>
                                                                            <td><b>Notes</b></td>
                                                                            <td>
                                                                                <asp:Label ID="lblNotes" runat="server" Width="100px" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("CustNotes")%>'><%#Eval("CustNotes")%></asp:Label></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>Source</b></td>
                                                                            <td>
                                                                                <asp:Label ID="Label9" runat="server" Width="100px"><%#Eval("CustSource")%> </asp:Label></td>
                                                                            <td><b>Sub Source</b></td>
                                                                            <td>
                                                                                <asp:Label ID="lblSubSource" runat="server" Width="100px"></asp:Label></td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <AlternatingRowStyle />
                                            <%--  <PagerTemplate>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        <div class="pagination">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />--%>
                                        </asp:GridView>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div>
                            <div class="hpanel">
                                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                                <div class="bodymianbg">
                                    <div>
                                        <div class="col-md-12" id="div32" runat="server" visible="false">
                                            <cc1:TabContainer ID="TabContainer2" runat="server" ActiveTabIndex="0" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                                AutoPostBack="true">
                                                <cc1:TabPanel ID="TabPanel1" runat="server" HeaderText="Summary">
                                                    <ContentTemplate>
                                                        <div class="table-responsive">
                                                            <uc1:companysummary ID="companysummary2" runat="server" />
                                                        </div>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                                <cc1:TabPanel ID="TabPanel2" runat="server" HeaderText="Contact">
                                                    <ContentTemplate>
                                                        <uc2:contacts ID="contacts2" runat="server" />
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                                <cc1:TabPanel ID="TabPanel3" runat="server" HeaderText="Project">
                                                    <ContentTemplate>
                                                        <uc3:project ID="project2" runat="server" />
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                                <cc1:TabPanel ID="TabPanel4" runat="server" HeaderText="Follow Ups">
                                                    <ContentTemplate>
                                                        <uc4:info ID="info2" runat="server" />
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                                <cc1:TabPanel ID="TabPanel5" runat="server" HeaderText="Conversation" Visible="false">
                                                    <ContentTemplate>
                                                        <uc5:conversation ID="conversation2" runat="server" />
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                            </cc1:TabContainer>
                                        </div>
                                    </div>
                                </div>
                                <asp:Button ID="Button4" Style="display: none;" runat="server" />
                                <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                                    DropShadow="false" PopupControlID="divName" OkControlID="btnOKName" TargetControlID="btnNULLData1">
                                </cc1:ModalPopupExtender>
                                <div id="div33" runat="server" style="display: none" class="modal_popup">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="color-line"></div>
                                            <div class="modal-header text-center">
                                                <h4 class="modal-title" id="H5">Duplicate Name</h4>
                                            </div>
                                            <div class="modal-body paddnone">
                                                <div class="panel-body">
                                                    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                                                        summary="">
                                                        <tbody>
                                                            <tr align="center">
                                                                <td>
                                                                    <h3 class="noline"><b>The following Location(s) have a<br />
                                                                        similar name. Check for Duplicates.</b> </h3>
                                                                </td>
                                                            </tr>
                                                            <tr align="center">
                                                                <td>
                                                                    <asp:Button ID="Button5" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeName_Onclick"
                                                                        Text="Dupe" CausesValidation="false" />
                                                                    <asp:Button ID="Button6" runat="server" OnClick="btnNotDupeName_Onclick" CausesValidation="false"
                                                                        CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                                    <asp:Button ID="Button7" Style="display: none; visible: false;" runat="server"
                                                                        CssClass="btn" Text=" OK " /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="table-responsive">
                                                        <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                            <asp:GridView ID="GridView3" DataKeyNames="ContactID" runat="server" OnPageIndexChanging="rptName_PageIndexChanging" ShowFooter="true"
                                                                PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Contacts">
                                                                        <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                                <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                                <EmptyDataRowStyle Font-Bold="True" />
                                                                <RowStyle CssClass="GridviewScrollItem" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:Button ID="Button8" Style="display: none;" runat="server" />
                                <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                                    DropShadow="false" PopupControlID="divMobileCheck"
                                    OkControlID="btnOKMobile" TargetControlID="btnNULLData2">
                                </cc1:ModalPopupExtender>
                                <div id="div34" runat="server" style="display: none" class="modal_popup">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="color-line"></div>
                                            <div class="modal-header text-center">
                                                <h4 class="modal-title" id="H6">Duplicate Mobile</h4>
                                            </div>
                                            <div class="modal-body paddnone">
                                                <div class="panel-body ">
                                                    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                                                        summary="">
                                                        <tbody>
                                                            <tr align="center">
                                                                <td>
                                                                    <h3 class="noline marbtmzero"><b>This looks like a Duplicate Entry.</b> </h3>
                                                                </td>
                                                            </tr>
                                                            <tr align="center">

                                                                <td>
                                                                    <asp:Button ID="Button9" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeMobile_Onclick"
                                                                        Text="Dupe" CausesValidation="false" />
                                                                    <asp:Button ID="Button10" runat="server" OnClick="btnNotDupeMobile_Onclick"
                                                                        CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                                    <asp:Button ID="Button11" Style="display: none; visible: false;" runat="server"
                                                                        CssClass="btn" Text=" OK " /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="table-responsive">
                                                        <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                            <asp:GridView ID="GridView4" DataKeyNames="ContactID" runat="server" OnPageIndexChanging="rptMobile_PageIndexChanging"
                                                                PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Contacts">
                                                                        <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                                <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                                <EmptyDataRowStyle Font-Bold="True" />
                                                                <RowStyle CssClass="GridviewScrollItem" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:Button ID="Button12" Style="display: none;" runat="server" />
                                <cc1:ModalPopupExtender ID="ModalPopupExtender3" runat="server" BackgroundCssClass="modalbackground"
                                    DropShadow="false" PopupControlID="divPhoneCheck"
                                    OkControlID="btnOKPhone" TargetControlID="btnNULLDataPhone">
                                </cc1:ModalPopupExtender>
                                <div id="div35" runat="server" style="display: none" class="modal_popup">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="color-line"></div>
                                            <div class="modal-header text-center">
                                                <h4 class="modal-title" id="H7">Duplicate Phone</h4>
                                            </div>
                                            <div class="modal-body paddnone">
                                                <div class="panel-body">
                                                    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                                                        summary="">
                                                        <tbody>
                                                            <tr align="center">
                                                                <td>
                                                                    <h3 class="noline marbtmzero"><b>This looks like a Duplicate Entry.</b></h3>
                                                                </td>
                                                            </tr>
                                                            <tr align="center">
                                                                <td>
                                                                    <asp:Button ID="Button13" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupePhone_Onclick"
                                                                        Text="Dupe" CausesValidation="false" />
                                                                    <asp:Button ID="Button14" runat="server" OnClick="btnNotDupePhone_Onclick"
                                                                        CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                                    <asp:Button ID="Button15" Style="display: none; visible: false;" runat="server"
                                                                        CssClass="btn" Text=" OK " /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="table-responsive">
                                                        <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                            <asp:GridView ID="GridView5" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptPhone_PageIndexChanging"
                                                                PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Customer">
                                                                        <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Phone" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate><%# Eval("CustPhone")%> </ItemTemplate>
                                                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                                <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                                <EmptyDataRowStyle Font-Bold="True" />
                                                                <RowStyle CssClass="GridviewScrollItem" />
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:Button ID="Button16" Style="display: none;" runat="server" />
                                <cc1:ModalPopupExtender ID="ModalPopupExtender4" runat="server" BackgroundCssClass="modalbackground"
                                    DropShadow="false" PopupControlID="divEmailCheck"
                                    OkControlID="btnOKEmail" TargetControlID="btnNULLData3">
                                </cc1:ModalPopupExtender>
                                <div id="div36" runat="server" style="display: none" class="modal_popup">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="color-line"></div>
                                            <div class="modal-header text-center">
                                                <h4 class="modal-title" id="H8">Duplicate Email</h4>
                                            </div>
                                            <div class="modal-body paddnone">
                                                <div class="panel-body">
                                                    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                                        <tbody>
                                                            <tr align="center">
                                                                <td>
                                                                    <h3 class="noline marbtmzero"><b>This looks like a Duplicate Entry.</b></h3>
                                                                </td>
                                                            </tr>
                                                            <tr align="center">
                                                                <td>
                                                                    <asp:Button ID="Button17" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeEmail_Onclick"
                                                                        Text="Dupe" CausesValidation="false" />
                                                                    <asp:Button ID="Button18" runat="server" OnClick="btnNotDupeEmail_Onclick"
                                                                        CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                                    <asp:Button ID="Button19" Style="display: none; visible: false;" runat="server"
                                                                        CssClass="btn" Text=" OK " /></td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="table-responsive">
                                                        <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                            <asp:GridView ID="GridView6" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" DataKeyNames="ContactID" runat="server" OnPageIndexChanging="rptEmail_PageIndexChanging"
                                                                PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" AllowSorting="true">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Contacts">
                                                                        <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:Button ID="Button20" Style="display: none;" runat="server" />
                                <cc1:ModalPopupExtender ID="ModalPopupExtender5" runat="server" BackgroundCssClass="modalbackground"
                                    DropShadow="false" PopupControlID="divAddressCheck" CancelControlID="ibtnCancel"
                                    OkControlID="btnOKAddress" TargetControlID="Button2">
                                </cc1:ModalPopupExtender>
                                <div id="div37" runat="server" style="display: none">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <div style="float: right">
                                                    <button id="Button21" runat="server" onclick="ibtnCancel_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">
                                                        Close

                                                    </button>
                                                </div>
                                                <h4 class="modal-title" id="H9">Duplicate Address</h4>
                                            </div>
                                            <div class="modal-body paddnone">
                                                <div class="panel-body">
                                                    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                                        <tbody>
                                                            <tr align="center">
                                                                <td>
                                                                    <h3 class="noline"><b>There is a Contact in the database already who has this address.
                                                    <br />
                                                                        This looks like a Duplicate Entry.</b></h3>
                                                                </td>
                                                            </tr>
                                                            <tr align="center">
                                                                <td>
                                                                    <%--<asp:Button ID="btnDupeAddress" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeAddress_Click"
                                                                            Text="Dupe" CausesValidation="false" />
                                          <asp:Button ID="btnNotDupeAddress" runat="server" OnClick="btnNotDupeAddress_Click"
                                                                            CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                          <asp:Button ID="btnOKAddress" Style="display: none; visible: false;" runat="server"
                                                                            CssClass="btn" Text=" OK " /></td>--%>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="tablescrolldiv">
                                                        <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                            <asp:GridView ID="GridView7" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptEmail_PageIndexChanging"
                                                                PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" AllowSorting="true">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Customers">
                                                                        <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Street Address" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate><%#Eval("StreetAddress")+" "+Eval("StreetCity")+" "+Eval("StreetState") %> </ItemTemplate>
                                                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:HiddenField runat="server" ID="HiddenField1" />
                                <asp:Button ID="Button22" Style="display: none;" runat="server" />

                                <asp:Button ID="btnNULLData12" Style="display: none;" runat="server" />
                                <cc1:ModalPopupExtender ID="ModalPopupExtender6" runat="server" BackgroundCssClass="modalbackground"
                                    CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="div_popup" TargetControlID="btnNULLData12">
                                </cc1:ModalPopupExtender>
                                <div id="div_popup" runat="server" class="modal_popup" style="display: none">
                                    <%--style="width: 900px; overflow-x: scroll;display:none;"--%>
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="color-line"></div>
                                            <div class="modal-header">
                                                 <div style="float: right">
                                                    <button id="ibtnCancelStatus" runat="server" onclick="ibtnCancel1_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">
                                                        Close

                                                    </button>
                                                    <!-- <a href="javascript:printContent();" class="printicon marright15"> <i class="icon-print printpage fa fa-print"></i> </a> -->
                                                    <a href="javascript:printContent();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>
                                                </div>
                                                <h4 class="modal-title" id="H10">View Image</h4>
                                            </div>
                                            <div class="modal-body paddnone">
                                                <div class="panel-body">
                                                    <div class="panel-body formareapop" style="background: none!important; min-height: 50px!important; max-height: 550px!important; overflow: auto!important;">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group dateimgarea" id="printimage" runat="server">
                                                                    <div class="lightBoxGallery">
                                                                        <asp:Repeater ID="RepeaterImages" runat="server">
                                                                            <ItemTemplate>
                                                                                <div class="galleryimage" style="border: 2px solid #000;">
                                                                                    <asp:HyperLink ID="hyp1" runat="server" NavigateUrl='<%# "~/userfiles/customerimage/" +Eval("Imagename") %>' title="Image from Unsplash" data-gallery="">
                                                                                        <asp:Image ID="imgcustomer" runat="server" BorderStyle="Outset" ImageUrl='<%# "~/userfiles/customerimage/" +Eval("Imagename") %>' CssClass="wdth100" />
                                                                                    </asp:HyperLink>
                                                                                    <asp:LinkButton ID="gvbtnDelete" runat="server" CommandName="Delete" CausesValidation="false" OnClientClick="return ComfirmDelete(event,this)"
                                                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="delete" class="btndeleted" CommandArgument='<%# Eval("CustImageID") %>'> <i class="fa fa-times-circle-o"></i> </asp:LinkButton>
                                                                                    <div style='clear: both; page-break-before: always;'></div>
                                                                                    <%--   <a href="#" class="btndeleted"><i class="fa fa-times-circle-o"></i></a>--%>
                                                                                </div>
                                                                                <br />

                                                                            </ItemTemplate>
                                                                        </asp:Repeater>
                                                                    </div>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <%-- <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="updatepanelgrid" DisplayAfter="0">
                <ProgressTemplate>
                    
                </ProgressTemplate>
            </asp:UpdateProgress>
            <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress1"
                PopupControlID="updateprogress1" BackgroundCssClass="modalPopup z_index_loader" />--%>
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div id="divleft" runat="server">
                            <div class="messesgarea">
                                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false"><i class="icon-ok-sign"></i>&nbsp;Transaction Successful! </div>
                                <div class="alert alert-danger" id="PanError" visible="false" runat="server">
                                    <i class="icon-remove-sign"></i>&nbsp;
                <asp:Label ID="lblError" runat="server" Text="Transaction Failed." Visible="false"></asp:Label>
                                </div>
                                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false"><i class="icon-remove-sign"></i>&nbsp;Record with this name already exists. </div>
                                <%--  <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view
                            </div>--%>
                                <div class="alert alert-info" id="PAnAddress" runat="server" visible="false"><i class="icon-info-sign"></i>&nbsp;Record with this Address already exists. </div>
                                <div class="alert alert-info" id="divTeamTime" runat="server" visible="false"><i class="icon-info-sign"></i>&nbsp;Appointment Time over. </div>
                            </div>

                            <div class="searchfinal">
                                <div class="widget-body shadownone brdrgray">
                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                        <div class="dataTables_filter">
                                            <div class="dataTables_filter Responsive-search row">
                                                <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="inlineblock">
                                                                <div class="col-md-12">
                                                                    <div class="input-group col-sm-1">
                                                                        <asp:DropDownList ID="ddlSearchType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                            <asp:ListItem Value="">Type</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-1" style="width: 146px;">
                                                                        <asp:DropDownList ID="ddlSearchRec" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                            <asp:ListItem Value="">Rec/Com</asp:ListItem>
                                                                            <asp:ListItem Value="1">Residential</asp:ListItem>
                                                                            <asp:ListItem Value="2">Commercial</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-1">
                                                                        <asp:DropDownList ID="ddlSearchArea" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                            <asp:ListItem Value="">Area</asp:ListItem>
                                                                            <asp:ListItem Value="1">Metro</asp:ListItem>
                                                                            <asp:ListItem Value="2">Regional</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-2">
                                                                        <asp:DropDownList ID="ddlSearchSource" runat="server"
                                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSearchSource_SelectedIndexChanged1" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                            <asp:ListItem Value="">Source</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-2">
                                                                        <asp:DropDownList ID="ddlSearchSubSource" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                            <asp:ListItem Value="">Sub Source</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-2">
                                                                        <asp:TextBox ID="txtSearchCompanyNo" runat="server" placeholder="CompanyNumber" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtSearchCompanyNo"
                                                                            WatermarkText="CompanyNumber" />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtSearchCompanyNo" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyNumber"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                            ControlToValidate="txtSearchCompanyNo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                            ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                    </div>
                                                                    <div class="input-group col-sm-1">
                                                                        <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                            <asp:ListItem Value="">State</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                    <div class="input-group col-sm-1">
                                                                        <asp:DropDownList ID="ddladdressverification" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                            <asp:ListItem Value="">Add.Ver</asp:ListItem>
                                                                            <asp:ListItem Value="0">NO</asp:ListItem>
                                                                            <asp:ListItem Value="1">YES</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="inlineblock">
                                                                <div class="col-md-12">
                                                                    <div class="input-group col-sm-2 autocompletedropdown">
                                                                        <asp:TextBox ID="txtSearch" runat="server" placeholder="Company Name" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                                                                            WatermarkText="Company Name" />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtSearch" ServicePath="~/Search.asmx"
                                                                            ServiceMethod="GetCompanyList"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                    </div>
                                                                    <div class="input-group col-sm-1" style="width: 200px">
                                                                        <asp:TextBox ID="txtSearchStreet" runat="server" placeholder="Street" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txtSearchStreet"
                                                                            WatermarkText="Street" />
                                                                    </div>
                                                                    <div class="input-group col-sm-1" style="width: 200px">
                                                                        <asp:TextBox ID="txtSerachCity" runat="server" placeholder="City" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtSerachCity"
                                                                            WatermarkText="City" />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                        </cc1:AutoCompleteExtender>
                                                                    </div>
                                                                    <div class="input-group col-sm-1" style="width: 138px">
                                                                        <asp:TextBox ID="txtSearchPostCode" runat="server" placeholder="Post Code" CssClass="form-control m-b"></asp:TextBox>
                                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtSearchPostCode"
                                                                            WatermarkText="Post Code" />
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtSearchPostCode" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                        </cc1:AutoCompleteExtender>
                                                                    </div>
                                                                    <div class="input-group date datetimepicker1 col-sm-2">
                                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                                        <asp:TextBox ID="txtStartDate" placeholder="From" runat="server" class="form-control"></asp:TextBox>
                                                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="dynamic"
                                                                                            ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                    </div>
                                                                    <div class="input-group date datetimepicker1 col-sm-2">
                                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                                        <asp:TextBox ID="txtEndDate" placeholder="To" runat="server" class="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="dynamic"
                                                                            ControlToValidate="txtEndDate" ErrorMessage="* Required" ValidationGroup="company1"></asp:RequiredFieldValidator>
                                                                        <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                            Display="Dynamic" ValidationGroup="company1"></asp:CompareValidator>
                                                                    </div>
                                                                    <div class="input-group">
                                                                        <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-primary btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:LinkButton ID="btnClearAll" runat="server" data-toggle="tooltip" data-placement="left"
                                                                            CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="datashowbox">
                                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="PanSearch" runat="server">
                                            <div class="row ">
                                                <div class="dataTables_length showdata col-sm-8">
                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                    aria-controls="DataTables_Table_0" class="myvalcomp">
                                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <%-- <td valign="middle" id="tdAll2" class="paddtop3td" runat="server">&nbsp;View All</td>--%>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-md-4">
                                                    <%--  <div class="col-sm-6">
                                                        <div class="checkbox checkbox-info">
                                                            <asp:CheckBox ID="chkActive" runat="server" />
                                                            <label for="<%=chkActive.ClientID %>">
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>--%>
                                                    <div class="pull-right martop7 marleft8">
                                                        <div class="checkbox-info ">
                                                            <!-- <a href="#" class="btn btn-darkorange btn-xs Excel"><i class="fa fa-check-square-o"></i> View All</a>-->

                                                            <span valign="top" id="tdAll1" class="paddtop3td alignchkbox btnviewallorange" runat="server" align="right">
                                                                <label for="<%=chkViewAll.ClientID %>" class="btn btn-darkorange btn-xs">
                                                                    <asp:CheckBox ID="chkViewAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkViewAll_OnCheckedChanged" />
                                                                    <span class="text">&nbsp;View All</span>
                                                                </label>
                                                            </span>

                                                        </div>
                                                    </div>
                                                    <div id="tdExport" class="pull-right btnexelicon" runat="server">

                                                        <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                                        <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                            CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                        <%-- </ol>--%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </asp:Panel>

                <div class="content padtopzero marbtm50 finalgrid" id="divgrid" runat="server">
                    <div id="leftgrid" runat="server">
                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                            <div id="PanGrid" runat="server" visible="false">
                                <div class="">
                                    <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                        OnRowDataBound="GridView1_RowDataBound" OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_OnRowCommand"
                                        OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                        OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="20px">
                                                <ItemTemplate>
                                                    <a href="JavaScript:divexpandcollapse('div<%# Eval("CustomerID") %>','tr<%# Eval("CustomerID") %>');">
                                                        <img id='imgdiv<%# Eval("CustomerID") %>' src="../../../images/icon_plus.png" />
                                                        <%--    <i id='imgdiv<%# Eval("CustomerID") %>' class="fa fa-plus"></i>--%>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Company Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hndCustomerID" runat="server" Value='<%#Eval("CustomerID") %>' />
                                                    <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                    <asp:Label ID="Label2" runat="server" Width="150px"><%#Eval("Customer")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("StreetAddress")%>'
                                                        Width="180px"><%#Eval("StreetAddress")%> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label4" runat="server" Width="180px"><%#Eval("Location")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Phone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="server" Width="100px"><%#Eval("CustPhone")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6" runat="server" Width="100px"><%#Eval("ContMobile")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" ItemStyle-Width="260px">
                                                <ItemTemplate>

                                                    <asp:HyperLink ID="gvbtnDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" CssClass="btn btn-primary btn-xs"
                                                        Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/company/ECompany.aspx?m=comp&compid="+Eval("CustomerID") %>'> <i class="fa fa-link"></i> Detail</asp:HyperLink>


                                                    <asp:LinkButton runat="server" ID="lnkphoto" CommandName="lnkphoto" CssClass="btn btn-warning btn-xs" CommandArgument='<%#Eval("CustomerID")%>' CausesValidation="false" data-toggle="tooltip" data-placement="top" title="" data-original-title="Image"> <i class="fa fa-image"></i> Image</asp:LinkButton>
                                                    <%--  <asp:HyperLink ID="gvbtndetails" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail"
                                                        Target="_blank" NavigateUrl='<%# "~/admin/adminfiles/company/customerimage.aspx?id="+Eval("CustomerID") %>'> <i class="fa fa-eye"></i></asp:HyperLink>--%>


                                                    <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"> <i class="fa fa-edit"></i> Edit </asp:LinkButton>





                                                    <!--END DELETE Modal Templates-->
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <!--DELETE Modal Templates-->

                                                    <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-xs" CausesValidation="false"
                                                        CommandName="Delete" CommandArgument='<%#Eval("CustomerID")%>'>
                        <i class="fa fa-trash"></i> Delete
                                                    </asp:LinkButton>

                                                    <!--DELETE Modal Templates-->
                                                </ItemTemplate>
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                <ItemTemplate>
                                                    <tr id='tr<%# Eval("CustomerID") %>' style="display: none;" class="dataTable GridviewScrollItem left-text">
                                                        <td colspan="98%" class="details">
                                                            <div id='div<%# Eval("CustomerID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                <table width="100%" class="table table-bordered table-hover subtablecolor">
                                                                    <tr class="GridviewScrollItem">
                                                                        <td width="180px"><b>Company Number</b></td>
                                                                        <td runat="server" id="tdcompnum">
                                                                            <asp:Label ID="Label1" runat="server" Width="130px"> <%#Eval("CompanyNumber")%> </asp:Label></td>
                                                                        <td width="180px" runat="server" id="tdassignto"><b>Assigned To</b></td>
                                                                        <td runat="server" id="tdassignto1">
                                                                            <asp:Label ID="Label7" runat="server" Width="130px"><%#Eval("AssignedTo")%> </asp:Label></td>
                                                                    </tr>
                                                                    <tr runat="server" id="tdtype">
                                                                        <td><b>Type</b></td>
                                                                        <td>
                                                                            <asp:Label ID="Label8" runat="server" Width="100px"><%#Eval("CustType")%> </asp:Label></td>
                                                                        <td><b>Res Com</b></td>
                                                                        <td>
                                                                            <asp:Label ID="lblres" runat="server" Width="100px"></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Email</b></td>
                                                                        <td>
                                                                            <asp:Label ID="lblEmail" runat="server" Width="100px"><%#Eval("ContEmail")%></asp:Label></td>
                                                                        <td><b>Notes</b></td>
                                                                        <td>
                                                                            <asp:Label ID="lblNotes" runat="server" Width="100px" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("CustNotes")%>'><%#Eval("CustNotes")%></asp:Label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Source</b></td>
                                                                        <td>
                                                                            <asp:Label ID="Label9" runat="server" Width="100px"><%#Eval("CustSource")%> </asp:Label></td>
                                                                        <td><b>Sub Source</b></td>
                                                                        <td>
                                                                            <asp:Label ID="lblSubSource" runat="server" Width="100px"></asp:Label></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <AlternatingRowStyle />
                                        <%--  <PagerTemplate>
                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                    <div class="pagination">
                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                    </div>
                                </PagerTemplate>
                                <PagerStyle CssClass="paginationGrid" />
                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />--%>
                                    </asp:GridView>
                                </div>
                                <div class="paginationnew1" runat="server" id="divnopage">
                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
<%--                                            </td>
                                            <td style="float: right;">--%>
                                                <div class="pagination paginationGrid">
                                                    <asp:LinkButton ID="lnkfirst" runat="server" CausesValidation="false" CssClass="Linkbutton firstpage nextbtn" OnClick="lnkfirst_Click">First</asp:LinkButton>
                                                    <asp:LinkButton ID="lnkprevious" runat="server" CausesValidation="false" CssClass="Linkbutton prebtn" OnClick="lnkprevious_Click">Previous</asp:LinkButton>
                                                    <asp:Repeater runat="server" ID="rptpage" OnItemCommand="rptpage_ItemCommand">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="psotId" runat="server" Value='<%#Eval("ID") %>' />
                                                            <asp:LinkButton runat="server" ID="lnkpagebtn" CssClass="Linkbutton" CausesValidation="false" CommandArgument='<%#Eval("ID") %>' CommandName="Pagebtn"><%#Eval("ID") %></asp:LinkButton>

                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <asp:LinkButton ID="lnknext" runat="server" CausesValidation="false" CssClass="Linkbutton nextbtn" OnClick="lnknext_Click">Next</asp:LinkButton>
                                                    <asp:LinkButton ID="lnklast" runat="server" CausesValidation="false" CssClass="Linkbutton lastpage nextbtn" OnClick="lnklast_Click">Last</asp:LinkButton>
                                                </div>
                                            </td>

                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="hpanel">
                            <asp:Literal ID="lblCount" runat="server"></asp:Literal>
                            <div class="bodymianbg">
                                <div>
                                    <div class="col-md-12" id="divright" runat="server" visible="false">
                                        <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" OnActiveTabChanged="TabContainer1_ActiveTabChanged"
                                            AutoPostBack="true">
                                            <cc1:TabPanel ID="TabSummary" runat="server" HeaderText="Summary">
                                                <ContentTemplate>
                                                    <div class="table-responsive">
                                                        <uc1:companysummary ID="companysummary1" runat="server" />
                                                    </div>
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="TabContact" runat="server" HeaderText="Contact">
                                                <ContentTemplate>
                                                    <uc2:contacts ID="contacts1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="TabProject" runat="server" HeaderText="Project">
                                                <ContentTemplate>
                                                    <uc3:project ID="project1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="TabInfo" runat="server" HeaderText="Follow Ups">
                                                <ContentTemplate>
                                                    <uc4:info ID="info1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                            <cc1:TabPanel ID="TabConversation" runat="server" HeaderText="Conversation" Visible="false">
                                                <ContentTemplate>
                                                    <uc5:conversation ID="conversation1" runat="server" />
                                                </ContentTemplate>
                                            </cc1:TabPanel>
                                        </cc1:TabContainer>
                                    </div>
                                </div>
                            </div>


                            <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderName" runat="server" BackgroundCssClass="modalbackground"
                                DropShadow="false" PopupControlID="divName" OkControlID="btnOKName" TargetControlID="btnNULLData1">
                            </cc1:ModalPopupExtender>
                            <div id="divName" runat="server" style="display: none" class="modal_popup">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="color-line"></div>
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title" id="myModalLabel">Duplicate Name</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body">
                                                <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                                                    summary="">
                                                    <tbody>
                                                        <tr align="center">
                                                            <td>
                                                                <h3 class="noline"><b>The following Location(s) have a<br />
                                                                    similar name. Check for Duplicates.</b> </h3>
                                                            </td>
                                                        </tr>
                                                        <tr align="center">
                                                            <td>
                                                                <asp:Button ID="btnDupeName" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeName_Onclick"
                                                                    Text="Dupe" CausesValidation="false" />
                                                                <asp:Button ID="btnNotDupeName" runat="server" OnClick="btnNotDupeName_Onclick" CausesValidation="false"
                                                                    CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                                <asp:Button ID="btnOKName" Style="display: none; visible: false;" runat="server"
                                                                    CssClass="btn" Text=" OK " /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="table-responsive">
                                                    <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                        <asp:GridView ID="rptName" DataKeyNames="ContactID" runat="server" OnPageIndexChanging="rptName_PageIndexChanging" ShowFooter="true"
                                                            PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Contacts">
                                                                    <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                            <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                            <EmptyDataRowStyle Font-Bold="True" />
                                                            <RowStyle CssClass="GridviewScrollItem" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:Button ID="btnNULLData2" Style="display: none;" runat="server" />
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderMobile" runat="server" BackgroundCssClass="modalbackground"
                                DropShadow="false" PopupControlID="divMobileCheck"
                                OkControlID="btnOKMobile" TargetControlID="btnNULLData2">
                            </cc1:ModalPopupExtender>
                            <div id="divMobileCheck" runat="server" style="display: none" class="modal_popup">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="color-line"></div>
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title" id="H1">Duplicate Mobile</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body ">
                                                <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                                                    summary="">
                                                    <tbody>
                                                        <tr align="center">
                                                            <td>
                                                                <h3 class="noline marbtmzero"><b>This looks like a Duplicate Entry.</b> </h3>
                                                            </td>
                                                        </tr>
                                                        <tr align="center">
                                                            <td>
                                                                <asp:Button ID="btnDupeMobile" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeMobile_Onclick"
                                                                    Text="Dupe" CausesValidation="false" />
                                                                <asp:Button ID="btnDupeNotMobile" runat="server" OnClick="btnNotDupeMobile_Onclick"
                                                                    CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                                <asp:Button ID="btnOKMobile" Style="display: none; visible: false;" runat="server"
                                                                    CssClass="btn" Text=" OK " /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="table-responsive">
                                                    <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                        <asp:GridView ID="rptMobile" DataKeyNames="ContactID" runat="server" OnPageIndexChanging="rptMobile_PageIndexChanging"
                                                            PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Contacts">
                                                                    <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                            <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                            <EmptyDataRowStyle Font-Bold="True" />
                                                            <RowStyle CssClass="GridviewScrollItem" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:Button ID="btnNULLDataPhone" Style="display: none;" runat="server" />
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderPhone" runat="server" BackgroundCssClass="modalbackground"
                                DropShadow="false" PopupControlID="divPhoneCheck"
                                OkControlID="btnOKPhone" TargetControlID="btnNULLDataPhone">
                            </cc1:ModalPopupExtender>
                            <div id="divPhoneCheck" runat="server" style="display: none" class="modal_popup">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="color-line"></div>
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title" id="H2">Duplicate Phone</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body">
                                                <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                                                    summary="">
                                                    <tbody>
                                                        <tr align="center">
                                                            <td>
                                                                <h3 class="noline marbtmzero"><b>This looks like a Duplicate Entry.</b></h3>
                                                            </td>
                                                        </tr>
                                                        <tr align="center">
                                                            <td>
                                                                <asp:Button ID="btnDupePhone" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupePhone_Onclick"
                                                                    Text="Dupe" CausesValidation="false" />
                                                                <asp:Button ID="btnNotDupePhone" runat="server" OnClick="btnNotDupePhone_Onclick"
                                                                    CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                                <asp:Button ID="btnOKPhone" Style="display: none; visible: false;" runat="server"
                                                                    CssClass="btn" Text=" OK " /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="table-responsive">
                                                    <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                        <asp:GridView ID="rptPhone" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptPhone_PageIndexChanging"
                                                            PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Customer">
                                                                    <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Phone" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("CustPhone")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                                            <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                                            <EmptyDataRowStyle Font-Bold="True" />
                                                            <RowStyle CssClass="GridviewScrollItem" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:Button ID="btnNULLData3" Style="display: none;" runat="server" />
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderEmail" runat="server" BackgroundCssClass="modalbackground"
                                DropShadow="false" PopupControlID="divEmailCheck"
                                OkControlID="btnOKEmail" TargetControlID="btnNULLData3">
                            </cc1:ModalPopupExtender>
                            <div id="divEmailCheck" runat="server" style="display: none" class="modal_popup">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="color-line"></div>
                                        <div class="modal-header text-center">
                                            <h4 class="modal-title" id="H3">Duplicate Email</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body">
                                                <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                                    <tbody>
                                                        <tr align="center">
                                                            <td>
                                                                <h3 class="noline marbtmzero"><b>This looks like a Duplicate Entry.</b></h3>
                                                            </td>
                                                        </tr>
                                                        <tr align="center">
                                                            <td>
                                                                <asp:Button ID="btnDupeEmail" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeEmail_Onclick"
                                                                    Text="Dupe" CausesValidation="false" />
                                                                <asp:Button ID="btnNotDupeEmail" runat="server" OnClick="btnNotDupeEmail_Onclick"
                                                                    CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                                                <asp:Button ID="btnOKEmail" Style="display: none; visible: false;" runat="server"
                                                                    CssClass="btn" Text=" OK " /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="table-responsive">
                                                    <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                        <asp:GridView ID="rptEmail" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" DataKeyNames="ContactID" runat="server" OnPageIndexChanging="rptEmail_PageIndexChanging"
                                                            PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" AllowSorting="true">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Contacts">
                                                                    <ItemTemplate><%# Eval("Contacts")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Location" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("Locations")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Mobile" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("ContMobile")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Email" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%# Eval("ContEmail")%> </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:Button ID="Button2" Style="display: none;" runat="server" />
                            <cc1:ModalPopupExtender ID="ModalPopupExtenderAddress" runat="server" BackgroundCssClass="modalbackground"
                                DropShadow="false" PopupControlID="divAddressCheck" CancelControlID="ibtnCancel"
                                OkControlID="btnOKAddress" TargetControlID="Button2">
                            </cc1:ModalPopupExtender>
                            <div id="divAddressCheck" runat="server" style="display: none" class="modal_popup">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="color-line"></div>
                                        <div class="modal-header">
                                             <div style="float: right">
                                            <button id="ibtnCancel" runat="server" onclick="ibtnCancel_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">
                                                Close
                                            </button>
                                                 </div>
                                            <h4 class="modal-title" id="H4">Duplicate Address</h4>
                                        </div>
                                        <div class="modal-body paddnone">
                                            <div class="panel-body">
                                                <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                                                    <tbody>
                                                        <tr align="center">
                                                            <td>
                                                                <h4 class="noline"><b>There is a Contact in the database already who has this address.
                                                    <br />
                                                                    This looks like a Duplicate Entry.</b></h4>
                                                            </td>
                                                        </tr>
                                                        <tr align="center">
                                                            <td>
                                                                <%--<asp:Button ID="btnDupeAddress" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeAddress_Click"
                                                                            Text="Dupe" CausesValidation="false" />
                                          <asp:Button ID="btnNotDupeAddress" runat="server" OnClick="btnNotDupeAddress_Click"
                                                                            CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                          <asp:Button ID="btnOKAddress" Style="display: none; visible: false;" runat="server"
                                                                            CssClass="btn" Text=" OK " /></td>--%>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="tablescrolldiv">
                                                    <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                        <asp:GridView ID="rptaddress" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" DataKeyNames="CustomerID" runat="server" OnPageIndexChanging="rptEmail_PageIndexChanging"
                                                            PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" AllowSorting="true">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Customers">
                                                                    <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Street Address" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate><%#Eval("StreetAddress")+" "+Eval("StreetCity")+" "+Eval("StreetState") %> </ItemTemplate>
                                                                    <ItemStyle Width="150px" HorizontalAlign="Left" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:HiddenField runat="server" ID="hdnupdateaddress" />
                            <asp:Button ID="Button1" Style="display: none;" runat="server" />
                        </div>
                    </div>
                </div>
            </div>

            <asp:HiddenField runat="server" ID="hdncountdata" />


            <!--Danger Modal Templates-->
            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>
            <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content ">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header text-center">
                            <i class="glyphicon glyphicon-fire"></i>
                        </div>


                        <div class="modal-title">Delete</div>
                        <label id="ghh" runat="server"></label>
                        <div class="modal-body ">Are You Sure Delete This Entry?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                            <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>

            <asp:HiddenField ID="hdndelete" runat="server" />
            <asp:HiddenField ID="hdndelete1" runat="server" />

            <!--End Danger Modal Templates-->


        </ContentTemplate>
        <Triggers>

            <asp:PostBackTrigger ControlID="lnkAdd" />
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <asp:PostBackTrigger ControlID="chkViewAll" />
            <%--<asp:PostBackTrigger ControlID="TabContainer1" />--%>
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

