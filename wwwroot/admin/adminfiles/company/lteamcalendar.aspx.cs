﻿using System;
using System.Data;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.UI;

public partial class admin_adminfiles_company_lteamcalendar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            PanSuccess.Visible = false;
            int offset = DateTime.Now.AddHours(14).DayOfWeek - DayOfWeek.Monday;
            DateTime lastMonday = DateTime.Now.AddHours(14).AddDays(-offset);
            BindWeekDates(lastMonday);

            cmpNextDate.ValueToCompare = DateTime.Now.AddHours(14).ToShortDateString();
            cmpNextDate1.ValueToCompare = DateTime.Now.AddHours(14).ToShortDateString();
        }
    }
    public void BindWeekDates(DateTime startdate)
    {
        ArrayList values = new ArrayList();
        for (int i = 0; i < 7; i++)
        {
            values.Add(new PositionData(startdate.AddDays(i).ToShortDateString(), CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(startdate.AddDays(i).DayOfWeek)));
        }

        RptDays.DataSource = values;
        RptDays.DataBind();

        RepeaterItem item0 = RptDays.Items[0];
        string hdnDate0 = ((HiddenField)item0.FindControl("hdnDate")).Value;
        RepeaterItem item6 = RptDays.Items[6];
        string hdnDate6 = ((HiddenField)item6.FindControl("hdnDate")).Value;

        ltdate.Text = string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(hdnDate0)) + " - " + string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(hdnDate6));
    }
    public class PositionData
    {
        private string CDate;
        private string CDay;
        public PositionData(string CDate, string CDay)
        {
            this.CDate = CDate;
            this.CDay = CDay;
        }
        public string CDATE
        {
            get
            {
                return CDate;
            }
        }
        public string CDAY
        {
            get
            {
                return CDay;
            }
        }
    }

    protected void btntoday_Click(object sender, EventArgs e)
    {
        BindWeekDates(DateTime.Now.AddHours(14));
    }

    protected void btnnextweek_Click(object sender, EventArgs e)
    {
        if (RptDays.Items.Count > 6)
        {
            RepeaterItem item = RptDays.Items[6];
            string enddate = ((HiddenField)item.FindControl("hdnDate")).Value;
            BindWeekDates(Convert.ToDateTime(enddate).AddDays(1));
        }
    }
    protected void btnpreviousweek_Click(object sender, EventArgs e)
    {
        if (RptDays.Items.Count > 6)
        {
            RepeaterItem item = RptDays.Items[0];
            string startdate = ((HiddenField)item.FindControl("hdnDate")).Value;
            BindWeekDates(Convert.ToDateTime(startdate).AddDays(-7));
        }
    }
    protected void btnpreviousday_Click(object sender, EventArgs e)
    {
        if (RptDays.Items.Count > 6)
        {
            RepeaterItem item = RptDays.Items[0];
            string startdate = ((HiddenField)item.FindControl("hdnDate")).Value;
            BindWeekDates(Convert.ToDateTime(startdate).AddDays(-1));
        }
    }
    protected void btnnextday_Click(object sender, EventArgs e)
    {
        if (RptDays.Items.Count > 6)
        {
            RepeaterItem item = RptDays.Items[0];
            string enddate = ((HiddenField)item.FindControl("hdnDate")).Value;
            BindWeekDates(Convert.ToDateTime(enddate).AddDays(1));
        }
    }

    protected void RptDays_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string userid1 = "";
        if (Roles.IsUserInRole("leadgen"))
        {
            userid1 = userid;
        }
        else if (Roles.IsUserInRole("Lead Manager"))
        {
            userid1 = userid;
            userid = "";
        }
        else
        {
            userid = "";
            userid1 = "";
        }
        HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");

        DataTable dt = ClstblCustomers.tblCustomers_GetLTeamCalendar(hdnDate.Value, userid, userid1);
        Repeater rptinstaller1 = (Repeater)e.Item.FindControl("rptinstaller1");
        rptinstaller1.DataSource = dt;
        rptinstaller1.DataBind();
    }
    protected void rptinstaller1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = e.CommandArgument.ToString();
        HiddenField hndd2daapdate = (HiddenField)e.Item.FindControl("hndd2daapdate");
        string appointment_status = "";
        string customer_lead_status = "";
        string D2DAppDate = hndd2daapdate.Value;
        hndD2Ddate.Value = D2DAppDate;
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        if (e.CommandName.ToLower() == "confirmed")
        {
            appointment_status = "1";
            bool success = ClstblCustomers.tblCustomers_Updateappointment_status(appointment_status, id);
            bool success2 = ClstblCustomers.tblCustomers_UpdateAppFixBy(id, userid);
        }

        if (e.CommandName.ToLower() == "notconfirmed")
        {
            appointment_status = "2";
            customer_lead_status = "2";
            //bool success = ClstblCustomers.tblCustomers_Updateappointment_status(appointment_status, id);
            //bool success1 = ClstblCustomers.tblCustomers_Updatecustomer_lead_status(customer_lead_status, id);
            //bool success2 = ClstblContacts.tblContacts_UpdateContLeadStatusByCustID("4", id);
            //bool success3 = ClstblCustomers.tblCustomers_UpdateAppFixBy(id, userid);
        }

        if (e.CommandName.ToLower() == "rescheduleyes")
        {
            ClstblCustomers.tblCustomers_Updateappointment_status("1", id);
            ClstblCustomers.tblCustomers_Updatecustomer_lead_status("0", id);
        }

        if (e.CommandName.ToLower() == "rescheduleno")
        {
            ModalPopupExtender2.Show();
            divfollowsup.Visible = false;
            divappointment.Visible = false;
            divCancel.Visible = true;
            rblcustleadstatus.Visible = false;
            hndcustomerid.Value = e.CommandArgument.ToString();
        }
        if (e.CommandName.ToLower() == "reschnotes")
        {
            string CustomerID = e.CommandArgument.ToString();
            ModalPopupExtenderNotes.Show();
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
            lblAppTime.Text = stCust.AppTime;
            try
            {
                lblAppDate.Text = Convert.ToDateTime(stCust.D2DAppDate).ToShortDateString();
            }
            catch { }
            lblAppNotes.Text = stCust.D2DAppNote;
        }

        if (e.CommandName.ToLower() == "chngstatus")
        {
            hndcustomerid.Value = e.CommandArgument.ToString();
            DataTable dt = ClstblCustomers.tblCustLeadStatus_Select();
            if (dt.Rows.Count > 0)
            {
                ModalPopupExtender2.Show();

                rblcustleadstatus.DataSource = dt;
                rblcustleadstatus.DataValueField = "CustLeadStatusID";
                rblcustleadstatus.DataTextField = "CustLeadStatus";
                rblcustleadstatus.DataMember = "CustLeadStatus";
                rblcustleadstatus.DataBind();

                divInstallgrid.Visible = true;
                divappointment.Visible = false;
                divfollowsup.Visible = false;
            }
            else
            {
                divInstallgrid.Visible = false;
            }

            //ScriptManager.RegisterStartupScript(UpdatePanel11, this.GetType(), "MyAction", "doMyAction();", true);
        }

        if (e.CommandName.ToLower() == "quatation")
        {
            hndcustomerid.Value = e.CommandArgument.ToString();
            ModalPopupExtender2.Show();

            DataTable dt = ClstblCustomers.tblCustLeadStatus_SelectQuote();
            if (dt.Rows.Count > 0)
            {
                ModalPopupExtender2.Show();

                rblcustleadstatus.DataSource = dt;
                rblcustleadstatus.DataValueField = "CustLeadStatusID";
                rblcustleadstatus.DataTextField = "CustLeadStatus";
                rblcustleadstatus.DataMember = "CustLeadStatus";
                rblcustleadstatus.DataBind();

                divInstallgrid.Visible = true;
                divappointment.Visible = false;
                divfollowsup.Visible = false;
            }
            else
            {
                divInstallgrid.Visible = false;
            }
        }

        if (e.CommandName.ToLower() == "followups")
        {
            hndcustomerid.Value = e.CommandArgument.ToString();
            ModalPopupExtender2.Show();

            DataTable dt = ClstblCustomers.tblCustLeadStatus_Select();
            if (dt.Rows.Count > 0)
            {
                ModalPopupExtender2.Show();

                rblcustleadstatus.DataSource = dt;
                rblcustleadstatus.DataValueField = "CustLeadStatusID";
                rblcustleadstatus.DataTextField = "CustLeadStatus";
                rblcustleadstatus.DataMember = "CustLeadStatus";
                rblcustleadstatus.DataBind();

                divInstallgrid.Visible = true;
                divappointment.Visible = false;
                divfollowsup.Visible = false;
            }
            else
            {
                divInstallgrid.Visible = false;
            }
        }

        if (e.CommandName.ToLower() == "salesrep")
        {
            hndcustomerid.Value = e.CommandArgument.ToString();
            ModalPopupExtenderASR.Show();

            ListItem item5 = new ListItem();
            item5.Text = "Sales Rep ";
            item5.Value = "";
            ddlAssignSalesRep.Items.Clear();
            ddlAssignSalesRep.Items.Add(item5);

            if (Roles.IsUserInRole("Administrator"))
            {
                userid = "";
            }
            else
            {
                userid = userid;
            }
            ddlAssignSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectCloser(userid);
            ddlAssignSalesRep.DataValueField = "EmployeeID";
            ddlAssignSalesRep.DataTextField = "fullname";
            ddlAssignSalesRep.DataMember = "fullname";
            ddlAssignSalesRep.DataBind();
        }

        if (e.CommandName.ToLower() == "notconfirmed")
        {
            hndcustomerid.Value = e.CommandArgument.ToString();
            DataTable dt = ClstblCustomers.tblCustLeadStatus_ConfirmNo();
            if (dt.Rows.Count > 0)
            {
                ModalPopupExtender2.Show();

                rblcustleadstatus.DataSource = dt;
                rblcustleadstatus.DataValueField = "CustLeadStatusID";
                rblcustleadstatus.DataTextField = "CustLeadStatus";
                rblcustleadstatus.DataMember = "CustLeadStatus";
                rblcustleadstatus.DataBind();

                divInstallgrid.Visible = true;
                divappointment.Visible = false;
                divfollowsup.Visible = false;
            }
            else
            {
                divInstallgrid.Visible = false;
            }
        }

        if (e.CommandName.ToLower() == "attend")
        {
            hndcustomerid.Value = e.CommandArgument.ToString();
            ModalPopupExtenderAttnd.Show();
        }
        DateTime curruntdate = Convert.ToDateTime(D2DAppDate);
        BindWeekDates(curruntdate);
    }
    protected void ibtnCancelNotes_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtender2.Hide();
        Reset();
        divfollowsup.Visible = false;
        divappointment.Visible = false;
    }

    protected void rblcustleadstatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblcustleadstatus.SelectedValue != string.Empty)
        {
            string status = rblcustleadstatus.SelectedValue;
            string customerid = hndcustomerid.Value;
            if (status == "1")
            {
                divappointment.Visible = false;
                divfollowsup.Visible = false;
                divCancel.Visible = false;
            }
            else if (status == "2")
            {
                divappointment.Visible = false;
                divfollowsup.Visible = false;
                divCancel.Visible = true;

                ClstblCustomers.tblCustomers_UpdateCancelNotes(customerid, txtCancelNotes.Text);  // Cancel
            }
            else if (status == "3")
            {
                divfollowsup.Visible = false;
                divappointment.Visible = true;
                divCancel.Visible = false;

                string EmployeeID = "1";
                DataTable dt = ClstblLTeamTime.tblLTeamTime_SelectByEmp(EmployeeID);

                ListItem item1 = new ListItem();
                item1.Text = "Time";
                item1.Value = "";
                ddlAppTime.Items.Clear();
                ddlAppTime.Items.Add(item1);

                ddlAppTime.DataSource = dt;
                ddlAppTime.DataMember = "Time";
                ddlAppTime.DataTextField = "Time";
                ddlAppTime.DataValueField = "ID";
                ddlAppTime.DataBind();
            }
            else if (status == "4")
            {
                divappointment.Visible = false;
                divfollowsup.Visible = true;
                divCancel.Visible = false;

                ListItem item1 = new ListItem();
                item1.Text = "Select";
                item1.Value = "";
                ddlContact.Items.Clear();
                ddlContact.Items.Add(item1);

                ddlContact.DataSource = ClstblContacts.tblContacts_SelectByCustId(customerid);
                ddlContact.DataMember = "Contact";
                ddlContact.DataTextField = "Contact";
                ddlContact.DataValueField = "ContactID";
                ddlContact.DataBind();
            }
            else if (status == "5")
            {
                divappointment.Visible = false;
                divfollowsup.Visible = false;
                divCancel.Visible = false;
            }
        }
        Reset();
        ModalPopupExtender2.Show();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string status = rblcustleadstatus.SelectedValue;
        string appointment_status = "";
        string customer_lead_status = "";
        string customerid = hndcustomerid.Value;
        StUtilities stuti = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        int hours = 0;

        if (stuti.Hours != string.Empty)
        {
            hours = Convert.ToInt32(stuti.Hours);
        }
        if (status == "1")   // Quatation
        {
            appointment_status = "1";
            customer_lead_status = "1";
            bool success = ClstblCustomers.tblCustomers_Updateappointment_status(appointment_status, customerid);
            bool success1 = ClstblCustomers.tblCustomers_Updatecustomer_lead_status(customer_lead_status, customerid);
            bool success2 = ClstblCustomers.tblCustomers_UpdateD2DAppAttendedDate(customerid, DateTime.Now.AddHours(hours).ToShortDateString());

            if (success)
            {
                SetAdd1();
                //PanSuccess.Visible = true;
            }
        }
        else if (status == "2")  // Cancel
        {
            appointment_status = "2";
            customer_lead_status = "2";
            bool success = ClstblCustomers.tblCustomers_Updateappointment_status(appointment_status, customerid);
            bool success1 = ClstblCustomers.tblCustomers_Updatecustomer_lead_status(customer_lead_status, customerid);
            bool success2 = ClstblContacts.tblContacts_UpdateContLeadStatusByCustID("4", customerid);
            bool success3 = ClstblCustomers.tblCustomers_UpdateD2DAppAttendedDate(customerid, DateTime.Now.AddHours(hours).ToShortDateString());

            if (success2)
            {
                SetAdd1();
                //PanSuccess.Visible = true;
            }
        }
        else if (status == "3")    // Reschedule
        {
            customer_lead_status = "3";
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(customerid);
            if (stCust.RescheduleTime != string.Empty && stCust.RescheduleDate != string.Empty)
            {
                ClstblCustomers.tblCustomers_UpdateReschedule(customerid, stCust.D2DAppDate, stCust.D2DAppTime, stCust.D2DAppNote);
            }

            // bool success = ClstblCustomers.tblCustomers_Updateappointment_status(appointment_status, customerid);
            bool success1 = ClstblCustomers.tblCustomers_Updatecustomer_lead_status(customer_lead_status, customerid);
            bool success2 = ClstblCustomers.tblCustomers_UpdateD2DEmpAppDate(customerid, txtD2DAppDate.Text.Trim(), ddlAppTime.SelectedValue, txtnote.Text);
            bool success3 = ClstblCustomers.tblCustomers_UpdateD2DAppAttendedDate(customerid, DateTime.Now.AddHours(hours).ToShortDateString());
            if (success2)
            {
                SetAdd1();
                //PanSuccess.Visible = true;
            }
        }
        else if (status == "4")    // Follow Ups
        {
            customer_lead_status = "4";
            string description = txtDescription.Text;
            string NextFollowupDate = txtNextFollowupDate.Text;
            string ContactID = ddlContact.SelectedValue;

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string CustInfoEnteredBy = st.EmployeeID;

            //  bool success = ClstblCustomers.tblCustomers_Updateappointment_status(appointment_status, customerid);
            bool success1 = ClstblCustomers.tblCustomers_Updatecustomer_lead_status(customer_lead_status, customerid);
            int success2 = ClstblCustInfo.tblCustInfo_Insert(customerid, description, NextFollowupDate, ContactID, CustInfoEnteredBy, "1");
            bool success3 = ClstblCustomers.tblCustomers_UpdateD2DEmpAppDateBYcustid(customerid, NextFollowupDate);
            bool success4 = ClstblCustomers.tblCustomers_UpdateD2DAppAttendedDate(customerid, DateTime.Now.AddHours(hours).ToShortDateString());

            if (success2 != 0)
            {
                SetAdd1();
                //PanSuccess.Visible = true;
            }
        }
        else if (status == "5")   // Sale
        {
            customer_lead_status = "5";
            SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(customerid);
            string projectType = "";
            string ContactID1 = "";

            DataTable dtContact = ClstblContacts.tblContacts_SelectByCustId(customerid);
            if (dtContact.Rows.Count > 0)
            {
                ContactID1 = dtContact.Rows[0]["ContactID"].ToString();
            }

            DataTable dtProjectType = ClstblProjectType.tblProjectType_SelectType();
            if (dtProjectType.Rows.Count > 0)
            {
                projectType = dtProjectType.Rows[0]["ProjectTypeID"].ToString();
            }

            string CustomerID = customerid;
            string ContactID = ContactID1;
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string EmployeeID = stEmp.EmployeeID;
            string SalesRep = stEmp.EmployeeID;
            string ProjectTypeID = projectType;
            string OldProjectNumber = "";
            string ManualQuoteNumber = "";
            string ProjectOpened = DateTime.Now.AddHours(hours).ToShortDateString();

            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(customerid);
            string InstallAddress = st.StreetAddress;
            string InstallCity = st.StreetCity;
            string InstallState = st.StreetState;
            string InstallPostCode = st.StreetPostCode;
            string Project = InstallCity + "-" + InstallAddress;
            string ProjectNotes = "";
            string AdditionalSystem = "False";
            string GridConnected = "False";
            string RebateApproved = "False";
            string ReceivedCredits = "False";
            string CreditEligible = "False";
            string MoreThanOneInstall = "False";
            string RequiredCompliancePaperwork = "False";
            string OutOfPocketDocs = "False";
            string OwnerGSTRegistered = "False";
            string HouseTypeID = "";
            string RoofTypeID = "";
            string RoofAngleID = "";
            string InstallBase = "1";
            string StandardPack = "False";
            string NumberPanels = "0";
            string PanelConfigNW = "0";
            string PanelConfigOTH = "0";
            string SystemCapKW = "0";
            string STCMultiplier = "0";
            string STCZoneRating = "0";
            string STCNumber = "0";
            string ElecDistOK = "False";
            string Asbestoss = "False";
            string MeterUG = "False";
            string MeterEnoughSpace = "False";
            string SplitSystem = "False";
            string CherryPicker = "False";
            string TravelTime = "0";
            string VarRoofType = "0";
            string VarRoofAngle = "0";
            string VarHouseType = "0";
            string VarAsbestos = "0";
            string VarMeterInstallation = "0";
            string VarMeterUG = "0";
            string VarTravelTime = "0";
            string VarSplitSystem = "0";
            string VarEnoughSpace = "0";
            string VarCherryPicker = "0";
            string VarOther = "0";
            string SpecialDiscount = "0";
            string DepositRequired = "0";
            string TotalQuotePrice = "0";
            string PreviousTotalQuotePrice = "0";
            string InvoiceExGST = "0";
            string InvoiceGST = "0";
            string BalanceGST = "0";
            string FinanceWithID = "1";
            string ServiceValue = "0";
            string QuoteSentNo = "0";
            string SignedQuote = "False";
            string MeterBoxPhotosSaved = "False";
            string ElecBillSaved = "False";
            string ProposedDesignSaved = "False";
            string InvoiceTag = "False";
            string InvRefund = "0";
            string DepositAmount = "0";
            string ReceiptSent = "False";
            string MeterIncluded = "False";
            string OffPeak = "False";
            string RECRebate = "0";
            string InstallAM1 = "False";
            string InstallPM1 = "False";
            string InstallAM2 = "False";
            string InstallPM2 = "False";
            string InstallDays = "0";
            string STCFormsDone = "False";
            string CustNotifiedInstall = "False";
            string InstallerNotes = "";
            string WelcomeLetterDone = "False";
            string InstallRequestSaved = "False";
            string CertificateSaved = "False";
            string STCFormSaved = "False";
            string CustNotifiedMeter = "False";
            string MeterInstallerNotes = "";
            string ProjectEnteredBy = stEmp.EmployeeID;

            if (stCust.StreetCity != string.Empty && stCust.StreetState != string.Empty && stCust.StreetPostCode != string.Empty)
            {
                int success = ClstblProjects.tblProjects_Insert(CustomerID, ContactID, EmployeeID, SalesRep, ProjectTypeID, "2", "", "", ProjectOpened, "", OldProjectNumber, ManualQuoteNumber, Project, "", InstallAddress, InstallCity, InstallState, InstallPostCode, ProjectNotes, AdditionalSystem, GridConnected, RebateApproved, ReceivedCredits, CreditEligible, MoreThanOneInstall, RequiredCompliancePaperwork, OutOfPocketDocs, OwnerGSTRegistered, "", HouseTypeID, RoofTypeID, RoofAngleID, InstallBase, "", StandardPack, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", NumberPanels, "", PanelConfigNW, PanelConfigOTH, SystemCapKW, STCMultiplier, STCZoneRating, STCNumber, "", "", "", "", "", "", "", "", "", ElecDistOK, "", "", Asbestoss, MeterUG, MeterEnoughSpace, SplitSystem, CherryPicker, TravelTime, "", VarRoofType, VarRoofAngle, VarHouseType, VarAsbestos, VarMeterInstallation, VarMeterUG, VarTravelTime, VarSplitSystem, VarEnoughSpace, VarCherryPicker, VarOther, SpecialDiscount, DepositRequired, TotalQuotePrice, PreviousTotalQuotePrice, InvoiceExGST, InvoiceGST, BalanceGST, FinanceWithID, ServiceValue, "", QuoteSentNo, "", SignedQuote, MeterBoxPhotosSaved, ElecBillSaved, ProposedDesignSaved, "", "", "", InvoiceTag, "", "", "", "", "", InvRefund, "", "", "", DepositAmount, ReceiptSent, "", "", MeterIncluded, "", "", "", "", "", "", "", OffPeak, "", "", "", "", "", "", "", "", "", "", "", "", "", RECRebate, "", "", "", "", InstallAM1, InstallPM1, InstallAM2, InstallPM2, InstallDays, STCFormsDone, "", "", CustNotifiedInstall, "", InstallerNotes, "", "", "", WelcomeLetterDone, InstallRequestSaved, "", "", "", "", CertificateSaved, "", "", STCFormSaved, "", "", "", "", "", "", "", "", "", "", "", "", "", CustNotifiedMeter, "", "", MeterInstallerNotes, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ProjectEnteredBy);
                //bool suc = ClstblProjects.tblProjects_UpdateProjectNumber(Convert.ToString(success));
                int sucProject2 = ClstblProjects.tblProjects_InserttblProjects2(Convert.ToString(success), EmployeeID);
                bool sucCustType = ClstblCustomers.tblCustomers_UpdateCustType("4", CustomerID);

                if (projectType == "2")
                {
                    ClsProjectSale.tblProjects_UpdateFormsSolar(Convert.ToString(success));
                }
                if (projectType == "3")
                {
                    ClsProjectSale.tblProjects_UpdateFormsSolarUG(Convert.ToString(success));
                }

                string EmpID = stEmp.EmployeeID;
                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("2", Convert.ToString(success), EmpID, NumberPanels);

                //--- do not chage this code start------

                if (Convert.ToString(success) != "")
                {
                    bool success1 = ClstblCustomers.tblCustomers_Updatecustomer_lead_status(customer_lead_status, customerid);
                    bool success2 = ClstblCustomers.tblCustomers_UpdateD2DAppAttendedDate(customerid, DateTime.Now.AddHours(hours).ToShortDateString());
                    bool sucContStatus = ClstblContacts.tblContacts_UpdateContLeadStatus("5", ContactID);
                    SetAdd1();
                    //PanSuccess.Visible = true;
                }
            }
            else
            {
                SetError1();
                //PanError.Visible = true;
            }
        }
        else if (rblcustleadstatus.SelectedValue == "")
        {
            string CustomerID = hndcustomerid.Value;
            ClstblCustomers.tblCustomers_Updatecustomer_lead_status("2", CustomerID);  // Cancel
            ClstblCustomers.tblCustomers_UpdateCancelNotes(CustomerID, txtCancelNotes.Text);  // Cancel
        }
        DateTime D2DAppDate = Convert.ToDateTime(hndD2Ddate.Value);
        DateTime curruntdate = Convert.ToDateTime(D2DAppDate);
        BindWeekDates(curruntdate);
    }
    protected void Reset()
    {
        txtD2DAppDate.Text = string.Empty;
        txtDescription.Text = string.Empty;
        txtNextFollowupDate.Text = string.Empty;
        ddlAppTime.SelectedValue = "";
        ddlContact.SelectedValue = "";
    }
    protected void btnAssign_Click(object sender, EventArgs e)
    {
        if (ddlAssignSalesRep.SelectedValue != string.Empty)
        {
            ClstblCustomers.tblCustomers_UpdateEmployeeID(hndcustomerid.Value, ddlAssignSalesRep.SelectedValue);
        }
        ModalPopupExtenderASR.Hide();
        int offset = DateTime.Now.AddHours(14).DayOfWeek - DayOfWeek.Monday;
        DateTime lastMonday = DateTime.Now.AddHours(14).AddDays(-offset);
        BindWeekDates(lastMonday);
    }
    protected void rptinstaller1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            LinkButton lbtnAssignSalesRep = (LinkButton)e.Item.FindControl("lbtnAssignSalesRep");
            LinkButton lbtnStatus = (LinkButton)e.Item.FindControl("lbtnStatus");
            LinkButton lbtnAttended = (LinkButton)e.Item.FindControl("lbtnAttended");
            HiddenField hndCustID = (HiddenField)e.Item.FindControl("hndCustID");

            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Lead Manager"))
            {
                lbtnAssignSalesRep.Visible = true;
            }
            else
            {
                lbtnAssignSalesRep.Visible = false;
            }

            if (Roles.IsUserInRole("leadgen"))
            {
                lbtnAttended.Visible = true;

                SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(hndCustID.Value);
                if (stCust.AttendFlag == string.Empty)
                {
                    lbtnStatus.Enabled = false;
                    lbtnAttended.Visible = true;
                }
                else
                {
                    lbtnStatus.Enabled = true;
                    lbtnAttended.Visible = false;
                }
            }
            else
            {
                lbtnAttended.Visible = false;
            }
        }
    }
    protected void btnAttend_Click(object sender, EventArgs e)
    {
        if (rblAttend.SelectedValue != string.Empty)
        {
            if (rblAttend.SelectedValue == "1")
            {
                ClstblCustomers.tblCustomers_UpdateAttend(hndcustomerid.Value, "True", "");
            }
            if (rblAttend.SelectedValue == "2")
            {
                ClstblCustomers.tblCustomers_UpdateAttend(hndcustomerid.Value, "False", txtAttendReason.Text);
            }
            ModalPopupExtenderAttnd.Hide();
            int offset = DateTime.Now.AddHours(14).DayOfWeek - DayOfWeek.Monday;
            DateTime lastMonday = DateTime.Now.AddHours(14).AddDays(-offset);
            BindWeekDates(lastMonday);
        }
    }
    protected void rblAttend_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderAttnd.Show();
        if (rblAttend.SelectedValue != string.Empty)
        {
            if (rblAttend.SelectedValue == "2")
            {
                divAttndNo.Visible = true;
            }
            else
            {
                divAttndNo.Visible = false;
            }
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}