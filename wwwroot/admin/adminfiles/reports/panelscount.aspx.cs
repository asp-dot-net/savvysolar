﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_panelscount : System.Web.UI.Page
{
    protected string jscharts1;
    protected string jschartticks;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dtMonth = Reports.tblProjects_Panels_Count();
            rptMonth.DataSource = dtMonth;
            rptMonth.DataBind();

            DataTable dtMonthS = Reports.tblProjects_Panels_CountSold();
            rptMonthSold.DataSource = dtMonthS;
            rptMonthSold.DataBind();

            DataTable dtMonthI = Reports.tblProjects_Panels_CountInst();
            rptMonthInst.DataSource = dtMonthI;
            rptMonthInst.DataBind();
            
            DataTable dtStateS = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
            rptStateSold.DataSource = dtStateS;
            rptStateSold.DataBind();

            DataTable dtState = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
            rptState.DataSource = dtState;
            rptState.DataBind();

            DataTable dtStateI = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
            rptStateInst.DataSource = dtStateI;
            rptStateInst.DataBind();

            BindPanelChart();
        }
    }

    public void BindPanelChart()
    {
        DataTable dt = Reports.tblProjects_Panels_CountInst();
        foreach (DataRow row in dt.Rows)
        {
            jscharts1 += "," + row["totalpan"].ToString() + "";
            jschartticks += ",'" + row["Month"].ToString() + "'";
        }
        if (!string.IsNullOrEmpty(jschartticks))
        {
            jschartticks = jschartticks.Substring(1);
        }
        if (!string.IsNullOrEmpty(jscharts1))
        {
            jscharts1 = jscharts1.Substring(1);
        }
    }
    protected void rptMonth_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndMonth = (HiddenField)e.Item.FindControl("hndMonth");
            Repeater rptTotal = (Repeater)e.Item.FindControl("rptTotal");

            DataTable dt = Reports.tblProjects_Panels_CountAll(hndMonth.Value);
            rptTotal.DataSource = dt;
            rptTotal.DataBind();
        }
    }

    protected void rptMonthSold_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndMonth = (HiddenField)e.Item.FindControl("hndMonth");
            Repeater rptTotalSold = (Repeater)e.Item.FindControl("rptTotalSold");

            DataTable dt = Reports.tblProjects_Panels_CountAllSold(hndMonth.Value);
            rptTotalSold.DataSource = dt;
            rptTotalSold.DataBind();
        }
    }

    protected void rptMonthInst_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndMonth = (HiddenField)e.Item.FindControl("hndMonth");
            Repeater rptTotalInst = (Repeater)e.Item.FindControl("rptTotalInst");

            DataTable dt = Reports.tblProjects_Panels_CountAllInstalled(hndMonth.Value);
            rptTotalInst.DataSource = dt;
            rptTotalInst.DataBind();
        }
    }  
}