<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    Culture="en-GB" UICulture="en-GB" CodeFile="orderinstalledreport.aspx.cs" Inherits="admin_adminfiles_reports_orderinstalledreport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="small-header transition animated fadeIn">
        <div class="hpanel">
            <div class="panel-body printorder">
                <h2 class="font-light m-b-xs">Order Installed Report
                </h2>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }
    </script>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            $('.loading-container').css('display', 'none');
        }

    </script>
    <asp:Panel runat="server" ID="PanGridSearch">
        <div class="content animate-panel" style="padding-bottom: 0px!important;">
            <div class="messesgarea">

                <div class="alert alert-info" id="PanNoRecord" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>
            </div>
        </div>
        <div class="content animate-panel marbtmzero padbtmzero padtopzero">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel marbtmzero">
                        <%--<div class="panel-heading">
                            <div class="panel-tools">
                                
                            </div>
                            <br />
                        </div>--%>
                        <div class="panel-body">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer ">

                                <div class="row">
                                    <div>
                                       
                                        <div class="dataTables_filter col-sm-12 Responsive-search printorder" id="PanSearch" runat="server">
                                            <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="input-group" id="ddltypename" runat="server">
                                                            <asp:DropDownList ID="ddltype" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">

                                                                <asp:ListItem Value="1">Modules</asp:ListItem>
                                                                <asp:ListItem Value="2">Inverters</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>



                                                        <div class="input-group col-sm-2" id="div1" runat="server">
                                                            <asp:DropDownList ID="ddlitem" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="">Stock Item</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-2" id="div2" runat="server">
                                                            <asp:DropDownList ID="ddlSearchVendor" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="">Vendor</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="errormessage" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" CssClass="errormessage" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                        </div>
                                                        <div class="input-group">
                                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="search" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:LinkButton ID="btnClearAll" runat="server"
                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-info"><i class="fa-eraser fa"></i>Clear </asp:LinkButton>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                         <div>
                                                <div>
                                                    <div class="dataTables_length showdata col-sm-6">
                                                        <table border="0" cellspacing="0" cellpadding="0" id="show" runat="server" visible="false">
                                                            <tr>
                                                                <td>Show
                                                             <asp:DropDownList ID="ddlSelectRecords" runat="server"
                                                                aria-controls="DataTables_Table_0" class="myval"></asp:DropDownList>
                                                                    entries
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div id="tdExport" class="pull-right" runat="server">
                                                            <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                                            <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                                                CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>
                                                            <%-- </ol>--%>
                                                             <a href="javascript:window.print();" class="printicon">
                                                    <%--        <img src="../../../images/icon_print.png" class="tooltips" data-toggle="tooltip"
                                data-placement="top" title="Print" />--%>
                                                    <i class="fa fa-print"></i>
                                                                 </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
 <div class="xscroll">
                            <div class="panel-body">
                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="PanAddUpdate" runat="server">
                                   
    
                                    <div class="form-group spical multiselect  printArea xscroll" id="dvprojectstate" runat="server" visible="false">
                                        <dl class="dropdown xscroll" id="dlprojectstate">
                                            <dt>
                                                <a href="#">
                                                    <span class="hida" id="spanselectstate">State</span>
                                                    <p class="multiSel" id="pselectstate"></p>
                                                </a>
                                            </dt>
                                            <dd id="ddprojectstate" runat="server">
                                                <div class="mutliSelect xscroll" id="dvmutliSelectstate">
                                                    <ul>
                                                        <asp:Repeater ID="rptState" runat="server">
                                                            <ItemTemplate>
                                                                <li>
                                                                    <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("State") %>' />
                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                    <label for='<%# Container.FindControl("chkselect").ClientID %>' runat="server" id="lblrmo">
                                                                        <span></span>
                                                                    </label>
                                                                    <label class="chkval">
                                                                        <asp:Literal runat="server" ID="ltteam" Text='<%# Eval("State")%>'></asp:Literal></label>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </ul>
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>

                                    <table cellpadding="5" cellspacing="0" border="0" align="left" class="printpage printArea table table-bordered table-hover xscroll orderinstall">
                                        <%--  <table class="table table-bordered table-hover xscroll orderinstall">--%>
                                        <tr>

                                            <asp:Repeater runat="server" ID="rptlocations">
                                                <ItemTemplate>
                                                    <td colspan="2">
                                                        <table class="table table-bordered">
                                                            <tr>
                                                                <td colspan="2" style="text-align: center;"><%# Eval("State") %></td>
                                                            </tr>
                                                            <tr>
                                                                <td width="50%">Installed</td>
                                                                <td width="50%">Received</td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                        <asp:Literal runat="server" ID="ltdata"></asp:Literal>
                                        <%--<asp:Repeater runat="server" ID="rpt" OnItemDataBound="rpt_ItemDataBound">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td width="150px">
                                                                    <%# Eval("StockItem") %>
                                                                    <asp:HiddenField runat="server" ID="hdnStockItemID" Value='<%# Eval("StockItemID") %>' /> 
                                                                </td>
                                                                <asp:Repeater runat="server" ID="rptqty" OnItemDataBound="rptqty_ItemDataBound">
                                                                    <ItemTemplate>
                                                                       
                                                                        <td  width="50%"> <asp:HiddenField runat="server" ID="hdnStockItemID" Value='<%# Eval("StockItemID") %>' />
                                                                        <asp:HiddenField runat="server" ID="hdnState" Value='<%# Eval("state") %>' />
                                                                            <asp:Literal runat="server" ID="ltinstalled"></asp:Literal>
                                                                        </td>
                                                                        <td  width="50%">
                                                                            <asp:Literal runat="server" ID="ltstate"></asp:Literal>
                                                                        </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>--%>
                                        <%--</table>--%>
                                    </table>
                                    <div id="PanGrid" runat="server" class="table-responsive noPagination" visible="false">
                                        <div class="messesgarea printpage">
                                            <div class="alert alert-success" id="PanSuccess" runat="server">
                                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                            </div>
                                            <div class="alert alert-danger" id="PanError" runat="server">
                                                <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                                            </div>
                                            <div class="alert alert-info" id="PanAlreadExists" runat="server">
                                                <i class="icon-info-sign"></i><strong>&nbsp;Stock Order already exists.</strong>
                                            </div>
                                            <%--   <div class="alert alert-info" id="PanNoRecord" runat="server">
                                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                        </div>--%>
                                        </div>
                                    </div>
                                </div>
                                     
                                   
                                </div>
     </div>
    </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
