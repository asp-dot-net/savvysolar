﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" Culture="en-GB" UICulture="en-GB" 
     AutoEventWireup="true" CodeFile="panelscount.aspx.cs" Inherits="admin_adminfiles_reports_panelscount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<%@ Register Src="../../../includes/reports.ascx" TagName="reports" TagPrefix="uc1" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  <%--  <script>
        $(document).ready(function () {
            $.jqplot.config.enablePlugins = true;
            var s1 = [<%=jscharts1 %>];
            var ticks = [<%=jschartticks %>];

            plot1 = $.jqplot('chart11', [s1], {
                // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                animate: !$.jqplot.use_excanvas,
                seriesDefaults: {
                    renderer: $.jqplot.BarRenderer,
                    pointLabels: { show: true }
                },
                seriesColors: ['#00749F'],
                grid: {
                    background: '#fff',
                    borderWidth: 0,
                    shadow: false
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks,
                        tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                        tickOptions: {
                            // labelPosition: 'middle',
                            angle: -30
                        }
                    }
                },
                highlighter: { show: false }
            });

            $('#chart11').bind('jqplotDataClick',
                function (ev, seriesIndex, pointIndex, data) {
                    // $('#info1').html('series: ' + seriesIndex + ', point: ' + pointIndex + ', data: ' + data);
                }
            );
        });
    </script>--%>
   
         <div class="small-header transition animated fadeIn printorder">
        <div class="hpanel">
            <div class="panel-body">
                <div id="tdExport" class="pull-right" runat="server">
                   
                  <%--  <a href="javascript:window.print();" class="printicon">
                 
                        <i class="fa fa-print"></i>
                    </a>--%>
                    
                </div>
                <h2 class="font-light m-b-xs">Panel Graph
                </h2>
            </div>
        </div>
    </div>
     <section class="row m-b-md">
        <div class="col-sm-12 minhegihtarea" >
              <div class="panel-body">
           <%-- <h3 class="m-b-xs text-black">Panel Graph</h3>--%>
          <%--  <div class="contactsarea">--%>
                <div class="bodymianbg">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="table-responsive" align="left" width="100%">
                                    <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover gridcss">
                                        <tr>
                                            <td align="center" width="50%">
                                                <div>                                                  
                                                    <h3>Month Wise Panel Installed</h3>
                                                    <br />
                                                </div>
                                                <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover gridcss table-striped">
                                                    <tr>
                                                        <th>Month</th>
                                                        <th class="center-text">Total Panels</th>
                                                        <asp:Repeater ID="rptStateInst" runat="server" >
                                                            <ItemTemplate>
                                                                <th  class="center-text">
                                                                    <%#Eval("State") %>
                                                                </th>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tr>
                                                    <asp:Repeater ID="rptMonthInst" runat="server" OnItemDataBound="rptMonthInst_ItemDataBound">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td ><%#Eval("MonthName") %><asp:HiddenField ID="hndMonth" runat="server" Value='<%#Eval("ID") %>' />
                                                                </td>
                                                                <td  class="center-text"><%#Eval("totalpan") %></td>
                                                                <asp:Repeater ID="rptTotalInst" runat="server">
                                                                    <ItemTemplate>
                                                                        <td  class="center-text"><%#Eval("total") %>
                                                                            <%--<asp:Label ID="" runat="server" Visible='<%# Eval("").ToString %>' Text='<%#Eval("total") %>'></asp:Label>--%>
                                                                        </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </table>
                                            </td>

                                            <td align="center" width="50%">
                                                <div>
                                                    <h3>Panels Graph</h3>
                                                    <br />
                                                </div>
                                                <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover gridcss table-striped">
                                                    <tr>
                                                        <td align="center">
                                                            <div id="chart11" style="min-height: 335px;"></div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td align="center" width="50%">
                                                <div>                                                   
                                                    <h3>Month Wise Panel Sold</h3>
                                                    <br />
                                                </div>
                                                <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover gridcss table-striped">
                                                    <tr>
                                                        <th>Month</th>
                                                        <th  class="center-text">Total Panels</th>
                                                        <asp:Repeater ID="rptStateSold" runat="server">
                                                            <ItemTemplate>
                                                                <th  class="center-text">
                                                                    <%#Eval("State") %>
                                                                </th>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tr>
                                                    <asp:Repeater ID="rptMonthSold" runat="server" OnItemDataBound="rptMonthSold_ItemDataBound">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%#Eval("MonthName") %><asp:HiddenField ID="hndMonth" runat="server" Value='<%#Eval("ID") %>' />
                                                                </td>
                                                                <td   class="center-text"><%#Eval("totalpan") %></td>
                                                                <asp:Repeater ID="rptTotalSold" runat="server">
                                                                    <ItemTemplate>
                                                                        <td  class="center-text"><%#Eval("total") %>
                                                                            <%--<asp:Label ID="" runat="server" Visible='<%# Eval("").ToString %>' Text='<%#Eval("total") %>'></asp:Label>--%>
                                                                        </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </table>
                                            </td>
                                            <td align="center" width="50%">
                                                <div>
                                                    <h3>Panels Sold But Not Installed</h3>
                                                    <br />
                                                </div>
                                                <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover gridcss table-striped">
                                                    <tr>
                                                        <th>Month</th>
                                                        <th>Total Panels</th>
                                                        <asp:Repeater ID="rptState" runat="server">
                                                            <ItemTemplate>
                                                                <th  class="center-text">
                                                                    <%#Eval("State") %>
                                                                </th>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tr>
                                                    <asp:Repeater ID="rptMonth" runat="server" OnItemDataBound="rptMonth_ItemDataBound">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%#Eval("MonthName") %><asp:HiddenField ID="hndMonth" runat="server" Value='<%#Eval("ID") %>' />
                                                                </td>
                                                                <td  class="center-text"><%#Eval("totalpan") %></td>
                                                                <asp:Repeater ID="rptTotal" runat="server">
                                                                    <ItemTemplate>
                                                                        <td  class="center-text"><%#Eval("total") %>
                                                                            <%--<asp:Label ID="" runat="server" Visible='<%# Eval("").ToString %>' Text='<%#Eval("total") %>'></asp:Label>--%>
                                                                        </td>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
