﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="transfer.aspx.cs" Inherits="admin_adminfiles_reports_transfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <link rel="stylesheet" type="text/css" media="screen" href="../../../css/style.css" />
   <%-- <link href="../../assets/styles/print.css" rel="stylesheet" type="text/css" media="print"/>--%>
    <script>

        function printContent() {
            var PageHTML = document.getElementById('<%= (PanGrid.ClientID) %>').innerHTML;
            var html = '<html><head>' +
             '<link href="../../assets/styles/print.css" rel="stylesheet" type="text/css" media="print"/>' +
        '</head><body style="background:#ffffff;">' +
        PageHTML +
        '</body></html>';
            var WindowObject = window.open("", "PrintWindow",
        "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(html);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
            document.getElementById('print_link').style.display = 'block';
        }
    </script>
    
    <section class="row m-b-md">
        <div class="col-sm-12 minhegihtarea">
            <%-- <h3 class="m-b-xs text-black printpage">Stock Transfer Detail</h3>
            <div class="contactsarea">
                <div class="addcontent printpage">
                    <asp:LinkButton ID="lnkBack" runat="server" CausesValidation="false" OnClick="lnkBack_Click"><img src="../../../images/btn_back.png" /></asp:LinkButton>
                </div>--%>
            <div class="small-header transition animated fadeIn printorder">
                <div class="hpanel">
                    <div class="panel-body">
                        <div id="hbreadcrumb" class="pull-right">
                            <ol class="hbreadcrumb breadcrumb fontsize16">
                               <%-- <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click"><i class="fa fa-plus"></i> Add</asp:LinkButton>--%>
                                <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-info"><i class="fa fa-chevron-left"></i> Back</asp:LinkButton>

                            </ol>
                        </div>
                        <h2 class="font-light m-b-xs">Stock Transfer

                        </h2>

                    </div>
                </div>
            </div>


           
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="bodymianbg">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div align="right" class="printorder" >
                                            <a href="javascript:window.print();" >
                                                <i class="icon-print printpage fa fa-print"></i>
                                            </a>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div runat="server" id="PanGrid" class="printpage">
                                            <div class="stocktransfer">
                                                <h1>Stock Transfer Detail</h1>
                                                <div class="martopbtm20">
                                             
                                                            <table class="table table-bordered table-striped printarea">
                                                                <tr>
                                                                    <th align="left" width="100">From&nbsp;Location:&nbsp;</th>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblFrom" runat="server"></asp:Label></td>
                                                                         <th align="left" width="100">To&nbsp;Location:&nbsp;</th>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblTo" runat="server"></asp:Label></td>
                                                                </tr>
                                                                
                                                                 <tr>
                                                                    <th align="left" width="100">Transfer Date: </th>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblTransferDate" runat="server"></asp:Label></td>
                                                                          <th align="left" width="100">Transfer By: </th>
                                                                    <td align="left">
                                                                        <asp:Label ID="lblTransferBy" runat="server"></asp:Label></td>
                                                                </tr>
                                                                
                                                            </table>
                                                       
                                                </div>
                                            </div>
                                            <div class="qty marbmt25">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="stockitem">
                                                    <tr>
                                                        <th width="5%" align="center">Qty</th>
                                                        <th width="20%" align="left">Code</th>
                                                        <th align="left">Stock Item</th>
                                                    </tr>
                                                    <asp:Repeater ID="rptItems" runat="server">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:Label ID="lblQty" runat="server"><%#Eval("TransferQty") %></asp:Label></td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblCode" runat="server"><%#Eval("StockCode") %></asp:Label></td>
                                                                <td align="left">
                                                                    <asp:Label ID="lblItem" runat="server"><%#Eval("StockItem") %></asp:Label></td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </table>
                                            </div>
                                            <div class="tracking">
                                                <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                                    <tr class="bold">
                                                        <th colspan="2">Total Qty:
                            <asp:Label ID="lbltotalqty" runat="server"></asp:Label>
                                                        </th>
                                                    </tr>
                                                    <tr class="bold">
                                                        <th>Tracking No:
                            <asp:Label ID="lblTrackingNo" runat="server"></asp:Label></th>
                                                        <th>Received Date:
                            <asp:Label ID="lblReceivedDate" runat="server"></asp:Label></th>
                                                    </tr>
                                                    <tr class="bold">
                                                        <th>Notes:</th>
                                                        <th>Received By:&nbsp;<asp:Label ID="lblReceivedBy" runat="server"></asp:Label></th>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblNotes" runat="server"></asp:Label></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
       
        </div>
         
    </section>
</asp:Content>

