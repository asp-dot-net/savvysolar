﻿using System;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Text.RegularExpressions;

public partial class admin_adminfiles_invoice_invcommon : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ListItem item2 = new ListItem();
            item2.Text = "Installer";
            item2.Value = "";
            ddlInstaller.Items.Clear();
            ddlInstaller.Items.Add(item2);

            ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataMember = "Contact";
            ddlInstaller.DataBind();
          

        }
        BindScript();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string ProjectID =  hdnprojectid.Value.ToString();
            //Request.QueryString["id"];
        string InvType = hdntype.Value.ToString();
            //Request.QueryString["type"];
        string InvNo = txtInstallerInvNo.Text;
        string InvAmnt = txtInstallerAmnt.Text;
        string InvDate = txtInstallerInvDate.Text;
        string PayDate = txtInstallerPayDate.Text;
        string Notes = txtElectricianInvoiceNotes.Text;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string InvEntered = stEmp.EmployeeID;
        string InvDoc = "";

        int success = ClstblInvoiceCommon.tblInvoiceCommon_Insert(InvType, ProjectID, InvNo, InvAmnt, InvDate, PayDate, Notes, InvEntered, InvEntered, "", "", "", ddlInstaller.SelectedValue);
        if (fuInvDoc.HasFile)
        {
            InvDoc = Convert.ToString(success) + fuInvDoc.FileName;
            fuInvDoc.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/commoninvdoc/") + InvDoc);
            bool sucInvDoc = ClstblInvoiceCommon.tblInvoiceCommon_UpdateInvDoc(Convert.ToString(success), InvDoc);
            SiteConfiguration.UploadPDFFile("commoninvdoc", InvDoc);
            SiteConfiguration.deleteimage(InvDoc, "commoninvdoc");
        }

        if (Convert.ToString(success) != string.Empty)
        {
            ModalPopupExtenderInv.Hide();
            SetAdd1();
            if ( hdntype.Value.ToString() == "4")
            {
                Response.Redirect("~/admin/adminfiles/company/mtcecall.aspx");
            }

            if (Request.QueryString["type"] == "5")
            {
                Response.Redirect("~/admin/adminfiles/company/casualmtce.aspx");
            }
        }
        else
        {
            ModalPopupExtenderInv.Show();
            SetError1();
        }
        BindScript();
    }

    public void GetInvClickByProject(string ProjectID, string type)
    {
        ModalPopupExtenderInv.Show();
        hdnprojectid.Value = ProjectID;
        hdntype.Value = type;
    }

    protected void lbtnBack_Click(object sender, EventArgs e)
    {
        if ( hdntype.Value.ToString() == "4")
        {
           
            Response.Redirect("~/admin/adminfiles/company/mtcecall.aspx");
        }

        if (Request.QueryString["type"] == "5")
        {
            Response.Redirect("~/admin/adminfiles/company/casualmtce.aspx");
        }
    }
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


}