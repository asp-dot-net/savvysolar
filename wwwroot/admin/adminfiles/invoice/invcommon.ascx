﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="invcommon.ascx.cs" Inherits="admin_adminfiles_invoice_invcommon" %>
<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="cc1" %>
<script type="text/javascript">
 

  
    function doMyAction() {

        $('#<%=btnAdd.ClientID %>').click(function (e) {
          
            formValidate();
          
        });


    }





    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }
    function HighlightControlToValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                $('#' + Page_Validators[i].controltovalidate).blur(function () {
                    var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                    if (validatorctrl != null && !validatorctrl.isvalid) {
                        $(this).css("border-color", "#FF5F5F");
                    }
                    else {
                        $(this).css("border-color", "#B5B5B5");
                    }
                });
            }
        }
    }
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
</script>

<asp:UpdatePanel ID="updatepanelgrid" runat="server">
    <ContentTemplate>


        <script>
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoadedpro);
            //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
            prm.add_beginRequest(beginrequesthandler);
            // raised after an asynchronous postback is finished and control has been returned to the browser.
            prm.add_endRequest(endrequesthandler);

            function beginrequesthandler(sender, args) {
                //shows the modal popup - the update progress
                $('.loading-container').css('display', 'block');

            }
            function endrequesthandler(sender, args) {
                //hide the modal popup - the update progress


            }

            function pageLoadedpro() {
                $('.datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('.loading-container').css('display', 'none');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();

                $(".myval").select2({
                    //placeholder: "select",
                    allowclear: true
                });
                $("[data-toggle=tooltip]").tooltip();
                // gridviewScroll();
                //alert($(".search-select").attr("class"));



            }
        </script>
        <asp:HiddenField ID="hdnprojectid" runat="server" />
        <asp:HiddenField ID="hdntype" runat="server" />
        <asp:Button ID="btnNULLInvPay" Style="display: none;" runat="server" />

        <cc1:ModalPopupExtender ID="ModalPopupExtenderInv" runat="server" BackgroundCssClass="modalbackground"
            DropShadow="false" PopupControlID="divAddInvPay" TargetControlID="btnNULLInvPay"
            CancelControlID="btnClose">
        </cc1:ModalPopupExtender>

        <div id="divAddInvPay" runat="server" style="display: none" class="modal_popup">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header">
                         <div style="float: right">
                        <button id="btnClose" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close</button>
                        <h4 class="modal-title" id="myModalLabel">Add Invoice</h4>
                             </div>
                    </div>

                    <div class="modal-body paddnone">
                        <div class="panel-body">
                            <div class="formainline formnew">
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group spicaldivin">
                                            <span class="name">
                                                <label class="control-label">Inv No </label>
                                            </span><span>
                                                <asp:TextBox ID="txtInstallerInvNo" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass="reqerror"
                                                    SetFocusOnError="true" ValidationGroup="invcommon" ControlToValidate="txtInstallerInvNo" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group spicaldivin ">
                                            <span class="name">
                                                <label class="control-label">Inv Amnt </label>
                                            </span><span>
                                                <asp:TextBox ID="txtInstallerAmnt" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                                                    ControlToValidate="txtInstallerAmnt" Display="Dynamic"
                                                    ErrorMessage="Please enter a number" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass="reqerror"
                                                    SetFocusOnError="true" ValidationGroup="invcommon" ControlToValidate="txtInstallerAmnt" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group dateimgarea spicaldivin">
                                            <span class="name">
                                                <asp:Label ID="Label23" runat="server" class=" control-label">

                                               Inv Date</asp:Label>
                                            </span>
                                            <div class="input-group date datetimepicker1 col-sm-12">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtInstallerInvDate" runat="server" class="form-control" placeholder="Inv Date">
                                                </asp:TextBox>
                                            </div>


                                            <div class="clear"></div>

                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group spicaldivin" >
                                            <span class="name">
                                                <asp:Label ID="Label1" runat="server" class="control-label">
                                           Pay Date </asp:Label>
                                            </span>
                                            <div class="input-group date datetimepicker1 col-sm-12">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtInstallerPayDate" runat="server" class="form-control" placeholder="Pay Date">
                                                </asp:TextBox>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-6">
                                        <div class="form-group spicaldivin">
                                            <span class="name">
                                                <label class="control-label">
                                                    Installer
                                                </label>
                                            </span><span>
                                                <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                                                    CssClass="form-control search-select myval">
                                                    <asp:ListItem Value="">Installer</asp:ListItem>
                                                </asp:DropDownList>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group spicaldivin">
                                            <span class="name">
                                                <label class="control-label">Upload Inv Doc </label>
                                            </span><span>
                                                <span class="file-input btn btn-azure btn-file">Browse
                                                                <asp:HyperLink ID="lblInvDoc" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                    <asp:FileUpload ID="fuInvDoc" runat="server" />
                                                </span>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorInvDoc" runat="server" ErrorMessage="Choose File to Upload !" CssClass="reqerror"
                                                    SetFocusOnError="true"  ValidationGroup="invcommon" ControlToValidate="fuInvDoc" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group spicaldivin">
                                        <span class="name col-md-12">
                                            <label class="control-label">Notes </label>
                                        </span><span class="col-md-12">
                                            <asp:TextBox ID="txtElectricianInvoiceNotes" runat="server" TextMode="MultiLine"
                                                Height="50px" CssClass="form-control"></asp:TextBox>
                                        </span>
                                        <div class="clear"></div>
                                    </div>


                                </div>


                                <div class="col-md-12">
                                    <div class="row" id="divAddUpdate" runat="server">
                                        <div class="col-md-12 text-center">
                                            <asp:Button CssClass="btn btn-primary addwhiteicon btnaddicon" ID="btnAdd"  ValidationGroup="invcommon" CausesValidation="true" runat="server" OnClick="btnAdd_Click"
                                                Text="Add" />
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

    </ContentTemplate>
</asp:UpdatePanel>






