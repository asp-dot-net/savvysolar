﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using EurosolarReporting;
//using Telerik.Reporting.Services.WebApi;
using Telerik.ReportViewer.WebForms;
using Telerik.Reporting;

public partial class Telerik_reports_EWR : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {

        string ProjectID = "758900";

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);
        SttblContacts stcont = ClstblContacts.tblContacts_SelectByCustomerID(stPro.CustomerID);
        DataTable dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        //To get installer eleclicense
        string InstallerContactId = stPro.Installer;
        DataTable dt2;
        if (string.IsNullOrEmpty(InstallerContactId))
        {
            InstallerContactId = "";
            dt2 = ClstblContacts.tblContacts_SelectElectricianByContactID(InstallerContactId);
        }
        else
        {
            dt2 = ClstblContacts.tblContacts_SelectElectricianByContactID(InstallerContactId);
        }



        Telerik.Reporting.Report rpt = new EurosolarReporting.EWR();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.TextBox txtcustname = rpt.Items.Find("txtcustname", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmobile = rpt.Items.Find("txtmobile", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstreetno = rpt.Items.Find("txtstreetno", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtaddress = rpt.Items.Find("txtaddress", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsuburb = rpt.Items.Find("txtsuburb", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsolarpanel = rpt.Items.Find("txtsolarpanel", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtinverter = rpt.Items.Find("txtinverter", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtinstaller = rpt.Items.Find("txtinstaller", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtaddress2 = rpt.Items.Find("txtaddress2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcust2 = rpt.Items.Find("txtcust2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txt_elec_licence = rpt.Items.Find("txt_elec_licence", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmobile2 = rpt.Items.Find("txtmobile2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtday = rpt.Items.Find("txtday", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmonth = rpt.Items.Find("txtmonth", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtyear = rpt.Items.Find("txtyear", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtday2 = rpt.Items.Find("txtday2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmonth2 = rpt.Items.Find("txtmonth2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtyear2 = rpt.Items.Find("txtyear2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtprojectno = rpt.Items.Find("txtprojectno", true)[0] as Telerik.Reporting.TextBox;


        try
        {

            //System.Web.HttpContext.Current.Response.Write(string.Format("{0:yyyy}", Convert.ToDateTime(stPro.InstallBookingDate)));
            //System.Web.HttpContext.Current.Response.End();
            string day = "", month = "", year = "";
            if (!string.IsNullOrEmpty(stPro.InstallBookingDate))
            {
                day = string.Format("{0:dd}", Convert.ToDateTime(stPro.InstallBookingDate));
                month = string.Format("{0:MM}", Convert.ToDateTime(stPro.InstallBookingDate));
                year = string.Format("{0:yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            txtcustname.Value = st.Customer;
            txtmobile.Value = stcont.ContMobile;
            txtstreetno.Value = st.street_number;
            txtaddress.Value = st.street_name + " " + st.street_type + " " + st.StreetCity;
            txtsuburb.Value = st.StreetState + " " + st.StreetPostCode;
            txtsolarpanel.Value = stPro.SystemCapKW;
            txtinverter.Value = stPro.InverterOutput;
            txtinstaller.Value = dt.Rows[0]["InstallerName"].ToString();
            txtaddress2.Value = st.StreetAddress + " " + st.StreetState + " " + st.StreetPostCode;
            txtcust2.Value = dt.Rows[0]["InstallerName"].ToString();
            txtmobile2.Value = stcont.ContMobile;
            txtday.Value = day;
            txtday2.Value = day;
            txtmonth.Value = month;
            txtmonth2.Value = month;
            txtyear.Value = year;
            txtyear2.Value = year;
            txtprojectno.Value = stPro.ProjectNumber;
            if (dt2.Rows.Count > 0)
            {
                txt_elec_licence.Value = dt2.Rows[0]["ElecLicence"].ToString();
            }
            else
            {
                txt_elec_licence.Value = "";
            }

        }
        catch { }


        ir.ReportDocument = rpt;
        ReportViewer1.ReportSource = ir;
        ReportViewer1.RefreshReport();

    }


}