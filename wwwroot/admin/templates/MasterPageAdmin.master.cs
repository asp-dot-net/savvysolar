using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class BossAdmin_MasterPageAdmin : System.Web.UI.MasterPage
{
    protected static string Siteurl;
    protected static string SiteName;
    protected void Page_Load(object sender, EventArgs e)
    {
        //   StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        Siteurl = st.siteurl;
        string pagename = Utilities.GetPageName(Request.Url.PathAndQuery);
        if (!IsPostBack)
        {
            SiteName = ConfigurationManager.AppSettings["SiteName"].ToString();
            System.IO.FileInfo image = new System.IO.FileInfo(Request.PhysicalApplicationPath + "/userfiles/sitelogo/" + st.sitelogoupload);
            bool isfileUploaded = image.Exists;
        }

        if (!Request.IsAuthenticated)
        {
            Response.End();
            Response.Redirect("~/admin/Default.aspx");
        }

        switch (pagename.ToLower())
        {
            //case "dashboard":
            //    dash.Visible = true;
            //    Dashboard.Attributes.Add("class", "activePriNav");
            //    break;

            //case "adminutilities":
            //    divutility.Visible = true;
            //    hyputilities.CssClass = "menuOn";
            //    break;

            //case "adminsiteconfiguration":
            //     divutility.Visible = true;
            //     hypsiteconfig.CssClass = "menuOn";
            //    break;

            //case "announcement":
            //    divannouncement.Visible = true;
            //    hypannounctment.CssClass = "menuOn";
            //    liannouncement.Attributes.Add("class", "activePriNav");
            //    break;

            //case "news":
            //    divnews.Visible = true;
            //    linews.Attributes.Add("class", "activePriNav");
            //    hypnews.CssClass = "menuOn";
            //    break;

            //case "newsdetail":
            //    divnews.Visible = true;
            //    linews.Attributes.Add("class", "activePriNav");
            //    hypnews.CssClass = "menuOn";
            //    break;

            //case "demo":
            //    divdemo.Visible = true;
            //    liDemo.Attributes.Add("class", "activePriNav");
            //    hypdemo.CssClass = "menuOn";
            //    break;
        }


        if (pagename != "profile")
        {
            // secpage.Attributes.Add("class", "scrollable padder shdoimg");
        }
        else
        {
            //   secpage.Attributes.Add("class", "scrollable shdoimg");
        }


        //////title
        if (pagename == "SalesInfo")
        {
            pagetitle.Text = "Savvy Solar | SalesInfo";
        }
        if (pagename == "profile")
        {
            pagetitle.Text = "Savvy Solar | Profile";
        }
        if (pagename == "dashboard")
        {
            pagetitle.Text = "Savvy Solar | Dashboard";
        }
        if (pagename == "setting")
        {
            pagetitle.Text = "Savvy Solar | Setting";
        }
        if (pagename == "profile")
        {
            pagetitle.Text = "Savvy Solar | Profile";
        }
        if (pagename == "help")
        {
            pagetitle.Text = "Savvy Solar | Help";
        }
        if (pagename == "project")
        {
            pagetitle.Text = "Savvy Solar | Project";
        }
        if (pagename == "installations")
        {
            pagetitle.Text = "Savvy Solar | Installation";
        }
        if (pagename == "invoicesissued")
        {
            pagetitle.Text = "Savvy Solar | Invoices Issued";
        }
        if (pagename == "invoicespaid")
        {
            pagetitle.Text = "Savvy Solar | Invoices Paid";
        }
        if (pagename == "lead")
        {
            pagetitle.Text = "Savvy Solar | Lead";
        }
        if (pagename == "lteamcalendar")
        {
            pagetitle.Text = "Savvy Solar | Team Calender";
        }
        if (pagename == "installercalendar")
        {
            pagetitle.Text = "Savvy Solar | Installer Calender";
        }


        #region master
        if (pagename == "companylocations")
        {
            pagetitle.Text = "Savvy Solar | Company Locations";
        }
        if (pagename == "custsource")
        {
            pagetitle.Text = "Savvy Solar | Company Source";
        }
        if (pagename == "custsourcesub")
        {
            pagetitle.Text = "Savvy Solar | Company Source Sub";
        }
        if (pagename == "CustType")
        {
            pagetitle.Text = "Savvy Solar | Company Type";
        }
        if (pagename == "elecdistributor")
        {
            pagetitle.Text = "Savvy Solar | Elec Distributor";
        }
        if (pagename == "elecretailer")
        {
            pagetitle.Text = "Savvy Solar | Elec Retailer";
        }
        if (pagename == "employee")
        {
            pagetitle.Text = "Savvy Solar | Employee";
        }
        if (pagename == "FinanceWith")
        {
            pagetitle.Text = "Savvy Solar | Finance With";
        }
        if (pagename == "housetype")
        {
            pagetitle.Text = "Savvy Solar | House Type";
        }
        if (pagename == "leadcancelreason")
        {
            pagetitle.Text = "Savvy Solar | Lead Cancel Reason";
        }
        if (pagename == "mtcereason")
        {
            pagetitle.Text = "Savvy Solar | Mtce Reasons";
        }
        if (pagename == "mtcereasonsub")
        {
            pagetitle.Text = "Savvy Solar | Mtce Reasons Sub";
        }
        if (pagename == "projectcancel")
        {
            pagetitle.Text = "Savvy Solar | Project Cancel";
        }
        if (pagename == "projectonhold")
        {
            pagetitle.Text = "Savvy Solar | Project On-Hold";
        }
        if (pagename == "projectstatus")
        {
            pagetitle.Text = "Savvy Solar | Project Status";
        }
        if (pagename == "projecttrackers")
        {
            pagetitle.Text = "Savvy Solar | Project Trackers";
        }
        if (pagename == "projecttypes")
        {
            pagetitle.Text = "Savvy Solar | Project Types";
        }
        if (pagename == "projectvariations")
        {
            pagetitle.Text = "Savvy Solar | Project Variations";
        }
        if (pagename == "promotiontype")
        {
            pagetitle.Text = "Savvy Solar | Promotion Type";
        }
        if (pagename == "prospectcategory")
        {
            pagetitle.Text = "Savvy Solar | Prospect Categories";
        }
        if (pagename == "roofangles")
        {
            pagetitle.Text = "Savvy Solar | Roof Angles";
        }
        if (pagename == "roottye")
        {
            pagetitle.Text = "Savvy Solar | Root Type";
        }
        if (pagename == "salesteams")
        {
            pagetitle.Text = "Savvy Solar | Sales Team";
        }
        if (pagename == "StockCategory")
        {
            pagetitle.Text = "Savvy Solar | Stock Category";
        }
        if (pagename == "transactiontypes")
        {
            pagetitle.Text = "Savvy Solar | Transaction Types";
        }
        if (pagename == "paymenttype")
        {
            pagetitle.Text = "Savvy Solar | Finance Payment Type";
        }
        if (pagename == "invcommontype")
        {
            pagetitle.Text = "Savvy Solar | Invoice Type";
        }
        if (pagename == "postcodes")
        {
            pagetitle.Text = "Savvy Solar | Post Codes";
        }
        if (pagename == "custinstusers")
        {
            pagetitle.Text = "Savvy Solar | Cust/Inst Users";
        }
        if (pagename == "newsletter")
        {
            pagetitle.Text = "Savvy Solar | News Letter";
        }
        if (pagename == "logintracker")
        {
            pagetitle.Text = "Savvy Solar | Login Tracker";
        }
        if (pagename == "leftempprojects")
        {
            pagetitle.Text = "Savvy Solar | Left Employee Projects";
        }
        if (pagename == "lteamtime")
        {
            pagetitle.Text = "Savvy Solar | Manage L-Team App Time";
        }
        if (pagename == "refundoptions")
        {
            pagetitle.Text = "Savvy Solar | Refund Options";
        }

        #endregion

        if (pagename == "ProjectNew")
        {
            //Response.Write("Hi");
            //Response.End();
            pagetitle.Text = "Savvy Solar | ProjectNew";
        }
        if (pagename == "LeadTrackerNew")
        {
            //Response.Write("Hi");
            //Response.End();
            pagetitle.Text = "Savvy Solar | LeadTrackerNew";
        }

        if (pagename == "Customer")
        {
            //Response.Write("Hi");
            //Response.End();
            pagetitle.Text = "Savvy Solar | Customer";
        }
        if (pagename == "company")
        {
            //Response.Write("Hi");
            //Response.End();
            pagetitle.Text = "Savvy Solar | Company";
        }
        if (pagename == "contacts")
        {
            pagetitle.Text = "Savvy Solar | Contact";
        }

        if (pagename == "leadtracker")
        {
            pagetitle.Text = "Savvy Solar | Lead Tracker";
        }
        if (pagename == "gridconnectiontracker")
        {
            pagetitle.Text = "Savvy Solar | GridConnection Tracker.";
        }

        if (pagename == "lteamtracker")
        {
            pagetitle.Text = "Savvy Solar | Team Lead Tracker";
        }
        if (pagename == "stctracker")
        {
            pagetitle.Text = "Savvy Solar | STC Tracker";
        }
        if (pagename == "instinvoice")
        {
            pagetitle.Text = "Savvy Solar | Inst Invoice Tracker";
        }
        if (pagename == "salesinvoice")
        {
            pagetitle.Text = "Savvy Solar | Sales Invoice Tracker";
        }
        if (pagename == "installbookingtracker")
        {
            pagetitle.Text = "Savvy Solar | Install Booking Tracker";
        }
        if (pagename == "ApplicationTracker")
        {
            pagetitle.Text = "Savvy Solar | Application Tracker";
        }
        if (pagename == "accrefund")
        {
            pagetitle.Text = "Savvy Solar | Refund";
        }
        if (pagename == "formbay")
        {
            pagetitle.Text = "Savvy Solar | FormBay";
        }
        if (pagename == "stockitem")
        {
            pagetitle.Text = "Savvy Solar | Stock Item";
        }
        if (pagename == "stocktransfer")
        {
            pagetitle.Text = "Savvy Solar | Stock Transfer";
        }
        if (pagename == "stockorder")
        {
            pagetitle.Text = "Savvy Solar | Stock Order";
        }
        if (pagename == "stockdatasheet")
        {
            pagetitle.Text = "Savvy Solar | Data Sheet";
        }

        if (pagename == "Wholesale")
        {
            pagetitle.Text = "Savvy Solar | Wholesale";
        }
        if (pagename == "wholesalededuct")
        {
            pagetitle.Text = "Savvy Solar | Wholesale Deduct";
        }
        if (pagename == "stockdeduct")
        {
            pagetitle.Text = "Savvy Solar | Stock Deduct";
        }
        if (pagename == "stockin")
        {
            pagetitle.Text = "Savvy Solar | Stock In";
        }
        if (pagename == "WholesaleIn")
        {
            pagetitle.Text = "Savvy Solar | Wholesale In";
        }
        if (pagename == "stockusage")
        {
            pagetitle.Text = "Savvy Solar | Stock Usage";
        }
        if (pagename == "stockorderreport")
        {
            pagetitle.Text = "Savvy Solar | Stock Order Report";
        }

        if (pagename == "promosend")
        {
            pagetitle.Text = "Savvy Solar | Promo Send";
        }

        if (pagename == "lead")
        {
            pagetitle.Text = "Savvy Solar | Lead";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "Savvy Solar | Maintenance";
        }
        if (pagename == "casualmtce")
        {
            pagetitle.Text = "Savvy Solar| Casual Maintenance";
        }
        if (pagename == "noinstalldate")
        {
            pagetitle.Text = "Savvy Solar | NoInstall Date Report";
        }
        if (pagename == "installdate")
        {
            pagetitle.Text = "Savvy Solar | Install Date Report";
        }
        if (pagename == "panelscount")
        {
            pagetitle.Text = "Savvy Solar | Panel Graph";
        }
        if (pagename == "accountreceive")
        {
            pagetitle.Text = "Savvy Solar | Account Receive Report";
        }
        if (pagename == "paymentstatus")
        {
            pagetitle.Text = "Savvy Solar | Payment Status Report";
        }
        if (pagename == "formbaydocsreport")
        {
            pagetitle.Text = "Savvy Solar | Formbay Docs Report";
        }
        if (pagename == "weeklyreport")
        {
            pagetitle.Text = "Savvy Solar | Weekly Report";
        }
        if (pagename == "leadassignreport")
        {
            pagetitle.Text = "Savvy Solar | Lead Assign Report";
        }
        if (pagename == "stockreport")
        {
            pagetitle.Text = "Savvy Solar | Stock Report";
        }
        if (pagename == "custproject")
        {
            pagetitle.Text = "Savvy Solar | Customer System Detail";
        }
        if (pagename == "customerdetail")
        {
            pagetitle.Text = "Savvy Solar | Customer System Detail";
        }
        if (pagename == "instavailable")
        {
            pagetitle.Text = "Savvy Solar | Installer Available";
        }
        if (pagename == "printinstallation")
        {
            pagetitle.Text = "Savvy Solar | Installations";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "Savvy Solar | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "Savvy Solar | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "Savvy Solar | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "Savvy Solar | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "Savvy Solar | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "Savvy Solar | Maintenance";
        }
        if (pagename == "stocktransferreportdetail")
        {
            pagetitle.Text = "Savvy Solar | Stock Transfer Report";
        }
        if (pagename == "promotracker")
        {
            pagetitle.Text = "Savvy Solar | Promo Tracker";
        }
        if (pagename == "unittype")
        {
            pagetitle.Text = "Savvy Solar | Unit Type";
        }
        if (pagename == "streettype")
        {
            pagetitle.Text = "Savvy Solar | Street Type";
        }
        if (pagename == "streetname")
        {
            pagetitle.Text = "Savvy Solar | Street Name";
        }
        if (pagename == "promooffer")
        {
            pagetitle.Text = "Savvy Solar | Promo Offer";
        }
        if (pagename == "updateformbayId")
        {
            pagetitle.Text = "Savvy Solar | updateformbayId";
        }
        if (pagename == "pickup")
        {
            pagetitle.Text = "Savvy Solar | Pick Up";
        }
        if (pagename == "paywaytracker")
        {
            pagetitle.Text = "Savvy Solar | PayWay";
        }
        if (pagename == "quickform")
        {
            pagetitle.Text = "Savvy Solar | Quick Form";
        }
        if (pagename == "clickcustomer")
        {
            pagetitle.Text = "Savvy Solar | Click Customer";
        }



    }


    protected void LoginStatus1_LoggedOut(object sender, EventArgs e)
    {
        Session["userid"] = "";
        Session.Abandon();
        Response.Redirect("~/admin/Default.aspx");
    }


}


