﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="employeemenu.ascx.cs"
    Inherits="includes_employeemenu" %>
<div class="paddnone">
    <div class="spicaltab">
        <div class="projecttabnavi">
            <div class="col-md-12">
                <table>
                    <tr>
                        <td id="li_empdetails" runat="server">
                            <asp:HyperLink ID="hypDetail" runat="server">Details</asp:HyperLink>
                        </td>
                        <td>&nbsp;|&nbsp;
                        </td>
                        <td id="li_emppermissions" runat="server">
                            <asp:HyperLink ID="hypPermissions" runat="server">Permissions</asp:HyperLink>
                        </td>
                        <td>&nbsp;|&nbsp;
                        </td>
                        <td id="li_empreferences" runat="server">
                            <asp:HyperLink ID="hypReferences" runat="server">References</asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </div>
            <%-- <ul>
                <li id="li_empdetails" runat="server">
                    <asp:HyperLink ID="hypDetail" runat="server">Details</asp:HyperLink>
                </li>
                <li id="li_emppermissions" runat="server">
                    <asp:HyperLink ID="hypPermissions" runat="server">Permissions</asp:HyperLink>
                </li>
                <li id="li_empreferences" runat="server">
                    <asp:HyperLink ID="hypReferences" runat="server">References</asp:HyperLink>
                </li>
            </ul>--%>
            <div class="clear">
            </div>
        </div>
    </div>
</div>

