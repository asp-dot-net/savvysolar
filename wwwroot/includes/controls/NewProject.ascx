﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewProject.ascx.cs" Inherits="includes_controls_NewProject" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<style type="text/css">
    .selected_row {
        background-color: #A1DCF2 !important;
    }
</style>
<script src="../../assets/js/jquery.min.js"></script>

<script>
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoaded);
    function pageLoaded() {
        $(".myvalcomp").select2({
            //placeholder: "select",
            allowclear: true
        });

    }
</script>
<script>
    $(".myval").select2({
        width: "50%",
        formatResult: function (state) {
            if (!state.id) return state.text;
            if ($(state.element).data('active') == "0") {
                return state.text + "<i class='fa fa-dot-circle-o'></i>";
            } else {
                return state.text;
            }
        },
        formatSelection: function (state) {
            if ($(state.element).data('active') == "0") {
                return state.text + "<i class='fa fa-dot-circle-o'></i>";
            } else {
                return state.text;
            }
        }
    });
    $(".myval1").select2({
        minimumResultsForSearch: -1
    });
</script>
<script type="text/javascript">
    $(function () {
        $("[id*=GridView1] td").bind("click", function () {
            var row = $(this).parent();
            $("[id*=GridView1] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    $("td", this).removeClass("selected_row");
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("selected_row")) {
                    $(this).addClass("selected_row");
                } else {
                    $(this).removeClass("selected_row");
                }
            });
        });
    });
</script>
<style>
    .ui-autocomplete-loading {
        background: white url("../../../images/indicator.gif") right center no-repeat;
    }
</style>
<script>

    function formValidate() {

        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                }
            }
        }
    }
</script>

<script type="text/javascript">


    function InstallAutopostback() {

        $("#<%=txtInstallAddressline.ClientID %>").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "address.aspx",
                    dataType: "jsonp",
                    data: {
                        s: 'auto',
                        term: request.term
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 3,
            select: function (event, ui) {
                //                    log(ui.item ?
                //					"Selected: " + ui.item.label :
                //					"Nothing selected, input was " + this.value);
            },
            open: function () {
                //$(this).removeClass("ui-corner-all").addClass("ui-corner-top");
            },
            close: function () {
                // $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
            }
        });
    }

    function getDetailInstallParsedAddress() {
        $.ajax({
            url: "address.aspx",
            dataType: "jsonp",
            data: {
                s: 'address',
                term: $("#<%=txtInstallAddressline.ClientID %>").val()
            },
            success: function (data) {
                if (data != null) {
                    document.getElementById("<%= txtInstallAddress.ClientID %>").value = data.StreetLine;
                    document.getElementById("<%= txtInstallPostCode.ClientID %>").value = data.Postcode;
                    document.getElementById("<%= hndstreetno.ClientID %>").value = data.Number;
                    document.getElementById("<%= hndstreetname.ClientID %>").value = data.Street;
                    document.getElementById("<%= hndstreettype.ClientID %>").value = data.StreetType;
                    document.getElementById("<%= hndstreetsuffix.ClientID %>").value = data.StreetSuffix;
                    document.getElementById("<%= txtInstallCity.ClientID %>").value = data.Suburb;
                    document.getElementById("<%= txtInstallState.ClientID %>").value = data.State;
                    document.getElementById("<%= hndunittype.ClientID %>").value = data.UnitType;
                    document.getElementById("<%= hndunitno.ClientID %>").value = data.UnitNumber;
                }
                validateAddress();
            }
        });
    }

    function validateProjectAddress() {
        $.ajax({
            url: "address.aspx",
            dataType: "jsonp",
            data: {
                s: 'validate',
                term: $("#<%=txtInstallAddressline.ClientID %>").val()
            },
            success: function (data) {
                if (data == true) {
                    document.getElementById("validaddressid").style.display = "block";
                    document.getElementById("invalidaddressid").style.display = "none";

                    document.getElementById("<%= hndaddress.ClientID %>").value = "1";
                }
                else {
                    document.getElementById("validaddressid").style.display = "none";
                    document.getElementById("invalidaddressid").style.display = "block";
                    document.getElementById("<%= hndaddress.ClientID %>").value = "0";
                }
            }
        });
    }
    function ChkFun(source, args) {
        validateProjectAddress();
        var elem = document.getElementById('<%= hndaddress.ClientID %>').value;

        if (elem == '1') {
            args.IsValid = true;
        }
        else {

            args.IsValid = false;
        }
    }


    function HidePanel() {
        //var tab = document.getElementById('ctl00_ContentPlaceHolder1_TabContainer1');
        //tab._tabs[2]._hide();
        //tab.get_tabs()[4].set_enabled(false);
        //tab.get_tabs()[5].set_enabled(false);
        //tab.get_tabs()[6].set_enabled(false);

        //$find("ctl00_ContentPlaceHolder1_TabContainer1")._tabs[2]._hide();

        //$find("TabContainer1")._tabs[5]._hide();
        //$find("TabContainer1")._tabs[6]._hide();
        //$find("TabContainer1")._tabs[9]._hide();
        //$find("TabContainer1")._tabs[10]._hide();
        //$find("TabContainer1")._tabs[11]._hide();
        //$find("TabContainer1")._tabs[12]._hide();
        //$find("TabContainer1")._tabs[13]._hide();
        //alert(tab);
    }

       <%-- function DisableEnableTab() {
            var tab = $find('<%=TabContainer1.ClientID%>');
            tab.get_tabs()[2].set_enabled(false);
            //tab.get_tabs()[4].set_enabled(false);
            //tab.get_tabs()[5].set_enabled(false);
            //tab.get_tabs()[6].set_enabled(false);
          //  alert("Kiran");
        }--%>

</script>

<asp:UpdatePanel ID="updatepanelgrid" runat="server">
    <ContentTemplate>

        <section class="row m-b-md" id="panprojects" runat="server">
            <div class="col-sm-12">
                <%--<h3 class="m-b-xs text-black">
            <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>&nbsp;Projects</h3>--%>
                <div class="contactsarea">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Literal ID="litError" runat="server"
                                Text="Transaction Failed."></asp:Literal></strong>
                        </div>
                        <%-- <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>--%>
                        <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                        </div>
                        <div class="alert alert-danger" id="PanAddressValid" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Address is not verified. Please verify the address first.</strong>
                        </div>
                    </div>
                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate" Visible="false">
                        <div class="contacttoparea" id="PanSearch" runat="server">
                            <div class="form-inline searchfinal">
                                <div class="paddall15 marginbtm15 row Responsive-search">
                                    <div class="col-md-12">
                                        <div id="divrightbtn" runat="server" style="text-align: right;">
                                            <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false"
                                                CssClass="btn btn-default purple" OnClick="lnkAdd_Click"><i class="fa fa-plus"></i>Add</asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>

                        <div class="contactbottomarea finalgrid">
                            <div class="tableblack tableminpadd">
                                <div class="table-responsive" id="PanGrid" runat="server" visible="false">
                                    <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server"
                                        OnPageIndexChanging="GridView1_PageIndexChanging" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                        AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound" AutoGenerateColumns="false"
                                        OnSorting="GridView1_Sorting" OnSelectedIndexChanging="GridView1_SelectedIndexChanging" OnRowCommand="GridView1_RowCommand">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Project Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="brdrgrayleft">
                                                <ItemTemplate>
                                                    <%#Eval("ProjectNumber")%>
                                                </ItemTemplate>
                                                <ItemStyle Width="140px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                    <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />
                                                    <%#Eval("Address")%>
                                                </ItemTemplate>
                                                <ItemStyle />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="hypStatus" runat="server" CommandName="Status" CommandArgument='<%#Eval("ProjectID") %>'
                                                        CausesValidation="false"><%#Eval("ProjectStatus")%></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Width="100px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Updated By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <%#Eval("updatedbyempname")%>
                                                </ItemTemplate>
                                                <ItemStyle Width="150px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-VerticalAlign="Top">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="gvbtnDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" CssClass="btn-primary btn btn-xs"
                                                        NavigateUrl='<%# "~/admin/adminfiles/company/SalesInfo.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>' Target="_blank">
                                          <%--  <img src="<%=SiteURL%>/images/icon_detail.png" />--%>
                                            <i class="fa fa-link"></i>Detail
                                                    </asp:HyperLink>
                                                    <%--<asp:HyperLink ID="gvbtnDetail2" runat="server" data-toggle="tooltip" data-placement="top" Visible="false" data-original-title="Detail" CssClass="btn-primary btn btn-xs"
                                                        NavigateUrl='<%# "~/admin/adminfiles/company/ECompany.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                          <img src="<%=SiteURL%>/images/icon_detail.png" />
                                            <i class="fa fa-link"></i>Detail
                                                    </asp:HyperLink>--%>

                                                    <!--DELETE Modal Templates-->
                                                    <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-xs" CausesValidation="false"
                                                        CommandName="Delete" CommandArgument='<%#Eval("ProjectID")%>'>
                                            <i class="fa fa-trash"></i> Delete
                                                    </asp:LinkButton>

                                                    <!--END DELETE Modal Templates-->


                                                </ItemTemplate>
                                                <ItemStyle Width="1%" HorizontalAlign="Center" CssClass="verticaaline" />
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="PanAdd" Visible="false">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="paddall">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4>Add New Project</h4>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="graybgarea">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">
                                                                            Project Type<span class="symbol required"></span>
                                                                        </label>
                                                                    </span>
                                                                    <div class="drpValidate">

                                                                        <asp:DropDownList ID="ddlProjectTypeID" runat="server" aria-controls="DataTables_Table_0" CssClass="myvalcomp"
                                                                            AppendDataBoundItems="true"> <%--OnSelectedIndexChanged="ddlProjectTypeID_SelectedIndexChanged AutoPostBack="true"" --%>
                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required."
                                                                            ControlToValidate="ddlProjectTypeID" Display="Dynamic" ValidationGroup="project"></asp:RequiredFieldValidator>
                                                                    </div>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group dateimgarea">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Project Opened
                                                                        </label>
                                                                    </span><span class="dateimg">
                                                                        <asp:TextBox ID="txtProjectOpened" runat="server" Enabled="false" class="form-control"></asp:TextBox>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Manual No.
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtManualQuoteNumber" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2 form-group" id="divSalesRep" runat="server">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        Sales Rep<span class="symbol required"></span>
                                                                    </label>
                                                                </span><span>
                                                                    <asp:TextBox ID="ddlSalesRep" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required."
                                                                        ControlToValidate="ddlSalesRep" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                </span>
                                                                <div class="clear">
                                                                </div>

                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">
                                                                            Contact<span class="symbol required"></span>
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:DropDownList ID="ddlContact" runat="server" aria-controls="DataTables_Table_0" class="myvalcomp"
                                                                            AppendDataBoundItems="true">
                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required."
                                                                            ControlToValidate="ddlContact" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div runat="server" class="form-group" id="divoldproject">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Old Project No.
                                                                        </label>
                                                                    </span><span>
                                                                        <%--<asp:TextBox ID="txtOldProjectNumber" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>--%>
                                                                        <asp:DropDownList runat="server" ID="ddllinkprojectid" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="This value is required." CssClass="comperror" ControlToValidate="ddllinkprojectid" Display="Dynamic" Visible="false" ValidationGroup="project"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="graybgarea">
                                                        <div class="row">

                                                            <div class="col-md-2" id="Div1" visible="false" runat="server">
                                                                <div class="form-group">
                                                                    <div class="marginbtm10">
                                                                        <asp:HiddenField ID="hndaddress" runat="server" />
                                                                        <asp:TextBox ID="txtInstallAddressline" runat="server" MaxLength="50" CssClass="form-control" onblur="getDetailInstallParsedAddress();"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This value is required."
                                                                            ControlToValidate="txtInstallAddressline" Display="Dynamic" Enabled="false"></asp:RequiredFieldValidator>
                                                                        <asp:CustomValidator ID="CustomInstallAddressline" runat="server"
                                                                            ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="detail"
                                                                            ClientValidationFunction="ChkFun"></asp:CustomValidator>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Install Site
                                                                        </label>
                                                                    </span>
                                                                    <div id="Div2" class="marginbtm10" visible="false" runat="server">
                                                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                                                        <asp:TextBox ID="TextBox1" runat="server" MaxLength="50" CssClass="form-control" onblur="getDetailInstallParsedAddress();"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This value is required."
                                                                            ControlToValidate="txtInstallAddressline" Display="Dynamic" Enabled="false"></asp:RequiredFieldValidator>
                                                                        <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                                            ErrorMessage="" Display="Dynamic" CssClass="requiredfield" ValidationGroup="detail"
                                                                            ClientValidationFunction="ChkFun"></asp:CustomValidator>
                                                                    </div>
                                                                    <div class="marginbtm10">

                                                                        <asp:Panel runat="server" ID="PanInstallAddress">
                                                                            <asp:TextBox ID="txtInstallAddress" runat="server" MaxLength="200" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="This value is required."
                                                                                ControlToValidate="txtInstallAddress" Display="Dynamic" ValidationGroup="detail"></asp:RequiredFieldValidator>
                                                                        </asp:Panel>

                                                                    </div>

                                                                    <asp:HiddenField ID="hndstreetno" runat="server" />
                                                                    <asp:HiddenField ID="hndstreetname" runat="server" />
                                                                    <asp:HiddenField ID="hndstreettype" runat="server" />
                                                                    <asp:HiddenField ID="hndstreetsuffix" runat="server" />
                                                                    <asp:HiddenField ID="hndunittype" runat="server" />
                                                                    <asp:HiddenField ID="hndunitno" runat="server" />
                                                                    <div id="validaddressid" style="display: none">
                                                                        <%-- <img src="../../../images/check.png" alt="check">--%>
                                                                        <i class="fa fa-check"></i>
                                                                        Address is valid.
                                                                    </div>
                                                                    <div id="invalidaddressid" style="display: none">
                                                                        <%--<img src="../../../images/x.png" alt="cross">--%>
                                                                        <i class="fa fa-close"></i>
                                                                        Address is invalid.
                                                                    </div>

                                                                </div>


                                                                <div class="onelindiv marginbtm10 row">
                                                                    <span class="col-md-6">
                                                                        <asp:TextBox ID="txtformbayUnitNo" runat="server" placeholder="Unit No" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtformbayUnitNo_TextChanged"></asp:TextBox></span>
                                                                    <span class="col-md-6">
                                                                        <asp:DropDownList ID="ddlformbayunittype" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlformbayunittype_SelectedIndexChanged" aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                                            <asp:ListItem Value="">Unit Type</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                                <div class="onelindiv marginbtm10 row">
                                                                    <span class="col-md-3">
                                                                        <asp:TextBox ID="txtformbayStreetNo" runat="server" placeholder="Street No" AutoPostBack="true" OnTextChanged="txtformbayStreetNo_TextChanged" CssClass="form-control"></asp:TextBox></span>
                                                                    <span class="col-md-4">
                                                                        <asp:TextBox ID="txtformbaystreetname" runat="server" placeholder="Street Name" AutoPostBack="true" OnTextChanged="txtformbaystreetname_TextChanged" CssClass="form-control"></asp:TextBox>
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtformbaystreetname" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetStreetNameList"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                        <asp:CustomValidator ID="Customstreetadd" runat="server"
                                                                            ErrorMessage="" Display="Dynamic" CssClass="requiredfield"
                                                                            ClientValidationFunction="GetStreetNameList"></asp:CustomValidator>
                                                                    </span>
                                                                    <span class="col-md-5">
                                                                        <asp:DropDownList ID="ddlformbaystreettype" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlformbaystreettype_SelectedIndexChanged" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                                                            <asp:ListItem Value="">Street Type</asp:ListItem>
                                                                        </asp:DropDownList></span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>

                                                                <div class="marginbtm10 row">
                                                                    <span class="col-md-6">
                                                                        <asp:TextBox ID="txtInstallCity" runat="server" OnTextChanged="txtInstallCity_OnTextChanged" AutoPostBack="true"
                                                                            MaxLength="50" class="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="rfvinstall" runat="server" ErrorMessage="This value is required."
                                                                            ControlToValidate="txtInstallCity" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                        <cc1:AutoCompleteExtender ID="AutoCompleteSearch" MinimumPrefixLength="2" runat="server"
                                                                            UseContextKey="true" TargetControlID="txtInstallCity" ServicePath="~/Search.asmx"
                                                                            CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCitiesList"
                                                                            EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                    </span>
                                                                    <span class="col-md-3">
                                                                        <asp:TextBox ID="txtInstallState" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox></span>
                                                                    <span class="col-md-3">
                                                                        <asp:TextBox ID="txtInstallPostCode" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox></span></span>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group textareaboxheight">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Project Notes
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtProjectNotes" runat="server" CssClass="form-control" TextMode="MultiLine" Height="170px"></asp:TextBox>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4" runat="server" visible="false"></div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="col-md-5" runat="server" visible="false">
                                                    <div class="graybgarea margintop50 spicaltextbox">

                                                        <div class="form-group textareaboxheight">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Installer Notes
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtInstallerNotes" runat="server" CssClass="form-control" TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group textareaboxheight">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Notes for Installation Department
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtMeterInstallerNotes" runat="server" CssClass="form-control" TextMode="MultiLine" Height="80px"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div>
                                                <span></span>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-12 textcenterbutton">
                                                <div style="text-align: center;">
                                                    <asp:Button class="btn btn-primary addwhiteicon redreq btnaddicon" ID="btnAdd" runat="server"
                                                        Text="Add" CausesValidation="true" ValidationGroup="project" OnClick="btnAdd_Click" />
                                                    <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdate" runat="server"
                                                        Text="Save" Visible="true" />
                                                    <asp:Button class="btn btn-purple resetbutton btnreseticon" ID="btnReset" runat="server"
                                                        CausesValidation="false" Text="Reset" OnClick="btnReset_Click" />
                                                    <asp:Button class="btn btn-dark-grey calcelwhiteicon btncancelicon" ID="btnCancel" runat="server"
                                                        CausesValidation="false" Text="Cancel" OnClick="btnCancel_Click" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </section>


    </ContentTemplate>
    <Triggers>
        <%--<asp:AsyncPostBackTrigger ControlID="gvbtnDelete"  />--%>
    </Triggers>
</asp:UpdatePanel>

<!--Danger Modal Templates-->
<asp:Button ID="btndelete" Style="display: none;" runat="server" />
<cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
    PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
</cc1:ModalPopupExtender>
<div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

    <div class="modal-dialog " style="margin-top: -300px">
        <div class=" modal-content ">
            <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
            <div class="modal-header text-center">
                <i class="glyphicon glyphicon-fire"></i>
            </div>


            <div class="modal-title">Delete</div>
            <label id="ghh" runat="server"></label>
            <div class="modal-body ">Are You Sure Delete This Entry?</div>
            <div class="modal-footer " style="text-align: center">
                <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow" CausesValidation="false">OK</asp:LinkButton>
                <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
            </div>
        </div>
    </div>

</div>

<asp:HiddenField ID="hdndelete" runat="server" />
<!--End Danger Modal Templates-->

<asp:Button ID="Button2" Style="display: none;" runat="server" />
<cc1:ModalPopupExtender ID="ModalPopupExtenderAddressExistency" runat="server" BackgroundCssClass="modalbackground"
    DropShadow="false" PopupControlID="divAddressCheck" CancelControlID="ibtnCancel"
    OkControlID="btnOKAddress" TargetControlID="Button2">
</cc1:ModalPopupExtender>
<div id="divAddressCheck" runat="server" style="display: none">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div style="float: right">
                    <button id="ibtnCancel" runat="server" onclick="btnCancle_Click" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">Close</button>
                </div>
                <h4 class="modal-title" id="H4">Duplicate Address</h4>
            </div>
            <div class="modal-body paddnone">
                <div class="panel-body">
                    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable" summary="">
                        <tbody>
                            <tr align="center">
                                <td>
                                    <h4 class="noline" style="color: black"><b>There is a Contact in the database already who has this address.
                                                    <br />
                                        This looks like a Duplicate Entry.</b></h4>
                                </td>
                            </tr>
                            <tr align="center">
                                <td>
                                    <%--<asp:Button ID="btnDupeAddress" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnDupeAddress_Click"
                                                                            Text="Dupe" CausesValidation="false" />
                                          <asp:Button ID="btnNotDupeAddress" runat="server" OnClick="btnNotDupeAddress_Click"
                                                                            CausesValidation="false" CssClass="btn btn-danger btn-rounded" Text="Not Dupe" />
                                          <asp:Button ID="btnOKAddress" Style="display: none; visible: false;" runat="server"
                                                                            CssClass="btn" Text=" OK " /></td>--%>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="tablescrolldiv">
                        <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                            <asp:GridView ID="rptaddress" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" DataKeyNames="CustomerID" runat="server"
                                PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" AllowSorting="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="Customers">
                                        <ItemTemplate><%# Eval("Customer")%> </ItemTemplate>
                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Street Address" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate><%#Eval("StreetAddress")+" "+Eval("StreetCity")+" "+Eval("StreetState") %> </ItemTemplate>
                                        <ItemStyle Width="150px" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<cc1:ModalPopupExtender ID="ModalPopupExtenderAddress" runat="server" BackgroundCssClass="modalbackground statuspopup"
    CancelControlID="ibtnCancelAddress" DropShadow="false" PopupControlID="divAddress"
    OkControlID="btnOKAddress" TargetControlID="btnNULLAddress">
</cc1:ModalPopupExtender>
<div id="divAddress" runat="server" style="display: none; z-index: 999!important;" class="modal_popup">
    <div class="modal-dialog" style="width: 350px;">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <div style="float: right">
                    <asp:LinkButton ID="ibtnCancelAddress" CausesValidation="false" runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                   Close
                    </asp:LinkButton>
                </div>
                <h4 class="modal-title" id="H2">
                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                    Update Address</h4>
            </div>
            <div class="modal-body paddnone">
                <div class="panel-body">
                    <table cellpadding="5" cellspacing="0" border="0" width="100%" class="formtable"
                        summary="">
                        <tbody>
                            <tr style="text-align: center">
                                <td>
                                    <h3 class="noline"><b>Address is not verified. Please verify the address first.</b> </h3>
                                </td>
                            </tr>
                            <tr style="text-align: center">
                                <td>
                                    <asp:Button ID="btnYesAddress" runat="server" CssClass="btn btn-danger btn-rounded" OnClick="btnYesAddress_Click"
                                        Text="Yes" CausesValidation="false" />
                                    <asp:Button ID="btnCancleAddress" runat="server" CausesValidation="false"
                                        CssClass="btn btn-danger btn-rounded" Text="Cancle" />

                                    <asp:Button ID="btnOKAddress" Style="display: none; visible: false;" runat="server"
                                        CssClass="btn" Text=" OK " /></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:Button ID="btnNULLAddress" Style="display: none;" runat="server" />


<cc1:ModalPopupExtender ID="ModalPopupExtenderStatus" runat="server" BackgroundCssClass="modalbackground statuspopup"
    CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="divUpdateStatus"
    OkControlID="btnOKStatus" TargetControlID="btnNULLStatus">
</cc1:ModalPopupExtender>
<div id="divUpdateStatus" runat="server" style="display: none; z-index: 999!important;" class="modal_popup">
    <div class="modal-dialog" style="width: 350px;">
        <div class="modal-content">
            <div class="color-line"></div>
            <div class="modal-header">
                <div style="float: right">
                    <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                    Close
                    </asp:LinkButton>
                </div>
                <h4 class="modal-title" id="myModalLabel">
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                    Update Status</h4>
            </div>
            <div class="modal-body paddnone">
                <div class="panel-body">
                    <div class="formainline">
                        <div class="form-group">
                            <label>
                                Status:
                            </label>
                            <asp:DropDownList ID="ddlProjectStatusID" runat="server" Width="150px" OnSelectedIndexChanged="ddlProjectStatusID_SelectedIndexChanged"
                                AppendDataBoundItems="true" AutoPostBack="true" aria-controls="DataTables_Table_0" CssClass="myvalcomp">
                                <asp:ListItem Value="">Select</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required." CssClass="reqerror" ValidationGroup="status" ControlToValidate="ddlProjectStatusID" Display="Dynamic"></asp:RequiredFieldValidator>

                        </div>

                        <div class="form-group" id="trHold" runat="server" visible="false">
                            <label>
                                OnHold Reason:
                            </label>
                            <asp:DropDownList ID="ddlProjectOnHoldID" runat="server" Width="150px" AppendDataBoundItems="true"
                                aria-controls="DataTables_Table_0" class="myvalcomp">
                                <asp:ListItem Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                ControlToValidate="ddlProjectOnHoldID" Display="Dynamic" ValidationGroup="status"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group" id="trCancel" runat="server" visible="false">
                            <label>
                                Cancel Reason:
                            </label>
                            <asp:DropDownList ID="ddlProjectCancelID" runat="server" AppendDataBoundItems="true"
                                Width="150px" aria-controls="DataTables_Table_0" class="myvalcomp">
                                <asp:ListItem Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                ValidationGroup="status" ControlToValidate="ddlProjectCancelID" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group modaldesc" id="trComment" runat="server" visible="false">
                            <label>
                                Comment:
                            </label>
                            <asp:TextBox ID="txtComment" runat="server" Width="200px" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                        </div>
                        <div class="form-group" id="precancel" runat="server">
                            <div class="form-group modaldesc" id="ddlcancelReason" runat="server">
                                <asp:DropDownList ID="ddlprecancel" runat="server" AppendDataBoundItems="true"
                                    Width="150px" aria-controls="DataTables_Table_0" class="myvalcomp">
                                    <asp:ListItem Value="">Select</asp:ListItem>
                                    <asp:ListItem Value="1">Roof Space Issue</asp:ListItem>
                                    <asp:ListItem Value="2">Shading Issue</asp:ListItem>
                                    <asp:ListItem Value="3"> Not interested </asp:ListItem>
                                    <asp:ListItem Value="4">Financial issue</asp:ListItem>
                                    <asp:ListItem Value="5">Not applicable.</asp:ListItem>
                                </asp:DropDownList>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                    ControlToValidate="ddlprecancel" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                            <div class="form-group modaldesc" runat="server">
                                <label>
                                    Pre Cancel Reason
                                </label>
                                <asp:TextBox ID="txtPreCancelReason" runat="server" Width="200px" TextMode="MultiLine" MaxLength="500"></asp:TextBox>

                                <asp:HiddenField ID="hndProjectStatusId" runat="server" />
                                <%--<div class="input-group date datetimepicker1" style="width: 180px">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtActiveDate1"  runat="server" class="form-control"></asp:TextBox>
                                                                   
                                                                </div>--%>
                                <div class="form-group" id="Div3" runat="server" visible="false" style="display: none">
                                    <label>
                                        Active date
                                    </label>
                                    <asp:TextBox ID="txtActiveDate1" CssClass="form-control datetimepicker1" Enabled="false" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="form-group marginleft center-text">
                        <asp:Button ID="ibtnUpdateStatus" runat="server" Text="Update" OnClick="ibtnUpdateStatus_Onclick"
                            class="btn btn-primary savewhiteicon btnsaveicon" />
                        <%--ValidationGroup="status" --%>
                        <asp:Button ID="btnOKStatus" Style="display: none; visible: false;" runat="server"
                            CssClass="btn" Text=" OK " />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:Button ID="btnNULLStatus" Style="display: none;" runat="server" />
<asp:HiddenField ID="hndStatusProjectID" runat="server" />

<div class="loaderPopUP">
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');

        }
        function endrequesthandler(sender, args) {
            $('.loading-container').css('display', 'none');
        }

        function pageLoadedpro() {

            $(".myvalinfo").select2({
                //placeholder: "select",
                allowclear: true
            });

            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });

            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $('.redreq').click(function () {
                formValidate();
            });


            $('.redreq1').click(function () {
                form_Validate();
            });

            $('.loading-container').css('display', 'none');

            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            // $('.loading-container').css('display', 'none');
        }
    </script>


</div>
