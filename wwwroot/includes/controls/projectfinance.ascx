﻿<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectfinance.ascx.cs"
    Inherits="includes_controls_projectfinance" %>
<script>
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadedsalefinance);
    function pageLoadedsalefinance() {
        $('.datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        $(".myval").select2({
            //placeholder: "select",
            allowclear: true
        });
    }
</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
          </ContentTemplate>
</asp:UpdatePanel>
        <section class="row m-b-md">
            <div class="col-sm-12 minhegihtarea">
                <div class="contactsarea">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                        <div class="alert alert-danger" id="PanNoPayment" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;No Payment Option for Finance.</strong>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:Panel ID="PanAppDetail" runat="server">
                                <div>
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">Application Detail</span>
                                        </div>
                                        <div class="widget-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <span class="name">
                                                            <asp:Label ID="Label2" runat="server" class="  control-label">
                                             Application Date:</asp:Label></span>
                                                        <div class="input-group date datetimepicker1 ">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtApplicationDate" runat="server" class="form-control" Width="152px">
                                                            </asp:TextBox>
                                                        </div>
                                                    </div>


                                                    <%--<div class="form-group selpen">
                                            <span class="name">
                                                <label class="control-label">
                                                   
                                                </label>
                                            </span><span>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtApplicationDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td style="padding-left: 7px;">
                                                            <asp:ImageButton ID="Image15" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                            <cc1:CalendarExtender ID="CalendarExtender15" runat="server" PopupButtonID="Image15"
                                                                TargetControlID="txtApplicationDate" Format="dd/MM/yyyy">
                                                            </cc1:CalendarExtender>
                                                            <asp:RegularExpressionValidator ValidationGroup="finance" ControlToValidate="txtApplicationDate" ID="RegularExpressionValidator4" runat="server" ErrorMessage="Enter valid date"
                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>--%>

                                                    <div class="form-group">
                                                        <span class="name">
                                                            <asp:Label ID="Label1" runat="server" class="  control-label">
                                                  Document Sent Date:</asp:Label></span>
                                                        <div class="input-group date datetimepicker1 ">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtDocumentSentDate" runat="server" class="form-control" Width="152px">
                                                            </asp:TextBox>
                                                        </div>
                                                    </div>



                                                    <%--<div class="form-group selpen">
                                            <span class="name">
                                                <label class="control-label">
                                                  
                                                </label>
                                            </span><span>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtDocumentSentDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                        </td>
                                                        <td style="padding-left: 7px;">
                                                            <asp:ImageButton ID="Image1" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="Image1"
                                                                TargetControlID="txtDocumentSentDate" Format="dd/MM/yyyy">
                                                            </cc1:CalendarExtender>
                                                            <asp:RegularExpressionValidator ValidationGroup="finance" ControlToValidate="txtDocumentSentDate" ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter valid date"
                                                                ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>--%>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group selpen" style="width: 190px;">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Applied By:
                                                            </label>
                                                        </span><span>
                                                            <asp:DropDownList ID="ddlAppliedBy" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                                AppendDataBoundItems="true">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                    <div class="form-group selpen" style="width: 190px;">
                                                        <span class="name disblock">
                                                            <label class="control-label">
                                                                Document Sent By :
                                                            </label>
                                                        </span><span>
                                                            <asp:DropDownList ID="ddlDocumentSentBy" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                                AppendDataBoundItems="true">
                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group selpen" style="width: 190px;">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Purchase No. :
                                                            </label>
                                                        </span><span>
                                                            <asp:TextBox ID="txtPurchaseNo" runat="server" MaxLength="100" class="form-control"></asp:TextBox>
                                                        </span>
                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="PanDocument" runat="server">
                                <asp:Panel ID="PanDocReceive" runat="server">
                                    <div>
                                        <div class="widget flat radius-bordered borderone">
                                            <div class="widget-header bordered-bottom bordered-blue">
                                                <span class="widget-caption">Document Receive</span>
                                            </div>
                                            <div class="widget-body">
                                                <div class="row">
                                                    <div class="col-md-4">


                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Document Received Date:</label></span>
                                                            <div class="input-group date datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtDocumentReceivedDate" runat="server" CssClass="form-control"
                                                                    Width="152px"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ValidationGroup="finance" ControlToValidate="txtDocumentReceivedDate" ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid date"
                                                                    ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                            </div>
                                                        </div>




                                                        <%--                                            <div class="form-group selpen">
                                                <span class="name">
                                                    <label class="control-label">
                                                        Document Received Date:
                                                    </label>
                                                </span> <span class="fa fa-calendar"></span><span>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                              
                                                            </td>
                                                            <td style="padding-left: 7px;">
                                                           
                                                              
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>--%>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen" style="width: 190px;">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Document Received By:
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlDocumentReceivedBy" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">

                                                        <div class="form-group selpen">
                                                            <div style="margin-top: 30px;">
                                                                <span class="checkbox-info checkbox">
                                                                    <label for="<%=chkDocumentVerified.ClientID %>">
                                                                        <asp:CheckBox ID="chkDocumentVerified" runat="server" />
                                                                        <span class="text">Document Verified</span>
                                                                    </label>
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="PanDocSent" runat="server">
                                    <div>
                                        <div class="widget flat radius-bordered borderone">
                                            <div class="widget-header bordered-bottom bordered-blue">
                                                <span class="widget-caption">Document Sent</span>
                                            </div>
                                            <div class="widget-body">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group selpen" style="width: 190px;">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Sent By:
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlSentBy" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">

                                                        <div class="form-group">
                                                            <span class="name">
                                                                <asp:Label ID="Label3" runat="server" class="  control-label">
                                                 Sent Date:</asp:Label></span>
                                                            <div class="input-group date datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtSentDate" runat="server" class="form-control" Width="152px">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>


                                                        <%--<div class="form-group selpen">
                                                <span class="name">
                                                    <label class="control-label">
                                                       
                                                    </label>
                                                </span><span>
                                                    <table style="margin-top: -5px;">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtSentDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                            </td>
                                                            <td style="padding-left: 7px;">
                                                                <asp:ImageButton ID="Image3" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" PopupButtonID="Image3"
                                                                    TargetControlID="txtSentDate" Format="dd/MM/yyyy">
                                                                </cc1:CalendarExtender>
                                                                <asp:RegularExpressionValidator ValidationGroup="finance" ControlToValidate="txtSentDate" ID="RegularExpressionValidator3" runat="server" ErrorMessage="Enter valid date"
                                                                    ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>--%>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group selpen" style="width: 190px;">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Postal Tracking No. :
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtPostalTrackingNumber" runat="server" MaxLength="100" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group selpen">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Remarks:
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtRemarks" runat="server" MaxLength="1000" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="PanPayReceive" runat="server">
                                    <div>
                                        <div class="widget flat radius-bordered borderone">
                                            <div class="widget-header bordered-bottom bordered-blue">
                                                <span class="widget-caption">Payment Receive</span>
                                            </div>
                                            <div class="widget-body">
                                                <div class="row">
                                                    <div class="col-md-4">

                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Received Date:</label></span>
                                                            <div class="input-group date datetimepicker1 ">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtReceivedDate" runat="server" CssClass="form-control" Width="125px"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ValidationGroup="finance" ControlToValidate="txtReceivedDate" ID="RegularExpressionValidator5" runat="server" ErrorMessage="Enter valid date"
                                                                    ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                            </div>
                                                        </div>




                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group selpen" style="width: 190px;">
                                                            <span class="name disblock">
                                                                <label class="control-label">
                                                                    Received By:
                                                                </label>
                                                            </span><span>
                                                                <asp:DropDownList ID="ddlReceivedBy" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                                    AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <div style="margin-top: 30px">
                                                                <span class="checkbox-info checkbox">
                                                                    <label for="<%=chkPaymentVerified.ClientID %>">
                                                                        <asp:CheckBox ID="chkPaymentVerified" runat="server" />
                                                                        <span class="text">Payment Verified</span>
                                                                    </label>
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </asp:Panel>
                        </div>
                    </div>
                    <div class="row center-text">
                        <div class="col-md-12 textcenterbutton">
                            <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdateFinance" runat="server" Text="Save"
                                CausesValidation="true" ValidationGroup="finance" OnClick="btnUpdateFinance_Click" />
                        </div>
                        <br />
                    </div>
                </asp:Panel>
            </div>
        </section>


