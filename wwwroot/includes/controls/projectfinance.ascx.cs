﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;

public partial class includes_controls_projectfinance : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        PanSuccess.Visible = false;
        PanError.Visible = false;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if ((Roles.IsUserInRole("Sales Manager")))
            {

                if (st2.ProjectStatusID == "3")
                {
                    btnUpdateFinance.Visible = false;
                }
            }
            if (Roles.IsUserInRole("SalesRep"))
            {
                if (st2.DepositReceived != string.Empty)
                {
                    btnUpdateFinance.Visible = false;
                }
            }

        }
    }
    public void BindProjects()
    {
        GridView GridView1 = (GridView)this.Parent.Parent.FindControl("GridView1");
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblProjects.tblProjects_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")) || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
        {
            dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
        }
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }
    public void BindProjectFinance()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {

            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
            // if (stPro.FinanceWithID == "" || stPro.FinanceWithID == "1")
            if (string.IsNullOrEmpty(stPro.FinanceWithID) || stPro.FinanceWithID == "1")
            {
                PanNoPayment.Visible = true;
                PanAddUpdate.Visible = false;
            }
            else
            {
                PanNoPayment.Visible = false;
                PanAddUpdate.Visible = true;
            }
            if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("DSalesRep")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("SalesRep")) || (Roles.IsUserInRole("Accountant")))
            {
                if (stPro.FinanceWithID == "" || stPro.FinanceWithID == "1")
                {
                    PanAddUpdate.Enabled = false;
                }
                else
                {
                    PanAddUpdate.Enabled = true;
                }
            }
            else
            {
                if (Roles.IsUserInRole("Finance") || Roles.IsUserInRole("Administrator"))//remove admin
                {
                    PanAddUpdate.Enabled = true;
                    PanAppDetail.Enabled = true;
                    PanDocument.Enabled = true;
                    PanDocReceive.Enabled = true;
                    PanDocSent.Enabled = true;
                    PanPayReceive.Enabled = true;
                }
            }
            if (Roles.IsUserInRole("Installer"))
            {
                PanAddUpdate.Enabled = false;
            }

            if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("DSalesRep")) || (Roles.IsUserInRole("SalesRep")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("Accountant")))
            {
                PanAppDetail.Enabled = true;

                if (stPro.ApplicationDate == string.Empty || stPro.PurchaseNo == string.Empty || stPro.DocumentSentDate == string.Empty)
                {
                    PanAppDetail.Enabled = true;
                }
                else
                {
                    PanAppDetail.Enabled = false;
                }
            }
            else
            {
                PanAppDetail.Enabled = false;
                if (Roles.IsUserInRole("Finance") || Roles.IsUserInRole("Administrator"))
                {
                    PanAddUpdate.Enabled = true;
                    PanAppDetail.Enabled = true;
                    PanDocument.Enabled = true;
                    PanDocReceive.Enabled = true;
                    PanDocSent.Enabled = true;
                    PanPayReceive.Enabled = true;
                }
                if (Roles.IsUserInRole("Installation Manager"))
                {
                    PanAddUpdate.Enabled = true;
                    PanAppDetail.Enabled = true;
                    PanDocument.Enabled = true;
                    PanDocReceive.Enabled = true;
                    PanDocSent.Enabled = true;
                    PanPayReceive.Enabled = true;
                }
            }

            if ((Roles.IsUserInRole("Accountant")))//// add condition 
            {
                PanDocument.Enabled = true;
                PanDocReceive.Enabled = true;
                txtDocumentReceivedDate.Enabled = false;
                //  Image2.Visible = false;
                ddlDocumentReceivedBy.Enabled = true;
                chkDocumentVerified.Attributes.Add("onclick", "return false;");

                PanDocSent.Enabled = false;
                //PanPayReceive.Enabled = false;
                txtReceivedDate.Enabled = false;
                //Image4.Visible = false;
                ddlReceivedBy.Enabled = false;
                chkPaymentVerified.Attributes.Add("onclick", "return false;");

                if (stPro.ApplicationDate != string.Empty && stPro.PurchaseNo != string.Empty && stPro.DocumentSentDate != string.Empty)
                {
                    PanDocReceive.Enabled = true;
                }
                if (stPro.DocumentReceivedDate != string.Empty && stPro.DocumentReceivedBy != string.Empty && stPro.DocumentVerified.ToLower() != "false")
                {
                    PanDocSent.Enabled = true;
                    //PanDocReceive.Enabled = false;
                    txtDocumentReceivedDate.Enabled = false;
                    //   Image2.Visible = false;
                    ddlDocumentReceivedBy.Enabled = false;
                    chkDocumentVerified.Attributes.Add("onclick", "return false;");
                    chkDocumentVerified.Visible = true;
                }
                if (stPro.SentDate != string.Empty && stPro.SentBy != string.Empty && stPro.PostalTrackingNumber != string.Empty)
                {
                    PanPayReceive.Enabled = true;
                    PanDocSent.Enabled = false;
                    //PanDocReceive.Enabled = false;
                    txtDocumentReceivedDate.Enabled = false;
                    //  Image2.Visible = false;
                    ddlDocumentReceivedBy.Enabled = false;
                    chkDocumentVerified.Attributes.Add("onclick", "return false;");
                    chkDocumentVerified.Visible = true;
                }
                if (stPro.ReceivedDate != string.Empty && stPro.ReceivedBy != string.Empty && stPro.PaymentVerified != "False")
                {
                    //PanPayReceive.Enabled = false;
                    txtReceivedDate.Enabled = false;
                    //  Image4.Visible = false;
                    ddlReceivedBy.Enabled = false;
                    chkPaymentVerified.Attributes.Add("onclick", "return false;");
                    PanDocSent.Enabled = false;
                    //PanDocReceive.Enabled = false;
                    txtDocumentReceivedDate.Enabled = false;
                    //    Image2.Visible = false;
                    ddlDocumentReceivedBy.Enabled = false;
                    chkDocumentVerified.Attributes.Add("onclick", "return false;");
                    chkDocumentVerified.Visible = true;
                }
            }
            else
            {
                PanDocument.Enabled = false;
            }

            BindFinanceDropDown();

            try
            {
                txtApplicationDate.Text = Convert.ToDateTime(stPro.ApplicationDate).ToShortDateString();
            }
            catch { }

            txtPurchaseNo.Text = stPro.PurchaseNo;
            try
            {
                txtDocumentSentDate.Text = Convert.ToDateTime(stPro.DocumentSentDate).ToShortDateString();
            }
            catch { }
            try
            {
                txtDocumentReceivedDate.Text = Convert.ToDateTime(stPro.DocumentReceivedDate).ToShortDateString();
            }
            catch { }

            chkDocumentVerified.Checked = Convert.ToBoolean(stPro.DocumentVerified);
            try
            {
                txtSentDate.Text = Convert.ToDateTime(stPro.SentDate).ToShortDateString();
            }
            catch { }
            txtPostalTrackingNumber.Text = stPro.PostalTrackingNumber;
            txtRemarks.Text = stPro.Remarks;
            try
            {
                txtReceivedDate.Text = Convert.ToDateTime(stPro.ReceivedDate).ToShortDateString();
            }
            catch { }
            chkPaymentVerified.Checked = Convert.ToBoolean(stPro.PaymentVerified);


            if (Roles.IsUserInRole("Finance") || Roles.IsUserInRole("Administrator"))
            {
                PanAddUpdate.Enabled = true;
                PanAppDetail.Enabled = true;
                PanDocument.Enabled = true;
                PanDocReceive.Enabled = true;
                PanDocSent.Enabled = true;
                PanPayReceive.Enabled = true;
            }
            if (Roles.IsUserInRole("Installation Manager"))
            {
                PanAddUpdate.Enabled = true;
                PanAppDetail.Enabled = true;
                PanDocument.Enabled = true;
                PanDocReceive.Enabled = true;
                PanDocSent.Enabled = true;
                PanPayReceive.Enabled = true;
            }
        }
    }

    public void BindFinanceDropDown()
    {
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlAppliedBy.Items.Clear();
        ddlAppliedBy.Items.Add(item1);

        ddlAppliedBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlAppliedBy.DataValueField = "EmployeeID";
        ddlAppliedBy.DataMember = "fullname";
        ddlAppliedBy.DataTextField = "fullname";
        ddlAppliedBy.DataBind();

        try
        {
            if (stPro.AppliedBy == string.Empty)
            {
                ddlAppliedBy.SelectedValue = stEmp.EmployeeID;
            }
            else
            {
                ddlAppliedBy.SelectedValue = stPro.AppliedBy;
            }
        }
        catch { }
        ListItem item2 = new ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddlDocumentSentBy.Items.Clear();
        ddlDocumentSentBy.Items.Add(item2);

        ddlDocumentSentBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlDocumentSentBy.DataValueField = "EmployeeID";
        ddlDocumentSentBy.DataMember = "fullname";
        ddlDocumentSentBy.DataTextField = "fullname";
        ddlDocumentSentBy.DataBind();

        if (stPro.DocumentSentBy == string.Empty)
        {
            try
            {
                ddlDocumentSentBy.SelectedValue = stEmp.EmployeeID;
            }
            catch { }
        }
        else
        {
            ddlDocumentSentBy.SelectedValue = stPro.DocumentSentBy;
        }

        if ((Roles.IsUserInRole("DSalesRep")) || (Roles.IsUserInRole("SalesRep")))
        {
            ddlAppliedBy.Enabled = false;
            ddlDocumentSentBy.Enabled = false;
        }
        ListItem item3 = new ListItem();
        item3.Text = "Select";
        item3.Value = "";
        ddlDocumentReceivedBy.Items.Clear();
        ddlDocumentReceivedBy.Items.Add(item3);

        ddlDocumentReceivedBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlDocumentReceivedBy.DataValueField = "EmployeeID";
        ddlDocumentReceivedBy.DataMember = "fullname";
        ddlDocumentReceivedBy.DataTextField = "fullname";
        ddlDocumentReceivedBy.DataBind();

        try
        {
            if (stPro.DocumentReceivedBy == string.Empty)
            {
                ddlDocumentReceivedBy.SelectedValue = stEmp.EmployeeID;
            }
            else
            {
                ddlDocumentReceivedBy.SelectedValue = stPro.DocumentReceivedBy;
            }
        }
        catch (Exception ex)
        {

            //sthrow;
        }

        ListItem item4 = new ListItem();
        item4.Text = "Select";
        item4.Value = "";
        ddlSentBy.Items.Clear();
        ddlSentBy.Items.Add(item4);

        ddlSentBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlSentBy.DataValueField = "EmployeeID";
        ddlSentBy.DataMember = "fullname";
        ddlSentBy.DataTextField = "fullname";
        ddlSentBy.DataBind();

        if (stPro.SentBy == string.Empty)
        {try
            {
                ddlSentBy.SelectedValue = stEmp.EmployeeID;
            }
            catch (Exception ex)
            {

            }
        }
        else
        {
            try
            {
                ddlSentBy.SelectedValue = stPro.SentBy;
            }
            catch (Exception ex)
            {

            }
            
        }

        ListItem item5 = new ListItem();
        item5.Text = "Select";
        item5.Value = "";
        ddlReceivedBy.Items.Clear();
        ddlReceivedBy.Items.Add(item5);

        ddlReceivedBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlReceivedBy.DataValueField = "EmployeeID";
        ddlReceivedBy.DataMember = "fullname";
        ddlReceivedBy.DataTextField = "fullname";
        ddlReceivedBy.DataBind();

        if (stPro.ReceivedBy == string.Empty)
        {
            try
            {
                ddlReceivedBy.SelectedValue = stEmp.EmployeeID;
            }
            catch (Exception ex)
            {

            }
           
        }
        else
        {
            try
            {
                ddlReceivedBy.SelectedValue = stPro.ReceivedBy;
            }
            catch (Exception ex)
            {

            }
          
        }
    }

    protected void btnUpdateFinance_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        string ApplicationDate = txtApplicationDate.Text.Trim();
        string AppliedBy = ddlAppliedBy.SelectedValue;
        string PurchaseNo = txtPurchaseNo.Text;
        string DocumentSentDate = txtDocumentSentDate.Text.Trim();
        string DocumentSentBy = ddlDocumentSentBy.SelectedValue;
        string DocumentReceivedDate = txtDocumentReceivedDate.Text.Trim();
        string DocumentReceivedBy = ddlDocumentReceivedBy.SelectedValue;
        string DocumentVerified = Convert.ToString(chkDocumentVerified.Checked);
        string SentBy = ddlSentBy.SelectedValue;
        string SentDate = txtSentDate.Text.Trim();
        string PostalTrackingNumber = txtPostalTrackingNumber.Text;
        string Remarks = txtRemarks.Text;
        string ReceivedDate = txtReceivedDate.Text.Trim();
        string ReceivedBy = ddlReceivedBy.SelectedValue;
        string PaymentVerified = Convert.ToString(chkPaymentVerified.Checked);

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);

        if (DocumentReceivedDate != string.Empty && DocumentReceivedBy != string.Empty && DocumentVerified != "False")
        {
        }
        else
        {
            DocumentReceivedBy = "";
        }

        if (SentDate != string.Empty && SentBy != string.Empty && PostalTrackingNumber != string.Empty)
        {
        }
        else
        {
            SentBy = "";
        }
        if (ReceivedDate != string.Empty && ReceivedBy != string.Empty && PaymentVerified != "False")
        {
        }
        else
        {
            ReceivedBy = "";
        }


        bool sucFinance = ClsProjectSale.tblProjects_UpdateFinance(ProjectID, ApplicationDate, AppliedBy, PurchaseNo, DocumentSentDate, DocumentSentBy, DocumentReceivedDate, DocumentReceivedBy, DocumentVerified, SentBy, SentDate, PostalTrackingNumber, Remarks, ReceivedDate, ReceivedBy, PaymentVerified, UpdatedBy);

        if (sucFinance)
        {
            BindProjects();
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetError1();
            //PanError.Visible = true;
        }

    }
    public void activemethod()//put by roshni
    {
        string ProjectID = Request.QueryString["proid"];

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (stPro.ElecDistributorID == "13")
        {

            if ((stPro.FinanceWithID == "4" || stPro.FinanceWithID == string.Empty))
            {
                //Response.Write(stPro.DocumentVerified);
                //Response.End();
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.RegPlanNo != string.Empty && stPro.LotNumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.ActiveDate != string.Empty)
                {
                    //Response.Write(stPro.DocumentVerified );
                    //Response.End();
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {
                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    else
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }
            else
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.RegPlanNo != string.Empty && stPro.LotNumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.ActiveDate != string.Empty)
                {
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {
                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    else
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }

                if ((stPro.ElecDistApprovelRef == string.Empty || stPro.QuoteAccepted == string.Empty || stPro.SignedQuote == string.Empty || stPro.ElecDistributorID == string.Empty || stPro.NMINumber == string.Empty || stPro.RegPlanNo == string.Empty || stPro.LotNumber == string.Empty || stPro.DocumentVerified == false.ToString()) && stPro.DepositReceived != string.Empty)
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
                }
        }
        else if (stPro.ElecDistributorID == "12")
        {
            if ((stPro.FinanceWithID == "4" || stPro.FinanceWithID == string.Empty))
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.ActiveDate != string.Empty)
                {
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {
                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    else
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }
            else
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.ActiveDate != string.Empty)
                {
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {
                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    else
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }

                if ((stPro.ElecDistApprovelRef == string.Empty || stPro.QuoteAccepted == string.Empty || stPro.SignedQuote == string.Empty || stPro.ElecDistributorID == string.Empty || stPro.NMINumber == string.Empty) && stPro.DepositReceived != string.Empty)
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
                }
        }
        else
        {
            if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.ActiveDate != string.Empty)
            {
                if (stPro.InstallState == "VIC")
                {
                    if (!string.IsNullOrEmpty(stPro.VicAppReference))
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
                else
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                }
            }
            else if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.ActiveDate != string.Empty)
            {
                if (stPro.InstallState == "VIC")
                {
                    if (!string.IsNullOrEmpty(stPro.VicAppReference))
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
                else
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                }
            }
        }



    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}