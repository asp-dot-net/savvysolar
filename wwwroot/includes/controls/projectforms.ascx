﻿<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectforms.ascx.cs"
    Inherits="includes_controls_projectforms" %>

<%--<script>
    function doMyAction() {
        $(".myval").select2({
            placeholder: "select",
            allowclear: true
        });
    }
</script>--%>
<%--<asp:UpdatePanel ID="UpdatePanelGrid" runat="server">
    <ContentTemplate>--%>
<script>
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoaded);
    function pageLoaded() {
        $(".myval").select2({
            //placeholder: "select",
            allowclear: true
        });

        //$('.i-checks').iCheck({
        //    checkboxClass: 'icheckbox_square-green',
        //    radioClass: 'iradio_square-green'
        //});
    }
</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

      
        <section class="row m-b-md">
            <div class="col-sm-12 minhegihtarea formpage">
                <div class="contactsarea">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                    </div>
                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                        <div class="row">
                            <div class="col-md-12 formarea">
                                <div class="chkwidth">
                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">For STC Form</span>
                                        </div>
                                        <div class="widget-body">
                                            <div class="row">
                                                <asp:Panel ID="Panel1" runat="server">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <span></span>
                                                            <div class=" checkbox-info checkbox">
                                                                <span class="fistname">
                                                                    <label for="<%=chkAdditionalSystem.ClientID %>" class="control-label">
                                                                        <asp:CheckBox ID="chkAdditionalSystem" runat="server" AutoPostBack="true"
                                                                            OnCheckedChanged="chkAdditionalSystem_CheckedChanged" />
                                                                        <span class="text">Additional System  &nbsp;</span>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span></span>
                                                            <div class=" checkbox-info checkbox">
                                                                <span class="fistname">
                                                                    <label for="<%=chkGridConnected.ClientID %>" class="control-label">
                                                                        <asp:CheckBox ID="chkGridConnected" runat="server" />
                                                                        <span class="text">Grid Connected &nbsp;</span>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span></span>
                                                            <div class=" checkbox-info checkbox">
                                                                <span class="fistname">
                                                                    <label for="<%=chkRebateApproved.ClientID %>" class="control-label">
                                                                        <asp:CheckBox ID="chkRebateApproved" runat="server" />
                                                                        <span class="text">Rebate Approved &nbsp;</span>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <span></span>
                                                            <div class=" checkbox-info checkbox">

                                                                <span class="fistname">
                                                                    <label for="<%=chkReceivedCredits.ClientID %>" class="control-label">
                                                                        <asp:CheckBox ID="chkReceivedCredits" runat="server" />
                                                                        <span class="text">Previous Credits &nbsp;</span>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span></span>
                                                            <div class=" checkbox-info checkbox">
                                                                <span class="fistname">
                                                                    <label for="<%=chkCreditEligible.ClientID %>" class="control-label">
                                                                        <asp:CheckBox ID="chkCreditEligible" runat="server" />
                                                                        <span class="text">Credit Eligible &nbsp;</span>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span></span>
                                                            <div class=" checkbox-info checkbox">
                                                                <span class="fistname">
                                                                    <label for="<%=chkMoreThanOneInstall.ClientID %>" class="control-label">
                                                                        <asp:CheckBox ID="chkMoreThanOneInstall" runat="server" />
                                                                        <span class="text">Multiple Install &nbsp;</span>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <span></span>
                                                            <div class=" checkbox-info checkbox">
                                                                <span class="fistname">
                                                                    <label for="<%=chkRequiredCompliancePaperwork.ClientID %>" class="control-label">
                                                                        <asp:CheckBox ID="chkRequiredCompliancePaperwork" runat="server" />
                                                                        <span class="text">Req Paperwork &nbsp;</span>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span></span>
                                                            <div class=" checkbox-info checkbox">
                                                                <span class="fistname">
                                                                    <label for="<%=chkOutOfPocketDocs.ClientID %>" class="control-label">
                                                                        <asp:CheckBox ID="chkOutOfPocketDocs" runat="server" />
                                                                        <span class="text">Out Of Pocket Docs&nbsp;</span>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span></span>
                                                            <div class=" checkbox-info checkbox">
                                                                <span class="fistname">
                                                                    <label for="<%=chkOwnerGSTRegistered.ClientID %>" class="control-label">
                                                                        <asp:CheckBox ID="chkOwnerGSTRegistered" runat="server" />
                                                                        <span class="text">Owner GST Reg&nbsp;</span>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                                <div class="col-md-3">
                                                    <asp:Panel ID="Panel3" runat="server" Visible="false">
                                                        <div style="text-align: right;">
                                                            <asp:ImageButton ID="btnCreateSTCForm" runat="server" ImageUrl="~/images/new/btn_create_stc_form.png"
                                                                CausesValidation="false" OnClick="btnCreateSTCForm_Click" />
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="radio radio-info  row" style="padding-left: 0px;">
                                                        <%--<asp:RadioButtonList ID="rblInstallBase" runat="server" RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="1"><span style="padding-left:5px;">Building or Structure&nbsp;</span></asp:ListItem>
                                                        <asp:ListItem Value="2"><span style="padding-left:5px;">Ground Mounted or Free Standing</span></asp:ListItem>
                                                    </asp:RadioButtonList>--%>
                                                        <div class="col-md-3 " style="padding-left: 16px;">
                                                            <label for="<%=rblInstallBase1.ClientID %>">
                                                                <asp:RadioButton runat="server" ID="rblInstallBase1" GroupName="insbs" />
                                                                <span class="text">Building or Structure </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6 " style="padding-left: 16px;">
                                                            <label for="<%=rblInstallBase2.ClientID %> ">
                                                                <asp:RadioButton runat="server" ID="rblInstallBase2" GroupName="insbs" />
                                                                <span class="text">Ground Mounted or Free Standing </span>
                                                            </label>
                                                        </div>

                                                        <div class="clear">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                        <asp:Panel ID="Panel2" runat="server">
                            <fieldset>
                                <div class="row">
                                    <div id="Div1" class="col-md-6" runat="server" visible="false">
                                        <div class="form-group">
                                            <span class="name">
                                                <label class="control-label">
                                                    Company ABN
                                                </label>
                                            </span><span>
                                                <asp:TextBox ID="txtOwnerABN" runat="server" MaxLength="10" CssClass="form-control"
                                                    Width="200px"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtOwnerABN"
                                                    ValidationGroup="forms" Display="Dynamic" ErrorMessage="Please enter a number"
                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div id="Div2" class="form-group" runat="server" visible="false">
                                            <span class="name">
                                                <label class="control-label">
                                                    Inverter Certificate
                                                </label>
                                            </span><span>
                                                <asp:TextBox ID="txtInverterCert" runat="server" MaxLength="200" Width="200px" CssClass="form-control"></asp:TextBox>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div id="Div3" class="form-group" runat="server" visible="false">
                                            <span class="name">
                                                <label class="control-label">
                                                    System Capacity
                                                </label>
                                            </span><span>
                                                <asp:TextBox ID="txtSystemCapacity" Enabled="false" runat="server"
                                                    CssClass="form-control" Width="200px"></asp:TextBox>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="Div4" class="form-group" runat="server" visible="false">
                                            <span class="name">
                                                <label class="control-label">
                                                    Electrical Retailer
                                                </label>
                                            </span><span>
                                                <asp:TextBox ID="txtElectricalRetailer" runat="server" Enabled="false" MaxLength="200"
                                                    CssClass="form-control" Width="250px"></asp:TextBox>
                                            </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="row" id="divAddUpdate" runat="server">
                                <div class="col-md-12 textcenterbutton center-text">
                                    <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdateForms" runat="server" OnClick="btnUpdateForms_Click"
                                        ValidationGroup="forms" Text="Save" CausesValidation="false" />
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                </div>
            </div>
        </section>


    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnCreateSTCForm" />
    </Triggers>
</asp:UpdatePanel>
