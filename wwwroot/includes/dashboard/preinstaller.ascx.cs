﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_preinstaller : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindState();
        }
    }
    public void BindState()
    {
        DataTable dtState = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
        rptState.DataSource = dtState;
        rptState.DataBind();

        rptStateHid.DataSource = dtState;
        rptStateHid.DataBind();

        rptStateHid2.DataSource = dtState;
        rptStateHid2.DataBind();
    }
    protected void rptStateHid_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptDone = (Repeater)e.Item.FindControl("rptDone");

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();            
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string EmployeeID = "0";
            if (Roles.IsUserInRole("PreInstaller"))
            {
                EmployeeID = st.EmployeeID;
            }
            else
            {
                EmployeeID = "0";
            }
            DataTable dt = ClsDashboard.tblProjects_PreInst_Done(hndState.Value, EmployeeID);
            rptDone.DataSource = dt;
            rptDone.DataBind();
        }
    }
    protected void rptStateHid2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptRemining = (Repeater)e.Item.FindControl("rptRemining");

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string EmployeeID = "0";
            if (Roles.IsUserInRole("PreInstaller"))
            {
                EmployeeID = st.EmployeeID;
            }
            else
            {
                EmployeeID = "0";
            }
            DataTable dt2 = ClsDashboard.tblProjects_PreInst_Remaining(hndState.Value, EmployeeID);
            rptRemining.DataSource = dt2;
            rptRemining.DataBind();
        }
    }
}