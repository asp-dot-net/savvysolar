<%@ Control Language="C#" AutoEventWireup="true" CodeFile="installer.ascx.cs" Inherits="includes_dashboard_installer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<script type="text/javascript">
    $(document).ready(function () {
        HighlightControlToValidate();
        $('#<%=ibtnUpdate.ClientID %>').click(function () {
            formValidate();
        });
        $('#<%=btnupdate.ClientID %>').click(function () {
            formValidate();
        });
    });
    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#b94a48");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }
    function HighlightControlToValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                $('#' + Page_Validators[i].controltovalidate).blur(function () {
                    var validatorctrl = getValidatorUsingControl($(This).attr("ID"));
                    if (validatorctrl != null && !validatorctrl.isvalid) {
                        $(This).css("border-color", "#b94a48");
                    }
                    else {
                        $(This).css("border-color", "#B5B5B5");
                    }
                });
            }
        }
    }
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
</script>

<div class="row">
    <div class="col-lg-12" id="divTodaysInst" runat="server">
        <div class="panel  panel-default" style="margin-top: 0px!important; padding-top: 0px;">
            <div class="panel-heading">
                Today's installation
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"><i class="icon-resize-full"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <asp:GridView ID="GridViewTodayInst" DataKeyNames="InstallBookingID" runat="server" AllowPaging="true"
                        PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" OnRowCommand="GridViewTodayInst_RowCommand"
                        class="table table-bordered table-hover" OnPageIndexChanging="GridViewTodayInst_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Project #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="hypDetail" runat="server" data-original-title="Detail View" class="tooltips"
                                        CausesValidation="false" CommandName="viewproject" CommandArgument='<%#Eval("ProjectID")+"|"+Eval("CustomerID")%>'><%#Eval("ProjectNumber")%></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="90px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="System Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="400px">
                                <ItemTemplate>
                                    <%#Eval("SystemDetails")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("Address")%>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="HouseType" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px">
                                <ItemTemplate>
                                    <%#Eval("HouseType")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="RoofType" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px">
                                <ItemTemplate>
                                    <%#Eval("RoofType")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BookingAmPm" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <%#Eval("BookingAmPm")%>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnDone" runat="server" CommandName="Done" CausesValidation="false" CommandArgument='<%#Eval("ProjectID") %>'>Done</asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12" id="divPendingInst" runat="server">
        <div class="panel  panel-default" style="margin-top: 0px!important; padding-top: 0px;">
            <div class="panel-heading">
                Installer Pending Checklist
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"><i class="icon-resize-full"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <asp:GridView ID="grdPendingInst" DataKeyNames="InstallBookingID" runat="server" AllowPaging="true"
                        PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" OnRowCommand="grdPendingInst_RowCommand"
                        class="table table-bordered table-hover" OnPageIndexChanging="grdPendingInst_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Project #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="hypDetail" runat="server" data-original-title="Detail View" class="tooltips"
                                        CausesValidation="false" CommandName="viewproject" CommandArgument='<%#Eval("ProjectID")+"|"+Eval("CustomerID")%>'><%#Eval("ProjectNumber")%></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="90px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="System Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="400px">
                                <ItemTemplate>
                                    <%#Eval("SystemDetails")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("Address")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="HouseType" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px">
                                <ItemTemplate>
                                    <%#Eval("HouseType")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="RoofType" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px">
                                <ItemTemplate>
                                    <%#Eval("RoofType")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="BookingAmPm" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <%#Eval("BookingAmPm")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnDone" runat="server" CommandName="Done" CausesValidation="false" CommandArgument='<%#Eval("ProjectID") %>'>Done</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12" id="divMtceInst" runat="server">
        <div class="panel  panel-default" style="margin-top: 0px!important; padding-top: 0px;">
            <div class="panel-heading">
                Today's Mtce installation
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"><i class="icon-resize-full"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <asp:GridView ID="GridViewMtce" DataKeyNames="ProjectMaintenanceID" runat="server" AllowPaging="true"
                        PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" OnRowCommand="GridViewMtce_RowCommand"
                        class="table table-bordered table-hover" OnPageIndexChanging="GridViewMtce_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Project #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="hypDetail" runat="server" data-original-title="Detail View" class="tooltips"
                                        CausesValidation="false" CommandName="viewproject" CommandArgument='<%#Eval("ProjectID")+"|"+Eval("CustomerID")%>'><%#Eval("ProjectNumber")%></asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="90px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="400px">
                                <ItemTemplate>
                                    <%#Eval("Project")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="System Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("SystemDetails")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Reason" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <%#Eval("ProjectMtceReason")%>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <%#Eval("ProjectMtceStatus")%>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="250px">
                                <ItemTemplate>
                                    <%#Eval("Description")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12" id="divpendinglist" runat="server">
        <div class="panel  panel-default" style="margin-top: 0px!important; padding-top: 0px;">
            <div class="panel-heading">
                Installer Pending Insatallation Booking Date List
                <div class="panel-tools">
                    <a class="btn btn-xs btn-link panel-expand" href="#"><i class="icon-resize-full"></i>
                    </a>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <asp:GridView ID="Gridpendinglist" DataKeyNames="ProjectID" runat="server" AllowPaging="true"
                        PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false"
                        class="table table-bordered table-hover" OnPageIndexChanging="Gridpendinglist_PageIndexChanging" OnRowCommand="Gridpendinglist_RowCommand">
                        <Columns>
                            <asp:TemplateField HeaderText="Project #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%#Eval("ProjectNumber")%>
                                </ItemTemplate>
                                <ItemStyle Width="90px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("Customer")%>
                                </ItemTemplate>
                                <ItemStyle Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cust Phone" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("CustPhone")%>
                                </ItemTemplate>
                                <ItemStyle Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cust Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("CustAltPhone")%>
                                </ItemTemplate>
                                <ItemStyle Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="InstallerNotes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("InstallerNotes")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="System Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="350px">
                                <ItemTemplate>
                                    <%#Eval("SystemDetails")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("Address")%>
                                </ItemTemplate>
                                <ItemStyle Width="350px" />
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnDone" runat="server" CommandName="insbookingdate" CausesValidation="false" CommandArgument='<%#Eval("ProjectID") %>'>Add Installation Detail</asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="panel-heading">
            Project Status
        </div>
        <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover gridcss">
            <tr>
                <th>&nbsp;</th>
                <asp:Repeater ID="rptState" runat="server">
                    <ItemTemplate>
                        <th>
                            <%#Eval("State") %>
                        </th>
                    </ItemTemplate>
                </asp:Repeater>
            </tr>
            <tr>
                <td>Paperwork Done</td>
                <asp:Repeater ID="rptStateHid" runat="server" OnItemDataBound="rptStateHid_ItemDataBound">
                    <ItemTemplate>
                        <asp:HiddenField ID="hndState" runat="server" Value='<%#Eval("State") %>' />
                        <asp:Repeater ID="rptDone" runat="server">
                            <ItemTemplate>
                                <td>
                                    <%#Eval("total") %>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                </asp:Repeater>
            </tr>
            <tr>
                <td>Paperwork Remaining</td>
                <asp:Repeater ID="rptStateHid2" runat="server" OnItemDataBound="rptStateHid2_ItemDataBound">
                    <ItemTemplate>
                        <asp:HiddenField ID="hndState" runat="server" Value='<%#Eval("State") %>' />
                        <asp:Repeater ID="rptRemining" runat="server">
                            <ItemTemplate>
                                <td>
                                    <asp:HyperLink ID="hypDetail" runat="server" NavigateUrl='<%# "~/admin/adminfiles/view/remaingpaper.aspx?state="+Eval("State") %>'><%#Eval("total") %></asp:HyperLink>
                                </td>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ItemTemplate>
                </asp:Repeater>
            </tr>
        </table>
    </div>
    <div class="col-md-6">
        <div class="panel-heading">
            Invoice Status
        </div>
        <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover gridcss">
            <tr>
                <td width="120px"><b>Total Price</b></td>
                <td>
                    <asp:Literal ID="lblTotalPrice" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td><b>Received</b></td>
                <td>
                    <asp:Literal ID="lblReceived" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td><b>Owing</b></td>
                <td>
                    <asp:Literal ID="lblOwing" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
</div>
<cc1:ModalPopupExtender ID="ModalPopupExtenderDocs" runat="server" BackgroundCssClass="modalbackground"
    CancelControlID="ibtnCancel" DropShadow="true" PopupControlID="divDocs"
    OkControlID="btnOK" TargetControlID="btnNULLDocs">
</cc1:ModalPopupExtender>
<div runat="server" id="divDocs" class="modalpopup" align="center" style="width: 450px;">
    <div class="popupdiv">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading pad15">
                        Update Data
                        <div class="panel-tools">
                            <div class="closbtn">
                                <asp:LinkButton ID="ibtnCancel" CausesValidation="false" runat="server">
                                    <asp:Image ID="imgClose" runat="server" ImageUrl="~/admin/images/icon_close.png" />
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body formareapop heghtauto" style="background: none!important;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group dateimgarea" id="trHold" runat="server">
                                    <span class="name">
                                        <label class="control-label">
                                            STC Form Sign:
                                        </label>
                                    </span><span class="dateimg">
                                        <asp:CheckBox ID="chkSTCFormSign" runat="server" />
                                        <label for="<%=chkSTCFormSign.ClientID %>">
                                                <span></span>
                                            </label>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group dateimgarea" runat="server">
                                    <span class="name">
                                        <label class="control-label">
                                            Panel Serial Numbers:
                                        </label>
                                    </span><span class="dateimg">
                                        <asp:CheckBox ID="chkSerialNumbers" runat="server" />
                                        <label for="<%=chkSerialNumbers.ClientID %>">
                                                <span></span>
                                            </label>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group dateimgarea" runat="server">
                                    <span class="name">
                                        <label class="control-label">
                                            Quotation Form:
                                        </label>
                                    </span><span class="dateimg">
                                        <asp:CheckBox ID="chkQuotationForm" runat="server" />
                                        <label for="<%=chkQuotationForm.ClientID %>">
                                                <span></span>
                                            </label>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group dateimgarea" runat="server">
                                    <span class="name">
                                        <label class="control-label">
                                            Customer Acknowledgement:
                                        </label>
                                    </span><span class="dateimg">
                                        <asp:CheckBox ID="chkCustomerAck" runat="server" />
                                        <label for="<%=chkCustomerAck.ClientID %>">
                                                <span></span>
                                            </label>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group dateimgarea" runat="server">
                                    <span class="name">
                                        <label class="control-label">
                                            Compliances Certificate:
                                        </label>
                                    </span><span class="dateimg">
                                        <asp:CheckBox ID="chkComplianceCerti" runat="server" />
                                        <label for="<%=chkComplianceCerti.ClientID %>">
                                                <span></span>
                                            </label>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group dateimgarea" runat="server">
                                    <span class="name">
                                        <label class="control-label">
                                            Customer Acceptance:
                                        </label>
                                    </span><span class="dateimg">
                                        <asp:CheckBox ID="chkCustomerAccept" runat="server" />
                                        <label for="<%=chkCustomerAccept.ClientID %>">
                                                <span></span>
                                            </label>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group dateimgarea">
                                    <span class="name">
                                        <label class="control-label">
                                            EWR Number:
                                        </label>
                                    </span><span class="dateimg">
                                        <asp:TextBox ID="txtEWRNumber" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group dateimgarea">
                                    <span class="name">
                                        <label class="control-label">
                                            Patment Method:
                                        </label>
                                    </span><span class="dateimg">
                                        <asp:DropDownList ID="ddlPatmentMethod" runat="server" Width="150px"
                                            AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This value is required."
                                            ControlToValidate="ddlPatmentMethod" Display="Dynamic"></asp:RequiredFieldValidator>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group dateimgarea">
                                    <span class="name"></span><span class="dateimg">
                                        <asp:Button ID="ibtnUpdate" runat="server" Text="Update" OnClick="ibtnUpdate_Click"
                                            class="btn btn-primary" />
                                        <asp:Button ID="btnOK" Style="display: none; visibility: hidden;" runat="server"
                                            CssClass="btn" Text=" OK " />
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:HiddenField ID="hndProjectID" runat="server" />
<asp:Button ID="btnNULLDocs" Style="display: none;" runat="server" />

<cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
    CancelControlID="btncancle" DropShadow="true" PopupControlID="divDocs1"
    OkControlID="btnOK1" TargetControlID="btnNULLDocs1">
</cc1:ModalPopupExtender>

<div runat="server" id="divDocs1" class="modalpopup" align="center" style="width: 450px;">
    <div class="popupdiv">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading pad15">
                        Update Installation Detail
                        <div class="panel-tools">
                            <div class="closbtn">
                                <asp:LinkButton ID="btncancle" CausesValidation="false" runat="server">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/admin/images/icon_close.png" />
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body formareapop heghtauto" style="background: none!important;">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group dateimgarea" id="Div2" runat="server">
                                    <span class="name">
                                        <label class="control-label">
                                            Install
                                        </label>
                                    </span><span class="dateimg">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtInstallBookingDate" runat="server" Width="100px"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorIBDate" runat="server" ErrorMessage="This value is required." Visible="true"
                                                        ControlToValidate="txtInstallBookingDate" Display="Dynamic" ValidationGroup="preinst"></asp:RequiredFieldValidator>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImageInstallBookingDate" runat="server" CausesValidation="false" Enabled="true" ImageUrl="~/admin/images/Calendar_scheduleHS.png" /></td>
                                                <td>
                                                    <cc1:CalendarExtender ID="CalendarExtender19" runat="server" PopupButtonID="ImageInstallBookingDate"
                                                        TargetControlID="txtInstallBookingDate" Format="dd/MM/yyyy">
                                                    </cc1:CalendarExtender>
                                                    <asp:RegularExpressionValidator ValidationGroup="preinst" ControlToValidate="txtInstallBookingDate" ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter valid date"
                                                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnRemoveInst" runat="server" Text="Remove Install" Visible="false" />
                                                </td>
                                            </tr>
                                        </table>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div id="Div3" class="form-group dateimgarea" runat="server">
                                    <span class="name">
                                        <label class="control-label">
                                            AM
                                        </label>
                                    </span><span>
                                        <asp:CheckBox ID="rblAM1" runat="server" />
                                        <label for="<%=rblAM1.ClientID %>">
                                                <span></span>
                                            </label>
                                        <asp:CheckBox ID="rblAM2" runat="server" />
                                        <label for="<%=rblAM2.ClientID %>">
                                                <span></span>
                                            </label>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div id="Div4" class="form-group dateimgarea" runat="server">
                                    <span class="name">
                                        <label class="control-label">
                                            PM
                                        </label>
                                    </span><span>
                                        <asp:CheckBox ID="rblPM1" runat="server" />
                                        <label for="<%=rblPM1.ClientID %>">
                                                <span></span>
                                            </label>
                                        <asp:CheckBox ID="rblPM2" runat="server" />
                                        <label for="<%=rblPM2.ClientID %>">
                                                <span></span>
                                            </label>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div id="Div5" class="form-group dateimgarea" runat="server">
                                    <span class="name">
                                        <label class="control-label">
                                            Days
                                        </label>
                                    </span><span style="width: 65px; margin-top: -5px;">
                                        <asp:TextBox ID="txtInstallDays" runat="server" Text="1" MaxLength="10"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtInstallDays"
                                            Display="Dynamic" ErrorMessage="Please enter a number"
                                            ValidationExpression="^\d+$" ValidationGroup="preinst"></asp:RegularExpressionValidator>
                                        <asp:RangeValidator ID="rangevalEditQty" runat="server" ErrorMessage="Please enter valid day"
                                            ControlToValidate="txtInstallDays" Type="Integer" MinimumValue="1"
                                            MaximumValue="365" Display="Dynamic" ValidationGroup="preinst"></asp:RangeValidator>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group dateimgarea">
                                    <span class="name"></span><span class="dateimg">
                                        <asp:Button ID="btnupdate" runat="server" Text="Update" OnClick="btnupdate_Click"
                                            class="btn btn-primary" ValidationGroup="preinst" />
                                        <asp:Button ID="btnOK1" Style="display: none; visibility: hidden;" runat="server"
                                            CssClass="btn" Text=" OK " />
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:HiddenField ID="hndprojectID1" runat="server" />
<asp:Button ID="btnNULLDocs1" Style="display: none;" runat="server" />
<style type="text/css">
        .selected_row {
            background-color: #A1DCF2!important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridViewTodayInst] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridViewTodayInst] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
	    $("[id*=grdPendingInst] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=grdPendingInst] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
	    $("[id*=GridViewMtce] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridViewMtce] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
$("[id*=Gridpendinglist] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=Gridpendinglist] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>