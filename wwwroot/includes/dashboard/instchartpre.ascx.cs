﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_instchartpre : System.Web.UI.UserControl
{
    protected string jscharts1;
    protected string SiteURL;
    protected string jschartticks;

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        if (!IsPostBack)
        {
            if (Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("Installation Manager"))
            {
                BindPanelChart();
            }
        }
    }
    public void BindPanelChart()
    {
        DataTable dt = ClsDashboard.tblProjects_InstCount();
        foreach (DataRow row in dt.Rows)
        {
            jscharts1 += "," + row["total"].ToString() + "";
            jschartticks += ",'" + row["ProjectStatus"].ToString() + "'";
        }
        if (!string.IsNullOrEmpty(jschartticks))
        {
            jschartticks = jschartticks.Substring(1);
        }
        if (!string.IsNullOrEmpty(jscharts1))
        {
            jscharts1 = jscharts1.Substring(1);
        }
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
}