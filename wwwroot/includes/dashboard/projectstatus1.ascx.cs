﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_projectstatus1 : System.Web.UI.UserControl
{
    protected string jschart1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager") ||Roles.IsUserInRole("PreInstaller")|| Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Lead Manager"))
            {
                BindProjectStatus();
            }
        }
    }
    public void BindProjectStatus()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        string EmployeeID = "0";
        if (Roles.IsUserInRole("Customer") || Roles.IsUserInRole("Installer"))
        {
            EmployeeID = "0";
        }
        else
        {
            EmployeeID = st.EmployeeID;
        }

        DataTable dt = ClstblProjects.tblProjects_CountProjectStatus(EmployeeID);
        if (dt.Rows.Count > 0)
        {
            //lblPlanned.Text = dt.Rows[0]["Planned"].ToString();
            //lblActive.Text = dt.Rows[0]["Active"].ToString();
            //lblDepRec.Text = dt.Rows[0]["DepRec"].ToString();
            //lblOnHold.Text = dt.Rows[0]["OnHold"].ToString();
            //lblCompleted.Text = dt.Rows[0]["Complete"].ToString();
            //lblCancelled.Text = dt.Rows[0]["Cancelled"].ToString();
        }
        DataTable dtStatus = ClstblProjects.tblProjectStatus_CountStatus(EmployeeID);
        string chartvalue = "";
        foreach (DataRow row in dtStatus.Rows)
        {
            //chartvalue += "['" + row["ProjectStatus"].ToString() + "'," + row["Status"].ToString() + "],";
            chartvalue += "{label:'" + row["ProjectStatus"].ToString() + "',data:" + row["Status"].ToString() + "},";
        }
        jschart1 = chartvalue;
    }
}