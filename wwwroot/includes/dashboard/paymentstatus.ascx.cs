﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_paymentstatus : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected string jschart1;
    protected string jschart2;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        if (!IsPostBack)
        {
            if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Lead Manager"))
            {
                BindProjectAmount();
            }
        }
    }
    public void BindProjectAmount()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = "0";
        if (Roles.IsUserInRole("Customer") || Roles.IsUserInRole("Installer"))
        {
            EmployeeID = "0";
        }
        else
        {
            EmployeeID = st.EmployeeID;
        }

        DataTable dt = ClstblProjects.tblProjects_GetTotalQuoteCreated(EmployeeID);
        if (dt.Rows.Count > 0)
        {
            int count = 0;
            foreach (DataRow dr in dt.Rows)
            {
                count += Convert.ToInt32(dr["RowNumber"].ToString());
            }
            lttotalquote.Text = count.ToString();
            string chartvalue = "";
            int num = 0;
            for (int j = 0; j <= 14; j++)
            {
                decimal l = 0;
                foreach (DataRow row in dt.Rows)
                {
                    
                    
                    DateTime baseDate = DateTime.Now;
                    var thisWeekStart = baseDate.AddDays(-(int)baseDate.DayOfWeek);
                    var thisWeekEnd = thisWeekStart.AddDays(7).AddSeconds(-1).ToShortDateString();
                    
                    //next week data
                    if (Convert.ToDateTime(row["QuoteSent"]) > Convert.ToDateTime(thisWeekEnd))
                    {
                        num = ((int)Convert.ToDateTime(row["QuoteSent"]).DayOfWeek == 0) ? 7 : (int)Convert.ToDateTime(row["QuoteSent"]).DayOfWeek +7;
                    }
                    else
                    {
                        num = ((int)Convert.ToDateTime(row["QuoteSent"]).DayOfWeek == 0) ? 7 : (int)Convert.ToDateTime(row["QuoteSent"]).DayOfWeek;
                    }
                    
                    if (j == num)
                    {
                        l = Convert.ToDecimal(row["RowNumber"]);
                    }
                }
                chartvalue += "['" + j + "'," + l + "],";
            }
            jschart1 = chartvalue;
        }


        DataTable dt1 = ClstblProjects.tblProjects_GetTotalFirstDepRec(EmployeeID);
        if (dt1.Rows.Count > 0)
        {
            lttotalfirstdep.Text = dt1.Rows.Count.ToString();
            string chartvalue = "";
            int num = 0;
            for (int j = 0; j <= 7; j++)
            {
                decimal l = 0;
                foreach (DataRow row in dt1.Rows)
                {

                    if (row["InvoicePayDate"] != string.Empty)
                    {
                        DataTable dtpro = ClstblProjects.tblProjects_GetFirstDep(row["ProjectID"].ToString());
                        if (dtpro.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dtpro.Rows)
                            {
                                num = ((int)Convert.ToDateTime(dr["InvoicePayDate"]).DayOfWeek == 0) ? 7 : (int)Convert.ToDateTime(dr["InvoicePayDate"]).DayOfWeek;
                                if (j == num)
                                {
                                    l = Convert.ToDecimal(row["RowNumber"]);
                                }
                            }
                        }
                        //    num = ((int)Convert.ToDateTime(row["InvoicePayDate"]).DayOfWeek == 0) ? 7 : (int)Convert.ToDateTime(row["InvoicePayDate"]).DayOfWeek;
                        //    if (j == num)
                        //    {
                        //        l = Convert.ToDecimal(row["InvoicePayTotal"]);
                        //    }
                    }
                }
                chartvalue += "['" + j + "'," + l + "],";
            }
            jschart2 = chartvalue;
        }
        //if (dt.Rows.Count > 0 && dt1.Rows.Count > 0)
        {
            pangraph.Visible = true;
        }
        //else
        //{
        //    pangraph.Visible = false;
        //}

        //if (dt1.Rows.Count > 0)
        //{
        //    lttotalfirstdep.Text = dt1.Rows.Count.ToString();
        //    string chartvalue = "";
        //    //for (int i = 0; i < Convert.ToInt32(dt1.Rows.Count); i++)
        //    //{
        //    //    if (dt1.Rows[i]["InvoicePayTotal"].ToString() != string.Empty)
        //    //    {
        //    //        chartvalue += "['" + i + "'," + dt1.Rows[i]["InvoicePayTotal"].ToString() + "],";
        //    //    }
        //    //}
        //    for (int i = 0; i <= 7; i++)
        //    {
        //        chartvalue += "['" + i + "'," + i + "],";
        //    }
        //    jschart2 = chartvalue;
        //}
        // DataTable dt = ClstblProjects.tblProjects_CountAmount(EmployeeID);
        //if (dt.Rows.Count > 0)
        //{
        //    litTotal.Text = dt.Rows[0]["Total"].ToString();
        //    litDepRec.Text = SiteConfiguration.ChangeCurrency(dt.Rows[0]["DepRec"].ToString());
        //    litActive.Text = SiteConfiguration.ChangeCurrency(dt.Rows[0]["Active"].ToString());
        //}
    }
    public static DateTime FirstDayOfWeek(DateTime date)
    {
        DayOfWeek fdow = CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek;
        int offset = fdow - date.DayOfWeek;
        DateTime fdowDate = date.AddDays(offset);
        return fdowDate;
    }

}