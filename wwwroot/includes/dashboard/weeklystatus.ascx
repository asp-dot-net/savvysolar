﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="weeklystatus.ascx.cs" Inherits="includes_dashboard_weeklystatus" %>
<style>
    h4{
          font-weight: 600 !important;
    }  
</style>
<asp:UpdatePanel runat="server" ID="UpdateLead">
    <ContentTemplate>
        <div class="col-md-6">
            <div class="databox" style="margin-bottom: unset; height: unset;">
                <div class="databox-row bg-orange no-padding" style="border-radius:10px 10px 0 0;">

                    <div runat="server" id="divemployee2">
                        <div class="databox-cell cell-3 text-align-right padding-20">
                            <div class="pull-right">
                                <div class="btn-group"></div>
                            </div>
                        </div>
                    </div>
                    <div id="divemployee" runat="server">
                        <div class="databox-cell cell-3 text-align-right padding-10">
                            <div class="pull-right">
                                <div class="btn-group">
                                    <asp:DropDownList ID="ddlFollowUpEmployees" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                        <asp:ListItem Value="">Employee</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <%--<span class="databox-text white">13 DECEMBER</span>--%>
                        </div>

                        <asp:Button runat="server" ID="btnFollowupgo" OnClick="btnFollowupgo_Click" CssClass="btn btn-primary" Text="Go" />
                    </div>

                </div>
                <%--<div class="panel-heading"><a href="#">Payment Status</a> </div>--%>

                <div class="col-md-12" style="padding: 0px;">
                    <div class="table-responsive well" style="border-radius: 0 0 10px 10px;box-shadow: 0 0 10px rgba(0,0,0,.3);" >
                        <div class="panel-body paddnone leadacntmain" runat="server">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="databox databox-lg databox-inverted radius-bordered databox-shadowed databox-graded databox-vertical" style="margin-bottom: 0px;box-shadow: 0 0 10px rgba(0,0,0,.3);">
                                        <div class="databox-top bg-palegreen">
                                            <div class="center-text">
                                                <h1 style="font-size: 25px; font-weight: 1000;">
                                                    <asp:Literal ID="litTotal" runat="server"></asp:Literal></h1>
                                            </div>
                                        </div>
                                        <div class="databox-bottom">
                                            <div class="databox-row">
                                                <div class="databox-cell cell-12 center-text">
                                                    <div class="stats-title">
                                                        <h4>Total Amt</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="databox databox-lg databox-inverted radius-bordered databox-shadowed databox-graded databox-vertical" style="margin-bottom: 0px;box-shadow: 0 0 10px rgba(0,0,0,.3);">
                                        <div class="databox-top bg-azure ">
                                            <div class="center-text">
                                                <h1 style="font-size: 25px; font-weight: 1000;">
                                                    <asp:Literal ID="litDepRec" runat="server"></asp:Literal></h1>
                                            </div>
                                        </div>
                                        <div class="databox-bottom">
                                            <div class="databox-row">
                                                <div class="databox-cell cell-12 center-text">
                                                    <div class="stats-title">
                                                        <h4>Dep Rec</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="databox databox-lg databox-inverted radius-bordered databox-shadowed databox-graded databox-vertical" style="margin-bottom: 0px;box-shadow: 0 0 10px rgba(0,0,0,.3);">
                                        <div class="databox-top bg-pink">
                                            <div class="center-text">
                                                <h1 style="font-size: 25px; font-weight: 1000;">
                                                    <asp:Literal ID="litActive" runat="server"></asp:Literal></h1>
                                            </div>
                                        </div>
                                        <div class="databox-bottom">
                                            <div class="databox-row">
                                                <div class="databox-cell cell-12 center-text">
                                                    <div class="stats-title">
                                                        <h4>Active</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panlenew" runat="server" visible="false">
                    <section class="panel">
                        <div class="panel-heading"><a href="#">Weekly Sales Target</a> </div>
                        <div class="panel-body">
                            <div class="imgresponsive">
                                <script type="text/javascript">
                                    jQuery(document).ready(function ($) {
                                        if (!$.isFunction($.fn.dxChart))
                                            $(".dx-warning").removeClass('hidden');
                                    });
                                </script>
                                <script>
                                    var xenonPalette = ['#dc3148', '#6fb7c5', '#ffa140', '#a980c1', '#4fcdfc', '#00b19d', '#ff6264', '#f7aa47'];
                                </script>
                                <div class="panel-body">
                                    <script type="text/javascript">
                                        jQuery(document).ready(function ($) {
                                            if (!$.isFunction($.fn.dxChart))
                                                return;

                                            $('#bar-gauge-1').dxBarGauge({
                                                startValue: 0,
                                                endValue: <%=endValue %>,
                                                //values: [47.27, 65.32, 84.59, 71.86],
                                                values: [<%=jschart1 %>],
                                                label: {
                                                    indent: 30,
                                                    format: 'fixedPoint',
                                                    precision: 1,
                                                },
                                                palette: xenonPalette
                                            });
                                        });
                                        function between(randNumMin, randNumMax) {
                                            var randInt = Math.floor((Math.random() * ((randNumMax + 1) - randNumMin)) + randNumMin);

                                            return randInt;
                                        }
                                    </script>

                                    <div id="bar-gauge-1" style="height: 300px; width: 100%;"></div>
                                    <br />
                                    <div class="listdata">
                                        <table border="0" cellpadding="0" cellspacing="4">
                                            <tr>
                                                <td>
                                                    <img src="../../images/img_target.jpg" width="12" height="12" /></td>
                                                <td>Target</td>
                                                <td>
                                                    <asp:Label ID="lblTarget" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="../../images/img_achive.jpg" width="12" height="12" /></td>
                                                <td>Achieve</td>
                                                <td>
                                                    <asp:Label ID="lblAchieve" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="../../images/img_balance.jpg" width="12" height="12" /></td>
                                                <td>Balance</td>
                                                <td>
                                                    <asp:Label ID="lblBalance" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <img src="../../images/img_lastweek.jpg" width="12" height="12" /></td>
                                                <td>Last Week Achievements</td>
                                                <td>
                                                    <asp:Label ID="lblLastWeekAchievements" runat="server"></asp:Label></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
    </ContentTemplate>
</asp:UpdatePanel>
<script type="text/javascript">
    function doMyAction() {
        if ($.fn.select2 !== 'undefined') {
            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });
            $(".myval1").select2({
                minimumResultsForSearch: -1
            });
        }
    }
</script>
