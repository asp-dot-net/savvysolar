﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


public partial class payway_CheckOut : System.Web.UI.Page
{
    protected IDictionary prices = null;
    protected IDictionary quantities = null;
    protected double totalAmount = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        bool tailored = ConfigurationSettings.AppSettings["tailoredSolution"] == "true";
       // bool tailored = ConfigurationSettings.AppSettings["tailoredSolution"] == "false";

        if (Session["prices"] != null)
        {
            prices = (IDictionary)Session["prices"];
        }
        else
        {
            Response.Redirect("ShoppingCart.aspx", true);
        }

        if (Session["quantities"] != null)
        {
            quantities = (IDictionary)Session["quantities"];
        }
        else
        {
            Response.Redirect("ShoppingCart.aspx", true);
        }

        totalAmount =
            (int)quantities["Product 1"] * (double)prices["Product 1"] +
            (int)quantities["Product 2"] * (double)prices["Product 2"] +
            (int)quantities["Product 3"] * (double)prices["Product 3"];

        Session["totalAmount"] = totalAmount;
       
        if ("Pay Now".Equals(Request.Form["Submit"]))
        {
            // Build the string of Secure Token request payment parameters
            StringBuilder tokenRequest = new StringBuilder();
            tokenRequest.Append("username=");
            tokenRequest.Append(ConfigurationSettings.AppSettings["username"]);
            tokenRequest.Append("&password=");
            tokenRequest.Append(ConfigurationSettings.AppSettings["password"]);
            tokenRequest.Append("&biller_code=");
            tokenRequest.Append(ConfigurationSettings.AppSettings["billerCode"]);
            tokenRequest.Append("&merchant_id=");
            tokenRequest.Append(ConfigurationSettings.AppSettings["merchantId"]);
            tokenRequest.Append("&paypal_email=");
            tokenRequest.Append(ConfigurationSettings.AppSettings["paypalEmail"]);
            tokenRequest.Append("&payment_reference=4");
            //tokenRequest.Append(Convert.ToString(DateTime.Now.Ticks));
            tokenRequest.Append("&payment_reference_change=false");
            tokenRequest.Append("&surcharge_rates=");
            tokenRequest.Append(HttpUtility.UrlEncode("VI/MC=0.0,AX=1.5,DC=1.5"));

            IEnumerator productNameIterator = quantities.Keys.GetEnumerator();
            while (productNameIterator.MoveNext())
            {
                string productName = (string)productNameIterator.Current;
                int quantity = (int)quantities[productName];
                if (quantity != 0)
                {
                    tokenRequest.Append("&");
                    tokenRequest.Append(HttpUtility.UrlEncode(productName));
                    tokenRequest.Append("=");
                    tokenRequest.Append(HttpUtility.UrlEncode(
                        quantity + "," + prices[productName]));
                }
            }
            Debug.WriteLine("Token Request: " + tokenRequest);
            //Response.Write(tokenRequest);
            //Response.End();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(
                ConfigurationSettings.AppSettings["payWayBaseUrl"] + "RequestToken");
            request.KeepAlive = false;
            request.Method = "POST";
            request.Timeout = 60000;
            request.ContentType = "application/x-www-form-urlencoded; charset=" +
                System.Text.Encoding.UTF8.WebName;

            byte[] requestBody = System.Text.Encoding.UTF8.GetBytes(
                tokenRequest.ToString());

            // Use a proxy if required
            if (ConfigurationSettings.AppSettings["proxyHost"].Length > 0 &&
                ConfigurationSettings.AppSettings["proxyPort"].Length > 0)
            {
                string address = ConfigurationSettings.AppSettings["proxyHost"] +
                    ":" + ConfigurationSettings.AppSettings["proxyPort"];
                request.Proxy = new WebProxy(address);
            }

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(requestBody, 0, requestBody.Length);
            requestStream.Close();
            requestStream = null;

            WebResponse response = request.GetResponse();

            // Code to use the WebResponse goes here.
            Stream responseStream = response.GetResponseStream();
            StreamReader responseReader =
                new StreamReader(responseStream, System.Text.Encoding.UTF8);
            string tokenResponse = responseReader.ReadToEnd();
            responseStream.Close();

            Debug.WriteLine("Token Response: " + tokenResponse);

            string[] responseParameters = tokenResponse.Split(new Char[] { '&' });
            string token = null;
            for (int i = 0; i < responseParameters.Length; i++)
            {
                string responseParameter = responseParameters[i];
                string[] paramNameValue = responseParameter.Split(new Char[] { '=' }, 2);
                if ("token".Equals(paramNameValue[0]))
                {
                    token = paramNameValue[1];
                }
                else if ("error".Equals(paramNameValue[0]))
                {
                    throw new Exception(paramNameValue[1]);
                }
            }
            string handOffUrl;
          
            if (tailored)
            {
                Session["token"] = token;
                handOffUrl = "./EnterCCDetails.aspx";
                
            }
            else
            {
                handOffUrl = ConfigurationSettings.AppSettings["payWayBaseUrl"] +
                    "MakePayment";
            }
            handOffUrl += "?biller_code=" +
                HttpUtility.UrlEncode(ConfigurationSettings.AppSettings["billerCode"]) +
                "&token=" + HttpUtility.UrlEncode(token);
            Debug.WriteLine("handOffUrl: " + handOffUrl);
          
            Response.Redirect(handOffUrl, true);
        }

    }
}