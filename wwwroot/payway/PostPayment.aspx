<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PostPayment.aspx.cs" Inherits="payway_PostPayment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Payment Status</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style.css" rel="stylesheet" type="text/css">
  </head>
 <body>
<%

    if ( "approved".Equals( (string)parameters["payment_status"] ) )
    {
        if ( true )
        {
%>
    <p>Your payment has been approved.  You will be notified when your order has been shipped.</p>
    <table cellspacing="0" cellpadding="4">
      <tr><td colspan="2"><b>Payment Receipt</b></td></tr>
      <tr><td>Card Type:</td><td><%=HttpUtility.HtmlEncode( (string)parameters["card_type"] )%></td></tr>
      <tr><td>Amount:</td><td>AUD <%=HttpUtility.HtmlEncode( String.Format("{0:c}", Double.Parse((string)parameters["payment_amount"]) ) )%></td></tr>
      <tr><td>Order Number:</td><td><%=HttpUtility.HtmlEncode( (string)parameters["payment_reference"] )%></td></tr>
      <tr><td>Bank Reference:</td><td><%=HttpUtility.HtmlEncode( (string)parameters["bank_reference"] )%></td></tr>
      <tr><td>Your IP Address:</td><td><%=HttpUtility.HtmlEncode( (string)parameters["remote_ip"] )%></td></tr>
      <tr><td>Transaction Status:</td><td><%=HttpUtility.HtmlEncode( (string)parameters["response_code"] ) + " - " + HttpUtility.HtmlEncode( (string)parameters["response_text"] )%></td></tr>
    </table>
<%
            // You should record the bank_reference parameter against the order for reconciliation
        }
        else
        {
%>
        <p>Your payment has been approved but the amount you paid was different from the amount expected.
          You paid <%=HttpUtility.HtmlEncode( String.Format("{0:c}", Double.Parse((string)parameters["payment_amount"]) ) )%> 
          but we expected you to pay <%=Session["totalAmount"] == null ? "$0" : HttpUtility.HtmlEncode( String.Format("{0:c}", (double)Session["totalAmount"] ) )%>.
          Please contact us for information on how to proceed.</p>
<%
        }
    }
    else
    {
%>
        <p>Your payment was not approved.  You may go back to your shopping cart and start again.</p>
        <p><a href="ShoppingCart.aspx">Back to shopping cart</a></p>
<%
    }
%>
  </body>
</html>
