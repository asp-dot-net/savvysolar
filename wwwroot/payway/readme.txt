Copyright (c) 2008 Qvalent Pty Ltd.

Qvalent PayWay Net Example for .NET
===================================

1. Requirements

.NET Framework 1.1 or higher is required.


2. Using the Example Code

* Use the files in this directory in your web project.

* Update the Web.config file with your configuration:

Replace _YOUR_ENCRYPTION_KEY_ with your encryption key from the PayWay web site.
Replace _BILLER_CODE_ with your biller code from the PayWay web site.
Replace _USERNAME_ with your PayWay Net username from the PayWay web site.
Replace _PASSWORD_ with your PayWay Net password from the PayWay web site.
If you require a proxy to access the PayWay server, set the following system
  properties accordingly:
    http.proxyHost, http.proxyPort, http.nonProxyHosts
Replace _LOG_DIR_ with the directory to log debug messages to.
If you have configured a PayPal Email address in the PayWay's Manage PayPal Accounts
screen (under the Administration menu item), you can use this address to receive
PayPal payments.

* Restrict the file permissions on Web.config so that only the ASP.NET user that 
runs your application server under can access this file.

* Set a reference to System.Web in your web project.

* Build your web project.  

* Using your web browser, browse to Index.aspx, then press the Check Out
button.  Press the Pay Now button on the next page.  You should then be sent to
the PayWay payment page.


======roshni=================================

-->first fall change all creadintial in web.config
-->then chnage customerusername and customer password in processcard .aspx backend
-->then put dot net certificate
-->then chnage path in global.aspx
