﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Qvalent.PayWay;

public partial class payway_EnterCCDetails : System.Web.UI.UserControl
{
    protected string cardamount;
    protected string projectno;
    protected void Page_Load(object sender, EventArgs e)
    {
        cardamount = Request.QueryString["Data"];
        projectno = Request.QueryString["prono"];
    }
    protected void btnMakePayment_Click(object sender, EventArgs e)
    {
        Response.Redirect("PostPayment.aspx", true);
    }
}