﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class payway_SavePayment : System.Web.UI.Page
{
    private void Page_Load(object sender, System.EventArgs e)
    {
        Debug.WriteLine("Server-to-server post Parameters:");
        IEnumerator parameterNames = Request.Form.GetEnumerator();
        while (parameterNames.MoveNext())
        {
            string parameterName = (string)parameterNames.Current;
            // Never log a password
            if (!"password".Equals(parameterName))
            {
                Debug.WriteLine("  " + parameterName + " = " +
                    Request.Form[parameterName]);
            }
        }

        if (!"POST".Equals(Request.HttpMethod.ToUpper()))
        {
            Response.StatusCode = 405;
            Response.Write("GET is not supported");
            return;
        }

        String username = ConfigurationSettings.AppSettings["username"];
        String password = ConfigurationSettings.AppSettings["password"];
        if (!username.Equals(Request.Form["username"]) ||
            !password.Equals(Request.Form["password"]))
        {
            Response.StatusCode = 403;
            Response.Write("Incorrect Username and password");
            return;
        }
        else
        {
            // Save payment

            // If error results, return HTTP 500

            // Otherwise, just return success
            Response.Write("Success");
            return;
        }
    }
}