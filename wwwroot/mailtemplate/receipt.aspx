﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="receipt.aspx.cs" Inherits="mailtemplate_receipt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <style>
        body {margin:0px; padding:0px;}
		form{ padding:0px!important; margin:0px!important;}
        @font-face {
            font-family: 'amaticbold';
            src: url('../fonts/amatic-bold.eot');
            src: url('../fonts/amatic-bold.eot?#iefix') format('embedded-opentype'), url('../fonts/amatic-bold.woff2') format('woff2'), url('../fonts/amatic-bold.woff') format('woff'), url('../fonts/amatic-bold.ttf') format('truetype'), url('../fonts/amatic-bold.svg#amaticbold') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'bobregular';
            src: url('../fonts/bob.eot');
            src: url('../fonts/bob.eot?#iefix') format('embedded-opentype'), url('../fonts/bob.woff2') format('woff2'), url('../fonts/bob.woff') format('woff'), url('../fonts/bob.ttf') format('truetype'), url('../fonts/bob.svg#bobregular') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'mission_scriptregular';
            src: url('../fonts/mission-script.eot');
            src: url('../fonts/mission-script.eot?#iefix') format('embedded-opentype'), url('../fonts/mission-script.woff2') format('woff2'), url('../fonts/mission-script.woff') format('woff'), url('../fonts/mission-script.ttf') format('truetype'), url('../fonts/mission-script.svg#mission_scriptregular') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'museo300';
            src: url('../fonts/museo300-regular.eot');
            src: url('../fonts/museo300-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/museo300-regular.woff2') format('woff2'), url('../fonts/museo300-regular.woff') format('woff'), url('../fonts/museo300-regular.ttf') format('truetype'), url('../fonts/museo300-regular.svg#museo300') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'museo500';
            src: url('../fonts/museo500-regular.eot');
            src: url('../fonts/museo500-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/museo500-regular.woff2') format('woff2'), url('../fonts/museo500-regular.woff') format('woff'), url('../fonts/museo500-regular.ttf') format('truetype'), url('../fonts/museo500-regular.svg#museo500') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face {
            font-family: 'museo700';
            src: url('../fonts/museo700-regular.eot');
            src: url('../fonts/museo700-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/museo700-regular.woff2') format('woff2'), url('../fonts/museo700-regular.woff') format('woff'), url('../fonts/museo700-regular.ttf') format('truetype'), url('../fonts/museo700-regular.svg#museo700') format('svg');
            font-weight: normal;
            font-style: normal;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="width: 960px; margin: 0px auto;">
            <div>
                  <div style="position: relative;">
                    <div class="toparea" style="font-family: Arial, Helvetica, sans-serif;">
                        <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 5px 0px 0px 10px; width: 30%;">
                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo.png") %>"
                                width="124" height="100" />
                        </div>
                        <div style="float: right; text-align: right; padding: 5px 10px 0px 0px; width: 60%;">
                            <p style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 21px; color: #00b0f0; font-weight: 400;">
                                <span style="font-size: 20px; font-weight: 600; line-height: 24px;">THINK GREEN SOLAR<br />
                                    1300 484 784</span><br>
                                <span style="font-size: 18px;"><a href="mailto:info@thinkgreensolar.com.au">info@thinkgreensolar.com.au</a><br />
                                    <a href="http://www.thinkgreensolar.com.au/" target="_blank">www.thinkgreensolar.com.au</a></span>
                            </p>
                        </div>
                        <div style="float: none; clear: both"></div>
                    </div>
                </div>
                <div align="center" style="padding: 10px 0px 20px 0px; font-family: 'Open Sans', sans-serif; font-size: 24px; line-height: 26px; font-weight: 700px; color: #00b0f0;">
                    <strong>Payment Receipt</strong><br />
                    <span style="font-size: 15px; font-weight: normal;">
                        <asp:Label ID="lblReceiptDate" runat="server"></asp:Label></span>
                </div>
                <div style="position: relative; padding-top:10px;">
                    <div style="position: relative; z-index: 1;">
                        <div>
                            <h1 style="font-family: 'Open Sans', sans-serif; font-weight: 600; font-size: 34px; line-height: 34px; color: #00b0f0; margin: 0px 0px 25px 0px; padding: 0px 0px 10px 0px; border-bottom: #000000 solid 3px;">Contact Details</h1>
                            <p style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 25px; color: #000; font-weight: 400; margin: 0px; padding: 0px 0px 35px 0px;">
                                Name:
                                <asp:Label ID="lblContact" runat="server"></asp:Label><br>
                                Mobile:
                                <asp:Label ID="lblContMobile" runat="server"></asp:Label><br>
                                Phone:
                                <asp:Label ID="lblCustPhone" runat="server"></asp:Label><br>
                                Email :
                                <asp:Label ID="lblContEmail" runat="server"></asp:Label>
                            </p>
                        </div>
                        <div>
                            <h1 style="font-family: 'Open Sans', sans-serif; font-weight: 600; font-size: 34px; line-height: 34px; color: #00b0f0; margin: 0px 0px 25px 0px; padding: 0px 0px 10px 0px; border-bottom: #000000 solid 3px;">Site Details</h1>
                            <p style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 25px; color: #000; font-weight: 400; margin: 0px; padding: 0px 0px 35px 0px;">
                                <asp:Label ID="lblInstallAddress" runat="server"></asp:Label>,<br>
                                <asp:Label ID="lblInstallAddress2" runat="server"></asp:Label>
                            </p>
                            <div>
                                <h1 style="font-family: 'Open Sans', sans-serif; font-weight: 600; font-size: 34px; line-height: 34px; color: #00b0f0; margin: 0px 0px 25px 0px; padding: 0px 0px 10px 0px; border-bottom: #000000 solid 3px;">Invoice Details</h1>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="140" valign="top" style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 25px; color: #000; font-weight: 400;">Invoice Number</td>
                                        <td width="15" valign="top" style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 25px; color: #000; font-weight: 400;">:</td>
                                        <td valign="top" style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 25px; color: #000; font-weight: 400;">
                                            <asp:Label ID="lblInvoiceNumber" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="140" valign="top" style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 25px; color: #000; font-weight: 400;">Invoice Date</td>
                                        <td width="15" valign="top" style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 25px; color: #000; font-weight: 400;">:</td>
                                        <td valign="top" style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 25px; color: #000; font-weight: 400;">
                                            <asp:Label ID="lblInvoiceSent" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="140" valign="top" style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 25px; color: #000; font-weight: 400;">Invoice Total</td>
                                        <td width="15" valign="top" style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 25px; color: #000; font-weight: 400;">:</td>
                                        <td valign="top" style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 25px; color: #000; font-weight: 400;">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" align="absmiddle" />
                                            <asp:Label ID="lblTotalQuotePrice" runat="server"></asp:Label></td>
                                    </tr>
                                </table>
                                <p style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 25px; color: #000; font-weight: 400; padding: 25px 0px 20px 0px; margin: 0px;">For the supply and installation of the Solar Power System</p>
                                <table width="100%" border="1" bordercolor="#000" cellspacing="0" cellpadding="0" style="border: #000 solid 1px; font-family: 'Open Sans', sans-serif; font-size: 13px; color: #000; font-weight: 400; border-collapse: collapse;">
                                    <tr>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #00b0f0; font-weight: 600; padding: 3px 10px;">Payment Date</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #00b0f0; font-weight: 600; padding: 3px 10px;">Payment Amount $</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #00b0f0; font-weight: 600; padding: 3px 10px;">CC Surcharge $</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #00b0f0; font-weight: 600; padding: 3px 10px;">Payment Method</td>
                                    </tr>
                                    <asp:Repeater ID="rptInvoice" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;"><%#Eval("InvoicePayDate","{0:dd MMM yyyy}")%></td>
                                                <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;"><%#Eval("InvoicePayTotal","{0:0.00}")%></td>
                                                <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">
                                                    <asp:Label ID="lblCCSurcharge" runat="server" Text='<%#Eval("CCSurcharge").ToString()==""?"-": Eval("CCSurcharge","{0:0.00}")%>'></asp:Label></td>
                                                <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;"><%#Eval("InvoicePayMethod")%></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    		<tr>
                                            	<td colspan="4" align="right" style="background:#00b0f0; color:#fff; font-family: 'Open Sans', sans-serif; font-size: 21px; padding:5px 10px;">
                                                	Balance Due: <span style="font-weight: 600; font-size: 19px;">
                                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" algn="absmiddle" />
                                                    <asp:Label ID="lblBalanceOwing" runat="server"></asp:Label></span> <span style="font-weight: 600; font-size: 14px;">[ Including – GST]</span>
                                                </td>
                                            </tr>
                                    <%--<tr>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                        <td align="center" style="font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #000; font-weight: 400; padding: 3px 10px;">&nbsp;</td>
                                    </tr>--%>
                                </table>
                                <!--<div align="right" style="font-family: 'Open Sans', sans-serif; font-size: 21px; color: #fff; font-weight: 700; background: #00b0f0; padding: 10px 5px 12px 5px;">
                                </div>-->
                            </div>
                            <div style="padding-bottom: 30px; border-top: #000 solid 3px; margin-top: 150px; padding-top:5px;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="25%" align="center" valign="middle">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/btm_logo1.jpg") %>" width="130" height="113" />
                                            <%--<img src="../images/btm_logo1.jpg" width="130" height="113" alt="" />--%></td>
<%--                                        <td width="25%" align="center" valign="middle">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/btm_logo2.jpg") %>" width="117" height="113" />
                                            </td>--%>
                                            <%-- <img src="../images/btm_logo2.jpg" width="117" height="113" alt="" />--%>
                                        <td width="25%" align="center" valign="middle">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/btm_logo3.jpg") %>" width="168" height="113" />
                                            <%--<img src="../images/btm_logo3.jpg" width="168" height="113" alt="" />--%></td>
                                        <td width="25%" align="center" valign="middle">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/btm_logo4.jpg") %>" width="84" height="113" />
                                            <%--<img src="../images/btm_logo4.jpg" width="84" height="113" alt="" />--%></td>
                                    </tr>
                                </table>
                            </div>
                            <%--<div style="padding-bottom: 30px; border-top: #000 solid 3px; margin-top: 150px;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="25%" align="center" valign="middle">
                                            <img src="../images/btm_logo1.jpg" width="130" height="113" alt="" /></td>
                                        <td width="25%" align="center" valign="middle">
                                            <img src="../images/btm_logo2.jpg" width="117" height="113" alt="" /></td>
                                        <td width="25%" align="center" valign="middle">
                                            <img src="../images/btm_logo3.jpg" width="168" height="113" alt="" /></td>
                                        <td width="25%" align="center" valign="middle">
                                            <img src="../images/btm_logo4.jpg" width="84" height="113" alt="" /></td>
                                    </tr>
                                </table>
                            </div>--%>
                        </div>
                    </div>
                    <div style="position: absolute; z-index: 2; right: 0px; top: 20px; width: 250px; height: 250px; background: url(<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/price.png") %>) no-repeat left top #fff; background-size: cover;">
                        <div style="font-family: 'bobregular'; color: #000; font-size: 13px; font-weight: normal; text-align: center; margin-top: 60px; margin-bottom: 7px;">
                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_balance.png") %>" width="143" height="26" align="absmiddle" />
                            <%--<img src="../images/img_balance.png" width="143" height="26">--%>
                        </div>
                        <div style="font-family: 'Open Sans', sans-serif; font-size: 15px; line-height: 21px; color: #000; font-weight: 600; text-align: center;">
                            <span style="font-size: 25px; display: inline-block; padding-right: 4px;">$</span>
                            <asp:Label ID="lblBalanceOwing1" runat="server" style="font-size: 25px;"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <%-- <div style="width: 960px; margin: 0px auto; border: 1px solid #ccc;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: Arial, Helvetica, sans-serif;">
                <%-- <tr>
                    <td width="26%" style="border-right: 1px solid #ccc; padding: 15px;">
                        <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/eurosolarlogo.png") %>" width="164" height="142" />
                    </td>
                    <td width="74%" align="right" valign="top" style="padding: 15px;">
                        <h1 style="font-family: Arial, Helvetica, sans-serif; margin-bottom: 7px;">Euro Solar</h1>
                        ABN 95 130 845 199 | Phone : 1300 959 013<br />
                        Email : <a href="mailto:sales@eurosolar.com.au">sales@eurosolar.com.au</a>
                        <br />
                        Visit us on : <a href="http://www.eurosolar.com.au" target="_blank">www.eurosolar.com.au</a>
                    </td>
                </tr>--%>
        <%--<tr>
                    <td colspan="2" style="border-top: 1px solid #ccc; padding: 15px; text-align: center;">
                        <asp:Label ID="lblReceiptDate" runat="server"></asp:Label>
                        <br />
                        <h2 style="margin: 0px; padding: 0px;">Payment Receipt</h2>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width: 960px; margin: 50px auto; padding: 0px; border: 1px solid #ccc;">
            <table border="0" cellpadding="0" cellspacing="10" width="100%" style="font-family: Arial, Helvetica, sans-serif; font-size: 20px;">
                <tr>
                    <th align="left">Customer Details </th>
                    <th align="left">Installation Details </th>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblContact" runat="server"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblInstallAddress" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblPostalAddress" runat="server"></asp:Label></td>
                    <td>
                        <asp:Label ID="lblInstallAddress2" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblPostalAddress2" runat="server"></asp:Label></td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
        <div style="width: 960px; margin: 20px auto; padding: 0px; border: 1px solid #ccc;">
            <table border="0" cellpadding="10" cellspacing="0" width="100%" style="font-family: Arial, Helvetica, sans-serif; font-size: 20px;">
                <tr>
                    <td width="30%"><strong>Receipt For: </strong></td>
                    <td width="70%">Supply and Installation of Solar Panel System </td>
                </tr>
                <tr>
                    <td width="30%"><strong>Invoice Number:</strong></td>
                    <td width="70%">
                        <asp:Label ID="lblInvoiceNumber" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td width="30%"><strong>Invoice Date:</strong></td>
                    <td width="70%">
                        <asp:Label ID="lblInvoiceSent" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td width="30%"><strong>Invoice Total:</strong></td>
                    <td width="70%">
                        <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" align="absmiddle" />
                        <asp:Label ID="lblTotalQuotePrice" runat="server"></asp:Label></td>
                </tr>
            </table>
            <table border="0" cellpadding="10" cellspacing="0" width="100%" style="font-family: Arial, Helvetica, sans-serif;">
                <tr>
                    <td width="25%" style="border-top: 1px solid #ccc; font-size: 20px; text-align: center;"><strong>Payment Date</strong></td>
                    <td width="25%" style="border-top: 1px solid #ccc; font-size: 20px; text-align: center;"><strong>Payment Amount(<img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" algn="absmiddle" />)</strong></td>
                    <td width="25%" style="border-top: 1px solid #ccc; font-size: 20px; text-align: center;"><strong>CC Surcharge(<img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" algn="absmiddle" />)</strong></td>
                    <td width="25%" style="border-top: 1px solid #ccc; font-size: 20px; text-align: center;"><strong>Payment Method</strong></td>
                </tr>
                <asp:Repeater ID="rptInvoice" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td width="25%" style="border-top: 1px solid #ccc; font-size: 20px; text-align: center;"><%#Eval("InvoicePayDate","{0:dd MMM yyyy}")%></td>
                            <td width="25%" style="border-top: 1px solid #ccc; font-size: 20px; text-align: center;"><%#Eval("InvoicePayTotal","{0:0.00}")%></td>
                            <td width="25%" style="border-top: 1px solid #ccc; font-size: 20px; text-align: center;">
                                <asp:Label ID="lblCCSurcharge" runat="server" Text='<%#Eval("CCSurcharge").ToString()==""?"-": Eval("CCSurcharge","{0:0.00}")%>'></asp:Label></td>
                            <td width="25%" style="border-top: 1px solid #ccc; font-size: 20px; text-align: center;"><%#Eval("InvoicePayMethod")%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
            <table border="0" cellpadding="10" cellspacing="0" width="100%" style="font-family: Arial, Helvetica, sans-serif;">
                <%--<tr>
                	<td colspan="2" width="100%" style="border-top: 1px solid #ccc; font-size: 20px; ">
                        PaymentString
                    </td>
                </tr>--%>
        <%--<tr>
                    <td width="30%" style="border-top: 1px solid #ccc; font-size: 20px; font-style: italic;"><strong>Balance Remaining:</strong></td>
                    <td style="border-top: 1px solid #ccc; font-size: 20px; font-style: italic;"><strong>
                        <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" algn="absmiddle" />
                        <asp:Label ID="lblBalanceOwing" runat="server"></asp:Label>
                    </strong><span style="font-size: 16px">(Including-GST)</span></td>
                </tr>
            </table>
        </div>--%>
        <%--<div style="width: 960px; margin: 20px auto; padding: 0px; font-family: Arial, Helvetica, sans-serif; height: 300px;"><span style="font-size: 16px;"></span></div>--%>
        <%--<div style="width: 960px; margin: 20px auto; padding: 0px;">
            <table border="0" cellpadding="1" cellspacing="0" width="100%" style="font-family: Arial, Helvetica, sans-serif;">
                <tr>
                    <td width="25%" style="font-size: 13px; text-align: center;">
                        <strong>New South Wales</strong>
                    </td>
                    <td width="25%" style="font-size: 13px; text-align: center;">
                        <strong>South Australia</strong>
                    </td>
                    <td width="25%" style="font-size: 13px; text-align: center;">
                        <strong>Queensland</strong>
                    </td>
                    <td width="25%" style="font-size: 13px; text-align: center;">
                        <strong>Victoria</strong>
                    </td>
                </tr>
                <tr>
                    <td width="25%" style="font-size: 13px; text-align: center;">8 Cowper St,<br />
                        Granville, NSW, 2142
                    </td>
                    <td width="25%" style="font-size: 13px; text-align: center;">50 Grand Junction Rd,<br />
                        Rosewater, SA, 5013
                    </td>
                    <td width="25%" style="font-size: 13px; text-align: center;">P O BOX 570,<br />
                        Archerfield, QLD, 4108
                    </td>
                    <td width="25%" style="font-size: 13px; text-align: center;">126/45 Gilby Road,<br />
                        Mount Waverley, VIC, 3141
                    </td>
                </tr>
            </table>
        </div>--%>
    </form>
</body>
</html>
