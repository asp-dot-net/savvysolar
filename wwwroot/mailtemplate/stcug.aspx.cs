﻿using System;
using System.IO;

public partial class mailtemplate_stcug : System.Web.UI.Page
{
    public static string MakeImageSrcData(string filename)
    {
        FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
        byte[] filebytes = new byte[fs.Length];
        fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
        return "data:image/png;base64," +
          Convert.ToBase64String(filebytes, Base64FormattingOptions.None);
    }
    protected string SiteURL;
    protected string buildingimg;
    protected string groundimg;

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        string ProjectID = "1017";//Request.QueryString["id"]; //   "-2146260946"; 
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stPro.EmployeeID);
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);

        litTitle.Text = stCont.ContMr;
        litFirst.Text = stCont.ContFirst;
        litLast.Text = stCont.ContLast;
        litStreet.Text = stCust.StreetAddress;
        litSuburb.Text = stCust.StreetCity;
        litPostcode.Text = stCust.StreetPostCode;
        lblManualQuoteNo.Text = stPro.ManualQuoteNumber;
        lblProjectNumber.Text = stPro.ProjectNumber;
        ProjectNumber3.Text = stPro.ProjectNumber;
        litPanelDetails.Text = stPro.PanelDetails;
        lblcustno2.Text = stPro.ProjectNumber;
        lblContFirst.Text = stCont.ContFirst;
        lblContLast.Text = stCont.ContLast;
        lblCustomer.Text = "";// stCust.Customer;
        if (stPro.ProjectTypeID == "2")
        {
            lblunit.Text = "Yes";
        }
        else
        {
            lblunit.Text = "No";
        }
        lblPostalAddressCity.Text = stCust.PostalAddress + ", " + stCust.PostalCity;
        lblPostalState.Text = stCust.PostalState;
        lblPostalPostCode.Text = stCust.PostalPostCode;

        if (stCust.CustPhone != string.Empty && stCont.ContMobile == string.Empty)
        {
            lblCustPhone.Text = stCust.CustPhone;
        }
        else
        {
            lblCustPhone.Text = stCont.ContMobile;
        }

        lblCustFax.Text = stCust.CustFax;
        lblContEmail.Text = stCont.ContEmail;
        lblInstallAddressCity.Text = stPro.InstallAddress + ", " + stPro.InstallCity;
        lblInstallState.Text = stPro.InstallState;
        lblInstallPostCode.Text = stPro.InstallPostCode;
        if (stPro.InstallBase == "1")
        {
            buildingimg = "img_checkbox.jpg";
            groundimg = "img_checkbox2.jpg";
        }
        else
        {
            buildingimg = "img_checkbox2.jpg";
            groundimg = "img_checkbox.jpg";
        }
        if (stPro.AdditionalSystem == "True")
        {
            lblAdditional.Text = "Yes";
        }
        else
        {
            lblAdditional.Text = "No";
        }
        lblPanelBrand.Text = stPro.PanelBrand;

        if (stPro.RebateApproved == "True")
        {
            lblREB.Text = "Yes";
        }
        else
        {
            lblREB.Text = "No";
        }
        if (stPro.OutOfPocketDocs == "True")
        {
            //lblEXP.Text = "Yes";
        }
        else
        {
            //lblEXP.Text = "No";
        }
        if (stPro.ReceivedCredits == "True")
        {
            lblCRED.Text = "Yes";
        }
        else
        {
            lblCRED.Text = "No";
        }
        if (stPro.CreditEligible == "True")
        {
            lblELIG.Text = "Yes";
        }
        else
        {
            lblELIG.Text = "No";
        }
        if (stPro.MoreThanOneInstall == "True")
        {
            lblMOR.Text = "Yes";
        }
        else
        {
            lblMOR.Text = "No";
        }

        lblPanelModel.Text = stPro.PanelModel;
        lblInverterBrand.Text = stPro.InverterBrand;
        lblInverterModel.Text = stPro.InverterModel;
        lblInverterSeries.Text = stPro.InverterSeries;
        try
        {
            lblInstallBookingDate.Text = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
        }
        catch { }
        lblSystemCap.Text = stPro.SystemCapKW;
        lblNumberPanels.Text = stPro.NumberPanels;

        if (stPro.RequiredCompliancePaperwork == "True")
        {
            lblPaperwork.Text = "Yes";
        }
        else
        {
            lblPaperwork.Text = "No";
        }
        if (stPro.GridConnected == "True")
        {
            //lblGrid.Text = "Yes";
        }
        else
        {
            //lblGrid.Text = "No";
        }

        if (stPro.Installer != "")
        {
            SttblContacts stInst = ClstblContacts.tblContacts_SelectByContactID(stPro.Installer);
            lblInstallerName1.Text = stInst.ContFirst + " " + stInst.ContLast;
            lblContMobile1.Text = stInst.ContMobile;
            SttblCustomers stCustInst = ClstblCustomers.tblCustomers_SelectByCustomerID(stInst.CustomerID);
            lblInstallerAddress1.Text = stCustInst.PostalAddress + ", " + stCustInst.PostalCity + ", " + stCustInst.PostalPostCode;
            lblAccreditation1.Text = stInst.Accreditation;

            lblInstallerName4.Text = stInst.ContFirst + " " + stInst.ContLast;
            lblAccreditation4.Text = stInst.Accreditation;
            lblInstallerName7.Text = stInst.ContFirst + " " + stInst.ContLast;
        }

        if (stPro.Designer != "")
        {
            SttblContacts stDes = ClstblContacts.tblContacts_SelectByContactID(stPro.Designer);
            lblInstallerName2.Text = stDes.ContFirst + " " + stDes.ContLast;
            lblContMobile2.Text = stDes.ContMobile;
            SttblCustomers stDesInst = ClstblCustomers.tblCustomers_SelectByCustomerID(stDes.CustomerID);
            lblInstallerAddress2.Text = stDesInst.PostalAddress + ", " + stDesInst.PostalCity + ", " + stDesInst.PostalPostCode;
            lblAccreditation2.Text = stDes.Accreditation;

            lblInstallerName5.Text = stDes.ContFirst + " " + stDes.ContLast;
            lblAccreditation5.Text = stDes.Accreditation;
        }

        if (stPro.Electrician != "")
        {
            SttblContacts stElec = ClstblContacts.tblContacts_SelectByContactID(stPro.Electrician);
            lblInstallerName3.Text = stPro.InstallerName;
            litContractorName.Text = stPro.InstallerName;
            litcontractorlicence.Text = stPro.InstallerName;
            litcontractorphone.Text = stElec.ContMobile;
            lblContMobile3.Text = stElec.ContMobile;
            SttblCustomers stElecInst = ClstblCustomers.tblCustomers_SelectByCustomerID(stElec.CustomerID);
            lblInstallerAddress3.Text = stElecInst.PostalAddress + ", " + stElecInst.PostalCity + ", " + stElecInst.PostalPostCode;
            lblAccreditation3.Text = stElec.ElecLicence;
            litElectricallicence.Text = stElec.ElecLicence;
        }

        lblInstallerName6.Text = stPro.InstallerName;
        lblInstallAddressCity2.Text = stCust.PostalAddress + " " + stCust.PostalCity;

        lblSTCNumber.Text = stPro.STCNumber;
        lblFinanceOption.Text = stPro.FinanceWith;

        try
        {
            decimal TotalCost = Convert.ToDecimal(stPro.RECRebate) + Convert.ToDecimal(stPro.TotalQuotePrice);
            //lblTotalCost.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(TotalCost));
        }
        catch { }

        try
        {
            //lblRECR.Text = SiteConfiguration.ChangeCurrencyVal(stPro.RECRebate);
        }
        catch { }
        try
        {
            //lblNetValue.Text = SiteConfiguration.ChangeCurrencyVal(stPro.TotalQuotePrice);
        }
        catch { }

        if (stCust.ResCom == "1")
        {
            lblResCom.Text = "Residential";
        }
        else
        {
            lblResCom.Text = "Commercial";
        }
        if (stPro.OwnerGSTRegistered == "True")
        {
            lblGstReg.Text = "Yes";
        }
        else
        {
            lblGstReg.Text = "N/A";
        }

        //lblCustomer2.Text = stCust.Customer;
        //lblOwnerABN.Text = stPro.OwnerABN;
        lblContact.Text = stCont.ContFirst + " " + stCont.ContLast;
        lblInstallAddressComplete.Text = stPro.InstallAddress + ", " + stPro.InstallCity + ", " + stPro.InstallState + ", " + stPro.InstallPostCode;

        lblInverterBrand2.Text = stPro.InverterBrand;
        lblInverterModel2.Text = stPro.InverterModel;
        lblInverterSeries2.Text = stPro.InverterSeries;

        try
        {
            lblInstallBookingDate2.Text = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
        }
        catch { }

        if (stPro.MeterPhase == "1")
        {
            lblMeterPhase.Text = "Single";
        }
        if (stPro.MeterPhase == "2")
        {
            lblMeterPhase.Text = "Double";
        }
        if (stPro.MeterPhase == "3")
        {
            lblMeterPhase.Text = "Three";
        }

        if (stPro.OffPeak == "True")
        {
            lblOffPeak.Text = "Yes";
        }
        else
        {
            lblOffPeak.Text = "No";
        }

        lblRetailer.Text = stPro.ElecRetailer;
        //lblMeterType.Text = "";
        lblPeakMeterNo.Text = stPro.MeterNumber1;
        lblProjectNumber2.Text = stPro.ProjectNumber;
        lblContact2.Text = stCont.ContFirst + " " + stCont.ContLast;
        lblInstallAddressComplete2.Text = stPro.InstallAddress + ", " + stPro.InstallCity + ", " + stPro.InstallState + ", " + stPro.InstallPostCode;
        lblProjectNumber5.Text = stPro.ProjectNumber;
        lblProjectNumber4.Text = stPro.ProjectNumber;
    }
}