﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class mailtemplate_printmtce : System.Web.UI.Page
{
    protected static string Siteurl;
    protected static string SiteName;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;
            SiteName = st.sitename;

            string MaintenanceID = Request.QueryString["id"]; //"1393382475";
            SttblProjectMaintenance stMtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectMaintenanceID(MaintenanceID);
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(stMtce.ProjectID);
            SttblProjects2 stPro2 = ClstblProjects.tblProjects2_SelectByProjectID(stMtce.ProjectID);
            SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stPro.EmployeeID);
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);

            lblProjectNumber.Text = stPro.ProjectNumber;
            lblManualQuoteNumber.Text = stPro.ManualQuoteNumber;
            try
            {
                lblOpenDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stMtce.OpenDate));
            }
            catch { }

            lblName.Text = stCont.ContFirst + " " + stCont.ContLast;
            lblAddress.Text = stPro.InstallAddress;
            lblCity.Text = stPro.InstallCity;
            lblState.Text = stPro.InstallState;
            lblPCode.Text = stPro.InstallPostCode;
            lblContact.Text = stCust.CustPhone;
            lblContactMobile.Text = stCont.ContMobile;
            lblEmail.Text = stCont.ContEmail;
            lblPanelDetails.Text = stPro.PanelDetails;
            lblInverterDetails.Text = stPro.InverterDetails;
            lblRoofType.Text = stPro.RoofType;
            lblRoofSlope.Text = stPro.RoofAngle;
            lblHouseType.Text = stPro.HouseType;
            try
            {
                lblInstDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            catch { }

            lblAssignedTo.Text = stMtce.Installer;
            lblAmount.Text = SiteConfiguration.ChangeCurrencyVal(stMtce.MtceCost);
            lblDiscount.Text = SiteConfiguration.ChangeCurrencyVal(stMtce.MtceDiscount);
            lblBalance.Text = SiteConfiguration.ChangeCurrencyVal(stMtce.MtceBalance);
            lblPaidBy.Text = stMtce.FPTransType;
            lblReceivedBy.Text = stMtce.MtceRecByName;

            lblCustomerInput.Text = stMtce.CustomerInput;
            lblFault.Text = stMtce.FaultIdentified;
            lblActionRequired.Text = stMtce.ActionRequired;
            lblWorkDone.Text = stMtce.WorkDone;
        }
    }

    public static string MakeImageSrcData(string filename)
    {
        FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
        byte[] filebytes = new byte[fs.Length];
        fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
        return "data:image/png;base64," +
          Convert.ToBase64String(filebytes, Base64FormattingOptions.None);
    }
}