﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mailtemplate_taxinvoice : System.Web.UI.Page
{
    public static string MakeImageSrcData(string filename)
    {
        FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
        byte[] filebytes = new byte[fs.Length];
        fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
        return "data:image/png;base64," +
          Convert.ToBase64String(filebytes, Base64FormattingOptions.None);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["id"]; 
        //string ProjectID = "-2146260946"; 
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblProjects2 stPro2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stPro.EmployeeID);
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);
        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);
        lblContMobile.Text=stCont.ContMobile;
        lblCustPhone.Text=stCont.ContPhone;
        lblContEmail.Text = stCont.ContEmail;

        try
        {
            ////lblSalesInvDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro2.SalesInvDate));
            //lblSalesInvDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InvoiceSent));
            lblSalesInvDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
        }
        catch { }
        lblSalesInvNo.Text = stPro.InvoiceNumber;
       
        lblSalesRepName.Text = stPro.Contact;
        lblInstallAddress.Text = stPro.InstallAddress;
        lblInstallAddress2.Text = stPro.InstallCity + ", " + stPro.InstallState + ", " + stPro.InstallPostCode;
        lblPanelDetails.Text = stPro.PanelDetails;
        //==================================================
        if (stPro.InverterDetailsID != "")
        {
            lblInverterDetails.Text = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";
            //lblInverterDetails.Text = "One " + stPro.InverterDetailsName + " KW Inverter";
        }
        if (stPro.SecondInverterDetailsID != "")
        {
            lblInverterDetails.Text = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";
            //lblInverterDetails.Text = "One " + stPro.SecondInverterDetails + " KW Inverter";
        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "")
        {
            lblInverterDetails.Text = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";
            //lblInverterDetails.Text = "One " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.SecondInverterDetails + " KW Inverter.";
        }
        //lblInverterDetails.Text = stPro.InverterDetails;
        try
        {
            decimal newtotalcost = Convert.ToDecimal(stPro.RECRebate) + Convert.ToDecimal(stPro.TotalQuotePrice);
            try
            {
                lblTotalQuotePrice.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(newtotalcost));
            }
            catch { }
        }
        catch { }
        //try
        //{
        //    lblRECRebate.Text = SiteConfiguration.ChangeCurrencyVal(stPro.RECRebate);
        //}
        //catch { }
        try
        {
            lblNetCost.Text = SiteConfiguration.ChangeCurrencyVal(stPro.TotalQuotePrice);
        }
        catch { }
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        decimal paiddate = 0;
        decimal balown = 0;
        if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
        {
            paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
            balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
            try
            {
                lblDepositPaid.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(paiddate));
            }
            catch { }
            try
            {
                lblBalanceDue.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balown));
            }
            catch { }
        }
        else
        {
            lblDepositPaid.Text = "0";
            try
            {
                lblBalanceDue.Text = SiteConfiguration.ChangeCurrencyVal(stPro.TotalQuotePrice);
            }
            catch { }
        }

        decimal invamnt = 0;
        try
        {
            //lblInstallCompleted.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallCompleted));
        }
        catch { }
        try
        {

            invamnt = (Convert.ToDecimal(stPro.TotalQuotePrice) * Convert.ToDecimal(0.9091));
            //invamnt = (Convert.ToDecimal(stPro2.SalesInvAmnt) * Convert.ToDecimal(0.9091));
            //lblSalesInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(invamnt));
        }
        catch { }
        //Response.Write(stEmp.GSTPayment);
        //if (stEmp.GSTPayment == "True")
        {
            decimal GST = 0;
            try
            {
                // ((SalesInvAmnt * 0.9091)*10)/100
                GST = ((invamnt) * 10) / 100;
            }
            catch { }

            try
            {
                //lblCommissionTotal.Text = SiteConfiguration.ChangeCurrencyVal(stPro2.SalesInvAmnt);
            }
            catch { }

            lblCommissionGST.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(GST));
        }
        //else
        //{
        //    //lblCommissionTotal.Visible = false;
        //    lblCommissionGST.Visible = false;
        //}

        //lblEmpAddress.Text = stEmp.EmpAddress;
        //lblEmpAddress2.Text = stEmp.EmpCity + ", " + stEmp.EmpState + ", " + stEmp.EmpPostCode;

        //lblEmpABN.Text = stEmp.TaxFileNumber;
        //lblCustomer.Text = stCust.Customer;
        //lblProject.Text = stPro.Project;
        //lblProjectNumber.Text = stPro.ProjectNumber;
        //lblSystemDetails.Text = stPro.SystemDetails;

        //decimal invamnt = 0;
        //try
        //{
        //    lblInstallCompleted.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallCompleted));
        //}
        //catch { }
        //try
        //{
        //    invamnt = (Convert.ToDecimal(stPro2.SalesInvAmnt) * Convert.ToDecimal(0.9091));
        //    lblSalesInvAmnt.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(invamnt));
        //}
        //catch { }

        //if (stEmp.GSTPayment == "True")
        //{
        //    decimal GST = 0;
        //    try
        //    {
        //        // ((SalesInvAmnt * 0.9091)*10)/100
        //        GST = ((invamnt) * 10) / 100;
        //    }
        //    catch { }

        //    try
        //    {
        //        lblCommissionTotal.Text = SiteConfiguration.ChangeCurrencyVal(stPro2.SalesInvAmnt);
        //    }
        //    catch { }

        //    lblCommissionGST.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(GST));
        //}
        //else
        //{
        //    lblCommissionTotal.Visible = false;
        //    lblCommissionGST.Visible = false;
        //}
        //lblSalesPayNotes.Text = stPro2.SalesPayNotes;
        //lblEmpBSB.Text = stEmp.EmpBSB;
        //lblEmpBankAcct.Text = stEmp.EmpBankAcct;
        //lblEmpAccountName.Text = stEmp.EmpAccountName;
    }
}