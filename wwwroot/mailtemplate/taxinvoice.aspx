﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="taxinvoice.aspx.cs" Inherits="mailtemplate_taxinvoice" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <style>
        body
        {
            margin: 0px;
            padding: 0px;
        }

        @font-face
        {
            font-family: 'amaticbold';
            src: url('../fonts/amatic-bold.eot');
            src: url('../fonts/amatic-bold.eot?#iefix') format('embedded-opentype'), url('../fonts/amatic-bold.woff2') format('woff2'), url('../fonts/amatic-bold.woff') format('woff'), url('../fonts/amatic-bold.ttf') format('truetype'), url('../fonts/amatic-bold.svg#amaticbold') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face
        {
            font-family: 'bobregular';
            src: url('../fonts/bob.eot');
            src: url('../fonts/bob.eot?#iefix') format('embedded-opentype'), url('../fonts/bob.woff2') format('woff2'), url('../fonts/bob.woff') format('woff'), url('../fonts/bob.ttf') format('truetype'), url('../fonts/bob.svg#bobregular') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face
        {
            font-family: 'mission_scriptregular';
            src: url('../fonts/mission-script.eot');
            src: url('../fonts/mission-script.eot?#iefix') format('embedded-opentype'), url('../fonts/mission-script.woff2') format('woff2'), url('../fonts/mission-script.woff') format('woff'), url('../fonts/mission-script.ttf') format('truetype'), url('../fonts/mission-script.svg#mission_scriptregular') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face
        {
            font-family: 'museo300';
            src: url('../fonts/museo300-regular.eot');
            src: url('../fonts/museo300-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/museo300-regular.woff2') format('woff2'), url('../fonts/museo300-regular.woff') format('woff'), url('../fonts/museo300-regular.ttf') format('truetype'), url('../fonts/museo300-regular.svg#museo300') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face
        {
            font-family: 'museo500';
            src: url('../fonts/museo500-regular.eot');
            src: url('../fonts/museo500-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/museo500-regular.woff2') format('woff2'), url('../fonts/museo500-regular.woff') format('woff'), url('../fonts/museo500-regular.ttf') format('truetype'), url('../fonts/museo500-regular.svg#museo500') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face
        {
            font-family: 'museo700';
            src: url('../fonts/museo700-regular.eot');
            src: url('../fonts/museo700-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/museo700-regular.woff2') format('woff2'), url('../fonts/museo700-regular.woff') format('woff'), url('../fonts/museo700-regular.ttf') format('truetype'), url('../fonts/museo700-regular.svg#museo700') format('svg');
            font-weight: normal;
            font-style: normal;
        }
    </style>
</head>
<body>

    <!-- ========== Strat static html ========== -->
    <div style="width: 960px; height: 1345px; overflow: hidden;">
        <div style="margin: 30px!important;">
            <div style="position: relative;">
                <div class="toparea" style="font-family: Arial, Helvetica, sans-serif;">
                    <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 5px 0px 0px 10px; width: 30%;">
                        <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo.png") %>"
                            width="124" height="100" />
                    </div>
                    <div style="float: right; text-align: right; padding: 5px 10px 0px 0px; width: 60%;">
                        <p style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 21px; color: #00b0f0; font-weight: 400;">
                            <span style="font-size: 20px; font-weight: 600; line-height: 24px;">THINK GREEN SOLAR<br />
                                1300 38 76 76</span><br>
                            <span style="font-size: 18px;"><a href="mailto:info@thinkgreensolar.com.au">info@thinkgreensolar.com.au</a><br />
                                <a href="http://www.thinkgreensolar.com.au/" target="_blank">www.thinkgreensolar.com.au</a>
                                <br />
                                ABN : 16 611 514 197
                            </span>
                        </p>
                    </div>
                    <div style="float: none; clear: both"></div>
                </div>
            </div>
            <div align="center" style="font-family: 'Open Sans', sans-serif; font-size: 24px; line-height: 26px; font-weight: 700px; color: #00b0f0;">
                <strong>Tax Invoice
                        -
                    <asp:Label ID="lblSalesInvNo" runat="server"></asp:Label></strong><br />
                <span style="font-size: 15px; font-weight: normal;">
                    <asp:Label ID="lblSalesInvDate" runat="server"></asp:Label></span>
            </div>
            <div style="position: relative;">
                <div style="position: relative; z-index: 1;">
                    <div>
                        <h1 style="font-family: 'Open Sans', sans-serif; font-weight: 600; font-size: 34px; line-height: 34px; color: #00b0f0; margin: 0px 0px 15px 0px; padding: 0px 0px 10px 0px; border-bottom: #000000 solid 3px;">Contact Details</h1>
                        <p style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 25px; color: #000; font-weight: 400; margin: 0px; padding: 0px 0px 15px 0px;">
                            Name:
                                <asp:Label ID="lblSalesRepName" runat="server"></asp:Label><br>
                            Mobile:
                                <asp:Label ID="lblContMobile" runat="server"></asp:Label><br>
                            Phone:
                                <asp:Label ID="lblCustPhone" runat="server"></asp:Label><br>
                            Email :
                                <asp:Label ID="lblContEmail" runat="server"></asp:Label>
                        </p>
                    </div>
                    <div>
                        <h1 style="font-family: 'Open Sans', sans-serif; font-weight: 600; font-size: 34px; line-height: 34px; color: #00b0f0; margin: 0px 0px 15px 0px; padding: 0px 0px 10px 0px; border-bottom: #000000 solid 3px;">Site Details</h1>
                        <p style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 25px; color: #000; font-weight: 400; margin: 0px; padding: 0px 0px 15px 0px;">
                            <asp:Label ID="lblInstallAddress" runat="server"></asp:Label><br />
                            <asp:Label ID="lblInstallAddress2" runat="server"></asp:Label>
                        </p>
                        <div>
                            <h1 style="font-family: 'Open Sans', sans-serif; font-weight: 600; font-size: 34px; line-height: 34px; color: #00b0f0; margin: 0px 0px 15px 0px; padding: 0px 0px 10px 0px; border-bottom: #000000 solid 3px;">System Details</h1>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="60%" valign="top">
                                        <p style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 25px; color: #000; font-weight: 400; margin: 0px; padding: 0px 0px 0px 0px;">
                                            Panels:
                                                <asp:Label ID="lblPanelDetails" runat="server"></asp:Label><br>
                                            Inverter:
                                                <asp:Label ID="lblInverterDetails" runat="server"></asp:Label>
                                        </p>
                                    </td>
                                    <td valign="top" align="right"><%--<p style="font-family: 'Open Sans', sans-serif; font-size:14px; line-height:25px; color:#000; font-weight:400; margin:0px; padding:0px 0px 0px 0px;"><strong>«ServiceValue»</strong></p>--%></td>
                                </tr>
                            </table>
                            <p style="font-family: 'Open Sans', sans-serif; font-size: 15px; line-height: 20px; color: #000; font-weight: 400; margin: 0px; padding: 10px 0px 5px 0px;">For the supply and installation of the Complete Solar System</p>
                            <div>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Open Sans', sans-serif; font-size: 13px; line-height: 20px; color: #000; font-weight: 400;">
                                    <tr>
                                        <td valign="top">&nbsp;</td>
                                        <td width="25%" align="right" style="padding: 1px 5px; font-weight: 600">Total Cost:</td>
                                        <td width="25%" align="right" style="padding: 1px 5px; font-weight: 600; font-size: 17px;">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" algn="absmiddle" /><asp:Label ID="lblTotalQuotePrice" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top">&nbsp;</td>
                                        <td align="right" style="padding: 1px 5px; font-weight: 600">Net Cost:</td>
                                        <td align="right" style="padding: 1px 5px; font-weight: 600; font-size: 17px;">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" algn="absmiddle" /><asp:Label ID="lblNetCost" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td valign="top">&nbsp;</td>
                                        <td align="right" style="padding: 1px 5px 13px 5px; font-weight: 600">Deposit Paid:</td>
                                        <td align="right" style="padding: 1px 5px 13px 5px; font-weight: 600; font-size: 17px;">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" algn="absmiddle" /><asp:Label ID="lblDepositPaid" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="right" style="font-size: 20px; color: #fff; font-weight: 600; background: #00b0f0; padding: 10px 5px 12px 5px;">Balance Due:</td>
                                        <td align="right" style="font-size: 23px; color: #fff; font-weight: 600; background: #00b0f0; padding: 10px 5px 12px 5px;">
                                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" algn="absmiddle" /><asp:Label ID="lblBalanceDue" runat="server"></asp:Label></td>
                                    </tr>
                                </table>
                                <p style="font-family: 'Open Sans', sans-serif; font-size: 15px; line-height: 20px; color: #000; font-weight: 400; margin: 0px; padding: 3px 0px 12px 0px;">Note: The Net Cost above includes GST of: $<%--<asp:Label ID="lblrupees" runat="server"></asp:Label>--%><asp:Label ID="lblCommissionGST" runat="server"></asp:Label></p>
                                <table width="100%" border="0" cellspacing="0" cellpadding="6" style="border: #000000 solid 1px; font-family: 'Open Sans', sans-serif; font-size: 13px; line-height: 18px; color: #000; font-weight: 400;">
                                    <tr>
                                        <td colspan="4" align="center" style="font-family: 'Open Sans', sans-serif; font-size: 18px; line-height: 18px; color: #00b0f0; font-weight: 600; border-bottom: #000 solid 1px;">Payment Methods</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="2" valign="middle" style="border-right: #000 solid 1px; font-weight: 600; line-height: 18px;" width="110">Payment Option :</td>
                                        <td style="border-bottom: #000 solid 1px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #00b0f0; font-weight: 600; border-right: #000000 solid 1px;">EFT / Bank Transfer</td>
                                        <td style="border-bottom: #000 solid 1px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #00b0f0; font-weight: 600; border-right: #000000 solid 1px;">Credit Card</td>
                                        <td style="border-bottom: #000 solid 1px; font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 18px; color: #00b0f0; font-weight: 600;">Cheque / Money Order</td>
                                    </tr>
                                    <tr>
                                        <td style="border-right: #000000 solid 1px;">A/c Name: Think Green Solar
                                                <br>
                                            BSB: 034008 A/c No. 308638</td>
                                        <td style="border-right: #000000 solid 1px;">(Master / Visa cards)<br>
                                            with 1.5% surcharge</td>
                                        <td>Payable to &quot;Think Green Solar&quot; and mail it to<br>
                                            409, Francis Street, Brooklyn, VIC – 3012</td>
                                    </tr>
                                </table>
                                <p style="font-family: 'Open Sans', sans-serif; font-size: 13px; line-height: 16px; color: #000; font-weight: 400; margin: 0px; padding: 30px 0px 15px 0px;text-align:justify!important;">
                                    <span style="font-family: 'Open Sans', sans-serif; font-size: 13px; color: #00b0f0; font-weight: 600;">Payment Schedules:</span> This Invoice needs to be paid on the day of installation<br>
                                    <span style="font-family: 'Open Sans', sans-serif; font-size: 13px; color: #00b0f0; font-weight: 600;">Note:</span> Customer agrees that he did not receive any solar grant in the past and also agrees to transfer STCs to Think Green Solar or Think Green Solar Agent. All products and services are supplied subject to Think Green Solar terms and conditions.<br>
                                    *Upgrade of Meter Box (if needed) will be negotiated and charged by our Electrician directly. The cost of the replacement of your current electric meter has to be borne by you, which can include legal charges imposed by the Electricity Distributor. Customer agrees to pay the balance amount of the system plus any additional costs towards the system installation.<br>
                                    *Customer must pay the Balance Due amount on the day of the installation. A $20 late payment each day/ collection fee, plus interest, will be applicable if customer fails to pay the Balance Due on time.
                                </p>
                            </div>
                        </div>
                        <div style="border-top: #000 solid 3px; margin-top: 50px; padding-top: 5px;">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="25%" align="center" valign="middle" style="margin-top: 10px;">
                                        <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/btm_logo1.jpg") %>" width="130" height="113" />
                                    </td>
                                    <%--<td width="25%" align="center" valign="middle">
                                        <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/btm_logo2.jpg") %>" width="117" height="113" />--%>
                                    </td>
                                    <td width="25%" align="center" valign="middle">
                                        <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/btm_logo3.jpg") %>" width="168" height="113" />
                                    </td>
                                    <td width="25%" align="center" valign="middle">
                                        <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/btm_logo4.jpg") %>" width="84" height="113" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ========== end static html ========== -->
    <%-- <br>
  <br>
  <br>
  <div style="width: 960px; margin: 0px auto;">
    <div style="text-align: center; color: #808080; font-size: 40px; margin-top: 40px; font-weight: bold;">Sales Commission Tax Invoice</div>
    <div style="float: right; margin-bottom: 30px; font-size: 20px; margin-top: 20px; font-weight: bold; text-align: right;">
      <asp:Label ID="lblSalesInvDate" runat="server"></asp:Label>
      <br />
      InvoiceNo:
      <asp:Label ID="lblSalesInvNo" runat="server"></asp:Label>
    </div>
    <div style="clear: both;"></div>
    <div style="background-color: #D9D9D9; float: left; width: 460px; font-size: 30px; color: #000; font-weight: bold; padding: 10px; height: 200px;"> To – Euro Solar<br>
      PO Box 570<br>
      Archerfield, QLD, 4108 </div>
    <div style="background-color: #808080; float: left; width: 460px; font-size: 30px; color: #fff; font-weight: bold; padding: 10px; height: 200px;"> From:
      <asp:Label ID="lblSalesRepName" runat="server"></asp:Label>
      <br />
      <asp:Label ID="lblEmpAddress" runat="server"></asp:Label>
      <br />
      <asp:Label ID="lblEmpAddress2" runat="server"></asp:Label>
      <br />
      ABN:
      <asp:Label ID="lblEmpABN" runat="server"></asp:Label>
    </div>
    <div style="clear: both;"></div>
    <table width="100%;" cellspacing="0" style="margin: 20px 0px; border: 1px solid #ccc;" class="tax_invoice">
      <tr>
        <th width="70%" style="padding: 5px; border: 1px solid #ccc; text-align: left; background-color: #D9D9D9; font-family: Arial, Helvetica, sans-serif;">DESCRIPTION OF WORK</th>
        <th width="30%" style="padding: 5px; border: 1px solid #ccc; text-align: left; background-color: #D9D9D9; font-family: Arial, Helvetica, sans-serif;">TOTAL AMOUNT</th>
      </tr>
      <tr>
        <td style="padding: 5px; border-right: 1px solid #ccc;"><strong>SALES COMMISSION FOR:</strong></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td style="padding: 5px; border-right: 1px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Customer Name:</strong>&nbsp;&nbsp;&nbsp;
          <asp:Label ID="lblCustomer" runat="server"></asp:Label></td>
        <td style="padding: 5px;">&nbsp;</td>
      </tr>
      <tr>
        <td style="padding: 5px; border-right: 1px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Project:</strong>&nbsp;&nbsp;&nbsp;
          <asp:Label ID="lblProject" runat="server"></asp:Label></td>
        <td style="padding: 5px;">&nbsp;</td>
      </tr>
      <tr>
        <td style="padding: 5px; border-right: 1px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Project Number:</strong>&nbsp;&nbsp;&nbsp;
          <asp:Label ID="lblProjectNumber" runat="server"></asp:Label></td>
        <td style="padding: 5px;">&nbsp;</td>
      </tr>
      <tr>
        <td style="padding: 5px; border-right: 1px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>System Details :</strong>&nbsp;&nbsp;&nbsp;
          <asp:Label ID="lblSystemDetails" runat="server"></asp:Label></td>
        <td style="padding: 5px;">&nbsp;</td>
      </tr>
      <tr>
        <td style="padding: 5px; border-right: 1px solid #ccc;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>Completion Date :</strong>&nbsp;&nbsp;&nbsp;
          <asp:Label ID="lblInstallCompleted" runat="server"></asp:Label></td>
        <td style="padding: 5px;">&nbsp;</td>
      </tr>
      <tr>
        <td style="padding: 5px; border-right: 1px solid #ccc;"><strong>COMMISSION AMOUNT :</strong></td>
        <td style="padding: 5px;"><strong> <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="12" style="text-align: center;" />
          <asp:Label ID="lblSalesInvAmnt" runat="server"></asp:Label>
          </strong></td>
      </tr>
      <tr id="trCommGST" runat="server">
        <td style="padding: 5px; border-right: 1px solid #ccc;"><strong>GST :</strong></td>
        <td style="padding: 5px;"><strong> <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="12" style="text-align: center;" />
          <asp:Label ID="lblCommissionGST" runat="server"></asp:Label>
          </strong></td>
      </tr>
      <tr id="trCommTotal" runat="server">
        <td style="padding: 5px; border-right: 1px solid #ccc; border-top: 1px solid #ccc;"><strong>TOTAL AMOUNT:&nbsp;</strong></td>
        <td style="padding: 5px; border-top: 1px solid #ccc;"><strong> <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="12" style="text-align: center;" />
          <asp:Label ID="lblCommissionTotal" runat="server"></asp:Label>
          </strong></td>
      </tr>
    </table>
    <div><strong>Notes:</strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <asp:Label ID="lblSalesPayNotes" runat="server"></asp:Label>
    </div>
    <div style="border: 1px solid #ccc; background-color: #D9D9D9; text-align: center; padding: 5px; font-size: 20px; margin-top: 20px;"> Pay by Direct Deposit to: BSB:
      <asp:Label ID="lblEmpBSB" runat="server"></asp:Label>
      A/C:
      <asp:Label ID="lblEmpBankAcct" runat="server"></asp:Label>
      <br />
      Account Name:
      <asp:Label ID="lblEmpAccountName" runat="server"></asp:Label>
    </div>
  </div>--%>
</body>
</html>
