﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="printmtce.aspx.cs" Inherits="mailtemplate_printmtce" %>

<%--
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body {
            margin: 0px;
            padding: 0px;
        }
        .pdfimages ul li {
            width: 318px;
            text-align: center;
            display: block;
            float: left;
            margin: 0px 0px 20px;
        }
        .pdfimages ul {
            width: 100%;
            float: left;
            margin: 0px;
            padding: 0px;
        }
        .terms tr td {
            font-size: 11px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">--%>
<div style="text-align: right; padding-right: 10px;" class="printpage printorder">
    <a href="javascript:window.print();" class="printicon">
       <i class="fa fa-print"></i>
    </a>
</div>
<div class="printorder" style="clear: both;"></div>
<div id="divPrintPage" runat="server">
    <div style="width: 600px; margin: 0px auto;">
        <div>
            <div class="toparea" style="font-family: Arial, Helvetica, sans-serif;">
                <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 15px 190px 0px 0px;">
                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo1.png") %>" />
                
                </div>
                <div style="float: left; width: 30%; margin-top: 50px; text-align: right;">
                    <table style="border: 1px solid #ccc; border-collapse: collapse; font-size: 11px;">
                        <tr>
                            <td style="border-right: 1px solid #ccc; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 5px 15px;">Project No:</td>
                            <td style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 5px 15px;">
                                <asp:label id="lblProjectNumber" runat="server"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-right: 1px solid #ccc; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 5px 15px;">Manual No:</td>
                            <td style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 5px 15px;">
                                <asp:label id="lblManualQuoteNumber" runat="server"></asp:label>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-right: 1px solid #ccc; border-collapse: collapse; padding: 5px 15px;">Call Date:</td>
                            <td style="padding: 5px 15px; border-collapse: collapse;">
                                <asp:label id="lblOpenDate" runat="server"></asp:label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="clear: both;"></div>
            <div style="background-color: #000; color: #fff; font-weight: bold; font-size: 13px; text-align: center; margin: 5px 0px; padding: 3px; font-weight: bold;">
                MAINTENANCE CONTRACT
            </div>
            <div style="width: 65%; float: left;">
                <div style="background-color: #8C8C8C; color: #fff; font-size: 13px; font-weight: bold; text-align: center; padding: 3px;">CUSTOMER DETAILS</div>
                <table style="border: 1px solid #ccc; border-collapse: collapse; width: 100%; font-size: 11px;">
                    <tr>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Name:</td>
                        <td colspan="10" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:label id="lblName" runat="server"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Address:</td>
                        <td colspan="10" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:label id="lblAddress" runat="server"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Suburb:</td>
                        <td colspan="8" style="border-bottom: 1px solid #ccc; border-right: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:label id="lblCity" runat="server"></asp:label>
                        </td>
                        <td colspan="1" style="border-bottom: 1px solid #ccc; border-right: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:label id="lblState" runat="server"></asp:label>
                        </td>
                        <td colspan="1" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 5px 15px;">
                            <asp:label id="lblPCode" runat="server"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Contact:</td>
                        <td colspan="8" style="border-bottom: 1px solid #ccc; border-right: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:label id="lblContact" runat="server"></asp:label>
                        </td>
                        <td colspan="1" style="border-bottom: 1px solid #ccc; border-right: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">Mobile
                        </td>
                        <td colspan="1" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 5px 15px;">
                            <asp:label id="lblContactMobile" runat="server"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Email:</td>
                        <td colspan="10" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:label id="lblEmail" runat="server"></asp:label>
                        </td>
                    </tr>
                </table>
                <div style="background-color: #8C8C8C; color: #fff; font-size: 13px; font-weight: bold; text-align: center; padding: 3px;">SYSTEM INSTALLED</div>
                <table width="100%" style="border: 1px solid #ccc; border-collapse: collapse; font-size: 11px;">
                    <tr>
                        <td width="70" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Panels:</td>
                        <td colspan="10" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:label id="lblPanelDetails" runat="server"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td width="70" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Inverter:</td>
                        <td colspan="10" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:label id="lblInverterDetails" runat="server"></asp:label>
                        </td>
                    </tr>
                </table>
                <table style="border: 1px solid #ccc; border-collapse: collapse; width: 100%; font-size: 11px;">
                    <tr>
                        <td width="120px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Roof Type:</td>
                        <td style="border-bottom: 1px solid #ccc; border-right: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:label id="lblRoofType" runat="server"></asp:label>
                        </td>
                        <td width="80px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Roof Slope:</td>
                        <td style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:label id="lblRoofSlope" runat="server"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td width="120px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">House Type:</td>
                        <td style="border-bottom: 1px solid #ccc; border-right: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:label id="lblHouseType" runat="server"></asp:label>
                        </td>
                        <td width="10px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Inst Date:</td>
                        <td style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:label id="lblInstDate" runat="server"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td width="120px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Assigned To:</td>
                        <td colspan="3" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:label id="lblAssignedTo" runat="server"></asp:label>
                        </td>
                    </tr>
                </table>
                <div style="background-color: #8C8C8C; color: #fff; font-size: 13px; font-weight: bold; text-align: center; padding: 3px;">PAYMENT FOR SERVICE</div>
                <table width="100%" style="border: 1px solid #ccc; border-collapse: collapse; font-size: 11px;">
                    <tr>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Amount</td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Discount</td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Balance</td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Paid By</td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Received By</td>
                    </tr>
                    <tr>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="5" height="10" algn="absmiddle" /><asp:label id="lblAmount" runat="server"></asp:label>
                        </td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="5" height="10" algn="absmiddle" /><asp:label id="lblDiscount" runat="server"></asp:label>
                        </td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="5" height="10" algn="absmiddle" /><asp:label id="lblBalance" runat="server"></asp:label>
                        </td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:label id="lblPaidBy" runat="server"></asp:label>
                        </td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:label id="lblReceivedBy" runat="server"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width: 33%; float: right;">
                <div style="background-color: #8C8C8C; color: #fff; font-size: 13px; font-weight: bold; text-align: center; padding: 3px;">SITE DIAGRAM</div>
                <table style="border: 1px solid #ccc; border-collapse: collapse; width: 100%; font-size: 11px;">
                    <tr>
                        <td width="30%;" style="height: 160px;">&nbsp;</td>
                    </tr>
                </table>
                <div style="background-color: #8C8C8C; color: #fff; font-size: 13px; font-weight: bold; text-align: center; padding: 3px;">PANEL CONFIGURATION</div>
                <table style="border: 1px solid #ccc; border-collapse: collapse; width: 100%; font-size: 11px;">
                    <tr>
                        <td width="100px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">North Side</td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:label id="lblNorth" runat="server"></asp:label>
                        </td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Volts</td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:label id="lblNVolts" runat="server"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">West Side</td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:label id="lblWest" runat="server"></asp:label>
                        </td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Volts</td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:label id="lblWVolts" runat="server"></asp:label>
                        </td>
                    </tr>
                    <tr>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Other</td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:label id="lblOther" runat="server"></asp:label>
                        </td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Volts</td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:label id="lblOVolts" runat="server"></asp:label>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="clear: both;"></div>
            <div style="margin: 10px 0px 0px 0px; width: 100%; height: 310px; border: 1px solid #ccc;">
                <div style="font-weight: bold; padding: 10px 0px 0px 10px;">Customer Input:</div>
                <div style="height: 50px; padding: 2px 0px 0px 10px;">
                    <asp:label id="lblCustomerInput" runat="server"></asp:label>
                </div>
                <div style="font-weight: bold; padding: 10px 0px 0px 10px;">Fault Identified:</div>
                <div style="height: 50px; padding: 2px 0px 0px 10px;">
                    <asp:label id="lblFault" runat="server"></asp:label>
                </div>
                <div style="font-weight: bold; padding: 10px 0px 0px 10px;">Action Required:</div>
                <div style="height: 50px; padding: 2px 0px 0px 10px;">
                    <asp:label id="lblActionRequired" runat="server"></asp:label>
                </div>
                <div style="font-weight: bold; padding: 10px 0px 0px 10px;">Work Done:</div>
                <div style="padding: 2px 0px 0px 10px; height: 50px;">
                    <asp:label id="lblWorkDone" runat="server"></asp:label>
                </div>
            </div>
            <div style="clear: both;"></div>

            <table width="100%;" style="font-size: 11px; margin-top: 10px;">

                <tr>
                    <td>I ......................................................................... confirm that the details above are correct and the Think Green Solar Assistant has resolved and serviced the solar system in regards to the details in Customer Input.</td>
                </tr>
            </table>
            <table width="100%;" style="font-size: 11px;">
                <tr>
                    <td colspan="3" valign="top" style="padding: 5px; display: block;">Customer Signature:_________________________________</td>
                    <td colspan="3" valign="top" style="padding: 5px;">Date:_______________ </td>
                </tr>
            </table>
            <table width="100%;" style="font-size: 11px;">
                <tr style="width: 100%; text-align: center;">
                    <td>P & N Pty Ltd T/A Euro Solar. ABN 95 130 845 199 Phone: 1300 959 013 <%=Siteurl %>
                      <%--  www.eurosolar.com.au sales@eurosolar.com.au--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<%--    </form>
</body>
</html>
--%>