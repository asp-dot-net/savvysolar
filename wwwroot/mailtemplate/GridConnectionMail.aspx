﻿<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />   
</head>

<title>Arise Solar</title>
<body style="margin:0px; padding:0px">
<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
  <tbody><tr>
    <th bgcolor="#ECECEC" scope="row" style="padding:70px 0"><table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
      <tbody><tr>
        <th bgcolor="#FFFFFF" scope="row"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tbody><tr>
            <th align="left" valign="top" scope="row" style="padding:19px 30px">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody>
                    <tr>
                    <td width="312" align="left"style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333; line-height:160%; font-weight:bold"> <strong style="font-size:30px;">Arise Solar</strong></td>
                 
                  <td align="right" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#333333; line-height:160%; font-weight:bold">We can help <br> <strong style="font-size:20px;">1300 274 737</strong></td>
                  </tr>
                </tbody></table></th>
          </tr>
          <tr>
            <th align="left" valign="top" bgcolor="#0060AC" scope="row" style="padding:17px 30px 30px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody><tr>
                <td height="45" align="left" valign="top" style="color:#fff; font-size:20px; font-family:Arial, Helvetica, sans-serif; font-weight:bold" scope="row">Dear,{CustName}</td>
              </tr>
              <tr>
                <td align="left" valign="top" scope="row" style="color:#fff; font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; padding-bottom:15px; line-height:150%">We are pleased to inform you that the Electrical Works Receipt (commonly referred to as EWR) has been generated. For your reference, your EWR Number is {EWR}. You are all set to get the dual benefit of saving money on your power bills by avoiding to buy power from the grid and also through feed-in tariffs by exporting the excess power produce to the grid. <br></td>
              </tr>
              <tr>
                <td align="left" valign="top" scope="row" style="color:#fff; font-size:14px; font-family:Arial, Helvetica, sans-serif; padding-bottom:15px;  font-weight:normal; line-height:160%">We would request you to please get in touch with your Electricity Billing Company to know the status of your grid connection. It’s recommended to keep the EWR Number ready while contacting them. <br></td>
              </tr>
              <tr>
                <td align="left" valign="top" scope="row" style="color:#fff; font-size:14px; font-family:Arial, Helvetica, sans-serif;  font-weight:normal; line-height:160%; padding-bottom:25px;">Please get back to us for any more concerns and we’d be more than happy to assist you. <br></td>
              </tr>
              <tr>
                <td align="left" valign="top" scope="row" style="color:#fff; font-size:14px; font-family:Arial, Helvetica, sans-serif;  font-weight:normal; line-height:160%"><strong>Sincere Regards,</strong><br>
                  Arise Solar – Grid Connection Team</td>
              </tr>
            </tbody></table></th>
          </tr>
          <tr>
            <th align="left" valign="top" scope="row" style="padding:17px 30px 30px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody><tr>
                <th align="left" valign="top" scope="row" style="font-size:17px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; padding-bottom:15px; ">Grid Connection FAQ:</th>
              </tr>
              <tr>
                <td align="left" valign="top" scope="row" style="font-size:13px; font-weight:normal; font-family:Arial, Helvetica, sans-serif; padding-bottom:10px; line-height:150%;">
                <strong>1. How long will Arise Solar take to process my documents needed for Grid Connection?</strong><br>
                
A. It usually takes up to 7 working days to process all the documents needed to apply for your Grid Connection.</td>
              </tr>
              <tr>
                <td align="left" valign="top" scope="row" style="font-size:13px; font-weight:normal; font-family:Arial, Helvetica, sans-serif; padding-bottom:10px; line-height:150%; ">
                <strong>2. How would I know about my grid connection process/application? </strong><br>
A. As we receive all the documents needed for grid connection, we will be sending an email to your power company. We will also ensure to mark a copy of that email to you (customer) and you can have those documents handy as and when you may need them in future. Please be informed that you will need those documents in future as and when you change your power billing company.</td>
              </tr>
              <tr>
                <td align="left" valign="top" scope="row" style="font-size:13px; font-weight:normal; font-family:Arial, Helvetica, sans-serif; padding-bottom:10px; line-height:150%;">
                <strong>3. How long does the Power Company take to process &amp; connect me to the grid? </strong><br>
				A. The time frame varies from one company to the other but it is anywhere in the range of 15 to 20 working days that your Power company may take to process your grid connection application. It is always advisable to contact your Power Company 10 days after you have received the email copy of the application addressed to them.

                
                </td>
              </tr>
              <tr>
                <td align="left" valign="top" scope="row" style="font-size:13px; font-weight:normal; font-family:Arial, Helvetica, sans-serif; padding-bottom:10px; line-height:150%;">
                	<strong>4. What does the Power Company do to connect me to the Grid? </strong><br>
A. The process varies from one company to the other. In certain cases, you may get a confirmation SMS&amp;/email. In several cases they also take an appointment to visit your property to and do the further procedure.
                </td>
              </tr>
              <tr>
                <td align="left" valign="top" scope="row" style="font-size:13px; font-weight:normal; font-family:Arial, Helvetica, sans-serif; padding-bottom:10px;line-height:150%; ">
             <strong>  5. Can there be any delays in applying for my Grid Connection?</strong> <br>
A. It is an unusual thing to happen, as we follow very strong customer service standards. However, in certain cases it may get delayed because of increased demand and the schedule of our electricians. There might be a need to get your paperwork assessed to make an error-free application.
</td>
              </tr>
              <tr>
                <td align="left" valign="top" scope="row" style="font-size:13px; font-weight:normal; font-family:Arial, Helvetica, sans-serif; padding-bottom:10px; line-height:150%;">
                <strong>6. How about changing my electricity retailer?</strong> <br>
A. It is advisable to stay connected to the same retailer whilst the process of Grid Connection is completed. If you change the retailer, the process would get delayed and may get complicated as well. If you're planning to change the retailer, it is also recommended to communicate the same with us. <br> <br>
If you're already in the process of changing your Electricity Retailer, or are doing it in future, please share the set of documents (the one shared with you while applying for your grid connection, from our end, marked a copy to you) with the new one.


                </td>
              </tr>
              <tr>
                <td align="left" valign="top" scope="row" style="font-size:13px; font-weight:normal; font-family:Arial, Helvetica, sans-serif; line-height:150%; ">
               <strong> 7. Once connected to the grid, what would be the further process?</strong> <br>
A. The system is usually turned off even if the installation is complete. Once your Electricity retailer confirms your grid connection, all you will have to do is to turn it on as guided by the installer. You may call up in case of any concerns and we would love to guide you through the process.
                </td>
              </tr>
            </tbody></table></th>
          </tr>
          <tr>
            <th align="left" valign="top" scope="row" style="padding:0px">&nbsp;</th>
          </tr>
          <tr>
            <td align="left" valign="top" bgcolor="#0060AC" scope="row" style="padding:22px 30px; font-size:12px; font-weight:normal; color:#ffffff; font-family:Arial, Helvetica, sans-serif; line-height:150% ">
            © 2020 Arisesolar Australia. All Rights Reserved. <br> <a target="_blank" href="https://arisesolar.com.au/terms-and-conditions/" style="text-decoration:none; color:#ffffff">Terms and Conditions</a> | <a target="_blank" href="https://arisesolar.com.au/privacy-policy/" style="text-decoration:none; color:#ffffff"> Privacy Policy </a></td>
          </tr>
        </tbody></table></th>
      </tr>
    </tbody></table></th>
  </tr>
</tbody></table>


</body></html>