﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Data;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Text;

public partial class assets_upload : System.Web.UI.Page
{
    public static string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {

            var postedfile = Request.Files.Get("image".ToString());
            string dirpath = Request.PhysicalApplicationPath + "/userfiles/customerimage/";
            string fileName = postedfile.FileName;


            int suc = ClstblCustomers.tbl_CustImage_Insert(Request.QueryString["id"].ToString(), "");
            fileName = suc + "Image.jpg";
            SaveFile(postedfile, dirpath, fileName);
            ClstblCustomers.tbl_CustImage_UpdateImage(suc.ToString(), fileName);


            //string fileName = "222222";
            //int suc = ClstblCustomers.tbl_CustImage_Insert(Request.QueryString["id"].ToString(), "");
            //fileName = RemoveSpecialCharacter(fileName) + ".jpg";
            //fileName = suc + fileName;
            //ClstblCustomers.tbl_CustImage_UpdateImage(suc.ToString(), fileName);
        }

    }

    protected void SaveFile(HttpPostedFile file, string dirpath, string fileName)
    {
        dirpath += (dirpath.EndsWith("/") ? "" : "/");
        string filepath = dirpath + fileName;
        file.SaveAs(filepath);
    }

    public static string RemoveSpecialCharacter(String name)
    {
        name = name.Trim();

        StringBuilder sb = new StringBuilder();
        foreach (char c in name)
        {
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '.' || c == '_')
            {
                sb.Append(c);
            }
        }
        return sb.ToString();
    }
}